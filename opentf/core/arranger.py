# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An orchestrator service."""

from typing import (
    Any,
    Dict,
    Callable,
    Iterable,
    List,
    NoReturn,
    Optional,
    Set,
    Tuple,
    Union,
)

import sys
import threading
import yaml

from collections import defaultdict
from copy import deepcopy
from datetime import datetime
from itertools import chain
from queue import Queue

from flask import request

from enum import Enum

from opentf.commons import (
    DEFAULT_NAMESPACE,
    WORKFLOW,
    WORKFLOWCANCELLATION,
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
    WORKFLOWRESULT,
    EXECUTIONCOMMAND,
    EXECUTIONRESULT,
    EXECUTIONERROR,
    GENERATORCOMMAND,
    GENERATORRESULT,
    PROVIDERRESULT,
    PROVIDERCOMMAND,
    NOTIFICATION,
    validate_schema,
    validate_pipeline,
    make_status_response,
    subscribe,
    unsubscribe,
    make_app,
    run_app,
    make_dispatchqueue,
    get_context_parameter,
    make_uuid,
    get_actor,
    make_event,
)
from opentf.commons.expressions import (
    evaluate_bool as evaluate_bool_unguarded,
    evaluate_item as evaluate_item_unguarded,
    evaluate_str as evaluate_str_unguarded,
)
from opentf.commons.exceptions import (
    ExecutionError,
    ServiceError,
    AlreadyExistsError,
    BadRequestError,
    InvalidRequestError,
    NotFoundError,
)

from opentf.toolkit.core import join_path

from opentf.core.telemetry import TelemetryManager

from . import channelallocator, jobcandidates

########################################################################
## Constants

JSON = Dict[str, Any]

CHANNEL_REQUEST = -1
CHANNEL_RELEASE = -2
CHANNEL_NOTIFY = -3

PHASE_RUNNING = 'RUNNING'
PHASE_PENDING = 'PENDING'
PHASE_COMPLETED = 'COMPLETED'

STATUS_CANCELED = 'cancelled'
STATUS_SUCCESS = 'success'
STATUS_FAILURE = 'failure'
STATUS_SKIPPED = 'skipped'

HOOKS_ANNOTATIONS = 'opentestfactory.org/hooks'
PHASE_ANNOTATIONS = 'opentestfactory.org/phase'

CATEGORY_LABEL = 'opentestfactory.org/category'
CATEGORYPREFIX_LABEL = 'opentestfactory.org/categoryPrefix'
CATEGORYVERSION_LABEL = 'opentestfactory.org/categoryVersion'

CURRENT_WORKFLOWS: Dict[str, 'Workflow'] = {}  # Inprocess workflows instances


########################################################################
## Logging Helpers


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def fatal(*args) -> NoReturn:
    """Log error message and exit with error code 2."""
    error(*args)
    sys.exit(2)


def warning(*args):
    """Log warning message."""
    app.logger.warning(*args)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


########################################################################
## Telemetry


def _get_pending_running_wf_ratio(_):
    pending_wf = sum(
        1 for wf in CURRENT_WORKFLOWS.values() if wf.phase == PHASE_PENDING
    )
    running_wf = sum(
        1 for wf in CURRENT_WORKFLOWS.values() if wf.phase == PHASE_RUNNING
    )
    return (
        pending_wf / (running_wf + pending_wf) if (running_wf + pending_wf) != 0 else 0
    )


def _get_running_pending_jobs_ratio(_):
    wfs = [wf for wf in CURRENT_WORKFLOWS.values() if wf.phase == PHASE_RUNNING]
    ready = running = 0
    for wf in wfs:
        running += len(wf.current_jobs)
        ready += len(
            [
                candidate
                for candidate in wf.jobs_candidates.values()
                if candidate.phase == PHASE_PENDING
            ]
        )
    return running / (running + ready) if (running + ready) != 0 else 0


class ArrangerMetrics(Enum):
    WORKFLOWS_RECEIVED_COUNTER = (
        'opentf.orchestrator.arranger.workflows.received',
        '1',
        'Received workflows count',
        None,
    )
    WORKFLOWS_RUNNING_GAUGE = (
        'opentf.orchestrator.arranger.workflows.running',
        '1',
        'Running workflows count',
        None,
    )
    WORKFLOWS_COMPLETED_COUNTER = (
        'opentf.orchestrator.arranger.workflows.completed',
        '1',
        'Completed workflows count',
        None,
    )
    WORKFLOWS_PENDING_RUNNING_OBSERVABLE_GAUGE = (
        'opentf.orchestrator.arranger.workflows.pending_running',
        '1',
        'Pending to running workflows ratio gauge',
        _get_pending_running_wf_ratio,
    )
    JOBS_STARTED_COUNTER = (
        'opentf.orchestrator.arranger.jobs.started',
        '1',
        'Initialized jobs count',
        None,
    )
    JOBS_RUNNING_GAUGE = (
        'opentf.orchestrator.arranger.jobs.running',
        '1',
        'Running jobs count',
        None,
    )
    JOBS_COMPLETED_COUNTER = (
        'opentf.orchestrator.arranger.jobs.completed',
        '1',
        'Completed jobs count',
        None,
    )
    JOBS_RUNNING_PENDING_OBSERVABLE_GAUGE = (
        'opentf.orchestrator.arranger.jobs.running_pending',
        '1',
        'Running to pending jobs ratio gauge',
        _get_running_pending_jobs_ratio,
    )
    COMMANDS_STARTED_COUNTER = (
        'opentf.orchestrator.arranger.commands.started',
        '1',
        'Started commands count',
        None,
    )
    COMMANDS_RUNNING_GAUGE = (
        'opentf.orchestrator.arranger.commands.running',
        '1',
        'Running commands count',
        None,
    )
    COMMANDS_COMPLETED_COUNTER = (
        'opentf.orchestrator.arranger.commands.completed',
        '1',
        'Completed commands count',
        None,
    )

    def __init__(
        self,
        metric_name: str,
        unit: str,
        description: str,
        callbacks: Optional[Callable],
    ):
        self.metric_name = metric_name
        self.unit = unit
        self.description = description
        self.callbacks = callbacks


SERVICE_NAME = 'opentf.orchestrator.arranger'

########################################################################
## Helpers


def _set_category_labels(metadata: JSON, category: str) -> None:
    """Set category labels in metadata."""
    category_prefix = category_version = '-'
    if '/' in category:
        category_prefix, category = category.split('/')
    if '@' in category:
        category, category_version = category.split('@')
    labels = metadata.setdefault('labels', {})
    labels[CATEGORY_LABEL] = category
    labels[CATEGORYPREFIX_LABEL] = category_prefix
    labels[CATEGORYVERSION_LABEL] = category_version


def _adjust_generated_jobs(
    jobs: Dict[str, Any], job: 'Job', parent_name: str, download_job: bool = False
):
    """Normalize generated jobs.

    Calls _adjust_job and sets tags for each generated job prior to
    adding them to the workflow.

    # Required parameters

    - jobs: a dictionary, jobs to normalize
    - job: a Job, parent job
    - parent_name: a string, parent job name
    - download_job: a boolean, True if the parent job is a `uses` download job
    """
    for job_name, job_def in jobs.items():
        _adjust_job(
            job_name,
            job_def,
            parent_name,
            job.manifest['metadata']['job_origin'] + [job.job_id],
        )
        tags = job_def.get('runs-on', [])
        if isinstance(tags, str):
            tags = [tags]
        if download_job and DOWNLOAD_JOB_PARENTS_TAGS == 'use':
            job_def['runs-on'] = list(set(job.manifest['runs-on']) | set(tags))
        if download_job:
            job_def['metadata'].update({'download_job': True})


def _adjust_job(
    job_name: str,
    job_def: JSON,
    parent_name: str = '',
    job_origin: Optional[List[str]] = None,
) -> None:
    """Normalize job definition.

    - Set the full name
    - Set the name if missing
    - Set the job_origin
    - Normalize the needs section as a possibly empty set of full names
    """
    job_def['_full_name'] = f'{parent_name} {job_name}'.strip()
    job_def.setdefault('name', job_name)
    job_def['metadata'] = {'job_origin': job_origin or []}
    needs = job_def.get('needs', [])
    needs = {needs} if isinstance(needs, str) else set(needs)
    job_def['needs'] = {f'{parent_name} {need}'.strip() for need in needs}


def _get_values(variables: Dict[str, Union[str, Dict[str, str]]]) -> Dict[str, str]:
    """Return literal variable values."""
    return {
        var: val if isinstance(val, str) else val['value']
        for var, val in variables.items()
    }


def _prepare_outputs(
    outputs: Dict[str, Any], contexts: Dict[str, Any]
) -> Dict[str, str]:
    """Safely evaluate outputs.

    # Required parameters

    - outputs: a dictionary
    - contexts: a dictionary

    # Returned value

    A dictionary.  Keys are output names.  Values are the evaluated
    expressions.

    Outputs using an invalid expression will have a value of:

        cound not evaluate expression, got: xxx
    """
    result = {}
    for output, value in _get_values(outputs).items():
        try:
            value = evaluate_str(value, contexts)
        except Exception as err:
            value = f'could not evaluate expression, got: {err}'
        result[output] = value
    return result


## Expressions

STEP_CONTEXTS = (
    'opentf',
    'squashtf',
    'inputs',
    'variables',
    'needs',
    'job',
    'runner',
    'secrets',
    'steps',
    'resources',
)

STEP_IF_CONTEXTS = (
    'opentf',
    'squashtf',
    'inputs',
    'variables',
    'needs',
    'job',
    'runner',
    'steps',
)

STEP_OUTPUT_CONTEXTS = ('opentf', 'inputs', 'variables', 'job', 'runner', 'steps')


def restrict_context(contexts: Dict[str, Any], keys: Tuple[str, ...]) -> Dict[str, Any]:
    """Limit contexts to a set of keys."""
    return {key: value for key, value in contexts.items() if key in keys}


def evaluate_items(items, contexts: JSON) -> Dict[str, Any]:
    """Safely perform expression evaluation in items."""
    try:
        return evaluate_item_unguarded(items, contexts)
    except Exception as err:
        raise ExecutionError(f'Invalid expression in {items}: {err}') from None


def evaluate_if(
    expr: str, contexts: JSON, *, special_functions: Optional[Dict[str, bool]] = None
) -> bool:
    """Safely evaluate boolean expression."""
    try:
        result = evaluate_bool_unguarded(expr, contexts, special_functions)
        if result and special_functions and '__called__' not in special_functions:
            result = result and special_functions['success']
        return result
    except Exception as err:
        raise ExecutionError(f'Invalid conditional `{expr}`: {err}') from None


def evaluate_str(expr: str, contexts: JSON) -> str:
    """Safely perform expression evaluation in expr."""
    try:
        return evaluate_str_unguarded(expr, contexts)
    except Exception as err:
        raise ExecutionError(f'Invalid expression `{expr}`: {err}') from None


def evaluate_int(expr: Union[int, float, str], contexts: JSON) -> int:
    """Safely perform integer expression evaluation of expr."""
    try:
        if isinstance(expr, str):
            return int(evaluate_str_unguarded(expr, contexts))
        return int(expr)
    except Exception as err:
        raise ExecutionError(f'Invalid number expression `{expr}`: {err}') from None


def evaluate_bool(expr: Union[bool, str], contexts: JSON) -> bool:
    """Safely perform integer expression evaluation of expr."""
    try:
        if isinstance(expr, str):
            return bool(evaluate_str_unguarded(expr, contexts))
        return bool(expr)
    except Exception as err:
        raise ExecutionError(f'Invalid boolean expression `{expr}`: {err}') from None


########################################################################
## Workflow handling

EVENTS_QUEUE = Queue()


class Workflow:
    """A workflow.

    The constructor takes one parameter, a workflow manifest.

    A workflow is in one of the following phase:

    - PHASE_PENDING
    - PHASE_RUNNING
    - PHASE_COMPLETED

    Its status is one of the following:

    - STATUS_FAILURE
    - STATUS_CANCELED
    - STATUS_SUCCESS

    The initial workflow status is STATUS_SUCCESS.

    Its contexts are `opentf`, `squashtf` (legacy), `variables`, and
    `resources`.
    """

    def __init__(self, manifest: JSON):
        """Initialize workflow.

        # Required parameter

        - manifest: a dictionary
        """
        self.timedout = False
        self._phase = PHASE_PENDING
        self.status = STATUS_SUCCESS
        self.manifest = manifest
        metadata = manifest['metadata']
        self.workflow_id: str = metadata['workflow_id']

        for job_name, job_def in manifest['jobs'].items():
            _adjust_job(job_name, job_def)
        self.candidates = jobcandidates.JobCandidates(manifest['jobs'])

        self.contexts = {
            'opentf': {
                'actor': metadata.pop('actor', None),
                'token': metadata.pop('token', None),
                'workflow': metadata['name'],
                'namespace': metadata.get('namespace', DEFAULT_NAMESPACE),
                'job': None,
            },
            'inputs': {},
        }
        self.contexts['squashtf'] = self.contexts['opentf']

        self.variables = evaluate_items(
            manifest.get('variables', {}),
            restrict_context(
                self.contexts, ('opentf', 'squashtf', 'secrets', 'inputs')
            ),
        )

        self.contexts['variables'] = _get_values(self.variables)
        self.contexts['resources'] = {
            k: {x['name']: evaluate_items(x, self.contexts) for x in v}
            for k, v in manifest.get('resources', {}).items()
        }

        oiv = restrict_context(self.contexts, ('opentf', 'inputs', 'variables'))
        self.manifest['timeout-minutes'] = evaluate_int(
            self.manifest.get('timeout-minutes', DEFAULT_TIMEOUT_MINUTES), oiv
        )
        self.defaults = evaluate_items(manifest.get('defaults', {}), oiv)
        self.max_parallel = min(
            evaluate_int(
                manifest.get('strategy', {}).get('max-parallel', MAX_PARALLEL), oiv
            ),
            MAX_WORKERS,
        )

        self.timers: Dict[str, threading.Timer] = {}

        self._jobs_count = 0
        self.current_jobs: Dict[str, 'Job'] = {}
        self.results = {}
        self.jobs_candidates = {}
        self.reasons = []
        self._counter = 0
        self.artifacts = []

    # Timers

    def make_uuid(self, origin: Optional[str] = None) -> str:
        if self.manifest.get('metadata', {}).get('labels', {}).get('uuid'):
            root = self.workflow_id[:-12]
            self._counter += 1
            if not origin:
                return root + f'{self._counter:012d}'
            return root[:-5] + f'{origin[-4:]}-{self._counter:012d}'
        return make_uuid()

    def start_timer(self, uuid: str, timeout: int) -> None:
        """Start timer."""
        self.timers[uuid] = threading.Timer(
            interval=timeout * 60, function=timedout, args=[self.workflow_id]
        )
        self.timers[uuid].start()

    def cancel_timer(self, uuid: str) -> None:
        """Cancel timer if it was started."""
        if timer := self.timers.pop(uuid, None):
            timer.cancel()

    # Getters

    def get_contexts(self) -> JSON:
        """Get a copy of the workflow contexts.

        # Returned value

        A dictionary with three entries, one per Workflow context:

        - `squashtf` (legacy)
        - `opentf`
        - `resources`
        """
        contexts = self.contexts.copy()
        contexts['opentf'] = contexts['opentf'].copy()
        contexts['squashtf'] = contexts['squashtf'].copy()  # legacy
        return contexts

    def get_metadata(self) -> JSON:
        """Get metadata."""
        return self.manifest['metadata']

    # phase

    @property
    def phase(self):
        return self._phase

    @phase.setter
    def phase(self, value):
        if self._phase != value:
            if self._phase == PHASE_PENDING and value == PHASE_RUNNING:
                TELEMETRY_MANAGER.set_metric(ArrangerMetrics.WORKFLOWS_RUNNING_GAUGE, 1)
            elif self._phase == PHASE_RUNNING and value == PHASE_COMPLETED:
                TELEMETRY_MANAGER.set_metric(
                    ArrangerMetrics.WORKFLOWS_RUNNING_GAUGE, -1
                )
                TELEMETRY_MANAGER.set_metric(
                    ArrangerMetrics.WORKFLOWS_COMPLETED_COUNTER, 1
                )
        self._phase = value

    # Jobs

    def get_needs_context(self, jobs_names: Iterable[str]) -> JSON:
        """Return outputs of job in jobs.

        `jobs_names` are full job names (including generators).

        # Required parameters

        - jobs_names: a list of strings

        # Returned value

        A dictionary.  Keys are short job names.  Values are
        dictionaries with the following entries:

        - result: a string, one of `success`, `failure`, or `cancelled`.
        - outputs: a dictionary
        """
        return {
            job_name.rpartition(' ')[2]: self.results[job_name]
            for job_name in jobs_names
        }

    def _find_all_statuses(self, needs: Iterable[str]) -> Set[str]:
        """Call dependent job statuses recursively.

        # Required parameters

        - needs: a possibly empty iterable of job full names
        """
        needed = set()
        for job_name in needs:
            needed.add(self.results[job_name]['result'])
            items = self.manifest['jobs'][job_name]['needs']
            needed |= self._find_all_statuses(items)
        return needed

    def _make_special_functions(self, job) -> Dict[str, bool]:
        """Special functions values for job."""
        if parent_name := job.full_name.rpartition(' ')[0]:
            parent = self.current_jobs[
                self.manifest['jobs'][parent_name]['metadata']['job_id']
            ]
        else:
            parent = self
        if not (needs := job.manifest['needs']):
            return {
                'success': parent.status == STATUS_SUCCESS,
                'failure': parent.status == STATUS_FAILURE,
                'cancelled': self.status == STATUS_CANCELED,
                'always': True,
            }

        statuses = self._find_all_statuses([needs] if isinstance(needs, str) else needs)
        return {
            'success': all(x == 'success' for x in statuses)
            and (self.status != STATUS_CANCELED),
            'failure': 'failure' in statuses,
            'cancelled': self.status == STATUS_CANCELED,
            'always': True,
        }

    def _skip_job(self, full_name: str, job, status: str) -> None:
        self.results[full_name] = {'result': status}
        self.candidates.update_job_status(full_name, 'SKIPPED')
        self.maybe_complete_generator_jobs()
        if full_name in self.jobs_candidates:
            del self.jobs_candidates[full_name]
        if job is not None:
            job.phase = PHASE_COMPLETED
            job.status = status

    def reschedule_jobs(self) -> None:
        """Reschedule job candidates.

        We must recompute schedulable jobs, as some candidates may no
        longer be legitimate candidates (their 'if' conditionals may
        no longer be true if the workflow status changes).
        """
        need_reschedule, need_require = True, False
        while need_reschedule:
            need_reschedule = False
            if candidates := self.candidates.get_next_candidates():
                for candidate in sorted(candidates):
                    try:
                        if not (job := self.jobs_candidates.get(candidate)):
                            job = Job(self.manifest['jobs'][candidate], self)
                            TELEMETRY_MANAGER.set_metric(
                                ArrangerMetrics.JOBS_STARTED_COUNTER,
                                1,
                                {'workflow_id': job.workflow_id},
                            )
                        if job.phase != PHASE_PENDING:
                            continue

                        contexts = job.get_contexts()
                        # should be parent's variables, not necessarily workflow's
                        contexts['variables'] = _get_values(self.variables)
                        if evaluate_if(
                            job.manifest.get('if', 'success()'),
                            contexts,
                            special_functions=self._make_special_functions(job),
                        ):
                            self.jobs_candidates[candidate] = job
                            continue
                        self._skip_job(candidate, job, STATUS_SKIPPED)
                    except ExecutionError as err:
                        error(str(err))
                        self._skip_job(candidate, job, STATUS_CANCELED)
                        if self.cancel(reason=str(err)):
                            return
                    need_reschedule = True
                need_require = True
        if need_require:
            self.start_or_require_channels_for_ready_jobs()
        if self.candidates.no_more_jobs():
            self.complete()

    def start_or_require_channels_for_ready_jobs(self) -> None:
        """Start or require channel for ready jobs."""
        gen_jobs = []
        run_jobs = []
        for job in self.jobs_candidates.values():
            if job.phase == PHASE_PENDING:
                if 'steps' in job.manifest:
                    run_jobs.append(job)
                else:
                    gen_jobs.append(job)
        debug(
            'Requiring channels for %d jobs (workflow_id=%s).',
            len(run_jobs),
            self.workflow_id,
        )
        channelallocator.require_channels(
            self.workflow_id,
            [
                (
                    job.job_id,
                    job.contexts['job']['runs-on'],
                    job.name,
                    job.manifest['metadata']['job_origin'],
                    job.workflow.manifest['metadata']['namespace'],
                )
                for job in run_jobs
            ],
            self.callback,
            self.max_parallel,
        )
        for job in gen_jobs:
            self.candidates.update_job_status(job.full_name, 'RUNNING')
            job.start()

    def maybe_complete_generator_jobs(self) -> None:
        """Ensure generator jobs are completed.

        Generator jobs completion check is indirect: they are complete
        when their generated jobs are complete.

        As a generator may have generated only generator jobs, we need
        to check the job completion in child to parent order.
        """
        for job_name in sorted(self.manifest['jobs'], reverse=True):
            job_def = self.manifest['jobs'][job_name]
            if job_def.get('status') != 'DONE' or 'steps' in job_def:
                continue
            if not (job := self.current_jobs.get(job_def['metadata']['job_id'])):
                continue

            if job.status != STATUS_CANCELED:
                for child, results in self.results.items():
                    if child.rpartition(' ')[0] != job_name:
                        continue
                    if results['result'] == STATUS_FAILURE and not self.manifest[
                        'jobs'
                    ][child].get('continue-on-error'):
                        job.status = STATUS_FAILURE
                        break
            job.complete()

    def _set_subworkflow_inputs(
        self, subwf: Dict[str, Any], job, parent_name: str
    ) -> bool:
        debug('Setting sub-workflow parent job %s inputs...', parent_name)
        subwf_inputs = {k: v.get('default', None) for k, v in subwf['inputs'].items()}

        if missing := set(subwf_inputs) - set(job.contexts['inputs']):
            msg = f'Cannot update job inputs for job {job.name}, missing following inputs: {", ".join(missing)}.'
            _notify_error(self, job, msg)
            return False
        elif additional := set(job.contexts['inputs']) - set(subwf_inputs):
            if not subwf.get('additionalInputs'):
                msg = f'Cannot update job inputs for job {job.name}, additional inputs not allowed, got {", ".join(additional)}.'
                _notify_error(self, job, msg)
                return False
        else:
            for entry, value in subwf_inputs.items():
                if job.contexts['inputs'].get(entry):
                    continue
                job.contexts['inputs'].update({entry: value})
        return True

    def expand_download_job(self, job: 'Job', job_name: str) -> bool:
        """Expand `uses` job download job.

        # Required parameters

        - job: a Job, download_job of the `uses` job
        - job_name: a string, the job name

        """
        subwf_paths = job.manifest['metadata']['subworkflow_paths']
        if len(subwf_paths) != 1:
            return False

        try:
            with open(subwf_paths.pop(), 'r', encoding='utf-8') as f:
                subwf = yaml.safe_load(f)
        except Exception as err:
            self.fail(reason=f'Failed to load sub-workflow: {str(err)}.', origin=job)
            return False

        valid, extra = validate_schema(WORKFLOW, subwf)
        if not valid:
            self.fail(reason=f'Not a valid workflow: {extra}.', origin=job)
            return False

        valid, extra = validate_pipeline(subwf)
        if not valid:
            self.fail(reason=f'Not a valid workflow: {extra}.', origin=job)
            return False

        debug('Got sub-workflow %s, will try to run.', subwf['metadata']['name'])
        parent_name = job.full_name.rpartition(' ')[0]
        if subwf.get('inputs', {}):
            if not self._set_subworkflow_inputs(subwf, job, job_name):
                return False

        debug('Setting parent job %s outputs...', parent_name)

        if subwf_outputs := subwf.get('outputs', {}):
            parent_id = self.manifest['jobs'][parent_name]['metadata']['job_id']
            parent_outputs = {}
            for k, v in subwf_outputs.items():
                parent_outputs[k] = v['value']
            self.current_jobs[parent_id].manifest['outputs'] = parent_outputs

        debug('Adding sub-workflow jobs...')
        jobs = subwf.get('jobs', {})
        for subjob in jobs.values():
            for step in subjob.get('steps', []):
                step.setdefault('id', make_uuid())

        DISPATCH_QUEUE.put(
            make_event(
                GENERATORRESULT, metadata=job.manifest['metadata'], jobs=deepcopy(jobs)
            )
        )
        _adjust_generated_jobs(jobs, job, parent_name)
        self.add_generated_jobs(parent_name, jobs)
        return True

    def set_job_result(self, job_name: str, status: str) -> None:
        """Set workflow's job result.

        Expands `uses` download job if appropriate.

        Fails the workflow if appropriate.

        # Required parameter

        - job_name: a string, the full job name
        - status: a string, the completed job status
        """
        job = self.jobs_candidates.pop(job_name)
        if job.manifest['metadata'].get('download_job') and status == STATUS_SUCCESS:
            if not self.expand_download_job(job, job_name):
                status = STATUS_FAILURE

        self.results.setdefault(job_name, {})['result'] = status
        self.results[job_name].setdefault('outputs', {})
        self.candidates.update_job_status(job_name, 'DONE')

        if status == STATUS_FAILURE and job.continue_on_error:
            self.results[job_name]['result'] = STATUS_SUCCESS
        if status == STATUS_FAILURE and not job.continue_on_error:
            if 'steps' in job.manifest:
                msg = (
                    f'At least one step in job {job.job_id} returned a non-zero value.'
                )
            else:
                msg = f'Generator job {job_name} ({job.job_id}) failed.'
            if parent_name := job_name.rpartition(' ')[0]:
                parent_id = self.manifest['jobs'][parent_name]['metadata']['job_id']
                self.current_jobs[parent_id].fail(reason=msg)
            else:
                self.fail(reason=msg, origin=job)

        self.maybe_complete_generator_jobs()
        self.reschedule_jobs()

    def set_job_output(self, job_name: str, output: str, value: str) -> None:
        """Set workflow's job output value."""
        self.results.setdefault(job_name, {}).setdefault('outputs', {})[output] = value

    def inc_jobs_count(self) -> None:
        """Increment jobs count, raises ExecutionError if > MAX_JOBS."""
        self._jobs_count += 1
        if self._jobs_count > MAX_JOBS:
            raise ExecutionError(
                f'Jobs count exceeded ({MAX_JOBS}) for workflow {self.workflow_id}'
            )

    def force_cancel_running_jobs(self) -> None:
        """Abord running and pending jobs."""
        channelallocator.require_channels(
            self.workflow_id,
            [],
            self.callback,
            self.max_parallel,
        )
        for job_id in list(self.current_jobs):
            if job := self.current_jobs.get(job_id):
                job.cancel(force=True)
        if candidates := self.candidates.get_next_candidates():
            for candidate in candidates:
                if job := self.jobs_candidates.get(candidate):
                    self._skip_job(candidate, job, STATUS_SKIPPED)
                else:
                    self.candidates.update_job_status(candidate, 'SKIPPED')
        for job_def in self.manifest['jobs'].values():
            if job_def.get('status') not in ('DONE', 'SKIPPED', 'RUNNING'):
                job_def['status'] = 'SKIPPED'

    def maybe_cancel_running_jobs(self) -> None:
        """Abort running jobs, if appropriate.

        The workflow has been cancelled.  We should cancel running jobs
        if their conditionals evaluate to false now.
        """
        for job in list(self.current_jobs.values()):
            try:
                contexts = job.get_contexts()
                contexts['variables'] = _get_values(self.variables)
                if not evaluate_if(
                    job.manifest.get('if', 'success()'),
                    contexts,
                    special_functions=self._make_special_functions(job),
                ):
                    job.cancel()
            except Exception as e:
                warning(f'Failed to re-evaluate condition for job {job.job_id}: {e}.')

    def add_generated_jobs(
        self, parent_name: str, jobs: Dict[str, Dict[str, Any]]
    ) -> None:
        """Add generated children jobs."""
        self.candidates.expand_job(parent_name, jobs)
        self.reschedule_jobs()

    # Lifecycle

    def callback(self, _: str, job_id: str, candidate_metadata) -> None:
        for job in self.jobs_candidates.values():
            if job.job_id == job_id:
                EVENTS_QUEUE.put(
                    {
                        'kind': 'StartJob',
                        'metadata': {'workflow_id': self.workflow_id, 'job_id': job_id},
                        'candidate': candidate_metadata,
                    }
                )

    def start(self):
        """Start workflow execution.

        A Workflow has a timeout, in minutes.  If the workflow is not
        completed before the timeout, it is considered as cancelled.

        # Raised exceptions

        An _Exception_ is raised if the workflow contains no candidates.
        """
        if not self.candidates.get_next_candidates():
            raise ExecutionError(
                'The world has ended, no candidates for a brand new workflow :('
            )
        self.start_timer(self.workflow_id, self.manifest['timeout-minutes'])
        self.reschedule_jobs()

    def cancel(
        self, reason: str = '', origin: Optional['Job'] = None, force: bool = False
    ) -> bool:
        """Set workflow status to cancelled if appropriate.

        Can be called many times.  Will only emit a WorkflowCanceled
        event once.

        # Optional parameters

        - reason: a string (an empty string by default)
        - origin: a Job or None (None by default)

        # Returned value

        A boolean.  True if the workflow was not already cancelled.
        """
        if reason:
            self.reasons.append(reason)
            _notify_error(self, None, reason)

        if self.phase == PHASE_COMPLETED or self.status == STATUS_CANCELED:
            return False

        self.status = STATUS_CANCELED

        if self.timedout or force:
            self.force_cancel_running_jobs()
        else:
            self.maybe_cancel_running_jobs()

        self.reschedule_jobs()
        return True

    def fail(
        self, reason: str = '', origin: Optional['Job'] = None, raised: bool = False
    ) -> None:
        """Set workflow status to failure.

        Publish an ExecutionError event followed by a WorkflowCanceled
        event.  Will only emit those events once.

        If `raised` is True, will not publish ExecutionError.

        # Optional parameters

        - reason: a string (an empty string by default)
        - origin: a Job or None (None by default)
        - raised: a boolean (False by default)
        """
        if reason:
            self.reasons.append(reason)

        if self.phase == PHASE_COMPLETED or self.status != STATUS_SUCCESS:
            return

        self.status = STATUS_FAILURE
        if not raised:
            metadata = (origin or self).get_metadata()
            annotations = metadata.get('annotations', {})
            annotations[PHASE_ANNOTATIONS] = self.phase
            metadata['annotations'] = annotations
            if origin and origin.channel_id:
                metadata['channel_id'] = origin.channel_id
            DISPATCH_QUEUE.put(
                make_event(
                    EXECUTIONERROR,
                    metadata=metadata,
                    details={'error': str(reason)},
                ),
            )

        self.reschedule_jobs()

    def complete(self) -> None:
        """Finalize workflow.

        Produces a WorkflowCompleted or WorkflowCanceled event.
        """
        if self.phase == PHASE_COMPLETED:
            return

        self.cancel_timer(self.workflow_id)

        channelallocator.require_channels(
            self.workflow_id, [], self.callback, self.max_parallel
        )
        self.phase = PHASE_COMPLETED
        del CURRENT_WORKFLOWS[self.workflow_id]

        metadata = self.get_metadata()
        annotations = metadata.get('annotations', {})
        annotations[PHASE_ANNOTATIONS] = self.phase
        metadata['annotations'] = annotations
        if self.status == STATUS_SUCCESS:
            state = 'completed'
            event = make_event(WORKFLOWCOMPLETED, metadata=metadata)
        elif self.status in (STATUS_FAILURE, STATUS_CANCELED):
            state = 'failed' if self.status == STATUS_FAILURE else 'cancelled'
            event = make_event(
                WORKFLOWCANCELED,
                metadata=metadata,
                details={'reason': '\n'.join(self.reasons), 'status': self.status},
            )
        else:
            error('Oh no, an unexpected workflow status: %s.', self.status)
            return

        if outputs := self.manifest.get('outputs'):
            self.contexts['jobs'] = {
                k: v for k, v in self.results.items() if ' ' not in k
            }
            event['outputs'] = _prepare_outputs(outputs, self.contexts)

        DISPATCH_QUEUE.put(event)
        info(
            'Workflow %s %s (workflow_id=%s).',
            self.get_metadata()['name'],
            state,
            self.workflow_id,
        )


class Job:
    """A workflow job.

    The constructor takes 2 parameters, a job manifest (a dictionary)
    and a Workflow object.

    # Attributes

    _Job_ objects expose the following attributes:

    - manifest: the job manifest (a dictionary)
    - timeout: the job maximum duration, in seconds (an integer)
    - variables: a copy of the provided set of variables, merged with
      the job variables part, if any
    - defaults: a copy of the provided defaults, merged with the job
      defaults part, if any
    - sequence_id: current step sequence id (an integer, starting at 0)
    - phase: PHASE_PENDING, PHASE_RUNNING
    - status: STATUS_SUCCESS, STATUS_FAILURE, STATUS_SKIPPED,
      STATUS_CANCELED
    - contexts: `opentf`, `squashtf` (legacy), `needs`, `variables`,
      `job`, `runner`, `steps`, and `resources`
    """

    def __init__(self, manifest: JSON, workflow: Workflow):
        """Create a Job.

        # Required parameters

        - manifest: a job definition
        - workflow: a Workflow object
        """
        self._phase = PHASE_PENDING
        self.status = STATUS_SUCCESS
        self.manifest = manifest
        metadata = manifest['metadata']
        self.workflow_id = metadata['workflow_id'] = workflow.workflow_id
        self.job_id = metadata['job_id'] = workflow.make_uuid()

        self.full_name: str = manifest['_full_name']
        self.workflow = workflow
        metadata['namespace'] = workflow.get_metadata()['namespace']

        if parent_name := self.full_name.rpartition(' ')[0]:
            parent = self.workflow.current_jobs[
                workflow.manifest['jobs'][parent_name]['metadata']['job_id']
            ]
        else:
            parent = workflow

        self.contexts = parent.get_contexts()

        # needs
        needs = manifest['needs']
        if self.full_name in needs:
            raise ExecutionError(
                f'Invalid job {manifest["name"]} ({self.job_id}): self-referencing'
            )
        self.contexts['needs'] = workflow.get_needs_context(needs)

        # name
        self.name = evaluate_str(manifest['name'], self.contexts)
        metadata['name'] = self.name
        self.contexts['opentf']['job'] = self.name
        self.contexts['squashtf']['job'] = self.name  # legacy

        self.variables = parent.variables.copy()
        self.variables.update(
            evaluate_items(
                manifest.get('variables', {}),
                restrict_context(
                    self.contexts,
                    ('opentf', 'squashtf', 'inputs', 'variables', 'needs', 'secrets'),
                ),
            )
        )
        self.contexts['variables'] = _get_values(self.variables)

        oivn = restrict_context(
            self.contexts, ('opentf', 'inputs', 'variables', 'needs')
        )
        manifest['timeout-minutes'] = evaluate_int(
            manifest.get('timeout-minutes', parent.manifest['timeout-minutes']), oivn
        )
        manifest['continue-on-error'] = self.continue_on_error = evaluate_bool(
            manifest.get('continue-on-error', False), oivn
        )
        self.defaults = parent.defaults.copy()
        self.defaults.update(evaluate_items(manifest.get('defaults', {}), oivn))
        for item, value in self.defaults.items():
            self.manifest.setdefault(item, value)

        runs_on = manifest.get('runs-on', [])
        self.contexts['job'] = {
            'status': self.status,
            'runs-on': evaluate_items(
                runs_on if isinstance(runs_on, list) else [runs_on], oivn
            ),
        }
        manifest['runs-on'] = self.contexts['job']['runs-on']

        self.contexts['steps'] = {}

        self.workspace = self.job_id
        self.keep_workspace = False
        self._steps_count: int = 0
        self.sequence_id: int = 0
        self.channel_id = None
        self.stack = []
        self._step_names = defaultdict(lambda: 1)
        self._current_step = None
        self.steps_backlog = []
        self.sequences = set()
        self.opentf_variables = []

        # job setup configuration
        self.teardown_hooks = None

    # Phase

    @property
    def phase(self):
        return self._phase

    @phase.setter
    def phase(self, value):
        if self._phase != value:
            if self._phase == PHASE_PENDING and value == PHASE_RUNNING:
                TELEMETRY_MANAGER.set_metric(
                    ArrangerMetrics.JOBS_RUNNING_GAUGE,
                    1,
                    {'workflow_id': self.workflow_id},
                )
            elif self._phase == PHASE_RUNNING and value == PHASE_COMPLETED:
                TELEMETRY_MANAGER.set_metric(
                    ArrangerMetrics.JOBS_RUNNING_GAUGE,
                    -1,
                    {'workflow_id': self.workflow_id},
                )
                TELEMETRY_MANAGER.set_metric(
                    ArrangerMetrics.JOBS_COMPLETED_COUNTER,
                    1,
                    {'workflow_id': self.workflow_id},
                )
        self._phase = value

    # Channels

    def _is_channel_hook_applicable(self, hook: Dict[str, Any]) -> bool:
        return evaluate_if(
            hook.get('if', 'success()'),
            restrict_context(self.get_contexts(), STEP_IF_CONTEXTS),
            special_functions=self._make_special_functions(),
        ) and (not self.name.startswith('.download'))

    def _apply_channel_hooks(self, hooks, stage: str) -> None:
        """Apply channel hooks to job.

        `hooks` are either setup hooks or teardown hooks.

        # Required parameters:

        - hooks: a possibly empty list of hooks
        - stage: a string, `setup` or `teardown`
        """
        handle_workspace: Dict[str, Any] = {'run': ' '}

        before_steps = chain.from_iterable(hook.get('before', []) for hook in hooks)
        after_steps = chain.from_iterable(
            hook.get('after', []) for hook in reversed(hooks)
        )
        hook_steps = (
            [step.copy() for step in before_steps]
            + [handle_workspace]
            + [step.copy() for step in after_steps]
        )
        for step in hook_steps:
            step.setdefault('metadata', {}).setdefault('annotations', {})
            step['metadata']['annotations'][HOOKS_ANNOTATIONS] = {'channel': stage}

        handle_workspace['metadata']['annotations'][HOOKS_ANNOTATIONS][
            'use-workspace' if stage == 'setup' else 'keep-workspace'
        ] = self.workspace
        self.prepend_steps(hook_steps)

    def _assign_channel(self, metadata: Dict[str, Any]) -> None:
        """Set channel for job."""
        self.channel_id = metadata['channel_id']
        self.contexts['runner'] = {
            'os': metadata['channel_os'],
            'temp': metadata['channel_temp'],
        }

        workflow_hooks = self.workflow.manifest.get('hooks', [])
        channel_hooks = metadata.get('annotations', {}).get('hooks', [])

        self.teardown_hooks = [
            hook
            for hook in workflow_hooks + channel_hooks
            if any(event.get('channel') == 'teardown' for event in hook['events'])
        ]
        if setup_hooks := [
            hook
            for hook in workflow_hooks + channel_hooks
            if any(event.get('channel') == 'setup' for event in hook['events'])
            and self._is_channel_hook_applicable(hook)
        ]:
            self._apply_channel_hooks(setup_hooks, 'setup')

    def _unassign_channel(self) -> None:
        """Release channel."""
        metadata = {
            **self.manifest['metadata'],
            'step_sequence_id': CHANNEL_RELEASE,
            'channel_id': self.channel_id,
            'step_origin': [],
            'step_id': self.workflow.make_uuid(self.job_id),
            'step_count': self.sequence_id,
        }
        annotations = metadata.setdefault('annotations', {})
        annotations[HOOKS_ANNOTATIONS] = {
            'channel': 'teardown',
            'keep-workspace': (self.workspace != self.job_id) or self.keep_workspace,
        }

        prepare = make_event(EXECUTIONCOMMAND, metadata=metadata, scripts=[])
        prepare['runs-on'] = self.manifest['runs-on']
        DISPATCH_QUEUE.put(prepare)

    def need_teardown(self) -> bool:
        """Add teardown steps if appropriate.

        TODO: for hooks that are 'on failure' or 'always', maybe do not
        require steps to have an 'if: failure()' or 'if: always()'
        conditional.
        """
        if not self.teardown_hooks:
            return False
        if teardown_hooks := [
            hook
            for hook in self.teardown_hooks
            if self._is_channel_hook_applicable(hook)
        ]:
            self._apply_channel_hooks(teardown_hooks, 'teardown')
        self.teardown_hooks = None
        return self.steps_backlog

    # Contexts

    def get_metadata(self) -> JSON:
        """Return job metadata."""
        return self.manifest['metadata']

    def get_contexts(self) -> JSON:
        """Get a shallow copy of the contexts."""
        contexts = self.contexts.copy()
        contexts['opentf'] = contexts['opentf'].copy()
        contexts['squashtf'] = contexts['squashtf'].copy()
        return contexts

    def get_variables(self) -> JSON:
        """Get a shallow copy of the variables."""
        return self.variables.copy()

    # Current step

    def get_step(self) -> JSON:
        """Get the current step definition.

        Raises an _Exception_ if there is no current step defined.
        """
        if not self._current_step:
            raise ExecutionError(
                f'Internal error: no current step in job {self.job_id}'
            )
        return self._current_step

    def set_step(self, step: JSON) -> None:
        """Set current step."""
        self._current_step = step
        name = step.get('uses', 'run').replace('/', '').replace('@', '')
        count = self._step_names[name]
        if not step.get('_answered'):
            self._step_names[name] += 1
        self.contexts['opentf']['step'] = f'{name}{"" if count == 1 else count}'
        self.contexts['squashtf']['step'] = self.contexts['opentf']['step']  # legacy

    def set_step_output(self, key: str, value: str) -> None:
        """Set job's step output.

        If current step is a top-level step, store output in context.
        Otherwise, store output on stack for later propagation.
        """
        step = self.get_step()
        if step['metadata']['step_origin']:
            steps_context = self.stack[-1]['steps']
        else:
            steps_context = self.contexts['steps']
        step_context = steps_context.setdefault(step['id'], {'outputs': {}})
        step_context['outputs'][key] = value

    def set_step_outcome(self, outcome: str) -> None:
        """Set step outcome and conclusion.

        If current step is a top-level step, adjust the job's status
        too.  Otherwise, store step outcome on stack for later
        propagation.
        """
        step = self.get_step()
        if origin := step['metadata']['step_origin']:
            steps_context = self.stack[-1]['steps']
        else:
            steps_context = self.contexts['steps']
        if outcome == STATUS_FAILURE and step.get('continue-on-error'):
            conclusion = STATUS_SUCCESS
        else:
            conclusion = outcome
        step_context = steps_context.setdefault(step['id'], {'outputs': {}})
        step_context['outcome'], step_context['conclusion'] = outcome, conclusion
        if conclusion == STATUS_FAILURE:
            if not origin:
                self.fail(reason=f'Step {step["id"]} failed.')
            elif self.stack[-1]['status'] == STATUS_SUCCESS:
                self.stack[-1]['status'] = STATUS_FAILURE
        step['_answered'] = True
        if outcome != STATUS_SKIPPED and 'run' in step:
            TELEMETRY_MANAGER.set_metric(
                ArrangerMetrics.COMMANDS_COMPLETED_COUNTER,
                1,
                {'job_id': self.job_id, 'workflow_id': self.workflow_id},
            )
            TELEMETRY_MANAGER.set_metric(ArrangerMetrics.COMMANDS_RUNNING_GAUGE, -1)

    def _make_special_functions(self) -> Dict[str, bool]:
        status = self.status
        if self._current_step and self._current_step['metadata']['step_origin']:
            status = self.stack[-1]['status']
        return {
            'success': status == STATUS_SUCCESS,
            'failure': status == STATUS_FAILURE,
            'cancelled': self.workflow.status == STATUS_CANCELED,
            'always': True,
        }

    # Steps

    def prepend_steps(
        self, steps: List[Any], step_origin: Optional[List[Any]] = None
    ) -> None:
        """Add steps in front of steps backlog.

        Ensure each step has an `id` and a `metadata.step_origin`.

        # Required parameters

        - steps: a list of steps

        # Optional parameters

        - step_origin: a list of step UUID or None (None by default)
        """
        for step in steps:
            step.setdefault('id', self.workflow.make_uuid(self.job_id))
            step.setdefault('metadata', {})
            step['metadata'].setdefault('step_origin', step_origin or [])
        self.steps_backlog = steps + self.steps_backlog

    def process_next_step(self) -> None:
        """Process next step.

        If there are no more steps to process, complete the job.
        """
        process_next = True
        while process_next:
            if self.steps_backlog or self.need_teardown():
                try:
                    process_next = _process_step(self.steps_backlog.pop(0), self)
                except ExecutionError as err:
                    msg = str(err)
                    error(
                        f'While processing step for workflow {self.workflow_id}, got: {msg}. Cancelling workflow.'
                    )
                    self.workflow.cancel(msg, origin=self, force=True)
                    self.set_step_outcome(STATUS_SKIPPED)
                    _notify_error(self.workflow, self, msg)
                    continue
            else:
                self.complete()
                return

    def _maybe_propagate_cancellation(self) -> None:
        """Propagate cancellation to stacked steps."""
        if not self.stack:
            return

        contexts = restrict_context(self.get_contexts(), STEP_IF_CONTEXTS)
        for frame in self.stack:
            if frame['status'] != STATUS_CANCELED:
                special_functions = {
                    'success': False,
                    'failure': False,
                    'cancelled': True,
                    'always': True,
                }
                contexts['variables'] = frame['variables']
                if evaluate_if(
                    frame['if'], contexts, special_functions=special_functions
                ):
                    break
                frame['status'] = STATUS_CANCELED

    # Job

    def _set_status(self, status: str) -> None:
        """Set job status."""
        self.contexts['job']['status'] = self.status = status

    def inc_steps_count(self) -> None:
        """Increment steps count, raises ExecutionError if > MAX_STEPS."""
        self._steps_count += 1
        if self._steps_count > MAX_STEPS:
            raise ExecutionError(
                f'Steps count exceeded ({MAX_STEPS}) in job {self.job_id} for workflow {self.workflow_id}'
            )

    def process_generator(self) -> None:
        """Process generator job."""
        contexts = self.get_contexts()
        contexts['variables'] = _get_values(self.variables)

        _set_category_labels(self.manifest['metadata'], self.manifest['generator'])
        DISPATCH_QUEUE.put(
            make_event(
                GENERATORCOMMAND,
                metadata=self.manifest['metadata'],
                generator=self.manifest['generator'],
                **{'with': evaluate_items(self.manifest.get('with'), contexts)},
            )
        )

    def process_uses(self):
        """Process `uses` job."""
        ref = self.manifest['uses']
        url, _, path = ref.partition('#')
        path, _, branch = path.partition('@')

        if not url or not path:
            self.fail(
                'Incomplete reference provided for `uses` job, missing repository URL or workflow file path.'
            )

        checkout_with = {'repository': url}
        if branch:
            checkout_with['ref'] = branch

        if inputs := self.manifest.get('with', {}):
            self.contexts['inputs'] = inputs

        self.manifest['generator'] = 'opentestfactory.org/.uses@v1'
        org_with = self.manifest.get('with')
        self.manifest['with'] = {}
        self.process_generator()
        del self.manifest['generator']
        if org_with is not None:
            self.manifest['with'] = org_with

        jobs = {
            f'.download_job_{self.name}': {
                'runs-on': (
                    list(set(DOWNLOAD_JOB_TAGS.split(','))) if DOWNLOAD_JOB_TAGS else []
                ),
                'steps': [
                    {'uses': 'actions/checkout@v2', 'with': checkout_with},
                    {'uses': 'actions/get-file@v1', 'with': {'path': path}},
                ],
            }
        }
        _adjust_generated_jobs(jobs, self, self.full_name, download_job=True)
        self.workflow.add_generated_jobs(self.full_name, jobs)

    # Lifecycle

    def start(self, channel_metadata: Optional[Dict[str, Any]] = None) -> None:
        """Start job."""
        self.workflow.phase = PHASE_RUNNING
        self.workflow.inc_jobs_count()
        debug(
            'Job %s started (job_id=%s, workflow_id=%s).',
            self.full_name,
            self.job_id,
            self.workflow_id,
        )
        self.phase = PHASE_RUNNING

        self.workflow.start_timer(self.job_id, self.manifest['timeout-minutes'])
        self.workflow.current_jobs[self.job_id] = self

        if 'steps' in self.manifest:
            self.prepend_steps(self.manifest['steps'])
            self._assign_channel(channel_metadata)
            self.process_next_step()
        elif 'generator' in self.manifest:
            self.process_generator()
        elif 'uses' in self.manifest:
            self.process_uses()

    def cancel(self, force: bool = False) -> None:
        """Cancel job.

        Sets the job status as `STATUS_CANCELED`.

        If the job was running, remaining steps will run if their
        conditionals include `cancelled()` or `always()`.

        Forced cancellation (`force=True`) skips the conditionals (only
        used internally, for timed-out jobs and unrecoverable errors).
        """
        self._set_status(STATUS_CANCELED)
        if 'steps' in self.manifest:
            if force:
                self.steps_backlog = []
                self.teardown_hooks = None
                self.process_next_step()
                return

            self._maybe_propagate_cancellation()
            if (
                self._current_step
                and 'uses' in self._current_step
                and '_answered' not in self._current_step
            ):
                self._current_step['_answered'] = True
                self.process_next_step()

        if 'generator' in self.manifest:
            if not self.manifest.get('_answered'):
                self.manifest['_answered'] = True
                self.complete()

    def fail(self, reason: str) -> None:
        """Fail job.

        If the job was running, run remaining steps if their conditionals
        include 'failed()'.  Sets the job's status as 'STATUS_FAILED' if
        the job isn't already cancelled.
        """
        if self.status == STATUS_SUCCESS:
            self._set_status(STATUS_FAILURE)
        _notify_error(self.workflow, self, reason)

    def complete(self) -> None:
        """Finalize job."""
        if self.phase == PHASE_COMPLETED:
            return

        self.workflow.cancel_timer(self.job_id)
        self.phase = PHASE_COMPLETED
        debug(
            'Job %s ended with status %s (job_id=%s, workflow_id=%s).',
            self.full_name,
            self.status,
            self.job_id,
            self.workflow_id,
        )
        del self.workflow.current_jobs[self.job_id]

        if 'steps' in self.manifest:
            self._unassign_channel()

        if 'generator' in self.manifest or 'uses' in self.manifest:
            self.contexts['jobs'] = self.workflow.get_needs_context(
                job_name
                for job_name in self.workflow.results
                if job_name.rpartition(' ')[0] == self.full_name
            )
        for output, value in _prepare_outputs(
            self.manifest.get('outputs', {}), self.contexts
        ).items():
            self.workflow.set_job_output(self.full_name, output, value)
        self.workflow.set_job_result(self.full_name, self.status)


########################################################################
## Providers helpers


def _make_providercommand(step: JSON, metadata: JSON, contexts: JSON) -> JSON:
    """Make a ProviderCommand event."""
    _set_category_labels(metadata, step['uses'])
    prepare = make_event(PROVIDERCOMMAND, metadata=metadata, contexts=contexts)
    prepare['step'] = {
        k: v
        for k, v in step.items()
        if k
        in [
            'name',
            'id',
            'uses',
            'with',
            'variables',
            'working-directory',
            'timeout-minutes',
            'continue-on-error',
        ]
    }
    return prepare


def _add_providersteps(job: Job, parent_step: JSON, response: JSON) -> None:
    """Add returned steps to job backlog.

    # Required parameters

    - job: the job the provider step is part of
    - parent_step: the provider step
    - response: the provider service response

    A _frame_ is pushed on the job's stack for each provider step.  The
    frame contains the step's outputs, status, and a reference to the
    parent step.  The parent step's outputs are merged with the step's
    outputs before the step is added to the backlog.
    """
    metadata = response['metadata']
    parent_variables = parent_step.get('variables', {})
    parent_directory = parent_step.get('working-directory')
    runner_on_windows = job.contexts.get('runner', {}).get('os') == 'windows'

    step_origin = metadata['step_origin'] + [metadata['step_id']]
    contexts = job.contexts.copy()
    contexts['variables'] = _get_values(parent_step.get('variables', {}))
    if 'with' in parent_step:
        contexts['inputs'] = parent_step['with']

    steps: List[JSON] = response['steps']
    try:
        _apply_provider_hooks(
            steps,
            metadata.get('labels', {}),
            workflow_hooks=job.workflow.manifest.get('hooks', []),
            provider_hooks=response.get('hooks', []),
            contexts=contexts,
            job=job,
        )

        for step in steps:
            if parent_directory:
                step['working-directory'] = join_path(
                    parent_directory, step.get('working-directory'), runner_on_windows
                )
            variables = parent_variables.copy()
            variables.update(step.get('variables', {}))
            if variables:
                step['variables'] = variables
            if annotations := metadata.get('annotations', {}).get(HOOKS_ANNOTATIONS):
                step.setdefault('metadata', {})
                step['metadata'].setdefault('annotations', {})[
                    HOOKS_ANNOTATIONS
                ] = annotations
        frame = {
            'inputs': parent_step.get('with'),
            'outputs': response.get('outputs'),
            'continue-on-error': parent_step.get('continue-on-error', False),
            'status': STATUS_SUCCESS,
            'variables': contexts['variables'],
            'steps': {},
            'if': parent_step.get('if', 'success()'),
        }
        if 'timeout-minutes' in parent_step:
            frame['timer_uuid'] = job.job_id + ' '.join(step_origin)
        job.stack.append(frame)
        job.prepend_steps(steps + [parent_step], step_origin)
    except Exception as err:
        error(f'Failed to process ProviderResult: {err}.')
        job.fail(reason=str(err))


def _notify_error(workflow: Workflow, job: Optional[Job], msg: str) -> None:
    prefix = f'[job {job.job_id}] ' if job else ''
    event = make_event(
        NOTIFICATION,
        metadata={'name': 'log notification', 'workflow_id': workflow.workflow_id},
        spec={'logs': [f'{prefix}ERROR: {msg}']},
    )
    if job:
        event['metadata']['job_id'] = job.job_id
    DISPATCH_QUEUE.put(event)


def _complete_providerstep(job: Job) -> None:
    """Complete provider step."""
    if not (frame := job.stack.pop()):
        raise Exception(f'Empty stack for job {job.job_id}.')

    if timer := frame.get('timer_uuid'):
        job.workflow.cancel_timer(timer)

    job.set_step_outcome(frame['status'])

    if outputs := frame['outputs']:
        contexts = job.get_contexts()
        contexts['steps'] = frame['steps']
        contexts['inputs'] = frame['inputs']
        contexts['variables'] = frame['variables']
        for key, value in outputs.items():
            job.set_step_output(
                key,
                evaluate_str(value, restrict_context(contexts, STEP_OUTPUT_CONTEXTS)),
            )


def _apply_provider_hooks(
    steps: List[JSON],
    labels: JSON,
    workflow_hooks: List[JSON],
    provider_hooks: List[JSON],
    contexts: JSON,
    job: Job,
) -> None:
    """Apply hooks, if any.

    `steps` is modified if hooks apply.
    """

    def _match(what: str) -> bool:
        return event[what] in ('_', labels.get(f'opentestfactory.org/{what}'))

    if not (workflow_hooks or provider_hooks):
        debug('No hooks defined for provider.')
        return

    before = []
    after = []
    for hook in chain(workflow_hooks, provider_hooks):
        debug('Checking hook %s.', hook['name'])
        for event in hook.get('events', []):
            if not ('category' in event or 'categoryPrefix' in event):
                continue
            if 'category' in event and 'categoryPrefix' in event:
                event_match = _match('category') and _match('categoryPrefix')
            elif 'category' in event:
                event_match = _match('category')
            else:
                event_match = _match('categoryPrefix')
            if event_match and 'categoryVersion' in event:
                event_match = _match('categoryVersion')
            if event_match:
                break
        else:
            continue
        if 'if' in hook and not evaluate_if(
            hook['if'], contexts, special_functions=job._make_special_functions()
        ):
            debug('Conditional is false for hook %s.', hook['name'])
            continue
        before += [step.copy() for step in hook.get('before', [])]
        after = [step.copy() for step in hook.get('after', [])] + after

    for pos, item in enumerate(before):
        steps.insert(pos, item)
    steps.extend(after)


def _make_executioncommand(step: JSON, metadata: JSON, job: Job) -> JSON:
    """Make an ExecutionCommand event."""
    if annotations := metadata.get('annotations', {}).get(HOOKS_ANNOTATIONS):
        channel_annotation = annotations.get('channel')
        if channel_annotation == 'setup' and annotations.get('use-workspace'):
            annotations['use-workspace'] = job.workspace
        if channel_annotation == 'teardown' and annotations.get('keep-workspace'):
            annotations['keep-workspace'] = job.keep_workspace
    job.sequences.add(job.sequence_id)
    metadata['step_sequence_id'] = job.sequence_id
    job.sequence_id += 1
    metadata['channel_id'] = job.channel_id
    if artifacts := job.workflow.artifacts:
        metadata['artifacts'] = artifacts
    prepare = make_event(EXECUTIONCOMMAND, metadata=metadata, scripts=[step['run']])
    for item in ('variables', 'working-directory', 'shell'):
        if value := step.get(item):
            prepare[item] = value
    return prepare


def timedout(workflow_id: str):
    """A workflow or a job timed out.  Notify the event handler."""
    if obj := CURRENT_WORKFLOWS.get(workflow_id):
        obj.timedout = True
        EVENTS_QUEUE.put(
            {'kind': 'TimeoutError', 'metadata': {'workflow_id': workflow_id}}
        )


def _prepare_and_publish_step_event(
    step: JSON, metadata: JSON, contexts: JSON, job: Job
):
    """Publish step event.

    # Required parameters

    - step: a dictionary
    - metadata: a dictionary
    - contexts: a dictionary
    - job: a Job
    """
    if job.phase == PHASE_PENDING:
        raise ExecutionError(f'Job {job.job_id} not started')

    if timeout := step.get('timeout-minutes'):
        uuid = job.job_id + ' '.join(metadata['step_origin']) + metadata['step_id']
        job.workflow.start_timer(uuid, timeout)

    if 'uses' in step:
        prepare = _make_providercommand(step, metadata, contexts)
    else:
        prepare = _make_executioncommand(step, metadata, job)

    prepare['runs-on'] = job.manifest['runs-on']
    DISPATCH_QUEUE.put(prepare)
    if 'run' in step:
        TELEMETRY_MANAGER.set_metric(
            ArrangerMetrics.COMMANDS_STARTED_COUNTER,
            1,
            {'job_id': job.job_id, 'workflow_id': job.workflow_id},
        )
        TELEMETRY_MANAGER.set_metric(ArrangerMetrics.COMMANDS_RUNNING_GAUGE, 1)


def _adjust_contexts_from_frame(
    frame: Dict[str, Any], contexts: Dict[str, Any], step_variables: JSON
) -> None:
    """Adjust contexts if step is not top-level.

    # Required parameters

    - frame: a dictionary, the stack top-most entry
    - contexts: a dictionary, the contexts
    - step_variables: a dictionary, the step variables
    """
    if inputs := frame.get('inputs'):
        contexts['inputs'] = inputs
        for key, val in inputs.items():
            if isinstance(val, (int, float, bool)):
                step_variables[f'INPUT_{key.replace("-", "_").upper()}'] = str(val)
            elif (
                isinstance(val, str)
                and val.replace('\t', '').isprintable()
                and len(val) <= 2048
            ):
                step_variables[f'INPUT_{key.replace("-", "_").upper()}'] = {
                    'value': val,
                    'verbatim': True,
                }
    contexts['steps'] = frame.get('steps')
    contexts['job'] = {'status': frame['status']}


def _evaluate_step_fields(
    step: Dict[str, Any], contexts: Dict[str, Dict[str, Any]]
) -> None:
    """Evaluate step fields."""
    _contexts = restrict_context(contexts, STEP_CONTEXTS)
    if 'with' in step:
        step['with'] = evaluate_items(step['with'], _contexts)
    if 'continue-on-error' in step:
        step['continue-on-error'] = evaluate_bool(step['continue-on-error'], _contexts)
    if 'timeout-minutes' in step:
        step['timeout-minutes'] = evaluate_int(step['timeout-minutes'], _contexts)
    for key in ('name', 'run', 'working-directory'):
        if key in step:
            step[key] = evaluate_str(step[key], _contexts)


def _process_step(step: JSON, job: Job) -> bool:
    """Process step.

    There was at least one step in the job's `steps_backlog`.

    Will publish an EXECUTIONCOMMAND message or a PROVIDERCOMMAND
    event if the step conditional applies.

    # Required parameters

    - step: a step
    - job: the job the step is part of

    # Returned value

    A boolean.  True if the next step is ready to be processed.
    """
    job.set_step(step)
    if 'uses' in step and '_answered' in step:
        _complete_providerstep(job)
        return True

    job.inc_steps_count()

    contexts = job.get_contexts()
    local_variables_names = list(step.get('variables', {}))
    step_variables = job.get_variables()
    step_variables.update(evaluate_items(step.get('variables', {}), contexts))
    contexts['variables'] = _get_values(step_variables)
    if step['metadata']['step_origin']:
        _adjust_contexts_from_frame(job.stack[-1], contexts, step_variables)

    if not evaluate_if(
        step.get('if', 'success()'),
        restrict_context(contexts, STEP_IF_CONTEXTS),
        special_functions=job._make_special_functions(),
    ):
        job.set_step_outcome(STATUS_SKIPPED)
        EVENTS_QUEUE.put(
            {
                'kind': 'NextStep',
                'metadata': {'workflow_id': job.workflow_id, 'job_id': job.job_id},
            }
        )
        return False

    if 'use-workspace' in step:
        job.workspace = step['use-workspace']
        return True
    if 'keep-workspace' in step:
        job.keep_workspace = step['keep-workspace']
        return True

    if step_variables:
        filtered = {
            k: v
            for k, v in step_variables.items()
            if k in local_variables_names or k not in job.opentf_variables
        }
        if filtered:
            step['variables'] = filtered
    for item, value in job.defaults.get('run', {}).items():
        step.setdefault(item, value)

    _evaluate_step_fields(step, contexts)
    metadata = {
        'name': contexts['opentf']['step'],
        'namespace': job.get_metadata()['namespace'],
        'workflow_id': job.workflow_id,
        'job_id': job.job_id,
        'job_origin': job.get_metadata()['job_origin'],
        'step_id': step['id'],
        'step_origin': step['metadata']['step_origin'],
        'annotations': {'local_variables_names': local_variables_names},
    }
    if 'annotations' in step['metadata']:
        metadata['annotations'].update(step['metadata']['annotations'])

    _prepare_and_publish_step_event(step, metadata, contexts, job)
    return False


########################################################################
# event handling


def _maybe_get_current_job(workflow: Workflow, event: Dict[str, Any]) -> Job:
    if job := workflow.current_jobs.get(event['metadata']['job_id']):
        return job
    raise InvalidRequestError(f'Job {event["metadata"]["job_id"]} not current.')


def _handle_workflow(workflow_id: str, event: Dict[str, Any]) -> None:
    try:
        debug('Preparing workflow %s.', workflow_id)
        workflow = Workflow(event)
        CURRENT_WORKFLOWS[workflow_id] = workflow
        TELEMETRY_MANAGER.set_metric(ArrangerMetrics.WORKFLOWS_RECEIVED_COUNTER, 1)
        workflow.start()
    except Exception as err:
        msg = f'Internal error while preparing workflow: {err}'
        error(msg)
        if workflow_id in CURRENT_WORKFLOWS:
            del CURRENT_WORKFLOWS[workflow_id]
        DISPATCH_QUEUE.put(
            make_event(
                WORKFLOWCANCELED,
                metadata=event['metadata'],
                details={'reason': msg, 'status': STATUS_FAILURE},
            )
        )


def _handle_executionresult(workflow: Workflow, event: Dict[str, Any]) -> None:
    metadata = event['metadata']
    step_sequence_id = metadata['step_sequence_id']
    if step_sequence_id == CHANNEL_REQUEST:
        channelallocator.process_candidate(workflow.workflow_id, event)
        return
    if step_sequence_id < CHANNEL_REQUEST:  # CHANNEL_NOTIFY or CHANNEL_RELEASE
        if step_sequence_id == CHANNEL_RELEASE:
            channelallocator.publish_channel_requests(app.config['CONTEXT'])
        return

    job = _maybe_get_current_job(workflow, event)
    try:
        job.sequences.remove(step_sequence_id)
    except KeyError:
        raise InvalidRequestError(
            f'Job {job.job_id} not expecting step {step_sequence_id}.'
        ) from None

    # TODO: check if a timeout-minutes is defined?
    uuid = job.job_id + ' '.join(metadata['step_origin']) + metadata['step_id']
    workflow.cancel_timer(uuid)

    if job.manifest['metadata'].get('download_job') and event.get('attachments'):
        job.manifest['metadata']['subworkflow_paths'] = [
            path for path in event['attachments']
        ]
    if variables := event.get('variables'):
        job.variables.update(variables)
        job.opentf_variables = list(variables)

    job.set_step_outcome(STATUS_FAILURE if event.get('status', 0) else STATUS_SUCCESS)

    for output, value in event.get('outputs', {}).items():
        job.set_step_output(output, value)

    job.process_next_step()


def _handle_generatorresult(workflow: Workflow, event: Dict[str, Any]) -> None:
    if event['metadata'].get('download_job'):
        return
    job = _maybe_get_current_job(workflow, event)

    if 'generator' not in job.manifest:
        raise InvalidRequestError(f'Job {job.job_id} has no generator.')

    if job.manifest.get('_answered'):
        return

    job.manifest['_answered'] = True
    valid, extra = validate_pipeline(event)
    if not valid:
        job.fail(reason=f'Circular dependencies in generated jobs: {extra}.')
        job.complete()
        return

    job.manifest['outputs'] = event.get('outputs', {})

    parent_tags = set(job.manifest['runs-on'])
    job_origin = event['metadata']['job_origin'] + [job.job_id]
    for job_name, job_def in event['jobs'].items():
        _adjust_job(job_name, job_def, job.full_name, job_origin)
        tags = job_def.get('runs-on', [])
        if isinstance(tags, str):
            tags = [tags]
        job_def['runs-on'] = list(parent_tags | set(tags))

    workflow.add_generated_jobs(job.full_name, event['jobs'])


def _handle_providerresult(workflow: Workflow, event: Dict[str, Any]) -> None:
    job = _maybe_get_current_job(workflow, event)
    if not job._current_step:
        raise InvalidRequestError(f'Job {job.job_id} has no steps.')

    metadata = event['metadata']
    parent_step = job.get_step()
    if metadata['step_id'] != parent_step['id']:
        if metadata['step_id'] in parent_step['metadata']['step_origin']:
            return
        raise InvalidRequestError(f'Step {metadata["step_id"]} not current.')

    if parent_step.get('_answered'):
        return

    _add_providersteps(job, parent_step, event)
    parent_step['_answered'] = True

    job.process_next_step()


def _handle_workflowresult(workflow: Workflow, event: Dict[str, Any]) -> None:
    if not event['metadata'].get('job_id'):
        return
    metadata = event['metadata']
    if metadata.get('attachments'):
        workflow.artifacts += list(metadata['attachments'].keys())


def _handle_executionerror(workflow: Workflow, event: Dict[str, Any]) -> None:
    if event['metadata'].get('annotations', {}).get(PHASE_ANNOTATIONS):
        return
    if 'job_id' in event['metadata']:
        job = _maybe_get_current_job(workflow, event)

        msg = 'No details specified for ExecutionError.'
        if 'steps' in job.manifest:
            parent_step = job.get_step()
            if 'uses' in parent_step:
                job.set_step_outcome(STATUS_FAILURE)
            if not parent_step.get('continue-on-error'):
                job.fail(event['details'].get('error', msg))
            job.process_next_step()
            return
        if 'error' in event['details']:
            msg = event['details']['error']
        elif details := event['details']:
            msg = '\n'.join(f'{k}: {v}' for k, v in details.items())
        job.fail(msg)
        if workflow.manifest['jobs'][job.full_name]['status'] == 'RUNNING':
            job.complete()
        return
    try:
        workflow.cancel()  # fail(raised=True)
    except Exception as err:
        error(f'Internal error while failing workflow {workflow.workflow_id}: {err}.')


def _handle_workflowcancellation(workflow: Workflow, event: Dict[str, Any]) -> None:
    workflow.cancel(
        reason=event.get('details', {}).get('reason', 'client cancellation')
    )


def _handle_timeouterror(workflow: Workflow, _: Dict[str, Any]) -> None:
    workflow.cancel(reason='timeout error', force=True)


def _handle_nextstep(workflow: Workflow, event: Dict[str, Any]) -> None:
    job = _maybe_get_current_job(workflow, event)
    job.process_next_step()


def _handle_startjob(workflow: Workflow, event: Dict[str, Any]) -> None:
    for job in list(workflow.jobs_candidates.values()):
        if job.job_id == event['metadata']['job_id']:
            try:
                workflow.candidates.update_job_status(job.full_name, 'RUNNING')
                job.start(event['candidate'])
            except ExecutionError as err:
                warning(f'Could not start job {job.job_id}: {err}.')
                if job.phase == PHASE_PENDING:
                    workflow._skip_job(job.full_name, job, STATUS_SKIPPED)
                else:
                    job.cancel(force=True)
                workflow.cancel(reason=f'Could not start job {job.job_id}: {err}')
            except Exception as err:
                error(
                    f'Could not start job {job.job_id}, got an unexpected error: {err}'
                )
                workflow.cancel(reason=f'Could not start job {job.job_id}: {err}.')


EVENTS_HANDLERS = {
    'ExecutionResult': _handle_executionresult,
    'ProviderResult': _handle_providerresult,
    'GeneratorResult': _handle_generatorresult,
    'ExecutionError': _handle_executionerror,
    'WorkflowCancellation': _handle_workflowcancellation,
    'WorkflowResult': _handle_workflowresult,
    'TimeoutError': _handle_timeouterror,
    'NextStep': _handle_nextstep,
    'StartJob': _handle_startjob,
}

GAUGES = defaultdict(lambda: {'sum': 0, 'count': 0, 'min': 999999, 'max': -1})


def handle_events():
    """Prepare workflow handler and start it."""
    while True:
        event = EVENTS_QUEUE.get()
        workflow_id = event['metadata']['workflow_id']
        start = datetime.now()
        try:
            workflow = CURRENT_WORKFLOWS.get(workflow_id)
            if handler := EVENTS_HANDLERS.get(event['kind']):
                if not workflow:
                    raise InvalidRequestError(f'Workflow {workflow_id} not current.')
                handler(workflow, event)
            else:
                if workflow:
                    raise AlreadyExistsError(f'Workflow {workflow_id} already exists.')
                _handle_workflow(workflow_id, event)
        except ExecutionError as err:
            if workflow := CURRENT_WORKFLOWS.get(workflow_id):
                workflow.cancel(reason=str(err))
                continue
            error(f'Execution error while processing {event}: {err}.')
        except ServiceError as err:
            warning(f'{err.status_name}: {err.msg} details={err.details}')
        except Exception as err:
            error(f'Internal error while processing {event}: {err}.')
        finally:
            stop = datetime.now()
            gauge = GAUGES[event['kind']]
            gauge['count'] += 1
            delta = (stop - start).microseconds
            gauge['sum'] += delta
            gauge['max'] = max(gauge['max'], delta)
            gauge['min'] = min(gauge['min'], delta)
            gauge['timestamp'] = stop.timestamp()


########################################################################
## Requests Handlers


def process_workflow(body: JSON, schema: str) -> None:
    """Process a WORKFLOW event.

    # Required parameters

    - body: a JSON document

    # Raised exceptions

    _BadRequest_ if `body` is malformed.
    """
    valid, extra = validate_schema(schema, body)
    if not valid:
        raise BadRequestError(f'Not a valid {schema}: {extra}.')

    if not body['metadata'].get('namespace'):
        raise BadRequestError('Missing namespace.')
    if not body['metadata'].get('workflow_id'):
        raise BadRequestError('Missing workflow_id.')

    if auth := request.headers.get('Authorization'):
        actor = get_actor()
        token = auth.split()[1]
    else:
        actor = '_localhost_'
        token = None
    body['metadata']['actor'] = actor
    body['metadata']['token'] = token

    EVENTS_QUEUE.put(body)


def process_workflowresult(body: JSON, schema: str) -> None:
    """Process a WORKFLOWRESULT event.

    # Required parameters

    - body: a JSON document

    # Raised exceptions

    _BadRequest_ if `body` is malformed.
    """
    valid, extra = validate_schema(schema, body)
    if not valid:
        raise BadRequestError(f'Not a valid {schema}: {extra}.')

    if body['metadata']['workflow_id'] in CURRENT_WORKFLOWS:
        EVENTS_QUEUE.put(body)


def process_otherevent(body: JSON, schema: str) -> None:
    """Process an event.

    Duplicates and out-of-band responses are silently ignored.

    # Required parameters

    - body: a JSON document

    # Raised exceptions

    _BadRequest_ if body is malformed.  _NotFound_ if unknown
    `metadata.workflow_id`.
    """
    valid, extra = validate_schema(schema, body)
    if not valid:
        raise BadRequestError(f'Not a valid {schema}: {extra}.')

    if body['metadata']['workflow_id'] in CURRENT_WORKFLOWS:
        EVENTS_QUEUE.put(body)
        return

    metadata = body['metadata']
    if metadata.get('step_sequence_id') == CHANNEL_RELEASE:
        channelallocator.publish_channel_requests(app.config['CONTEXT'])
    completed = metadata.get('annotations', {}).get(PHASE_ANNOTATIONS) is not None
    completed |= metadata.get('step_sequence_id') == CHANNEL_RELEASE
    if not completed:
        raise NotFoundError(f'Workflow {body["metadata"]["workflow_id"]} not found.')


def process_channelhandler_notification(body: JSON, schema: str) -> None:
    if body.get('spec', {}).get('channel.handler'):
        valid, extra = validate_schema(schema, body)
        if not valid:
            raise BadRequestError(f'Not a valid {schema}: {extra}.')
        channelallocator.update_channels(**body['spec']['channel.handler'])


########################################################################
## Main

SUBSCRIPTIONS = {
    WORKFLOW: process_workflow,
    WORKFLOWCANCELLATION: process_otherevent,
    GENERATORRESULT: process_otherevent,
    PROVIDERRESULT: process_otherevent,
    EXECUTIONRESULT: process_otherevent,
    EXECUTIONERROR: process_otherevent,
    WORKFLOWRESULT: process_workflowresult,
    NOTIFICATION: process_channelhandler_notification,
}


app = make_app(
    name='arranger',
    description='Create and start an orchestration service.',
)

try:
    TELEMETRY_MANAGER = TelemetryManager(SERVICE_NAME, ArrangerMetrics)
except Exception as err:
    fatal('Failed to initialize TelemetryManager for %s: %s.', SERVICE_NAME, str(err))

DISPATCH_QUEUE = make_dispatchqueue(app)

MAX_JOBS = get_context_parameter(app, 'max_jobs')
MAX_PARALLEL = get_context_parameter(app, 'max_parallel')
MAX_STEPS = get_context_parameter(app, 'max_job_steps')
MAX_WORKERS = get_context_parameter(app, 'max_workers')
DEFAULT_TIMEOUT_MINUTES = get_context_parameter(app, 'default_timeout_minutes')
DOWNLOAD_JOB_TAGS = get_context_parameter(app, 'download_job_tags')
DOWNLOAD_JOB_PARENTS_TAGS = get_context_parameter(app, 'download_job_parents_tags')


@app.route('/metrics', methods=['GET'])
def get_metrics():
    """Get metrics."""
    workflows = list(CURRENT_WORKFLOWS.values())
    return {
        'gauges': GAUGES,
        'workflows': [
            {
                'workflow_id': w.workflow_id,
                'phase': w.phase,
                'status': w.status,
                'jobs_count': w._jobs_count,
                'jobs_running': list(w.current_jobs),
                'jobs_candidates': list(w.jobs_candidates),
            }
            for w in workflows
        ],
        'events.queue.size': EVENTS_QUEUE.qsize(),
    }


@app.route('/internals', methods=['GET'])
def get_internals():
    """Get internals."""
    return {
        'channelallocator.events.queue.size': channelallocator.EVENTS_QUEUE.qsize(),
        'channelallocator.jobs.timestamps': channelallocator.JOBS_TIMESTAMPS,
        'channelallocator.jobs.pendings': {
            k: {
                'workflow_id': v.workflow_id,
                'jobs': v.jobs,
                'max_parallel': v.max_parallel,
            }
            for k, v in channelallocator.PENDING_JOBS.items()
        },
    }


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication.

    It accepts and process incoming publications as specified via
    `SUBSCRIPTIONS`.

    # Workflow format

    Please refer to `schemas/opentestfactory.org/v1/Workflow.json'.

    # Returned value

    A _status manifest_.
    """
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict) or 'kind' not in body or 'apiVersion' not in body:
        return make_status_response(
            'Invalid', 'Not an object or no kind or no apiVersion specified.'
        )

    schema = f'{body["apiVersion"]}/{body["kind"]}'
    if processor := SUBSCRIPTIONS.get(schema):
        try:
            processor(body, schema)
            status = make_status_response('OK', '')
        except ServiceError as err:
            status = make_status_response(
                err.http_status_name, err.msg, details=err.details
            )
        except Exception as err:
            status = make_status_response(
                'InternalError', f'Internal error while processing {body}: {err}.'
            )
    else:
        status = make_status_response('BadRequest', 'Unexpected event received.')

    return status


def main(svc):
    """Start the arranger service."""
    try:
        debug('Starting workflow events handling thread.')
        threading.Thread(target=handle_events, daemon=True).start()
    except Exception as err:
        fatal('Could not start workflow events handling thread: %s.', str(err))

    debug(f'Subscribing to {", ".join(SUBSCRIPTIONS)}.')
    try:
        subs = [subscribe(kind=kind, target='inbox', app=svc) for kind in SUBSCRIPTIONS]
    except Exception as err:
        fatal('Could not subscribe to eventbus: %s.', str(err))

    try:
        channelallocator.init(app.config['CONTEXT'])
    except Exception as err:
        fatal('Could not start channelallocator threads: %s.', str(err))

    debug('Starting service.')
    try:
        run_app(svc)
    finally:
        for sub in subs:
            unsubscribe(sub, app=svc)


if __name__ == '__main__':
    main(app)
