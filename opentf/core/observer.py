# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An observer service."""

from typing import Any, Dict, List, NoReturn, Optional, Set, Tuple, Union

import copy
import json
import os
import re
import sys
import pickle
import tempfile
import threading
import time

from collections import defaultdict
from datetime import datetime, timedelta, timezone
from math import ceil
from queue import Queue

import yaml

from werkzeug.middleware.proxy_fix import ProxyFix
from flask import request, send_file

from opentf.commons import (
    WORKFLOW,
    WORKFLOWCANCELLATION,
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
    WORKFLOWRESULT,
    GENERATORCOMMAND,
    GENERATORRESULT,
    PROVIDERCOMMAND,
    PROVIDERRESULT,
    EXECUTIONCOMMAND,
    EXECUTIONRESULT,
    EXECUTIONERROR,
    NOTIFICATION,
    RETENTION_POLICY,
    authorizer,
    can_use_namespace,
    list_accessible_namespaces,
    make_app,
    make_status_response,
    annotate_response,
    run_app,
    get_context_parameter,
    subscribe,
    unsubscribe,
    validate_schema,
    DEFAULT_NAMESPACE,
)

from opentf.commons.expressions import evaluate_bool
from opentf.commons.selectors import (
    compile_selector,
    match_selectors,
    prepare_selectors,
)
from opentf.core.datasources import (
    get_testcases,
    get_jobs,
    get_tags,
    in_scope,
    DataSourceDataError,
    DataSourceScopeError,
)
from opentf.toolkit import watch_file

from opentf.tools.ctlworkflows import (
    emit_event,
    OutputContext,
    OUTPUTCONTEXT_PARAMETERS,
)


########################################################################

## Constants

BOM_FILE = '/app/BOM.json'

DEFAULT_PER_PAGE = 100
MAX_PER_PAGE = 1000

SUBSCRIPTIONS = (
    WORKFLOW,
    WORKFLOWCANCELLATION,
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
    WORKFLOWRESULT,
    GENERATORCOMMAND,
    GENERATORRESULT,
    PROVIDERCOMMAND,
    PROVIDERRESULT,
    EXECUTIONCOMMAND,
    EXECUTIONRESULT,
    EXECUTIONERROR,
    NOTIFICATION,
)

JOBS = 'jobs'
TAGS = 'tags'
TESTCASES = 'testcases'
DATASOURCE_KINDS = (JOBS, TAGS, TESTCASES)

DATASOURCE_EVENTSELECTOR = compile_selector(
    'kind in (Workflow, GeneratorResult, ProviderCommand,'
    ' ExecutionCommand, ExecutionResult, Notification)'
)
META_EVENTS = ('Workflow', 'GeneratorResult', 'WorkflowCanceled', 'WorkflowCompleted')
COMMAND_EVENTS = ('ProviderCommand', 'ExecutionCommand', 'ExecutionResult')


WORKFLOW_IN_PROGRESS = 'Workflow in progress'

EXECLOG_PREFIX = 'live-executionlog'

########################################################################
## Helpers


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def fatal(*args) -> NoReturn:
    """Log error message and exit with error code 2."""
    error(*args)
    sys.exit(2)


def warning(*args):
    """Log warning message."""
    app.logger.warning(*args)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


def _is_integer(label: str, value: str) -> bool:
    try:
        int(value)
        return True
    except ValueError:
        warning('`%s` should be an integer, got `%s`, ignoring.', label, value)
        return False


def _totimedelta(duration: str) -> Optional[timedelta]:
    """Convert string to timedelta.

    # Requred parameters

    - duration: a string (`forever` or of form `3d5h10m`)

    # Returned value

    A timedelta object or None.  The returned timedelta is caped by
    MAX_RETENTION_PERIOD if set.

    # Raised exceptions

    A _ValueError_ exception is raised if the `duration` string is not
    of the expected form.
    """
    if duration == 'forever':
        return None
    if duration and (match := re.match(r'^(\d+d)?(\d+h)?(\d+m)?$', duration)):
        days, hours, minutes = match.groups()
        td = timedelta(
            days=int(days[:-1]) if days else 0,
            hours=int(hours[:-1]) if hours else 0,
            minutes=int(minutes[:-1]) if minutes else 0,
        )
        if MAX_RETENTION_PERIOD:
            td = min(td, MAX_RETENTION_PERIOD)
        return td
    raise ValueError(
        f'Could not parse duration: {duration}.  Was expecting "forever" or "3d5h10m".'
    )


def _parse_selectors(args: Dict[str, str]) -> Tuple[Any, Any, str]:
    fieldselector, labelselector = prepare_selectors(args)
    selectors = ''
    if fieldselector or labelselector:
        if fieldselector:
            selectors += f'&fieldSelector={args.get("fieldSelector")}'
        if labelselector:
            selectors += f'&labelSelector={args.get("labelSelector")}'
    return fieldselector, labelselector, selectors


########################################################################


class CacheNotReady(Exception):
    """CacheNotReady class"""

    def __init__(self, msg, details=None):
        self.msg = msg
        self.details = details


class BackingStoreItems:
    """An file-based implementation for backing store items."""

    def __init__(self) -> None:
        self._itemcount = 0
        self._tempdir = tempfile.TemporaryDirectory()
        self._logfilename = os.path.join(self._tempdir.name, 'executionlog')
        self._logfile = open(self._logfilename, 'w', encoding='utf-8')
        self.context = OutputContext(
            output_format=None,
            step_depth=1,
            job_depth=1,
            max_command_length=None,
            output_prefix=None,
            file=self._logfile,
        )

    def _read(self, index):
        with open(os.path.join(self._tempdir.name, f'{index}'), 'rb') as f:
            return pickle.load(f)

    def __getitem__(self, key):
        if isinstance(key, slice):
            return [
                self._read(i) for i in range(key.start, min(self._itemcount, key.stop))
            ]
        raise IndexError()

    def filter_items(self, fieldselector, labelselector):
        for i in range(self._itemcount):
            item = self._read(i)
            if match_selectors(item, fieldselector, labelselector):
                yield item

    def get_filtered_item(self, key, fieldselector, labelselector):
        if isinstance(key, slice):
            filtered_events = self.filter_items(fieldselector, labelselector)
            try:
                for _ in range(key.start):
                    filtered_events.__next__()
            except StopIteration:
                return [], key.start + 1
            items = []
            for _ in range(key.start, key.stop):
                try:
                    items.append(filtered_events.__next__())
                except StopIteration:
                    break
            return items, key.start + len(items) + 1
        raise IndexError()

    def get_executionlog(self) -> str:
        self._logfile.flush()
        return self._logfilename

    def append(self, item) -> None:
        with open(os.path.join(self._tempdir.name, f'{self._itemcount}'), 'wb') as f:
            pickle.dump(item, f)
        emit_event(
            item['kind'],
            item,
            self.context,
            first=True,
            namespace=item.get('metadata', {}).get('namespace'),
        )
        self._itemcount += 1

    def cleanup(self):
        self._logfile.close()
        self._tempdir.cleanup()

    def __len__(self):
        return self._itemcount


class BackingStore:
    """A backing store interface."""

    def __init__(self) -> None:
        self._store = defaultdict(BackingStoreItems)

    def __contains__(self, key):
        return key in self._store

    def __setitem__(self, key, value):
        self._store[key] = value

    def __delitem__(self, key):
        self._store[key].cleanup()
        del self._store[key]

    def __getitem__(self, key):
        return self._store.__getitem__(key)


########################################################################

PENDINGEVENT_QUEUE: "Queue[Tuple[str, str, Dict[str, Any]]]" = (
    Queue()
)  # schema, workflow_id, event
WORKFLOWS: Dict[str, Dict[str, Any]] = {}
WORKFLOWS_LASTSEEN: Dict[str, datetime] = {}
WORKFLOWS_STATUS = BackingStore()  # keys = workflow_id, values = list of events
WORKFLOWS_STATUSES: Dict[str, str] = {}
WORKFLOWS_WORKERS: Dict[str, Set[str]] = {}
WORKFLOWS_NAMESPACES: Dict[str, str] = {}
WORKFLOWS_LIVE_EVENT_CACHES: Dict[str, Any] = defaultdict(list)
LIVE_DATASOURCE_CACHES: Dict[str, Any] = defaultdict(
    lambda: {
        JOBS: defaultdict(dict),
        TAGS: defaultdict(dict),
        TESTCASES: defaultdict(dict),
    }
)
ATTACHMENTS_NOTIFICATIONS = defaultdict(dict)
CHANNELS: Dict[str, List[Dict[str, Any]]] = {}
RETENTIONPOLICY: List[Dict[str, Any]] = []


def _compute_retention_period(
    workflow_id: str, last: datetime
) -> Tuple[str, Optional[timedelta], int, datetime]:
    """Compute retention period tuple for workflow."""
    contexts = {'workflow': WORKFLOWS[workflow_id]['metadata']}
    for policy in list(RETENTIONPOLICY):
        try:
            if evaluate_bool(policy['scope'], contexts):
                debug(
                    "Retention policy '%s' matches workflow %s.",
                    policy['name'],
                    workflow_id,
                )
                return (workflow_id, policy['timedelta'], policy.get('weight', 1), last)
        except Exception as err:
            warning(
                "Could not evaluate retention policy '%s', got: %s.  Skipping.",
                policy['name'],
                err,
            )
    return workflow_id, RETENTION_PERIOD, 1, last


def _get_cleanup_candidates() -> List[str]:
    """Get list of workflows to forget.

    All expired workflows are included.  Non-expired completed workflows
    may be included if the number of non-expired workflows exceeds
    `MAX_RETENTION_WORKFLOW_COUNT`.

    # Returned value

    A list of workflow IDs (strings).
    """
    now = datetime.now(timezone.utc)
    completed = WORKFLOWS_LASTSEEN.copy()
    candidates = [
        _compute_retention_period(workflow_id, last)
        for workflow_id, last in completed.items()
    ]
    expired = [
        workflow_id
        for workflow_id, retention, _, last in candidates
        if retention and (now - last) > retention
    ]
    remaining = len(set(completed) - set(expired))
    if MAX_RETENTION_WORKFLOW_COUNT and remaining > MAX_RETENTION_WORKFLOW_COUNT:
        weighted: Dict[int, List[Tuple[str, Optional[timedelta], int, datetime]]] = (
            defaultdict(list)
        )
        for candidate in candidates:
            if candidate[0] in expired:
                continue
            weighted[candidate[2]].append(candidate)
        disposables = []
        for weight in sorted(weighted):
            disposables += sorted(weighted[weight], key=lambda candidate: candidate[3])
        expired += [
            x[0] for x in disposables[: remaining - MAX_RETENTION_WORKFLOW_COUNT]
        ]
    return expired


def clean_workflows() -> None:
    """Remove completed workflows events."""
    for workflow_id in _get_cleanup_candidates():
        info(f'Cleaning events for completed workflow {workflow_id}.')
        for collection in (
            WORKFLOWS_STATUS,
            WORKFLOWS_STATUSES,
            WORKFLOWS_WORKERS,
            WORKFLOWS_LASTSEEN,
            WORKFLOWS_NAMESPACES,
            WORKFLOWS_LIVE_EVENT_CACHES,
            LIVE_DATASOURCE_CACHES,
            ATTACHMENTS_NOTIFICATIONS,
            WORKFLOWS,
        ):
            try:
                del collection[workflow_id]
            except KeyError:
                pass


def _update_workers_state(workflow_id: str, body: Dict[str, Any]) -> None:
    """Adjust global workers statuses."""
    try:
        worker_id = body['spec']['worker']['worker_id']
        status = body['spec']['worker']['status']
    except KeyError:
        return
    if status == 'setup':
        WORKFLOWS_WORKERS[workflow_id].add(worker_id)
    elif status == 'teardown':
        if worker_id in WORKFLOWS_WORKERS[workflow_id]:
            WORKFLOWS_WORKERS[workflow_id].remove(worker_id)
        else:
            warning(f'Unknown worker {worker_id}.')


def _initialize_workflow_state(workflow_id: str, event: Dict[str, Any]) -> None:
    """Initialize workflow recording state."""
    WORKFLOWS_NAMESPACES[workflow_id] = event['metadata'].get(
        'namespace', DEFAULT_NAMESPACE
    )
    if workflow_id in WORKFLOWS:
        if 'apiVersion' in WORKFLOWS[workflow_id]:
            warning(f'Duplicate workflow {workflow_id} received, ignoring.')
        else:
            event['metadata'].update(WORKFLOWS[workflow_id]['metadata'])
    event.setdefault('status', {}).setdefault('phase', WORKFLOWS_STATUSES[workflow_id])
    WORKFLOWS[workflow_id] = event

    labels = event['metadata'].get('labels')
    if not labels:
        return

    exec_labels = {
        k[len(EXECLOG_PREFIX) + 1 :]: v
        for k, v in labels.items()
        if k.startswith(EXECLOG_PREFIX)
        and k[len(EXECLOG_PREFIX) + 1 :] in OUTPUTCONTEXT_PARAMETERS
    }
    for k, v in exec_labels.copy().items():
        if isinstance(OUTPUTCONTEXT_PARAMETERS[k], int):
            if not _is_integer(k, v):
                del exec_labels[k]
            else:
                exec_labels[k] = int(v)
    if exec_labels:
        WORKFLOWS_STATUS[workflow_id].context = WORKFLOWS_STATUS[
            workflow_id
        ].context._replace(**{k.replace('-', '_'): v for k, v in exec_labels.items()})


def _finalize_workflow_state(workflow_id: str, status: str) -> None:
    """Finalize workflow recording state.

    Triggers workflows cleanup routine.
    """
    metadata = WORKFLOWS.setdefault(workflow_id, {}).setdefault('metadata', {})
    if metadata.get('completionTimestamp') is None:
        last = WORKFLOWS_LASTSEEN[workflow_id] = datetime.now(timezone.utc)
        clean_workflows()
        WORKFLOWS_STATUSES[workflow_id] = status
        metadata['completionTimestamp'] = last.isoformat()
        metadata['status'] = 'success' if status == 'DONE' else 'failure'
        WORKFLOWS[workflow_id].setdefault('status', {})['phase'] = status
    else:
        debug(f'Workflow {workflow_id} already completed, ignoring.')


def _record_workflow_event(what: Tuple[str, str, Dict[str, Any]]) -> None:
    """Record a workflow event.

    Runs sequentially.
    """
    schema, workflow_id, event = what
    if workflow_id not in WORKFLOWS_STATUSES:
        WORKFLOWS_NAMESPACES[workflow_id] = ''
        WORKFLOWS_STATUSES[workflow_id] = 'RUNNING'
        WORKFLOWS_WORKERS[workflow_id] = set()
        if schema != WORKFLOW:
            debug(f'Out of band message, workflow {workflow_id} unknown in {event}.')

    event.setdefault('metadata', {}).setdefault(
        'namespace', WORKFLOWS_NAMESPACES.get(workflow_id)
    )
    WORKFLOWS_STATUS[workflow_id].append(event)

    if (
        event['kind'] in META_EVENTS
        or event['kind'] in COMMAND_EVENTS
        or event['kind'] == 'Notification'
    ):
        _maybe_add_event_to_live_cache(event)

    if schema == WORKFLOW:
        _initialize_workflow_state(workflow_id, event)
    elif schema == NOTIFICATION and 'worker' in event.get('spec', {}):
        _update_workers_state(workflow_id, event)
    elif schema in (WORKFLOWCOMPLETED, WORKFLOWCANCELED):
        _finalize_workflow_state(
            workflow_id, 'DONE' if schema == WORKFLOWCOMPLETED else 'FAILED'
        )


def record_event() -> None:
    """Record events.

    Events are serialized so that no race condition occurs and so that
    no event is lost.
    """
    while True:
        try:
            _record_workflow_event(PENDINGEVENT_QUEUE.get())
        except Exception as err:
            error('Internal error while recording event: %s.', str(err))


def refresh_channels(channelhandler_id: str, channels: List[Dict[str, Any]]) -> None:
    """Refresh known channels.

    Replace known channels from handler by the new channels.
    """
    last_seen = datetime.now(timezone.utc).isoformat()
    for channel in channels:
        channel['metadata']['channelhandler_id'] = channelhandler_id
        channel['status']['lastCommunicationTimestamp'] = last_seen
    CHANNELS[channelhandler_id] = channels


def _maybe_set_proxyfix(proxy: str) -> None:
    if proxy.lower() == 'auto':
        app.wsgi_app = ProxyFix(app.wsgi_app)
        return
    if ',' in proxy:
        try:
            app.wsgi_app = ProxyFix(app.wsgi_app, *[int(x) for x in proxy.split(',')])
            return
        except Exception:
            pass
    error(
        "Invalid OPENTF_REVERSEPROXY content.  Should be either 'auto'"
        ' or up to 5 integers separated by commas.'
    )
    sys.exit(2)


BOM = {}


def _initialize_bom() -> None:
    if not os.path.isfile(BOM_FILE):
        BOM['error'] = f"Could not find '{BOM_FILE}', no bill of materials available."
        return
    with open(BOM_FILE, 'r', encoding='utf-8') as f:
        try:
            bom = json.load(f)
        except Exception as err:
            BOM['error'] = f"Could not read '{BOM_FILE}', got: {err}."
            return
    if not isinstance(bom, dict):
        BOM['error'] = (
            f"'{BOM_FILE}' is not a dictionary, no bill of materials available."
        )
        return
    for k, v in bom.items():
        BOM[k] = v


def _get_bom():
    if not BOM:
        _initialize_bom()
        if 'error' in BOM:
            error(BOM['error'])
    return BOM


def _read_rp_definition(_, policyfile: str) -> None:
    global RETENTIONPOLICY
    try:
        with open(policyfile, 'r', encoding='utf-8') as f:
            policy = yaml.safe_load(f)

        valid, extra = validate_schema(RETENTION_POLICY, policy)
        if not valid:
            error('Invalid retention policy definition: %s.', extra)
            RETENTIONPOLICY = []
            return

        if RETENTIONPOLICY:
            info("Replacing retention policy definition using '%s'.", policyfile)
        else:
            info("Reading retention policy definition using '%s'.", policyfile)

        for entry in policy['retentionPolicy']:
            entry['timedelta'] = _totimedelta(entry['retentionPeriod'])
        RETENTIONPOLICY = policy['retentionPolicy']
    except Exception as err:
        error("Error while reading '%s' policy definition: %s.", policyfile, err)
        RETENTIONPOLICY = []


def _get_workflow_items(workflow_id, page, per_page):
    events = WORKFLOWS_STATUS[workflow_id]
    fieldselector, labelselector, selectors = _parse_selectors(request.args)
    if fieldselector or labelselector:
        items, items_count = events.get_filtered_item(
            slice((page - 1) * per_page, page * per_page), fieldselector, labelselector
        )
        last = ceil(items_count / per_page)
    else:
        items = events[(page - 1) * per_page : page * per_page]
        last = ceil(len(events) / per_page)
    return items, last, selectors


## Datasource cache helpers


def _is_useful_command_event(event: Dict[str, Any]) -> bool:
    kind = event['kind']
    metadata = event['metadata']
    if kind == 'ExecutionCommand':
        return metadata.get('step_sequence_id') in (0, -1)
    if kind == 'ExecutionResult':
        return event.get('attachments') or metadata.get('step_sequence_id') == -2
    return (
        metadata.get('labels', {}).get('opentestfactory.org/categoryPrefix')
        != 'actions'
    )


def _is_relevant_event(event: Dict[str, Any]) -> bool:
    kind = event['kind']
    if kind in META_EVENTS:
        return True
    if kind in COMMAND_EVENTS and _is_useful_command_event(event):
        return True
    if kind == 'Notification' and (
        event.get('spec', {}).get('testResults')
        or event.get('spec', {}).get('managedTestResult')
    ):
        return True
    return False


def _get_datasource_items(
    workflow_id: str, datasource_kind: str, page: int, per_page: int
) -> Tuple[List[Dict[str, Any]], int, str]:
    """Get filtered datasource items, last page and selectors

    # Returned values

    A tuple: items: a list, last: an integer, selectors: a string

    # Raised exceptions

    _CacheNotReady_, _DataSourceScopeError_, _DataSourceDataError_, or
    _ValueError_.
    """
    scope = request.args.get('scope', default='true', type=str)
    if scope in ('true', 'false'):
        scope = bool(scope)
    args = [workflow_id, scope, page, per_page]
    return _get_live_datasource(*args, datasource_kind=datasource_kind)


def _count_jobs_tags_live_testcases(
    workflow_id: str, datasource_kind: str, testcases: Dict[str, Any]
) -> Dict[str, Any]:
    data = copy.deepcopy(LIVE_DATASOURCE_CACHES[workflow_id][datasource_kind])
    if datasource_kind == JOBS:
        for testcase in testcases.values():
            data[testcase['metadata']['job_id']]['status']['testCaseCount'] += 1
            data[testcase['metadata']['job_id']]['status']['testCaseStatusSummary'][
                testcase['test']['outcome']
            ] += 1

    elif datasource_kind == TAGS:
        for testcase in testcases.values():
            for tag in testcase['test']['runs-on']:
                data[tag]['status']['testCaseCount'] += 1
                data[tag]['status']['testCaseStatusSummary'][
                    testcase['test']['outcome']
                ] += 1
    return data


def _paginate_items(
    source_items: Dict[str, Any], page: int, per_page: int
) -> Tuple[List[Dict[str, Any]], int, str]:
    fieldselector, labelselector, selectors = _parse_selectors(request.args)
    items = [
        {'uuid': key, **value}
        for key, value in source_items.items()
        if match_selectors(value, fieldselector, labelselector)
    ]
    items_count = len(items)
    last = ceil(items_count / per_page)
    return items[(page - 1) * per_page : page * per_page], last, selectors


def _get_live_datasource(
    workflow_id: str,
    scope: Union[bool, str],
    page: int,
    per_page: int,
    datasource_kind: str,
) -> Tuple[List[Dict[str, Any]], int, str]:
    if datasource_kind != TESTCASES and not LIVE_DATASOURCE_CACHES.get(
        workflow_id, {}
    ).get(JOBS):
        raise CacheNotReady(
            f'No completed jobs for workflow {workflow_id}, cannot initialize {datasource_kind} cache.'
        )
    try:
        items = {
            key: value
            for key, value in LIVE_DATASOURCE_CACHES[workflow_id][TESTCASES].items()
            if in_scope(scope, value)
        }
    except Exception as err:
        raise DataSourceScopeError(str(err)) from err
    if datasource_kind in (JOBS, TAGS):
        items = _count_jobs_tags_live_testcases(workflow_id, datasource_kind, items)
    return _paginate_items(items, page, per_page)


def _handle_notification(event: Dict[str, Any], workflow_id: str):
    testcases = get_testcases(WORKFLOWS_LIVE_EVENT_CACHES[workflow_id])

    if not testcases:
        return

    if not LIVE_DATASOURCE_CACHES.get(workflow_id, {}).get(TESTCASES):
        LIVE_DATASOURCE_CACHES[workflow_id].setdefault(TESTCASES, {})
    for tc_id, exec_data in testcases.items():
        if cached := LIVE_DATASOURCE_CACHES[workflow_id][TESTCASES].get(tc_id):
            outcome = exec_data['test']['outcome']
            cached['test']['outcome'] = outcome
            cached['status'] = exec_data['status']
            cached['executionHistory'].append(copy.deepcopy(exec_data['execution']))
            cached['execution'] = exec_data['execution']
            cached['metadata']['executions'] += 1
        else:
            exec_data['metadata']['executions'] += 1
            exec_data['executionHistory'] = [copy.deepcopy(exec_data['execution'])]
            LIVE_DATASOURCE_CACHES[workflow_id][TESTCASES][tc_id] = copy.deepcopy(
                exec_data
            )
    WORKFLOWS_LIVE_EVENT_CACHES[workflow_id].remove(event)

    if (testresults := event['spec'].get('testResults')) and testresults[0].get(
        'attachment_origin'
    ) in ATTACHMENTS_NOTIFICATIONS[workflow_id]:
        ATTACHMENTS_NOTIFICATIONS[workflow_id][testresults[0]['attachment_origin']][
            'handled'
        ] = True


def _maybe_add_event_to_live_cache(event: Dict[str, Any]):
    if not _is_relevant_event(event):
        return

    workflow_id = event['metadata']['workflow_id']
    kind = event['kind']
    if kind in COMMAND_EVENTS:
        event = {'kind': kind, 'metadata': event['metadata']}
    WORKFLOWS_LIVE_EVENT_CACHES[workflow_id].append(event)

    if kind == 'ExecutionResult' and (
        attachments := event['metadata'].get('attachments')
    ):
        for data in attachments.values():
            if 'vnd.opentestfactory' in data.get('type', '') and 'xml' in data.get(
                'type', ''
            ):
                ATTACHMENTS_NOTIFICATIONS[workflow_id][data['uuid']] = {
                    'handled': False,
                    'timestamp': time.time(),
                }

    if kind == 'Notification':
        _handle_notification(event, workflow_id)
    elif kind == 'ExecutionResult' and event['metadata']['step_sequence_id'] == -2:
        events = WORKFLOWS_LIVE_EVENT_CACHES[workflow_id]
        LIVE_DATASOURCE_CACHES[workflow_id][JOBS] = get_jobs(events)
        LIVE_DATASOURCE_CACHES[workflow_id][TAGS] = get_tags(events)


########################################################################


def _validate(_, old):
    if MAX_RETENTION_PERIOD and (old == 0 or old > MAX_RETENTION_PERIOD.seconds // 60):
        return (
            MAX_RETENTION_PERIOD.seconds // 60,
            'this value exceeds the maximum workflow retention period',
        )
    return old, None


def _check_workflow_namespace(workflow_id: str) -> Optional[Any]:
    if workflow_id not in WORKFLOWS_STATUSES:
        return make_status_response('NotFound', f'Workflow {workflow_id} not found.')
    ns = WORKFLOWS_NAMESPACES[workflow_id]
    if not can_use_namespace(ns, resource='workflows', verb='get'):
        return make_status_response(
            'Forbidden', f'Token not allowed to access workflows in namespace {ns}.'
        )
    return None


def _make_links(uri: str, extra: str, page: int, last: int, per_page: int) -> List[str]:
    if url := os.environ.get(
        'OPENTF_OBSERVER_BASE_URL', os.environ.get('OPENTF_BASE_URL')
    ):
        base_url = url.rstrip('/') + uri
    else:
        base_url = request.base_url
    links = [
        f'<{base_url}?page=1&{per_page=}{extra}>; rel="first"',
        f'<{base_url}?page={last}&{per_page=}{extra}>; rel="last"',
    ]
    if page > 1:
        links.append(f'<{base_url}?page={page-1}&{per_page=}{extra}>; rel="prev"')
    if page < last:
        links.append(f'<{base_url}?page={page+1}&{per_page=}{extra}>; rel="next"')
    return links


def _make_workflow_response(workflow_id: str, msg: str, details: Dict[str, Any]):
    """Shortcut for workflow manifest status queries."""
    uri = f'/workflows/{workflow_id}/status'
    selectors = '&fieldSelector=kind=Workflow'

    details['items'] = [WORKFLOWS[workflow_id]]
    return annotate_response(
        make_status_response('OK', message=msg, details=details),
        links=_make_links(uri, selectors, 1, 1, 1),
        processed=['page', 'per_page', 'fieldSelector', 'labelSelector'],
    )


def _make_paginated_response(
    itemsgenerator,
    *args,
    uri: str,
    msg: str = '',
    details: Optional[Dict[str, Any]] = None,
):
    page = request.args.get('page', default=1, type=int)
    per_page = min(
        request.args.get('per_page', default=DEFAULT_PER_PAGE, type=int),
        MAX_PER_PAGE,
    )

    try:
        items, last, selectors = itemsgenerator(*args, page, per_page)
    except ValueError as err:
        raise ValueError(str(err)) from err

    details = details or {}
    details['items'] = items
    return annotate_response(
        make_status_response('OK', message=msg, details=details),
        links=_make_links(uri, selectors, page, last, per_page),
        processed=['page', 'per_page', 'fieldSelector', 'labelSelector'],
    )


########################################################################
## Main

app = make_app(name='observer', description='Create and start an observer service.')
if proxy := os.environ.get('OPENTF_REVERSEPROXY'):
    _maybe_set_proxyfix(proxy)

MAX_RETENTION_WORKFLOW_COUNT = get_context_parameter(
    app, 'max_retention_workflow_count'
)
MAX_RETENTION_PERIOD = timedelta(
    minutes=get_context_parameter(app, 'max_retention_period_minutes')
)
RETENTION_PERIOD = timedelta(
    minutes=get_context_parameter(app, 'retention_period_minutes', validator=_validate)
)


@app.route('/version', methods=['GET'])
def get_version():
    """Get BOM used to build Docker image.

    # Returned value

    A _status manifest_.  `details.items` is a possibly empty BOM.

    A BOM (Bill of materials) is a JSON file that is used by
    OpenTestFactory build infrastructure to generate Docker images.

    If the BOM is unavailable or unreadable, the `details.item` will be
    a dictionary with an `error` entry describing the problem.

    A regular BOM can contain the following fields:

    - name: (a string), the name of the image.
    - base-images: a possibly empty list of dictionaries describing the
      base images.
    - internal-components: a possibly empty list of dictionaries
      describing the internal components added to the image.
    - external-components: a possibly empty list of dictionaries
      describing the external components added to the image.

    The dictionaries describing the base images, internal components,
    and external components have the following fields:

    - name: (a string), the name of the item.
    - version: (a string), the component's version.
    - env_version_key (a string): an environment variable name used to
      hold the component's version.
    - type (a string): the component's technology.

    It may contain additional entries.
    """
    return make_status_response('OK', 'BOM', details={'items': _get_bom()})


@app.route('/channelhandlers', methods=['GET'])
@authorizer(resource='channelhandlers', verb='list')
def list_channelhandlers():
    """Get known channel handlers.

    # Returned value

    A _status manifest_.  `details.items` is a possibly empty list of
    _channel handlers_.

    Each channel handler has the following structure:

    ```yaml
    apiVersion: opentestfactory.org/v1alpha1
    kind: ChannelHandler
    metadata:
      name: foo
      namespace: bar
      channelhandler_id: uuid
    spec:
    status:
        lastCommunicationTimestamp: atimestamp
    ```
    """
    return make_status_response(
        'OK', 'Known channel handlers', details={'items': list(CHANNELS)}
    )


@app.route('/channels', methods=['GET'])
@authorizer(resource='channels', verb='list')
def list_channels():
    """Get most recently known channels.

    # Returned value

    A _status manifest_.  `details.items` is a possibly empty list of
    _channels_.

    Each channel has the following structure:

    ```yaml
    apiVersion: opentestfactory.org/v1alpha1
    kind: Channel
    metadata:
      name: foo
      namespaces: bar[,baz]
      channelhandler_id: uuid
    spec:
      tags: [a, b, c]
    status:
      lastCommunicationTimestamp: a timestamp
      phase: 'IDLE' or 'BUSY' or 'PENDING' or 'UNREACHABLE'
      currentJobID: uuid or null
    ```
    """
    try:
        fieldselector, labelselector = prepare_selectors(request.args)
    except ValueError as err:
        return make_status_response('Invalid', str(err))

    items = [
        c
        for cs in CHANNELS.values()
        for c in cs
        if any(
            ns == '*' or can_use_namespace(ns, resource='channels', verb='list')
            for ns in c['metadata']['namespaces'].split(',')
        )
        and match_selectors(c, fieldselector, labelselector)
    ]
    accessible = list_accessible_namespaces(resource='channels', verb='list')
    if '*' not in accessible:
        for item in items:
            if '*' in item['metadata']['namespaces']:
                item['metadata']['namespaces'] = ','.join(accessible)
            else:
                item['metadata']['namespaces'] = ','.join(
                    ns
                    for ns in item['metadata']['namespaces'].split(',')
                    if ns in accessible
                )
    return annotate_response(
        make_status_response('OK', 'Known channels', details={'items': items}),
        processed=['fieldSelector', 'labelSelector'],
    )


@app.route('/namespaces', methods=['GET'])
@authorizer(resource='namespaces', verb='list')
def list_namespaces():
    """Get accessible namespaces.

    The `/namespaces` endpoints allow for two optional query parameters:

    - resource: a string
    - verb: a string

    They must either be both provided or not provided at all.

    # Returned value

    A _status manifest_.  `.details.items` is a possibly empty list of
    namespaces (strings) if status is `OK`.

    Status is `Invalid` if only one of the optional parameters is
    provided.
    """
    try:
        resource = request.args.get('resource', type=str)
        verb = request.args.get('verb', type=str)
    except ValueError:
        return make_status_response(
            'Invalid', 'resource and verb, if provided, must be strings.'
        )

    if resource and not verb or verb and not resource:
        return make_status_response(
            'Invalid', 'resource and verb must be both provided or not provided at all.'
        )

    return annotate_response(
        make_status_response(
            'OK',
            'Accessible namespaces',
            details={'items': list_accessible_namespaces(resource, verb)},
        ),
        processed=['resource', 'verb'],
    )


@app.route('/workflows', methods=['GET'])
@authorizer(resource='workflows', verb='list')
def list_workflows():
    """Get running and recently completed workflows.

    # Returned value

    A _status manifest_.  `.details.items` is a possibly empty list of
    workflow IDs.
    """
    manifest = request.args.get('expand', type=str) == 'manifest'
    try:
        fieldselector, labelselector = prepare_selectors(request.args)
    except ValueError as err:
        return make_status_response('Invalid', str(err))

    items = {
        workflow_id: body
        for workflow_id, namespace in WORKFLOWS_NAMESPACES.items()
        if can_use_namespace(namespace, resource='workflows', verb='list')
        and match_selectors(
            body := WORKFLOWS.get(workflow_id, {}), fieldselector, labelselector
        )
    }
    return annotate_response(
        make_status_response(
            'OK',
            'Running and recent workflows',
            details={'items': items if manifest else list(items)},
        ),
        processed=['expand', 'fieldSelector', 'labelSelector'],
    )


@app.route('/workflows/<workflow_id>/logs', methods=['GET'])
@authorizer(resource='workflows', verb='get')
def get_workflow_logs(workflow_id):
    """Get workflow log."""
    if ns_error := _check_workflow_namespace(workflow_id):
        return ns_error

    return send_file(WORKFLOWS_STATUS[workflow_id].get_executionlog())


@app.route('/workflows/<workflow_id>/status', methods=['GET'])
@authorizer(resource='workflows', verb='get')
def get_workflow_status(workflow_id):
    """Get workflow status.

    Pagination is per <https://datatracker.ietf.org/doc/html/rfc8288>

    # Required parameters

    - workflow_id: a string, the workflow UUID

    # Optional request parameters

    - page: an integer (1-based, 1 by default)
    - per_page: an integer (100 by default, 1000 max)

    # Returned value

    A _status manifest_.  `.details.status` contains an API-readable
    status of the workflow: a string, one of `PENDING`, `RUNNING`,
    `DONE`, or `FAILED`.
    """
    if ns_error := _check_workflow_namespace(workflow_id):
        return ns_error

    summary = WORKFLOWS_STATUSES[workflow_id]
    if summary == 'DONE':
        msg = 'Workflow completed'
    elif summary == 'FAILED':
        msg = 'Workflow canceled'
    else:
        msg = WORKFLOW_IN_PROGRESS

    try:
        details = {'status': summary}
        if request.args.get(
            'fieldSelector'
        ) == 'kind=Workflow' and not request.args.get('labelSelector'):
            return _make_workflow_response(workflow_id, msg, details=details)
        return _make_paginated_response(
            _get_workflow_items,
            workflow_id,
            uri=f'/workflows/{workflow_id}/status',
            msg=msg,
            details=details,
        )
    except ValueError as err:
        return make_status_response('Invalid', str(err))


@app.route('/workflows/status', methods=['GET'])
@authorizer(resource='status', verb='get')
def get_orchestrator_status():
    """Get orchestrator status.

    # Returned value

    A _status manifest_.  `.details.status` contains an API-readable
    status of the workflows: a string, one of `IDLE` or `BUSY`.
    """
    running = {
        workflow_id
        for workflow_id, status in WORKFLOWS_STATUSES.items()
        if status == 'RUNNING'
    }
    for workflow_id, workers in WORKFLOWS_WORKERS.items():
        if workers:
            running.add(workflow_id)

    return make_status_response(
        'OK',
        (
            f'{len(running)} workflows in progress'
            if running
            else 'No workflow in progress'
        ),
        details={'items': list(running), 'status': 'BUSY' if running else 'IDLE'},
    )


@app.route('/workflows/<workflow_id>/workers', methods=['GET'])
@authorizer(resource='workflows', verb='get')
def get_workflow_workers(workflow_id):
    """Get workflow workers.

    # Required parameters

    - workflow_id: an UUID

    # Returned value

    A _status manifest_.  `.details.status` contains an API-readable
    status of the workflow workers: a string, one of `BUSY` or
    `IDLE`.
    """
    if workflow_id not in WORKFLOWS_WORKERS:
        return make_status_response('NotFound', f'Workflow {workflow_id} not found.')
    if not can_use_namespace(
        WORKFLOWS_NAMESPACES[workflow_id], resource='workflows', verb='get'
    ):
        return make_status_response(
            'Forbidden',
            f'Token not allowed to access workflows in namespace {WORKFLOWS_NAMESPACES[workflow_id]}.',
        )

    workers = WORKFLOWS_WORKERS[workflow_id]
    return make_status_response(
        'OK',
        f'{len(workers)} workers active on workflow',
        details={'items': list(workers), 'status': 'BUSY' if workers else 'IDLE'},
    )


@app.route('/workflows/<workflow_id>/datasources/<datasource_kind>', methods=['GET'])
@authorizer(resource='workflows', verb='get')
def get_datasource(workflow_id, datasource_kind):
    """Get datasource.

    # Required parameters

    - workflow_id: an UUID
    - datasource_kind: a string (one of `testcases`, `jobs`, or `tags`)

    # Optional request parameters

    - page: an integer (1-based, 1 by default)
    - per_page: an integer (1000 max, 100 by default)
    - scope: a filter expression (string)

    # Returned value

    A _status manifest_. `.details.items` is a possibly empty list
    of datasource items.
    """
    if ns_error := _check_workflow_namespace(workflow_id):
        return ns_error

    if datasource_kind not in DATASOURCE_KINDS:
        return make_status_response(
            'NotFound',
            f'Datasource kind `{datasource_kind}` does not exist, was expecting {", ".join(DATASOURCE_KINDS)}.',
        )

    status = summary = WORKFLOWS_STATUSES[workflow_id]
    workers_count = 0
    handled = all(
        attachment['handled']
        for attachment in ATTACHMENTS_NOTIFICATIONS[workflow_id].values()
    )
    if not handled and summary != 'RUNNING':
        not_handled_ts = max(
            [
                attachment['timestamp']
                for attachment in ATTACHMENTS_NOTIFICATIONS[workflow_id].values()
                if not attachment['handled']
            ]
        )
        # TODO: TO value to review
        handled = time.time() - not_handled_ts > 20

    try:
        has_workers = (
            workflow_id in WORKFLOWS_WORKERS and WORKFLOWS_WORKERS[workflow_id]
        )
        if summary == 'RUNNING' or (summary == 'DONE' and has_workers):
            status = 'ONGOING'
            workers_count = len(WORKFLOWS_WORKERS.get(workflow_id, {}))
        else:
            status = 'COMPLETE' if summary == 'DONE' else 'INTERRUPTED'

        response = _make_paginated_response(
            _get_datasource_items,
            workflow_id,
            datasource_kind,
            uri=f'/workflows/{workflow_id}/datasources/{datasource_kind}',
            details={
                'status': status,
                'workers_count': workers_count,
                'handled': handled,
            },
        )
    except CacheNotReady as msg:
        response = make_status_response(
            'Accepted', str(msg), details={'status': status}
        )
    except DataSourceScopeError as err:
        response = make_status_response(
            'Invalid',
            str(err),
            details={'scope_error': 'Scope error occured.'},
            silent=True,
        )
    except DataSourceDataError as err:
        response = make_status_response(
            'Invalid',
            str(err),
            details={'data_error': 'Data error occured.'},
            silent=True,
        )
    except ValueError as err:
        response = make_status_response(
            'Invalid',
            str(err),
            details={'exception_error': 'Exception occured.'},
            silent=True,
        )
    return response


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication.

    It may be of any kind.

    # Returned value

    A _status manifest_.
    """
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict) or 'kind' not in body or 'apiVersion' not in body:
        return make_status_response(
            'Invalid', 'Not an object or no kind or no apiVersion specified.'
        )

    if not body.get('metadata', {}).get('workflow_id'):
        return make_status_response('Invalid', 'No workflow_id specified.')

    schema = f'{body["apiVersion"]}/{body["kind"]}'
    valid, extra = validate_schema(schema, body)
    if not valid:
        return make_status_response('BadRequest', f'Not a valid {schema}: {extra}')

    if schema == NOTIFICATION and body.get('spec', {}).get('channel.handler'):
        refresh_channels(**body['spec']['channel.handler'])
    else:
        workflow_id = body['metadata']['workflow_id']
        PENDINGEVENT_QUEUE.put((schema, workflow_id, body))

    return make_status_response('OK', '')


def main():
    """Start the Observer service."""
    if os.environ.get('OBSERVER_RETENTION_POLICY'):
        watch_file(
            app,
            os.environ['OBSERVER_RETENTION_POLICY'],
            _read_rp_definition,
        )

    try:
        debug('Starting event recording thread.')
        threading.Thread(target=record_event, daemon=True).start()
    except Exception as err:
        fatal('Could not start event recording thread: %s.', str(err))

    try:
        debug(f'Subscribing to {", ".join(SUBSCRIPTIONS)}.')
        uuids = [subscribe(kind=s, target='inbox', app=app) for s in SUBSCRIPTIONS]
    except Exception as err:
        fatal('Could not subscribe to events: %s.', str(err))

    try:
        debug('Starting service.')
        run_app(app)
    finally:
        for uuid in uuids:
            unsubscribe(uuid, app=app)


if __name__ == '__main__':
    main()
