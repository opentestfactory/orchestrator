# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A receptionist service.

Supports the following three query parameters:

- `ping`: return a `Pong!` message.
- `dryRun`: do not publish the workflow.
- `namespace`: specify the namespace to use.
"""

from typing import Any, Dict

from datetime import datetime, timezone

from flask import request

import yaml

from opentf.commons import (
    make_status_response,
    annotate_response,
    publish,
    validate_schema,
    WORKFLOW,
    DEFAULT_NAMESPACE,
    make_app,
    run_app,
    validate_pipeline,
    validate_inputs,
    make_uuid,
    authorizer,
    can_use_namespace,
)
from opentf.commons.exceptions import (
    ServiceError,
    ConflictError,
    InvalidRequestError,
    UnauthorizedError,
)

type Workflow = Dict[str, Any]

########################################################################
## Constants

IN_FILENAME_TEMPLATE = '/tmp/in_{uuid}_{name}'


########################################################################
## Multipart/form-data Helpers
##
## curl -X POST -F workflow=@Workflow -F log=XX=YY http://localhost:5000/demo


def _list_duplicates():
    """Return the set of duplicates fields/files in request."""
    return {field for field in request.form if request.files.get(field)}


def _maybe_fill_inputs(workflow: Workflow) -> None:
    """Set inputs, if any."""
    if data := request.form.get('inputs'):
        inputs = data.splitlines()
    elif data := request.files.get('inputs'):
        inputs = str(data.read(), 'UTF-8').splitlines()
    else:
        inputs = {}

    inputs = {entry.split('=', 1)[0]: entry.split('=', 1)[1] for entry in inputs}
    declaration: dict[str, Any] | None = workflow.get('inputs')
    if declaration is None:
        if inputs:
            raise InvalidRequestError('Inputs provided but no inputs in workflow.')
        return

    try:
        validate_inputs(declaration, inputs)
    except ValueError as err:
        raise InvalidRequestError(str(err))

    workflow['metadata']['annotations']['opentestfactory.org/inputs'] = inputs


def _maybe_adjust_variables(workflow: Workflow) -> None:
    """Prepend variables, if any."""
    if data := request.form.get('variables'):
        variables = data.splitlines()
    elif data := request.files.get('variables'):
        variables = str(data.read(), 'UTF-8').splitlines()
    else:
        variables = []
    if variables:
        workflow.setdefault('variables', {})
    for entry in variables:
        lhs, _, rhs = entry.partition('=')
        if lhs:
            workflow['variables'][lhs] = rhs


def _ensure_files_attached(workflow: Workflow) -> bool:
    """Ensure required files are attached."""
    expected = workflow.get('resources', {}).get('files', [])
    for idx, name in enumerate(expected):
        if not isinstance(name, str):
            continue
        if not request.files.get(name):
            app.logger.warning('Missing required resource file "%s".', name)
            return False
        filename = IN_FILENAME_TEMPLATE.format(
            uuid=workflow['metadata']['workflow_id'], name=name
        )
        if 'dryRun' not in request.args:
            with open(filename, 'wb') as file:
                file.write(request.files[name].read())
        expected[idx] = {'name': name, 'url': filename}
    return True


def _fill_workflow_metadata(workflow: Workflow, ordering: list[list[str]]) -> None:
    """Set or update workflow metadata."""
    workflow['apiVersion'], workflow['kind'] = WORKFLOW.rsplit('/', maxsplit=1)
    metadata = workflow.setdefault('metadata', {})
    annotations = metadata.get('annotations', {})
    annotations['opentestfactory.org/ordering'] = ordering
    metadata.setdefault('namespace', DEFAULT_NAMESPACE)
    metadata['annotations'] = annotations
    metadata['creationTimestamp'] = datetime.now(timezone.utc).isoformat()
    metadata['workflow_id'] = make_uuid()
    if 'name' not in metadata:
        metadata['name'] = workflow['name']

    if namespace := request.args.get('namespace', type=str):
        metadata['namespace'] = namespace
    if not can_use_namespace(
        metadata['namespace'], resource='workflows', verb='create'
    ):
        raise UnauthorizedError(
            f'Token not allowed to run workflows in namespace "{metadata['namespace']}".',
        )

    for job in workflow['jobs'].values():
        for step in job.get('steps', []):
            step.setdefault('id', make_uuid())


def _safe_load(data) -> Workflow:
    """Load YAML data."""
    try:
        return yaml.safe_load(data)
    except Exception as err:
        raise InvalidRequestError(f'Could not parse YAML workflow: {err}')


def _get_workflow() -> Workflow:
    """Attempt to extract a workflow from the request.

    # Raised exceptions

    An _InvalidRequestError_ exception is raised if the request is not
    a known format or if the workflow is not an object.
    """
    if request.is_json:
        try:
            return request.get_json() or {}
        except Exception as err:
            raise InvalidRequestError(f'Could not parse workflow: {err}.')

    if request.mimetype in ('application/x-yaml', 'text/yaml'):
        if request.content_length and request.content_length < 1000000:
            return _safe_load(request.data)
        raise InvalidRequestError(
            'Content-size must be specified and less than 1000000 for YAML workflows.'
        )

    if request.mimetype != 'multipart/form-data':
        raise InvalidRequestError('Not a known format, expecting JSON or YAML.')

    if duplicates := _list_duplicates():
        raise InvalidRequestError(
            f'Entries provided as a file and a field: {duplicates}.'
        )
    if 'workflow' in request.form:
        return _safe_load(request.form['workflow'])
    if 'workflow' in request.files:
        return _safe_load(request.files['workflow'].read())

    raise InvalidRequestError('No workflow specified.')


def _validate_workflow(workflow: Workflow) -> None:
    """Validate and fill workflow metadata.

    # Raised exceptions

    An _InvalidRequestError_ exception is raised if the workflow is not
    a valid workflow.
    """
    valid, extra = validate_schema(WORKFLOW, workflow)
    if not valid:
        raise InvalidRequestError(
            'Not a valid workflow manifest.', details={'error': str(extra)}
        )
    valid, extra = validate_pipeline(workflow)
    if not valid:
        raise InvalidRequestError(
            'Not a valid workflow manifest.', details={'error': str(extra)}
        )

    _fill_workflow_metadata(workflow, extra)
    _maybe_fill_inputs(workflow)

    if request.mimetype == 'multipart/form-data':
        _maybe_adjust_variables(workflow)
        if not _ensure_files_attached(workflow):
            missing = {
                name
                for name in workflow.get('resources', {}).get('files', [])
                if isinstance(name, str) and not request.files.get(name)
            }
            raise InvalidRequestError(
                f'Not all expected files were attached: {missing}.'
            )
    elif workflow.get('resources', {}).get('files', []):
        raise InvalidRequestError('Expecting files, must use "multipart/form-data".')


########################################################################
## Main

app = make_app(
    name='receptionist',
    description='Create and start a receptionist service.',
)


@app.route('/workflows', methods=['POST'])
@authorizer(resource='workflows', verb='create')
def handle_workflow():
    """Register a new workflow.

    # Workflow format

    Please refer to `schemas/opentestfactory.org/v1/Workflow.json'.

    Allowed content types are JSON and YAML or multipart/form-data
    documents:

    - application/json
    - application/*+json
    - application/x-yaml
    - text/yaml
    - multipart/form-data

    Content length must be specified for YAML documents, and must be
    less than 1000000 bytes long.

    # Returned value

    A _status manifest_. If the workflow is valid (`status` is
    `'Success'`), `details.workflow_id` is the workflow ID.
    """
    if 'ping' in request.args:
        return annotate_response(
            make_status_response('OK', 'Pong!'), processed=['ping']
        )

    try:
        workflow = _get_workflow()
        _validate_workflow(workflow)

        if 'dryRun' not in request.args:
            resp = publish(workflow, context=app.config['CONTEXT'])
            if resp.status_code != 200:
                raise ConflictError(
                    'Could not publish workflow.', details={'error': resp.json()}
                )
        workflow_id = workflow['metadata']['workflow_id']
        msg = f'Workflow {workflow["metadata"]["name"]} accepted (workflow_id={workflow_id}).'
        app.logger.info(msg)
        resp = make_status_response(
            'Created', msg, details={'workflow_id': workflow_id}
        )
    except ServiceError as err:
        resp = make_status_response(err.http_status_name, err.msg, details=err.details)

    return annotate_response(resp, processed=['dryRun', 'namespace'])


def main(svc):
    """Start the Receptionist service."""
    app.logger.debug('Starting service.')
    run_app(svc)


if __name__ == '__main__':
    main(app)
