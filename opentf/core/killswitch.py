# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A killswitch service.

A killswitch service can be used to cancel a currently executing
_workflow_.

Due to the asynchronous nature of workflow executions, some actions may
still occur on execution environments and SUT when a workflow is
canceled.
"""

from typing import Any, Dict, List, Optional

from flask import request

import requests

from opentf.commons import (
    make_status_response,
    annotate_response,
    publish,
    WORKFLOWCANCELLATION,
    make_app,
    run_app,
    get_context_service,
    authorizer,
    can_use_namespace,
    make_event,
    is_uuid,
)


########################################################################


def find_workflow(workflow_id: str, token: Optional[str]):
    observer = get_context_service(app, 'observer')
    url = f"{observer['endpoint']}/workflows/{workflow_id}/status"
    headers = {"Authorization": token} if token else None
    return requests.get(url, headers=headers)


def get_namespace(events: List[Dict[str, Any]]) -> str:
    """Find namespace from events.

    # Returned value

    A string.

    # Raised exceptions

    A _ValueError_ exception is raised if no namespace can be found in
    the list of events.
    """
    for event in events:
        if namespace := event.get('metadata', {}).get('namespace'):
            return namespace
    raise ValueError('Missing metadata in events.')


def maybe_delete_workflow(workflow_id: str, response):
    """Cancel workflow if allowed."""
    try:
        body = response.json()
        namespace = get_namespace(body.get('details', {}).get('items', []))
        if not can_use_namespace(namespace, resource='workflows', verb='delete'):
            return annotate_response(
                make_status_response(
                    'Forbidden',
                    f'Token not allowed to delete workflows in namespace {namespace}.',
                ),
                processed=['dryRun'],
            )
        event = make_event(
            WORKFLOWCANCELLATION,
            metadata={'name': 'User cancellation', 'workflow_id': workflow_id},
            details={},
        )
        if 'source' in request.args:
            event['details']['source'] = request.args.get('source', type=str)
        if 'reason' in request.args:
            event['details']['reason'] = request.args.get('reason', type=str)
        if not event['details']:
            del event['details']
        if 'dryRun' not in request.args:
            publish(event, context=app.config['CONTEXT'])
        return annotate_response(
            make_status_response('OK', f'Workflow {workflow_id} canceled.'),
            processed=['dryRun', 'source', 'reason'],
        )
    except Exception as err:
        return annotate_response(
            make_status_response(
                'InternalError', f'Unexpected response from observer: {err}.'
            ),
            processed=['dryRun', 'source', 'reason'],
        )


########################################################################
## Main

app = make_app(
    name='killswitch',
    description='Create and start a killswitch service.',
)


@app.route('/workflows/<workflow_id>', methods=['DELETE'])
@authorizer(resource='workflows', verb='delete')
def cancel_workflow(workflow_id: str):
    """Cancel workflow."""
    if not is_uuid(workflow_id):
        return make_status_response('Invalid', 'Not a valid UUID.')

    token = request.headers.get('Authorization')
    response = find_workflow(workflow_id, token)
    status_code = response.status_code

    if status_code == 200:
        return maybe_delete_workflow(workflow_id, response)
    if status_code == 404:
        return make_status_response('NotFound', f'Workflow {workflow_id} not found.')
    if status_code == 403:
        return make_status_response(
            'Forbidden', f'Token does not allow access to workflow {workflow_id}.'
        )
    if status_code == 401:
        return make_status_response('Unauthorized', 'Invalid or missing token.')

    return make_status_response(
        'InternalError', f'Unexpected observer response {status_code}.'
    )


def main():
    """Start the killswitch service."""
    _ = get_context_service(app, 'observer')

    app.logger.debug('Starting service.')
    run_app(app)


if __name__ == '__main__':
    main()
