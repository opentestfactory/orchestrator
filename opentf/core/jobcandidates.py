# Copyright (c) Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Any, Dict, Optional, Set

from toposort import toposort

CANDIDATE_RUNNING = 'RUNNING'
CANDIDATE_WAITING = 'WAITING'  # (sub jobs not all done)
CANDIDATE_DONE = 'DONE'
CANDIDATE_SKIPPED = 'SKIPPED'  # the job's conditional is false


class JobCandidates:
    """..."""

    def __init__(self, jobs):
        self.jobs = jobs
        self._compute_candidates()
        if not self.candidates:
            raise ValueError('Invalid workflow')

    def update_job_status(self, job_name: str, status: str) -> None:
        """Update job status, recursively if appropriate.

        If a sub-job is done, and if all its siblings are done too,
        mark the parent as done.  Recurse if appropriate.
        """
        if job_name in self.jobs and status in (
            CANDIDATE_DONE,
            CANDIDATE_SKIPPED,
            CANDIDATE_RUNNING,
            CANDIDATE_WAITING,
        ):
            self.jobs[job_name]['status'] = status
        if status in (CANDIDATE_DONE, CANDIDATE_SKIPPED):
            parent, _, child = job_name.rpartition(' ')
            if child:
                active_siblings = [
                    job_name
                    for job_name, job_def in self.jobs.items()
                    if job_name.startswith(f'{parent} ')
                    and job_def.get('status') not in (CANDIDATE_DONE, CANDIDATE_SKIPPED)
                ]
                if not active_siblings:
                    self.update_job_status(parent, CANDIDATE_DONE)
            self._compute_candidates()

    def expand_job(self, parent_name: str, extra_jobs: Dict[str, Any]) -> None:
        """Prepare child jobs.

        Extra jobs are added to the workflow.
        """
        if parent_name in self.jobs:
            self.update_job_status(parent_name, CANDIDATE_WAITING)
            for job_name, job_def in extra_jobs.items():
                self.jobs[f'{parent_name} {job_name}'] = job_def
            self._compute_candidates()

    def get_next_candidates(self) -> Optional[Set[str]]:
        if self.candidates:
            return {
                candidate
                for candidate in self.candidates[0]
                if self.jobs[candidate].get('status')
                not in (CANDIDATE_RUNNING, CANDIDATE_WAITING, CANDIDATE_SKIPPED)
            }
        return None

    def no_more_jobs(self) -> bool:
        return all(
            job_def.get('status') in (CANDIDATE_DONE, CANDIDATE_SKIPPED)
            for job_def in self.jobs.values()
        )

    def _compute_candidates(self) -> None:
        """Get job candidates.

        A job candidate is a job that is ready to run.

        # Return values

        None or a (possibly empty) list of sets of strings (jobs names
        as found in workflow manifest).
        """
        try:
            done = {
                job_name
                for job_name, job_def in self.jobs.items()
                if job_def.get('status') in (CANDIDATE_DONE, CANDIDATE_SKIPPED)
            }
            candidates = {
                job_name: job_def['needs'] - done
                for job_name, job_def in self.jobs.items()
                if job_name not in done
            }
            self.candidates = list(toposort(candidates))
        except Exception:
            self.candidates = None
