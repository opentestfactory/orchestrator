# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A dummy generator plugin.

# Usage

```
python3 -m opentf.plugins.dummygenerator.main [--context context] [--config configfile]
```
"""

from opentf.toolkit import make_plugin, run_plugin

from .implementation import generate

plugin = make_plugin(
    name='dummygenerator', description='A dummy generator.', generator=generate
)

if __name__ == '__main__':
    run_plugin(plugin)
