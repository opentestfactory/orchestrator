# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the cypress provider.

The following categories are provided:

- execute
- params
- cypress
"""


from opentf.toolkit import core
from opentf.commons import make_uuid

########################################################################
## Constants

EXECUTE_CATEGORY = 'cypress/execute'
PARAM_CATEGORY = 'cypress/params'
CYPRESS_CATEGORY = 'cypress/cypress'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'

REPORT_TYPE = 'application/vnd.opentestfactory.cypress-surefire+xml'


########################################################################
## 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    ## Test Reference format

    The test reference format used by `cypress/execute@v1` is as follows:

    If the Cypress project is situated at the root of the source code repository:

    - `{project}#[{spec_file}]`

    If the Cypress project is not situated at the root of the source code repository:

    - `{project}/[{cypress_project_directory}]#[{spec_file}]`

    Old versions of the orchestrator only supported the following test reference syntax:

    - `{project}/[{spec_file}]`

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: cypress/execute@v1
      with:
        test: cypressProject/custom/path#cypress/test.spec.js
    ```
    """
    test_reference = inputs['test']

    # convert old reference format to new reference format
    if '#' not in test_reference:
        parts = test_reference.partition('/')
        test_reference = parts[0] + '#' + parts[2]

    lhs, _, spec_file = test_reference.partition('#')

    repository_root, _, cypress_project_directory = lhs.partition('/')
    project_directory = f'./{cypress_project_directory}'.strip('/')  # '.' or './x/y'
    spec_file_path = f'{project_directory}/{spec_file}'
    spec_file_name = spec_file.rsplit('/')[-1].partition('.')[0]
    test_report_path = f'target/surefire-reports/{spec_file_name}-report.xml'
    screens_folder_name = make_uuid()
    screens_folder_path = f'{repository_root}/{project_directory}/{screens_folder_name}'
    test_screens_tar = f'{spec_file_name}_screenshots.tar'

    if core.runner_on_windows():
        create_screens_folder_command = f'@if not exist "{core.normalize_path(screens_folder_path)}" @mkdir "{core.normalize_path(screens_folder_path)}"'
        run_command = f'cypress run --project "{project_directory}" --spec "{spec_file_path}" --config screenshotsFolder="{screens_folder_name}" --reporter junit --reporter-options "mochaFile={test_report_path}" %CYPRESS_EXTRA_OPTIONS%'
    else:
        create_screens_folder_command = (
            f'mkdir -p "{core.normalize_path(screens_folder_path)}"'
        )
        run_command = f'cypress run --project "{project_directory}" --spec "{spec_file_path}" --config screenshotsFolder="{screens_folder_name}" --reporter junit --reporter-options "mochaFile={test_report_path}" $CYPRESS_EXTRA_OPTIONS'

    steps = [
        {
            'run': '\n'.join(
                (
                    core.export_variable(
                        'OPENTF_CYPRESS_SCREENSHOT_FOLDER', screens_folder_name
                    ),
                    core.delete_file(
                        f'{repository_root}/{project_directory}/{test_report_path}'
                    ),
                    create_screens_folder_command,
                )
            )
        },
        {
            'working-directory': repository_root,
            'run': run_command,
            'continue-on-error': True,
            'variables': {'NO_COLOR': '1'},
        },
        {
            'run': core.attach_file(
                f'{repository_root}/{project_directory}/{test_report_path}',
                type=REPORT_TYPE,
            ),
            'continue-on-error': True,
        },
        {
            'id': 'create_screenshots_tar',
            'uses': 'actions/create-archive@v1',
            'with': {
                'path': f'../{test_screens_tar}',
                'format': 'tar',
                'patterns': ['**/*'],
            },
            'working-directory': f'{screens_folder_path}',
            'continue-on-error': True,
        },
        {
            'if': "steps.create_screenshots_tar.outcome == 'success'",
            'uses': 'actions/get-file@v1',
            'with': {'path': test_screens_tar},
            'working-directory': f'{repository_root}/{project_directory}',
            'continue-on-error': True,
        },
        {
            'run': core.delete_file(
                f'{repository_root}/{project_directory}/{test_screens_tar}',
            )
        },
    ]

    return steps


########################################################################
# 'params' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: cypress/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    core.validate_params_inputs(inputs)

    global_data = inputs['data'].get('global', {})
    test_data = inputs['data'].get('test', {})
    global_data = {
        'CYPRESS_' + k: v for k, v in global_data.items() if k not in test_data
    }
    test_data = {'CYPRESS_' + k: v for k, v in test_data.items()}

    return core.export_variables(global_data, test_data, verbatim=True)


########################################################################
# 'cypress' action


def cypress_action(inputs):
    """Process 'cypress' action.

    ```yaml
    - uses: cypress/cypress@v1
      with:
        browser: chrome
        reporter: junit
        reporter-options: "mochaFile=mocha_results/test-output-[hash].xml,toConsole=true"
        headless: true
        env: profile=postgres
        config-file: cypress/config/...
        extra-options: --port 8023
    ```

    If the action is used more than once in a job, it is up to the
    caller to ensure no previous test execution results remains before
    executing a new test.

    It is also up to the caller to attach the relevant reports so that
    publishers can do their job too, by using the `actions/get-files@v1`
    action or some other means.
    """
    args = ''
    if inputs.get('headless'):
        args += ' --headless'
    if browser := inputs.get('browser'):
        args += f' --browser {browser}'
    if reporter := inputs.get('reporter'):
        args += f' --reporter {reporter}'
    if reporter_options := inputs.get('reporter-options'):
        args += f' --reporter-options {reporter_options}'
    if env := inputs.get('env'):
        args += f' --env {env}'
    if config_file := inputs.get('config-file'):
        args += f' --config-file {config_file}'
    if extra_options := inputs.get('extra-options'):
        args += f' {extra_options}'

    steps = [
        {
            'run': f'cypress run {args}',
            'continue-on-error': True,
            'variables': {'NO_COLOR': '1'},
        }
    ]

    return steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    CYPRESS_CATEGORY: cypress_action,
}
