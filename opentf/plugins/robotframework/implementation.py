# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the robotframework provider.

The following categories are provided:

- execute
- params
- robot
"""

import os
import re

from shlex import quote
from uuid import uuid4

from opentf.toolkit import core


########################################################################
## Constants

EXECUTE_CATEGORY = 'robotframework/execute'
PARAM_CATEGORY = 'robotframework/params'
ROBOT_CATEGORY = 'robotframework/robot'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'
SQUASHTM_PARAM_FILE = '_SQUASH_TF_TESTCASE_PARAM_FILES'

REPORT_TYPE = 'application/vnd.opentestfactory.robotframework-output+xml'

ROBOT_XML_REPORT = 'output.xml'
ROBOT_REPORTS = ['log.html', 'report.html']
ROBOT_EXT_SCREENSHOTS = [
    '*.png',
    '*.jpg',
    '*.jpeg',
    'browser/screenshot/*.png',
    'browser/screenshot/*.jpg',
    'browser/screenshot/*.jpeg',
]
ROBOT_REPORTS_ARCHIVE = 'RobotFramework_reports.tar'
ROBOT_ATTACH_REPORTS = [
    {
        'if': "steps.create_reports_archive.outcome == 'success'",
        'uses': 'actions/get-file@v1',
        'with': {'path': ROBOT_REPORTS_ARCHIVE},
        'continue-on-error': True,
    },
    {
        'uses': 'actions/get-file@v1',
        'with': {'path': ROBOT_XML_REPORT, 'type': REPORT_TYPE},
        'continue-on-error': True,
    },
]
ROBOT_ALLUREREPORTS = ['*-result.json', '*-attachment.html']
ROBOT_ATTACH_ALLUREREPORTS = [
    {
        'uses': 'actions/get-files@v1',
        'with': {
            'pattern': '*-result.json',
            'warn-if-not-found': 'No allure-robotframework in execution host',
        },
    },
    {
        'uses': 'actions/get-files@v1',
        'with': {
            'pattern': '*-attachment.html',
            'skip-if-not-found': 'true',
        },
    },
]

CLEAN_LIST = (
    ROBOT_REPORTS
    + [ROBOT_XML_REPORT]
    + [ROBOT_REPORTS_ARCHIVE]
    + ROBOT_ALLUREREPORTS
    + ROBOT_EXT_SCREENSHOTS
)

ROBOT_WILDCARDS = ('[', ']', '*', '?')

ALLURE_REPORTING_ENABLED = os.environ.get('OPENTF_ALLURE_ENABLED', 'no').lower() in (
    'true',
    'yes',
    'on',
)


########################################################################
## test reference formatting


def _double_backslashes(match_obj):
    """Double backslashes in the match object"""
    if matches := match_obj.group():
        return matches.replace('\\', '\\\\')


def format_testcase_name(testcase: str) -> str:
    """Format testcase name.

    First, escape wildcards in testcase name : '[]*?':

        abc[def] -> abc[[]def[]]

    Then, depending on environment:

    On Windows, escaping " and % and putting name into double quotes.

        Test "a%b" -> "Test ""a%%b""

    Also escaping \\ at the end of test reference or before ".

        Test "a\"b c\\ -> "Test ""a\""b c\\\\"

    On Linux, putting name into single quotes to prevent bash
    interpreting.

        Test "a%b" -> 'Test "a%b"'
    """
    formatted = [f'[{c}]' if c in ROBOT_WILDCARDS else c for c in testcase]
    testcase = ''.join(formatted)

    if not core.runner_on_windows():
        return quote(testcase)

    testcase = re.sub(r'(\\+\")|(\\+$)', _double_backslashes, testcase)
    if '"' in testcase:
        testcase = testcase.replace('"', '""')
    if '%' in testcase:
        testcase = testcase.replace('%', '%%')
    return f'"{testcase}"'


########################################################################
## 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: robotframework/execute@v1
      with:
        test: foobar
    ```

    `test` is of the form `{datasource}[#{testcase}]`.
    """
    test_reference = inputs['test']

    parts = test_reference.partition('#')
    datasource = parts[0]
    if testcase := parts[2].rstrip():
        testcase_arg = f'--test {format_testcase_name(testcase)} '
    else:
        testcase_arg = ''

    robot_screenshots = []
    for item in ROBOT_EXT_SCREENSHOTS:
        robot_screenshots.append(core.normalize_path(item))

    if ALLURE_REPORTING_ENABLED:
        robot_run_tail = f'--nostatusrc --listener "allure_robotframework;." {testcase_arg}"{datasource}"'
    else:
        robot_run_tail = f'--nostatusrc {testcase_arg}"{datasource}"'
    if core.runner_on_windows():
        run_test = {
            'run': f'robot %ROBOTFRAMEWORK_EXTRA_OPTIONS% {robot_run_tail}',
            'continue-on-error': True,
        }
    else:
        run_test = {
            'run': f'robot $ROBOTFRAMEWORK_EXTRA_OPTIONS {robot_run_tail}',
            'continue-on-error': True,
        }

    steps = [
        {'run': '\n'.join(core.delete_file(f) for f in CLEAN_LIST)},
        run_test,
        {
            'id': 'create_reports_archive',
            'uses': 'actions/create-archive@v1',
            'with': {
                'path': ROBOT_REPORTS_ARCHIVE,
                'format': 'tar',
                'patterns': ROBOT_REPORTS + robot_screenshots,
            },
            'continue-on-error': True,
        },
        *ROBOT_ATTACH_REPORTS,
    ]

    if ALLURE_REPORTING_ENABLED:
        steps.extend(ROBOT_ATTACH_ALLUREREPORTS)

    return steps


########################################################################
# 'params' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: robotframework/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    core.validate_params_inputs(inputs)

    path = f'{uuid4()}.ini'
    data = inputs['data']
    steps = [
        {
            'uses': 'actions/create-file@v1',
            'with': {'data': data, 'path': path, 'format': 'ini'},
        },
        {'run': core.export_variable(SQUASHTM_PARAM_FILE, path)},
    ]

    return steps


########################################################################
# 'robot' action


def robot_action(inputs):
    """Process 'robot' action.

    Run a Robot Framework test suite.

    `robot` actions have a mandatory `datasource` input.

    They also have three optional inputs:

    - `test`, which specify a test case present in the script given in
      the datasource.  By default, all test cases in the datasource
      are executed.
    - `reports-for-allure`, a boolean, that enables the generation of
      Allure reports.  By default, Allure reports are not generated.
    - `extra-options`, which allows to specify additional parameters.

    If you want to generate Allure reports, the `allure-robotframework`
    Python library must be installed in the execution environment.

    ## Examples

    This first example runs all tests in the `foobar` test suite:

    ```yaml
    - uses: robotframework/robot@v1
      with:
        datasource: foobar
    ```

    This second example runs the `foo` test in the `foobar` test suite,
    and an Allure report will be generated:

    ```yaml
    - uses: robotframework/robot@v1
      with:
        datasource: foobar
        test: foo
        reports-for-allure: true
    ```
    """
    datasource = inputs['datasource']

    testcase = inputs.get('test')
    allure_reporting = inputs.get('reports-for-allure', False)
    extra_options = inputs.get('extra-options')

    if not isinstance(allure_reporting, bool):
        core.fail('reports-for-allure must be a boolean, not a string.')

    if testcase:
        testcase_arg = f'--test {format_testcase_name(testcase)} '
    else:
        testcase_arg = ''
    if allure_reporting:
        allure_arg = '--listener "allure_robotframework;." '
    else:
        allure_arg = ''

    if extra_options:
        extra_options_arg = f'{extra_options} '
    else:
        extra_options_arg = ''

    robot_screenshots = []
    for item in ROBOT_EXT_SCREENSHOTS:
        robot_screenshots.append(core.normalize_path(item))

    steps = [
        {'run': '\n'.join(core.delete_file(f) for f in CLEAN_LIST)},
        {
            'run': f'robot --nostatusrc {allure_arg}{testcase_arg}{extra_options_arg}"{datasource}"',
            'continue-on-error': True,
        },
        {
            'id': 'create_reports_archive',
            'uses': 'actions/create-archive@v1',
            'with': {
                'path': ROBOT_REPORTS_ARCHIVE,
                'format': 'tar',
                'patterns': ROBOT_REPORTS + robot_screenshots,
            },
            'continue-on-error': True,
        },
        *ROBOT_ATTACH_REPORTS,
    ]
    if allure_reporting:
        steps.extend(ROBOT_ATTACH_ALLUREREPORTS)

    return steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    ROBOT_CATEGORY: robot_action,
}
