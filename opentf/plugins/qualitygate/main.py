# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A quality gate plugin."""

from typing import Any

import os
import sys
import threading
import time

from queue import Queue
from urllib.parse import urlparse

import requests
import yaml

from flask import request, Response

from opentf.commons import (
    make_app,
    run_app,
    get_context_parameter,
    get_context_service,
    make_status_response,
    annotate_response,
    make_uuid,
    is_uuid,
    authorizer,
    validate_schema,
    read_and_validate,
    QUALITY_GATE,
)
from opentf.commons.exceptions import InvalidRequestError
from opentf.commons.expressions import evaluate_bool

from opentf.toolkit import watch_file


type Contexts = dict[str, Any]
type QualityGate = dict[str, Any]
type TestCase = dict[str, Any]

########################################################################
## Constants

# status produced by the quality gate in details.status attribute :
# SUCCESS: the workflow has succeeded and quality gate strategy is green
# NOTEST: the workflow has succeeded, but it produced no test result.
# FAILURE: the workflow has failed, or the quality gate strategy is red
# RUNNING: the workflow execution is still ongoing.
# INVALID_SCOPE: some quality gate rule scopes are invalid.

SUCCESS = 'SUCCESS'
FAILURE = 'FAILURE'
RUNNING = 'RUNNING'
NOTEST = 'NOTEST'
INVALID_SCOPE = 'INVALID_SCOPE'

TESTCASES_CACHES_TIMEOUT = 15 * 60
PENDING_DURATION = 3600
SLEEP_DELAY = 60

TESTCASES_QUEUE: Queue[tuple[str, str | None]] = Queue()  # workflow_id, token
CLEANUP_PENDING: Queue[tuple[float, str]] = Queue()  # timestamp, workflow_id
TESTCASES_CACHES: dict[str, list[TestCase]] = {}

# Default quality gate configurations

DEFAULT_FAILURE_STATUS = ('failure', 'error', 'blocked')

DEFAULT_STRICT_QUALITY_GATE = [
    {
        'name': 'Default strict quality gate',
        'rule': {
            'scope': 'true',
            'threshold': '100%',
            'failure-status': DEFAULT_FAILURE_STATUS,
        },
    }
]

DEFAULT_PASSING_QUALITY_GATE = [
    {
        'name': 'Default passing quality gate',
        'rule': {
            'scope': 'true',
            'threshold': '0%',
            'failure-status': DEFAULT_FAILURE_STATUS,
        },
    }
]

IMG_DEFINITIONS = {}


########################################################################
## Helpers


def info(msg: str, *args) -> None:
    """Log info message."""
    plugin.logger.info(msg, *args)


def error(msg: str, *args) -> None:
    """Log error message."""
    plugin.logger.error(msg, *args)


def warning(msg: str, *args) -> None:
    """Log warning message."""
    plugin.logger.warning(msg, *args)


def debug(msg: str, *args) -> None:
    """Log debug message."""
    plugin.logger.debug(msg, *args)


def _check_duplicates(
    items: list[str], name: str, source: str | None = None
) -> list[str] | None:
    """Check for duplicate items."""
    if duplicates := [item for item in set(items) if items.count(item) >= 2]:
        found_in = f' in {source}' if source else ''
        msg = f'Duplicate {name} name(s) found{found_in}: {', '.join(duplicates)}. The last definition(s) will be used.'
        return [msg]
    return None


########################################################################
## Configuration files


def _filter_listdir(path: str, kind: tuple[str, ...]) -> list[str]:
    """listdir-like, filtering for files with specified extensions."""
    files = [
        f
        for f in os.listdir(path)
        if os.path.isfile(os.path.join(path, f)) and f.endswith(kind)
    ]
    if not files:
        debug('No %s files provided in %s.', ', '.join(kind), path)
    return sorted(files)


def _add_to_conf(qualitygates: list[QualityGate], source: str) -> None:
    warnings = _check_duplicates(
        [qualitygate['name'] for qualitygate in qualitygates],
        'quality gate',
        f'the configuration file {source}',
    )
    if warnings:
        for msg in warnings:
            warning(msg)
            plugin.config['CONFIG'].setdefault('qualitygate_warnings', set()).add(msg)

    _qualitygates = {
        qualitygate['name']: {
            'rules': qualitygate['rules'],
            'scope': qualitygate.get('scope', DEFAULT_QG_SCOPE),
        }
        for qualitygate in qualitygates
    }
    plugin.config['CONFIG']['qualitygates'].update(_qualitygates)


def _read_qualitygates(configfile: str, schema: str) -> list[QualityGate] | None:
    try:
        qualitygates = read_and_validate(configfile, schema)
    except ValueError as err:
        error(
            'Invalid quality gate definition file "%s": %s.  Ignoring.',
            configfile,
            str(err),
        )
        return None

    return qualitygates['qualitygates']


def _load_image_qualitygates() -> None:
    """Load quality gate definitions from `CONFIG_PATH` directory.

    Entries are stored in IMG_DEFINITIONS.
    """
    plugin.config['CONFIG']['qualitygates'] = {
        'strict': DEFAULT_STRICT_QUALITY_GATE,
        'passing': DEFAULT_PASSING_QUALITY_GATE,
    }
    for def_file in _filter_listdir(CONFIG_PATH, ('.yaml', '.yml')):
        filepath = os.path.join(CONFIG_PATH, def_file)
        try:
            if not (qualitygates := _read_qualitygates(filepath, QUALITY_GATE)):
                continue
            debug('Loading quality gate definitions from file "%s".', def_file)
            _add_to_conf(qualitygates, source=def_file)
        except Exception as err:
            error(
                'Failed to load quality gate definitions from file "%s": %s.',
                def_file,
                str(err),
            )
            sys.exit(2)

    IMG_DEFINITIONS.update(plugin.config['CONFIG']['qualitygates'])


def _load_image_configuration() -> None:
    """Exit with code 2 if CONFIG_PATH is not a directory."""
    if not os.path.isdir(CONFIG_PATH):
        error(
            'Quality gate definition files path "%s" not found or not a directory.',
            CONFIG_PATH,
        )
        sys.exit(2)

    info(
        'Loading default quality gate definitions and definitions from "%s".',
        CONFIG_PATH,
    )

    _load_image_qualitygates()


def _refresh_configuration(_, configfile: str, schema: str) -> None:
    """Read quality gate definition from environment variable specified file."""
    try:
        plugin.config['CONFIG']['qualitygates'] = IMG_DEFINITIONS.copy()

        if not (qualitygates := _read_qualitygates(configfile, schema)):
            return

        _add_to_conf(qualitygates, source=configfile)

        info(f'Reading quality gate definition from {configfile}.')
    except Exception as err:
        error(
            'Error while reading "%s" quality gate definition: %s.',
            configfile,
            str(err),
        )


########################################################################
# Strategies


def in_scope(expr: str, contexts: Contexts, scopes_errors: set[str]) -> bool:
    """Safely evaluate data source scope."""
    try:
        return evaluate_bool(expr, contexts)
    except ValueError as err:
        msg = f'Invalid conditional {expr}: {err}.'
        scopes_errors.add(msg)
    except KeyError as err:
        msg = f'Nonexisting context entry in expression {expr}: {err}.'
        scopes_errors.add(msg)
    return False


def evaluate_rule(
    rule: dict[str, Any], testcases: dict[str, Any], mode: str
) -> dict[str, Any]:
    """Evaluate testcase results in rule scope.

    # Returned value

    A dictionary.  It contains at least a `result` entry, which is one
    of `NOTEST`, `SUCCESS`, or `FAILURE`.

    If `result` is not `NOTEST`, it also includes the following entries:

    - scope: a string
    - test_in_scope: an integer
    - test_failed: an integer
    - test_passed; an integer
    - success_ratio: a string
    """

    def _pass(threshold: str) -> bool:
        try:
            expected = float(threshold.rstrip('%')) / 100
            return (passed_count / testcases_count) >= expected
        except ValueError as err:
            debug(f'Invalid threshold value {threshold} for qualitygate {mode}: {err}')
        raise ValueError(f'Qualitygate threshold {threshold} is invalid.')

    scopes_errors = set()
    scope = rule['scope']
    testcase_results = {
        testcase_id: metadata['status'].lower()
        for testcase_id, metadata in testcases.items()
        if in_scope(scope, metadata, scopes_errors)
    }

    threshold = rule['threshold']
    if scopes_errors:
        return {
            'result': INVALID_SCOPE,
            'scope': scope,
            'threshold': threshold,
            'message': '\n'.join(scopes_errors),
        }
    if not testcase_results:
        return {'result': NOTEST, 'scope': scope, 'threshold': threshold}

    failure_status = rule.get('failure-status', DEFAULT_FAILURE_STATUS)
    failedtests_count = sum(
        1 for result in testcase_results.values() if result in failure_status
    )

    testcases_count = len(testcase_results)
    passed_count = testcases_count - failedtests_count
    return {
        'result': SUCCESS if _pass(threshold) else FAILURE,
        'scope': scope,
        'tests_in_scope': testcases_count,
        'tests_failed': failedtests_count,
        'tests_passed': passed_count,
        'success_ratio': f'{round(((passed_count/ testcases_count)*100), 2)}%',
        'threshold': threshold,
    }


def evaluate_qualitygate(
    mode: str, testcases: list[TestCase], qualitygates: dict[str, QualityGate]
) -> tuple[dict[str, Any], list[str] | None]:
    """Evaluate a quality gate.

    # Returned value

    A tuple.  The first entry is a dictionary, with one entry per rule.
    See #evaluate_rule() for details.  The second entry is either None
    or a list of strings, the warnings issued while evaluating the
    quality gate.

    # Raised exceptions

    A _ValueError_ exception is raised if the `mode` is unknown.
    """
    testcases_dict = {item['uuid']: item for item in testcases}
    if qualitygate := qualitygates.get(mode):
        return (
            {
                rule.get('name', make_uuid()): evaluate_rule(
                    rule['rule'], testcases_dict, mode
                )
                for rule in qualitygate['rules']
            },
            _check_duplicates(
                [rule['name'] for rule in qualitygate['rules'] if rule.get('name')],
                'rule',
                f'the quality gate {mode}',
            ),
        )

    raise ValueError(f'Quality gate {mode} not defined.')


########################################################################
# handlers


def _compute_qualitygate_general_status(rules_results: dict[str, Any]) -> str:
    statuses = {value['result'] for value in rules_results.values()}
    if FAILURE in statuses or INVALID_SCOPE in statuses:
        return FAILURE
    return SUCCESS if SUCCESS in statuses else NOTEST


def handle_completed_workflow(
    workflow_id: str,
    mode: str,
    timeout: float,
    token: str | None,
    qualitygates: dict[str, QualityGate],
    warnings: list[str] | None = None,
) -> Response:
    """Completed workflow handler."""
    if workflow_id not in TESTCASES_CACHES:
        TESTCASES_QUEUE.put((workflow_id, token))
        start_time = time.time()
        while True:
            if time.time() - start_time > timeout:
                return make_status_response(
                    'Accepted', 'Workflow caching still in progress, retry later.'
                )
            if workflow_id not in TESTCASES_CACHES:
                time.sleep(1)
                continue
            break

    testcases = TESTCASES_CACHES[workflow_id]

    if err := testcases[0].get('exception_error'):
        msg = f'Error while filling test cases cache: {err}.'
        error(msg)
        return make_status_response('Invalid', msg)

    if notest_msg := testcases[0].get('EMPTY_ITEMS'):
        debug(notest_msg)
        return make_status_response('OK', '', {'status': NOTEST})

    scope = qualitygates[mode].get('scope', DEFAULT_QG_SCOPE)
    scopes_errors = set()
    filtered_testcases = [
        testcase for testcase in testcases if in_scope(scope, testcase, scopes_errors)
    ]

    if scopes_errors:
        return make_status_response(
            'Invalid', '\n'.join(scopes_errors), {'status': INVALID_SCOPE}
        )
    if not filtered_testcases:
        debug(
            'No test cases matching quality gate "%s" scope "%s" found.',
            mode,
            scope,
        )
        return make_status_response('OK', '', {'status': NOTEST})

    rules_results, extra = evaluate_qualitygate(mode, filtered_testcases, qualitygates)

    for rule, result in rules_results.items():
        if result['result'] == INVALID_SCOPE:
            error(f"Error in rule {rule}. {result['message']}")

    general_status = _compute_qualitygate_general_status(rules_results)
    status = {'status': general_status, 'rules': rules_results}
    if warnings or extra:
        relevant = [w for w in warnings if mode in w] if warnings else []
        status['warnings'] = (relevant) + (extra or [])
        for msg in status['warnings']:
            warning(msg)
    return annotate_response(make_status_response('OK', '', status), processed=['mode'])


def handle_running_workflow(*_) -> Response:
    """Running workflow handler."""
    return make_status_response('OK', '', {'status': RUNNING})


def handle_failed_workflow(*_) -> Response:
    """Failed workflow handler."""
    return make_status_response('OK', '', {'status': FAILURE})


STATUS_HANDLER = {
    'DONE': handle_completed_workflow,
    'COMPLETE': handle_completed_workflow,
    'RUNNING': handle_running_workflow,
    'PENDING': handle_running_workflow,
    'ONGOING': handle_running_workflow,
    'FAILED': handle_failed_workflow,
    'INTERRUPTED': handle_failed_workflow,
}


def process_known_workflow(
    workflow_id: str,
    observer_response: requests.Response,
    mode: str,
    timeout: float,
    token: str | None,
    qualitygates: dict[str, QualityGate],
    warnings: list[str] | None = None,
) -> Response:
    """Handling known workflow."""
    try:
        body = observer_response.json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if 'details' not in body or 'status' not in body['details']:
        return make_status_response(
            'BadRequest', 'Missing .details.status entry in body.'
        )

    workflow_status = body['details']['status']
    if workflow_status not in STATUS_HANDLER:
        return make_status_response(
            'Invalid',
            f'Unexpected workflow status {workflow_status}, was expecting one of {",".join(STATUS_HANDLER)}.',
        )
    return STATUS_HANDLER[workflow_status](
        workflow_id, mode, timeout, token, qualitygates, warnings
    )


########################################################################
# Testcases data source cache handling


def cleanup_cache():
    """Cleanup cache."""
    while True:
        try:
            stamp, workflow_id = CLEANUP_PENDING.get()
            if (time.time() - stamp) < PENDING_DURATION:
                CLEANUP_PENDING.put((stamp, workflow_id))
                time.sleep(SLEEP_DELAY)
                continue

            if workflow_id not in TESTCASES_CACHES:
                continue

            try:
                del TESTCASES_CACHES[workflow_id]
            except KeyError:
                pass
        except Exception as err:
            error('Internal error while cleaning test cases caches: %s.', err)


def _find_datasource_items(workflow_id: str, token: str | None) -> requests.Response:
    """Read first page of workflow data source items."""
    observer = get_context_service(plugin, 'observer')
    url = f'{observer["endpoint"]}/workflows/{workflow_id}/datasources/testcases'
    headers = {'Authorization': token} if token else None
    return requests.get(url, headers=headers)


def _fetch_testcases(workflow_id: str, token: str | None) -> requests.Response:
    start_time = time.time()
    while True:
        response = _find_datasource_items(workflow_id, token)
        if time.time() - start_time > TESTCASES_CACHES_TIMEOUT:
            raise TimeoutError('Timeout while waiting for testcases data source.')
        time.sleep(1)
        if response.status_code == 202:
            continue
        return response


def _get_testcases_from_observer(
    workflow_id: str, token: str | None
) -> list[TestCase | dict[str, str]]:
    observer = get_context_service(plugin, 'observer')
    url_base = f'{observer["endpoint"]}/workflows/{workflow_id}/datasources/testcases'
    response = ''
    try:
        response = _fetch_testcases(workflow_id, token)
        if response.status_code != 200:
            return [
                {
                    'exception_error': f'Failed to get test cases: {response.json().get('message', '')} (code: {response.status_code})'
                }
            ]
        testcases = response.json()['details']['items']
        if not testcases:
            return [
                {
                    'EMPTY_ITEMS': f'Workflow {workflow_id} contains no test cases, not filling cache.'
                }
            ]
        while 'next' in response.links:
            url = f'{url_base}?{urlparse(response.links['next']['url']).query}'
            response = requests.get(url, headers=response.request.headers)
            testcases += response.json()['details']['items']
        return testcases
    except TimeoutError as err:
        return [{'exception_error': str(err)}]
    except (ValueError, KeyError) as err:
        error('Could not deserialize observer response: %s.', str(err))
        if response:
            debug(response.text)
        return [{'exception_error': str(err)}]


def testcases_cache_worker(workflow_id: str, token: str | None) -> None:
    try:
        if workflow_id in TESTCASES_CACHES:
            debug('Workflow %s test cases already cached.', workflow_id)
            return
        events = _get_testcases_from_observer(workflow_id, token)
        TESTCASES_CACHES[workflow_id] = events
        CLEANUP_PENDING.put((time.time(), workflow_id))
        if 'EMPTY_ITEMS' not in events[0] and 'exception_error' not in events[0]:
            debug('Workflow %s test cases cache filled.', workflow_id)
    except Exception as err:
        error('Error while caching test cases events: %s.', str(err))


def cache_testcases():
    while True:
        try:
            workflow_id, token = TESTCASES_QUEUE.get()
            if not TESTCASES_CACHES.get(workflow_id):
                debug('Caching test cases for workflow %s...', workflow_id)
                testcases_cache_worker(workflow_id, token)
        except Exception as err:
            error(str(err))


########################################################################
## Definitions


def _get_request_qualitygate_definition() -> dict[str, Any]:
    """Get quality gate definition from request."""
    if request.is_json:
        try:
            return request.get_json() or {}
        except Exception as err:
            raise InvalidRequestError(f'Could not parse quality gate definition: {err}')

    if request.mimetype in ('application/x-yaml', 'text/yaml'):
        if request.content_length and request.content_length < 1000000:
            return yaml.safe_load(request.data)
        raise InvalidRequestError(
            'Content-size must be specified and less than 1000000 for YAML quality gate definitions.'
        )
    if 'qualitygates' in request.files:
        return yaml.safe_load(request.files['qualitygates'].read())
    raise InvalidRequestError('Could not parse quality gate definition from file.')


def _maybe_evaluate_qualitygate(
    workflow_id: str, qualitygates: dict[str, Any], warnings: list[str] | None
) -> Response:
    """Evaluate quality gate if the incoming request is valid.

    # Returned value

    A Response object.
    """
    timeout = float(request.args.get('timeout', 8))
    mode = request.args.get('mode', 'strict')
    if mode not in qualitygates:
        return make_status_response(
            'Invalid',
            f'Quality gate "{mode}" not found in quality gate definitions.',
        )
    if mode in ('strict', 'passing'):
        warnings = None
    token = request.headers.get('Authorization')
    if workflow_id in TESTCASES_CACHES:
        return handle_completed_workflow(
            workflow_id, mode, timeout, token, qualitygates, warnings
        )
    response = _find_datasource_items(workflow_id, token)
    status_code = response.status_code
    debug('Got observer response %d for workflow %s.', status_code, workflow_id)
    if status_code in (200, 202):
        return process_known_workflow(
            workflow_id, response, mode, timeout, token, qualitygates, warnings
        )
    if status_code == 401:
        return make_status_response('Unauthorized', 'Invalid or missing token.')
    if status_code == 403:
        return make_status_response(
            'Forbidden', f'Token does not allow access to workflow {workflow_id}.'
        )
    if status_code == 404:
        return make_status_response('NotFound', f'Workflow {workflow_id} not found.')
    if status_code == 422:
        return make_status_response(
            'Invalid',
            f'No test results found for workflow {workflow_id}.',
        )

    error('Unexpected observer response: %d.', status_code)
    return make_status_response(
        'InternalError', f'Unexpected observer response {status_code}.'
    )


########################################################################
# Main

plugin = make_app(
    name='qualitygate',
    description='Create and start a quality gate service.',
    descriptor='plugin.yaml',
)

DEFAULT_QG_SCOPE: str = get_context_parameter(plugin, 'default_scope')
CONFIG_PATH: str = get_context_parameter(plugin, 'definitions_path')


@plugin.route('/workflows/<workflow_id>/qualitygate', methods=['POST'])
@authorizer(resource='qualitygates', verb='get')
def get_qualitygate_status_from_definition(workflow_id: str):
    """Handle quality gate definition from request."""
    if not is_uuid(workflow_id):
        return make_status_response('Invalid', f'Not a valid UUID: {workflow_id}.')

    try:
        definition = _get_request_qualitygate_definition()
        valid, extra = validate_schema(QUALITY_GATE, definition)
        if not valid:
            raise InvalidRequestError(f'Not a valid quality gate definition: {extra}')

        warnings = _check_duplicates(
            [qualitygate['name'] for qualitygate in definition['qualitygates']],
            'quality gate',
        )
        qualitygates = {
            qualitygate['name']: {
                'rules': qualitygate['rules'],
                'scope': qualitygate.get('scope', DEFAULT_QG_SCOPE),
            }
            for qualitygate in definition['qualitygates']
        }
    except Exception as err:
        return make_status_response(
            'Invalid', str(err), details={'workflow_id': workflow_id}
        )

    return _maybe_evaluate_qualitygate(workflow_id, qualitygates, warnings)


@plugin.route('/workflows/<workflow_id>/qualitygate', methods=['GET'])
@authorizer(resource='qualitygates', verb='get')
def get_qualitygate_status(workflow_id: str):
    """
    Main method of the quality gate service. Will try to pull the testcases
    data source from observer for a given workflow and will analyze the response
    to send back a status to caller.

    - If the observer responds with an error, quality gate will respond
      with HTTP error codes.
    - If the observer responds HTTP 200, the quality gate will try to
      analyze tests results and provide a status.

    Given an HTTP 200 response from observer:

    - If the workflow has not a SUCCESS status, quality gate will return
      HTTP 200 + custom status in details.status.
    - If the workflow has SUCCESS status (aka is completed), quality
      gate will use a named strategy to analyze tests results, and
      return HTTP 200 + custom status in details.status attribute.
    """
    if not is_uuid(workflow_id):
        return make_status_response('Invalid', f'Not a valid UUID: {workflow_id}.')

    return _maybe_evaluate_qualitygate(
        workflow_id,
        plugin.config['CONFIG']['qualitygates'],
        plugin.config['CONFIG'].get('qualitygate_warnings'),
    )


def main(svc):
    """Run the quality gate service."""
    _ = get_context_service(svc, 'observer')

    _load_image_configuration()

    if os.environ.get('QUALITYGATE_DEFINITIONS'):
        watch_file(
            svc,
            os.environ['QUALITYGATE_DEFINITIONS'],
            _refresh_configuration,
            QUALITY_GATE,
        )

    try:
        debug('Starting test cases caching thread.')
        threading.Thread(target=cache_testcases, daemon=True).start()
    except Exception as err:
        error('Could not start test cases caching thread: %s.', str(err))
        sys.exit(2)

    try:
        debug('Starting test cases cache cleanup thread.')
        threading.Thread(target=cleanup_cache, daemon=True).start()
    except Exception as err:
        error('Could not start test cases cache cleanup thread: %s.', str(err))
        sys.exit(2)

    run_app(svc)


if __name__ == '__main__':
    main(plugin)
