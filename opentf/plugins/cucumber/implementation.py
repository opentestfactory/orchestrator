# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the cucumber provider.

Since v2021-12, Cucumber 5 and up are supported. This changes the
provider implementation as it now provides two versions of each
action (execute, params and cucumber):
- one for execution environments running Cucumber versions up to 5 ;
- one for those running Cucumber 5 and up.
For maintainability purposes, common content within these functions has
been factorised into other functions called by both version-specific actions.
As some versions of cucumber6 only produce valid HTML reports when using UTF-8,
we set maven's JVM to run with UTF-8 file encoding.

The following categories are provided:

- execute
- execute5
- params
- params5
- cucumber
- cucumber5
"""


from uuid import uuid4

import re

from opentf.toolkit import core


########################################################################
## Constants

EXECUTE_CATEGORY = 'cucumber/execute'
EXECUTE5_CATEGORY = 'cucumber5/execute'
PARAM_CATEGORY = 'cucumber/params'
PARAM5_CATEGORY = 'cucumber5/params'
CUCUMBER_CATEGORY = 'cucumber/cucumber'
CUCUMBER5_CATEGORY = 'cucumber5/cucumber'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'
SQUASHTM_TAG = 'SQUASH_TF_CUCUMBER_TEST_TAG'
OPENTF_PARAM_FILE = '_SQUASH_TF_TESTCASE_PARAM_FILES'
OPENTF_PARAM_FILE_ABSOLUTE = '_SQUASH_TF_TESTCASE_PARAM_FILES_ABSOLUTE'

HTML_REPORT_NAME = 'html-report.html'
HTML_ARCHIVE_PATH = 'target/html-report.tar'
REPORTERS = {
    'junit': 'target/report.xml',
    'html': f'target/{HTML_REPORT_NAME}',
}

CUCUMBER_TAGOPTION_SYNTAX = '--tags '
CUCUMBER5_TAGOPTION_SYNTAX = '-Dcucumber.filter.tags='

REPORT_TYPE = 'application/vnd.opentestfactory.cucumber-surefire+xml'

CUCUMBER_EXTRA_OPTIONS = 'CUCUMBER_EXTRA_OPTIONS'


########################################################################
## Helpers


def _use_remote_variable(varname):
    """Uses environment variable, using  OS-specific constructs."""
    if core.runner_on_windows():
        return f'%{varname}%'
    return f'${varname}'


########################################################################
## 'execute' actions (for Cucumber 5 and up)


def execute_action(inputs):
    """The execute action for Cucumber versions up to 4."""
    # maybe add @if not defined {SQUASHTM_TAG} @echo "{SQUASHTM_TAG}= " >> %OPENTF_VARIABLES%
    return cucumber_action(
        {
            'test': inputs['test'],
            'reporters': ['junit', 'html'],
            'extra-options': f'{_use_remote_variable(SQUASHTM_TAG)} {_use_remote_variable(CUCUMBER_EXTRA_OPTIONS)}',
        }
    )


def execute5_action(inputs):
    """The Cucumber 5 and up execute action."""
    return cucumber5_action(
        {
            'test': inputs['test'],
            'reporters': ['junit', 'html'],
            'extra-options': f'{_use_remote_variable(SQUASHTM_TAG)} {_use_remote_variable(CUCUMBER_EXTRA_OPTIONS)}',
        }
    )


########################################################################
## 'params' actions


def _param_preparation(inputs, tag_syntax):
    """
    The method called by both `param_action` and `param5_action` to:
        - check the params format
        - check the keys conformity in data
        - export data sets into execution environment variables
    """
    core.validate_params_inputs(inputs)

    data = inputs['data']

    tag_unsupported_chars = [' ', '"', "'", '(', ')', '`', '\\', ',']
    if not core.runner_on_windows():
        tag_unsupported_chars += ['&', ';', '<', '|']
    unsupported_char = ''
    if 'test' in data and (dsname := data['test'].get('DSNAME')):
        if core.runner_on_windows() and re.match(re.compile(r'%\w+%'), dsname):
            unsupported_char = 'The dataset name contains a pattern which is not supported in Cucumber tags (percent signs enclosing a string).'
            dsname = 'ERROR_UNSUPPORTED_PATTERN_IN_CUCUMBER_TAGS'
        elif any(c in dsname for c in tag_unsupported_chars):
            unsupported_char = 'The dataset name includes a character that the Cucumber provider does not support in Cucumber tags (space, single or double quote, parentheses, backtick, backslash, comma, ampersand, semicolon, pipe, or opening angle bracket).'
            dsname = 'ERROR_UNSUPPORTED_CHARACTER_IN_CUCUMBER_TAGS'
        value = f'{tag_syntax}@{dsname}'
    else:
        value = ' '

    steps = []
    if unsupported_char:
        if core.runner_on_windows():
            steps += [{'run': f'echo ::error::{unsupported_char}'}]
        else:
            steps += [{'run': f'echo "::error::{unsupported_char}"'}]

    path = f'{uuid4()}.ini'
    file_absolute_path = core.join_path(
        _use_remote_variable('OPENTF_WORKSPACE'), path, core.runner_on_windows()
    )

    # both create-file steps are necessary when param action has `working-directory` specified
    # see orchestrator#618
    steps += [
        {
            'uses': 'actions/create-file@v1',
            'with': {'data': data, 'path': path, 'format': 'ini'},
        },
        {
            'uses': 'actions/create-file@v1',
            'with': {'data': data, 'path': file_absolute_path, 'format': 'ini'},
        },
        *core.export_variables(
            {
                SQUASHTM_TAG: value,
                OPENTF_PARAM_FILE: path,
                OPENTF_PARAM_FILE_ABSOLUTE: file_absolute_path,
            }
        ),
    ]
    return steps


def param_action(inputs):
    """The parameters action for Cucumber versions up to 4."""
    return _param_preparation(inputs, CUCUMBER_TAGOPTION_SYNTAX)


def param5_action(inputs):
    """The parameters action for Cucumber 5 and up."""
    return _param_preparation(inputs, CUCUMBER5_TAGOPTION_SYNTAX)


########################################################################
## native actions


def _get_execution_parameters(inputs, cucumber_version_target):
    """
    The method called by both `cucumber_action` and `cucumber5_action` to:
    - extract data from PEAC
    - check reporters conformity
    - check tag compatibility

    Returns a dictionary containing all relevant data to the version-specific native action.
    """
    test_reference = inputs['test']
    if test_reference.count("#") == 3:
        # New test reference format:
        # full/path/to/cucumberProject#src/test/java/features/sample.feature#feature_name#scenario
        datasource, _, testsource = test_reference.partition('#')
    else:
        # Old test reference format:
        # cucumberProject/src/test/java/features/sample.feature#feature_name#scenario
        datasource, _, testsource = test_reference.partition('/')

    reporters = set(inputs.get('reporters', []))
    if reporters - set(REPORTERS):
        core.fail(f'Unexpected reporter(s) {reporters - set(REPORTERS)}.')

    prepare, reports, attachments = [], [], []
    for reporter in reporters:
        report = f'{datasource}/{REPORTERS[reporter]}'
        reports.append(f'{reporter}:{REPORTERS[reporter]}')

        if reporter == 'html':
            if cucumber_version_target == 5:
                prepare.append(core.delete_file(report))
            else:
                prepare.append(core.delete_directory(report))
            prepare.append(core.delete_file(f'{datasource}/{HTML_ARCHIVE_PATH}'))
            attachments.append(core.attach_file(f'{datasource}/{HTML_ARCHIVE_PATH}'))
        else:
            prepare.append(core.delete_file(report))
            attachments.append(core.attach_file(report, type=REPORT_TYPE))

    feature_path = testsource.partition('#')[0]

    if tags := inputs.get('tags', ''):
        if inputs.get('tag'):
            core.fail(
                "Conflict identified in workflow: 'tag' and 'tags' fields cannot coexist."
            )
    elif tag := inputs.get('tag'):
        tags = f'@{tag}'

    extra_options = inputs.get('extra-options', '')

    return {
        'datasource': datasource,
        'feature-path': feature_path,
        'reporters': reporters,
        'prepare': prepare,
        'reports': reports,
        'attachments': attachments,
        'tags': tags,
        'extra-options': extra_options,
    }


def _create_execution_steps(params, command):
    """
    The method called by both `cucumber_action` and `cucumber5_action`
    after `cucumber_prepare_launch` to prepare steps.
    """
    if extra_options := params['extra-options']:
        command += f' {extra_options}'
    if 'cucumber.options' in command:
        command += '"'

    steps = [
        {'run': '\n'.join(params['prepare'])},
        {
            'run': f'mvn test -f "{params["datasource"]}/pom.xml" -Dmaven.test.failure.ignore=true {command}',
            'variables': {
                '_JAVA_OPTIONS': f'{_use_remote_variable("_JAVA_OPTIONS")} -Dfile.encoding=UTF-8'
            },
            'continue-on-error': True,
        },
    ]
    if 'html' in params['reporters']:
        steps.append(
            {
                'run': f'tar -cvf {HTML_ARCHIVE_PATH} -C target {HTML_REPORT_NAME}',
                'working-directory': params["datasource"],
                'continue-on-error': True,
            }
        )
    steps.append(
        {
            'run': '\n'.join(a for a in params['attachments']),
            'continue-on-error': True,
        }
    )
    return steps


def cucumber_action(inputs):
    """The native action for Cucumber up to 4."""
    params = _get_execution_parameters(inputs, cucumber_version_target=4)
    if len(params) == 1 and 'error' in params['run']:
        return [params]
    feature_path, reports = params['feature-path'], params['reports']
    if tags := params['tags']:
        tag_option = f' {CUCUMBER_TAGOPTION_SYNTAX}{tags}'
    else:
        tag_option = ''
    command = f'-Dcucumber.options="{feature_path} {" ".join(f"--plugin {r}" for r in reports)}{tag_option}'

    return _create_execution_steps(params, command)


def cucumber5_action(inputs):
    """The Cucumber 5 and up native action."""
    params = _get_execution_parameters(inputs, cucumber_version_target=5)
    if len(params) == 1 and 'error' in params['run']:
        return [params]
    feature_path, reports = params['feature-path'], params['reports']
    command = (
        f'-Dcucumber.features="{feature_path}" -Dcucumber.plugin="{",".join(reports)}"'
    )
    if tags := params['tags']:
        command += f' {CUCUMBER5_TAGOPTION_SYNTAX}"{tags}"'

    return _create_execution_steps(params, command)


########################################################################
## known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    EXECUTE5_CATEGORY: execute5_action,
    PARAM_CATEGORY: param_action,
    PARAM5_CATEGORY: param5_action,
    CUCUMBER_CATEGORY: cucumber_action,
    CUCUMBER5_CATEGORY: cucumber5_action,
}
