# Copyright (c) 2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A provider aggregator plugin.

Provides default implementation for provider aggregator.

# Usage

```
python3 -m opentf.plugins.allinone.main [--context context] [--config configfile]
```


# Endpoints

This module exposes one endpoint:

- /inbox (POST)

Whenever calling this endpoint, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

from typing import Any, Dict, List, Optional, Set

import importlib
import os
import sys

import yaml

from opentf.commons import (
    validate_schema,
    read_config,
    PROVIDERCONFIG,
    read_descriptor,
)
from opentf.toolkit import make_plugin, run_plugin, watch_file


########################################################################

AGGREGATED_PLUGINS = set()
CATEGORIES = {}
KNOWN_CATEGORIES = {}


########################################################################
## Helpers

INVALID_HOOKS_DEFINITION_TEMPLATE = {
    'name': 'invalid-external-hooks-definition',
    'events': [],
    'before': [
        {
            'run': 'echo ::error::Invalid hooks definition.  Hooks defined by {name}_PROVIDER_HOOKS are disabled.  Please contact your orchestrator administrator for more info.',
            'if': "runner.os == 'windows'",
        },
        {
            'run': 'echo "::error::Invalid hooks definition.  Hooks defined by {name}_PROVIDER_HOOKS are disabled.  Please contact your orchestrator administrator for more info."',
            'if': "runner.os != 'windows'",
        },
    ],
}


def _make_invalid_definition_hook(provider: str) -> List[Dict[str, Any]]:
    """Make a hook definition that signals an invalid definition."""
    definition = INVALID_HOOKS_DEFINITION_TEMPLATE.copy()
    definition['events'] = [{'categoryPrefix': '_'}]
    definition['before'] = []
    for step in INVALID_HOOKS_DEFINITION_TEMPLATE['before']:
        new = step.copy()
        new['run'] = step['run'].format(name=provider.upper())
        definition['before'].append(new)
    return [definition]


def _add_if_relevant(
    hooks: List[Dict[str, Any]], hook: Dict[str, Any], provider: str
) -> None:
    """Add hook to hooks if relevant to provider.

    # Required parameters

    - hooks: the list of hooks
    - hook: the hook to add
    - provider: the provider name

    # Side effects

    Modifies the hooks list.
    """
    events = []
    for event in hook['events']:
        prefix = event.get('categoryPrefix')
        if prefix in (None, '_'):
            for cat in CATEGORIES[provider]:
                evt = event.copy()
                evt['categoryPrefix'] = cat
                events.append(evt)
        elif prefix in CATEGORIES[provider]:
            events.append(event)
        else:
            plugin.logger.warning(
                'Unexpected categoryPrefix %s in hooks definition file for provider %s, ignoring.',
                prefix,
                provider,
            )
    if events:
        hook['events'] = events
        hooks.append(hook)


def _merge(
    previous: Optional[List[Dict[str, Any]]], new: List[Dict[str, Any]], provider: str
) -> Optional[List[Dict[str, Any]]]:
    """Merge new hooks with previous ones.

    Remove all previous hooks for `provider`, and add `new` hooks to the
    list.

    # Required parameters

    - previous: the previous hooks
    - new: the new hooks
    - provider: the provider name

    # Returned value

    A list of hooks or None.
    """
    hooks = []
    for hook in new:
        _add_if_relevant(hooks, hook, provider)
    if previous:
        for hook in previous:
            if any(
                event.get('categoryPrefix') not in CATEGORIES[provider]
                for event in hook['events']
            ):
                hooks.append(hook)
    return hooks or None


def _read_hooks(svc, hooksfile: str, provider: str) -> None:
    """Refresh hooks definition for provider.

    IF the hooks definition file is invalid, a default hook is created
    that signals the error to the user.

    # Required parameters

    - svc: a service instance
    - hooksfile: the path to the hooks definition file
    - provider: the provider name
    """
    config = svc.config['CONFIG']
    previous = config.get('hooks')
    try:
        with open(hooksfile, 'r', encoding='utf-8') as src:
            hooks = yaml.safe_load(src)
        if not (isinstance(hooks, dict) and 'hooks' in hooks):
            svc.logger.error(
                "Hooks definition file for provider '%s' ('%s') needs a 'hooks' entry, ignoring.",
                provider,
                hooksfile,
            )
            config['hooks'] = _merge(
                previous, _make_invalid_definition_hook(provider), provider
            )
            return

        if previous:
            svc.logger.info(
                "Refreshing hooks definition for provider '%s' using '%s'.",
                provider,
                hooksfile,
            )
        else:
            svc.logger.info(
                "Reading hooks definition for provider '%s' from '%s'.",
                provider,
                hooksfile,
            )

        config['hooks'] = _merge(previous, hooks['hooks'], provider)
        valid, extra = validate_schema(PROVIDERCONFIG, config)
        if valid:
            return

        svc.logger.error(
            "Error while verifying provider '%s' ('%s') hooks definition: %s.",
            provider,
            hooksfile,
            extra,
        )
    except Exception as err:
        svc.logger.error(
            "Error while reading provider '%s' ('%s') hooks definition: %s.",
            provider,
            hooksfile,
            err,
        )

    config['hooks'] = _merge(
        previous, _make_invalid_definition_hook(provider), provider
    )


def _read_descriptors(svc, disabled: Set[str]) -> None:
    descriptors = []
    for provider in AGGREGATED_PLUGINS:
        if provider in disabled:
            svc.logger.info('Skipping disabled provider %s.', provider)
            continue
        try:
            module = importlib.import_module(
                f'{provider if "." in provider else f"opentf.plugins.{provider}"}.implementation'
            )
            KNOWN_CATEGORIES.update(**getattr(module, 'KNOWN_CATEGORIES'))
        except Exception as err:
            svc.logger.error(
                'Could not import provider definition for %s, got: %s.', provider, err
            )
            sys.exit(2)

        _, descs = read_descriptor(
            os.path.join(os.path.dirname(module.__file__), 'plugin.yaml'), None
        )
        descriptors += descs
    for descriptor in descriptors:
        if 'action' not in descriptor['metadata']:
            CATEGORIES[descriptor['metadata']['name']] = [
                event['categoryPrefix'] for event in descriptor['events']
            ]
        descriptor['metadata']['name'] = 'allinone'

    svc.config['DESCRIPTOR'] = descriptors


def _initialize_aggregated(svc, launcher_manifest: Dict[str, Any]) -> None:
    """Initialize the aggregated plugin."""
    disabled = set(launcher_manifest.get('disabled', []))
    if aggregated := launcher_manifest.get('aggregated'):
        AGGREGATED_PLUGINS.update(name.lower() for name in aggregated)
    _read_descriptors(svc, disabled)
    hooks = []
    for provider in AGGREGATED_PLUGINS - disabled:
        if not os.environ.get(f'{provider.upper()}_PROVIDER_HOOKS'):
            _, config = read_config(
                None, None, f'conf/{provider}.yaml', None, PROVIDERCONFIG
            )
            if config.get('hooks'):
                hooks = _merge(hooks, config['hooks'], provider)
    svc.config['CONFIG']['hooks'] = hooks
    for provider in AGGREGATED_PLUGINS - disabled:
        if env := os.environ.get(f'{provider.upper()}_PROVIDER_HOOKS'):
            watch_file(svc, env, _read_hooks, provider)


def _read_launcher_manifest(svc, manifest_name: str) -> Dict[str, Any]:
    """Read launcher manifest.

    Exits with code 2 if the manifest cannot be read.

    # Required parameters

    - svc: a Flask instance
    - manifest_name: the name of the launcher manifest

    # Returned value

    A dictionary, the launcher manifest.
    """
    try:
        with open(manifest_name, 'r', encoding='utf-8') as src:
            launcher_manifest = yaml.safe_load(src)
    except Exception as err:
        svc.logger.error(
            "Could not read launcher manifest '%s', got: %s.", manifest_name, err
        )
        sys.exit(2)
    return launcher_manifest


########################################################################
## Main

plugin = make_plugin(
    name='allinone',
    description='Create and start a provider aggregator.',
    providers=KNOWN_CATEGORIES,
)


if __name__ == '__main__':
    launcher_manifest = _read_launcher_manifest(
        plugin, os.environ.get('OPENTF_LAUNCHER_MANIFEST', 'squashtf.yaml')
    )
    _initialize_aggregated(plugin, launcher_manifest)
    if AGGREGATED_PLUGINS:
        plugin.logger.info(
            'Aggregating the following services: %s.',
            ', '.join(sorted(AGGREGATED_PLUGINS)),
        )
    else:
        plugin.logger.info('No services to aggregate.')
    run_plugin(plugin)
