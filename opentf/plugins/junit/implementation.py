# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the junit provider.

The following categories are provided:

- execute
- params
- mvntest
"""

from uuid import uuid4

from opentf.toolkit import core


########################################################################
## Constants

EXECUTE_CATEGORY = 'junit/execute'
PARAM_CATEGORY = 'junit/params'
MVNTEST_CATEGORY = 'junit/mvntest'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'
OPENTF_PARAM_FILE = '_SQUASH_TF_TESTCASE_PARAM_FILES'
OPENTF_PARAM_FILE_ABSOLUTE = '_SQUASH_TF_TESTCASE_PARAM_FILES_ABSOLUTE'
REPORT_TYPE = 'application/vnd.opentestfactory.junit-surefire+xml'

########################################################################
## 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: junit/execute@v1
      with:
        test: path/to/test/root/qualified.testsuite.ClassName#testName
    ```

    The path to test root is a relative path from the step's working
    directory.  If not specified, this is the job's working directory,
    in which case it must include the repository name.
    """
    core.debug('Identified as the "execute" action')

    test_reference = inputs['test']

    parts = test_reference.partition('/')
    datasource = ''
    test_case_arg = test_reference
    while len(parts[2].rstrip()) > 0:
        datasource = datasource + parts[0] + '/'
        test_case_arg = parts[2]
        parts = parts[2].partition('/')

    if not core.runner_on_windows():
        test_case_arg = test_case_arg.replace("$", "\\$")

    test_class = test_case_arg.partition('#')[0]
    target = f'{datasource}target'
    txt_report = f'{target}/surefire-reports/{test_class}.txt'
    xml_report = f'{target}/surefire-reports/TEST-{test_class}.xml'
    junit_run_log = f'{target}/junit-run-log.txt'
    junit_run_command = f'mvn test -f "{datasource}pom.xml" -Dtest={test_case_arg} -Dmaven.test.failure.ignore=true -DfailIfNoTests=true --log-file {junit_run_log}'

    if core.runner_on_windows():
        create_target_dir = {
            'run': f'@if not exist {target} @mkdir {target}'.replace('/', '\\'),
            'continue-on-error': True,
        }
        run_test = {
            'run': f'{junit_run_command} %JUNIT_EXTRA_OPTIONS%',
            'continue-on-error': True,
        }
        error_log = {
            'run': f'findstr /r "ERROR WARN" {target}\\junit-run-log.txt',
            'continue-on-error': True,
        }
    else:
        create_target_dir = {'run': f'mkdir -p {target}', 'continue-on-error': True}
        run_test = {
            'run': f'{junit_run_command} $JUNIT_EXTRA_OPTIONS',
            'continue-on-error': True,
        }
        error_log = {
            'run': f'grep -E "(ERROR|WARN)" {target}/junit-run-log.txt',
            'continue-on-error': True,
        }

    steps = [
        {'run': '\n'.join((core.delete_file(f) for f in (txt_report, xml_report)))},
        create_target_dir,
        run_test,
        error_log,
        {
            'uses': 'actions/get-files@v1',
            'with': {
                'pattern': f'{test_class}.txt',
                'warn-if-not-found': 'No Surefire *.txt reports found.',
            },
            'working-directory': f'{target}/surefire-reports/',
            'continue-on-error': True,
        },
        {
            'uses': 'actions/get-files@v1',
            'with': {
                'pattern': f'TEST-{test_class}.xml',
                'type': REPORT_TYPE,
                'warn-if-not-found': 'No Surefire *.xml reports found.',
            },
            'working-directory': f'{target}/surefire-reports/',
            'continue-on-error': True,
        },
        {
            'run': core.attach_file(junit_run_log),
            'continue-on-error': True,
        },
    ]

    return steps


########################################################################
# 'params' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: junit/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

     `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    core.debug('Identified as the "param" action')

    core.validate_params_inputs(inputs)

    path = f'{uuid4()}.ini'
    if core.runner_on_windows():
        file_absolute_path = '%OPENTF_WORKSPACE%\\'
    else:
        file_absolute_path = '$OPENTF_WORKSPACE/'
    file_absolute_path += path

    data = inputs['data']
    # both create-file steps are necessary when param action has `working-directory` specified
    # see orchestrator#618
    steps = [
        {
            'uses': 'actions/create-file@v1',
            'with': {'data': data, 'path': path, 'format': 'ini'},
        },
        {
            'uses': 'actions/create-file@v1',
            'with': {'data': data, 'path': file_absolute_path, 'format': 'ini'},
        },
        *core.export_variables(
            {OPENTF_PARAM_FILE: path, OPENTF_PARAM_FILE_ABSOLUTE: file_absolute_path}
        ),
    ]

    return steps


########################################################################
# 'mvntest' action


def mvntest_action(inputs):
    """Process 'mvntest' action.

    `mvntest` actions have a mandatory `test` input.  It may contain
    a properties input too.

    ```yaml
    - uses: junit/mvntest@v1
      with:
        test: class#method
        properties:
          foo: value1
          bar: value2
        extra-options: --name value
    ```

    This action will attach all `*.txt` and `*.xml` files found in the
    `target/surefire-reports` directory.
    """
    core.debug('Identified as the "mvntest" action')

    test = inputs['test']
    if not core.runner_on_windows():
        test = test.replace("$", "\\$")

    if props := inputs.get('properties'):
        properties = ' '.join(f'-D{k}={v}' for k, v in props.items())
    else:
        properties = ''
    if extra_options := inputs.get('extra-options'):
        extra_options_arg = extra_options
    else:
        extra_options_arg = ''

    txt_reports = 'target/surefire-reports/*.txt'
    xml_reports = 'target/surefire-reports/TEST-*.xml'
    steps = [
        {'uses': 'actions/delete-file@v1', 'with': {'path': txt_reports}},
        {'uses': 'actions/delete-file@v1', 'with': {'path': xml_reports}},
        {
            'run': f'mvn -Dtest={test} {properties} -DfailIfNoTests=true {extra_options_arg} test',
            'continue-on-error': True,
        },
        {
            'uses': 'actions/get-files',
            'with': {
                'pattern': '*.txt',
            },
            'working-directory': 'target/surefire-reports',
        },
        {
            'uses': 'actions/get-files',
            'with': {
                'pattern': '*.xml',
                'type': REPORT_TYPE,
            },
            'working-directory': 'target/surefire-reports',
        },
    ]

    return steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    MVNTEST_CATEGORY: mvntest_action,
}
