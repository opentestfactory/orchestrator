# Copyright (c) 2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Tracker publisher plugin

Sends the workflow execution results to the tracker issues as notes.

# Inputs

`WorkflowCompleted` events.

# Endpoints

This module exposes one endpoint:

- /inbox (POST)

Whenever calling this endpoint, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

from typing import Any

import os
import sys
import threading
import time

from datetime import datetime, timedelta

from queue import Queue
from urllib.parse import quote

import requests

from flask import request

from opentf.commons import (
    TRACKER_PUBLISHER,
    WORKFLOWCOMPLETED,
    validate_schema,
    read_and_validate,
    make_status_response,
    subscribe,
    unsubscribe,
    make_app,
    run_app,
    get_context_service,
)

from opentf.toolkit import watch_file

from opentf.tools.ctlplugin_gitlab import dispatch_results_by_target_type

########################################################################
## Constants

SCHEMAS = (WORKFLOWCOMPLETED,)

PROJECT_ID = 'gitlab/project-id'
PROJECT_PATH = 'gitlab/project-path'
ISSUE_IID = 'gitlab/issue-iid'
MERGE_REQUEST_IID = 'gitlab/mr-iid'
KEEP_HISTORY = 'gitlab/keep-history'
INSTANCE = 'gitlab/instance'
LABEL = 'gitlab/label'
QUALITYGATE_MODE = 'qualitygate-mode'

GITLAB_LABELS = (
    INSTANCE,
    PROJECT_ID,
    PROJECT_PATH,
    ISSUE_IID,
    MERGE_REQUEST_IID,
    KEEP_HISTORY,
    QUALITYGATE_MODE,
    LABEL,
)


########################################################################
## Helpers

PENDING_QUERIES: Queue[tuple[str, str | None, dict[str, str]]] = Queue()


def info(msg: str, *args) -> None:
    """Log info message."""
    plugin.logger.info(msg, *args)


def debug(msg: str, *args) -> None:
    """Log debug message."""
    plugin.logger.debug(msg, *args)


def warning(msg: str, *args) -> None:
    """Log warning message."""
    plugin.logger.warning(msg, *args)


def error(msg: str, *args) -> None:
    """Log error message."""
    plugin.logger.error(msg, *args)


class TrackerPublisherException(Exception):
    """Base trackerpublisher exceptions."""

    def __init__(self, msg, details=None):
        self.msg = msg
        self.details = details


class WorkflowFailure(TrackerPublisherException):
    """Workflow failure exception."""


########################################################################


def _read_configuration(_, configfile: str | None, schema: str) -> None:
    """Read tracker instances definitions.

    Storing instances definitions in .config['CONFIG'], using the
    following entries:

    - 'instances': a dictionary of name: instance entries
    - 'default_instance': the name of an instance in 'instances'
    """
    try:
        if not configfile:
            return

        if old := 'instances' in plugin.config['CONFIG']:
            del plugin.config['CONFIG']['default_instance']
            del plugin.config['CONFIG']['instances']

        try:
            instances = read_and_validate(configfile, schema)
        except ValueError as err:
            error(
                'Invalid Tracker Publisher configuration file "%s": %s.  Ignoring.',
                configfile,
                str(err),
            )
            return

        instances_by_name = {
            instance['name']: instance for instance in instances['instances']
        }
        if instances['default-instance'] not in instances_by_name:
            error(
                'Invalid Tracker Publisher configuration: default instance "%s" not found in instances.  Ignoring.',
                instances['default-instance'],
            )
            return
        if len(instances_by_name) != len(instances['instances']):
            names = [instance['name'] for instance in instances['instances']]
            duplicates = {name for name in names if names.count(name) > 1}
            error(
                'Duplicated instance names: "%s".  Ignoring.', '", "'.join(duplicates)
            )
            return

        for instance in instances['instances']:
            if instance['type'] != 'gitlab':
                warning(
                    'Found an instance of unsupported type "%s".  Ignoring.',
                    instance['type'],
                )

        if old:
            info('Replacing Tracker Publisher configuration using "%s".', configfile)
        else:
            info('Reading Tracker Publisher configuration from "%s".', configfile)

        config = plugin.config['CONFIG']
        config['instances'] = instances_by_name
        config['default_instance'] = instances['default-instance']
    except Exception as err:
        error(
            'Error while reading "%s" Tracker Publisher configuration: %s.',
            configfile,
            str(err),
        )


def _get_qualitygate_mode(
    labels: dict[str, str], instance: dict[str, Any]
) -> dict[str, str]:
    if mode := labels.get(QUALITYGATE_MODE):
        return {'mode': mode}
    if mode := instance.get('default-qualitygate'):
        return {'mode': mode}
    debug('Quality gate mode not found.  Applying "strict" mode by default.')
    return {'mode': 'strict'}


def _get_instance(labels: dict[str, str]) -> dict[str, Any] | None:
    config = plugin.config['CONFIG']
    default = config['default_instance']
    specified_instance = labels.get(INSTANCE, default)
    if specified_instance == 'default':
        specified_instance = default

    if specified_instance not in config['instances']:
        error(
            'Tracker instance "%s" not found in configuration file.', specified_instance
        )
        return None
    if config['instances'][specified_instance]['type'] != 'gitlab':
        error(
            'Tracker instance "%s" is not of type "gitlab", ignoring.',
            specified_instance,
        )
        return None
    return config['instances'][specified_instance]


def _is_workflow_done(response: dict[str, Any]) -> bool | None:
    status = response['details']['status']
    if status == 'RUNNING':
        return False
    if status in ('NOTEST', 'FAILED') and not response['details'].get('rules'):
        raise WorkflowFailure('Workflow failed or contains no test results.  Ignoring')
    return True


def _warn_if_empty_labels(labels: dict[str, str]) -> None:
    for name, value in labels.items():
        if not value:
            warning('Workflow label "%s" has an empty value.', name)


def publish_as_issue_note(
    response: dict[str, Any],
    labels: dict[str, str],
    mode: str,
    instance: dict[str, Any],
) -> None:
    """Publish qualitygate response as issue note on the
    specified GitLab instance.

    # Required parameters:

    - response: a dictionary
    - labels: a dictionary of strings
    - mode: a string
    - instance: a dictionary

    """

    data = {
        'rules': response['details']['rules'],
        'status': response['details']['status'],
    }

    if labels.get(PROJECT_PATH):
        project = quote(labels[PROJECT_PATH], safe='')
    else:
        project = labels[PROJECT_ID]

    gl_params = {
        'server': instance['endpoint'],
        'project': project,
        'token': instance.get('token'),
        'label': labels.get(LABEL),
    }
    if labels.get(ISSUE_IID):
        gl_params['issue'] = labels[ISSUE_IID]
    if labels.get(MERGE_REQUEST_IID):
        gl_params['mr'] = labels[MERGE_REQUEST_IID]

    gl_params['keep_history'] = labels.get(KEEP_HISTORY, instance['keep-history'])

    debug('Publishing quality gate "%s" results to GitLab.', mode)

    dispatch_results_by_target_type(gl_params, mode, data)


def query_and_publish_qualitygate_status(
    qualitygate_url: str,
    labels: dict[str, str],
    instance: dict[str, Any],
    headers: dict[str, str] | None,
) -> None:
    """Query qualitygate and publish its result on the
    specified GitLab instance.

    # Required parameters:

    - qualitygate_url: a string
    - labels: a dictionary of strings
    - instance: a dictionary
    - headers: a dictionary or None

    """
    params = _get_qualitygate_mode(labels, instance)
    mode = params['mode']

    start = datetime.now()
    while True:
        response = requests.get(qualitygate_url, headers=headers, params=params)
        result = response.json()

        if response.status_code == 200:
            if _is_workflow_done(result):
                publish_as_issue_note(result, labels, mode, instance)
                break
            time.sleep(1)
        else:
            error(
                'Failed to apply the quality gate "%s".  Error code %d, message: %s',
                mode,
                response.status_code,
                result['message'],
            )
            break
        if datetime.now() - start > timedelta(seconds=3600):
            error('Failed to apply the quality gate "%s".  Request timed out.', mode)
            break


def handle_completed_workflow():
    """Handle completed workflow containing gitlab labels."""
    qualitygate = get_context_service(plugin, 'qualitygate')

    while True:
        try:
            workflow_id, token, labels = PENDING_QUERIES.get()

            if not plugin.config['CONFIG'].get('instances'):
                warning(
                    'No Tracker instances configured, not attempting to publish result to tracker.'
                )
                continue

            instance = _get_instance(labels)
            if not instance:
                error(
                    'Could not find instance "%s", not attempting to publish result to tracker.',
                    labels.get(INSTANCE),
                )
                continue

            headers = {'Authorization': token} if token else None
            url = f'{qualitygate['endpoint']}/workflows/{workflow_id}/qualitygate'

            query_and_publish_qualitygate_status(url, labels, instance, headers)
        except Exception as err:
            error('Internal error while publishing qualitygate results: %s.', str(err))


########################################################################
## Main

plugin = make_app(
    name='trackerpublisher',
    description='Create and start a tracker publisher plugin.',
    descriptor='plugin.yaml',
)


@plugin.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new message."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict):
        return make_status_response('BadRequest', 'Body must be a JSON object.')

    if 'apiVersion' not in body or 'kind' not in body:
        return make_status_response('BadRequest', 'Missing apiVersion or kind.')

    schema = f'{body['apiVersion']}/{body['kind']}'
    if schema not in SCHEMAS:
        return make_status_response(
            'BadRequest',
            f'Unexpected schema: expected {', '.join(SCHEMAS)}, got {schema}.',
        )

    valid, extra = validate_schema(schema, body)
    if not valid:
        return make_status_response(
            'BadRequest', f'Not a valid {body['kind']} request: {extra}.'
        )

    labels = {
        label: value
        for label, value in body['metadata'].get('labels', {}).items()
        if label in GITLAB_LABELS
    }

    if labels:
        if not (
            (ISSUE_IID in labels or MERGE_REQUEST_IID in labels)
            and (PROJECT_ID in labels or PROJECT_PATH in labels)
        ):
            error(
                f'Workflow missing mandatory gitlab labels. Expected: "{PROJECT_ID}" or "{PROJECT_PATH}", "{ISSUE_IID}" or "{MERGE_REQUEST_IID}".'
            )
            return make_status_response(
                'OK', 'Workflow missing mandatory gitlab labels.'
            )
        debug('Adding workflow to the queue.')
        _warn_if_empty_labels(labels)

        workflow_id = body['metadata']['workflow_id']
        token = request.headers.get("Authorization")
        PENDING_QUERIES.put((workflow_id, token, labels))

    return make_status_response('OK', '')


def main(plugin):
    """Start the Tracker Publisher plugin."""
    _ = get_context_service(plugin, 'qualitygate')

    if os.environ.get('TRACKERPUBLISHER_INSTANCES'):
        watch_file(
            plugin,
            os.environ['TRACKERPUBLISHER_INSTANCES'],
            _read_configuration,
            TRACKER_PUBLISHER,
        )
    else:
        warning(
            'No tracker instances file provided.  If you want to publish results to a tracker, you must provide a list of instances in a file refered to by the TRACKERPUBLISHER_INSTANCES instance variable.  The service is disabled.'
        )

    try:
        debug('Starting completed workflows handling thread.')
        threading.Thread(target=handle_completed_workflow, daemon=True).start()
    except Exception as err:
        error('Could not start completed workflows handling thread: %s.', str(err))
        sys.exit(2)

    try:
        debug(f'Subscribing to {", ".join(SCHEMAS)}.')
        subs = [
            subscribe(kind=schema, target='/inbox', app=plugin) for schema in SCHEMAS
        ]
    except Exception as err:
        error('Could not subscribe to eventbus: %s.', str(err))
        sys.exit(2)

    try:
        debug('Starting plugin.')
        run_app(plugin)
    finally:
        for sub in subs:
            unsubscribe(sub, app=plugin)


if __name__ == '__main__':
    main(plugin)
