# Copyright (c) 2021-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A dummy event rewriter plugin.

This rewriter plugin translates events from one format to another format.

# Usage

```
python3 -m opentf.plugins.rewriter.main [--context context] [--config configfile]
```


# Endpoints

This module exposes one endpoint:

- /inbox (POST)

Whenever calling this endpoint, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

import os
import sys


from flask import request

from opentf.commons import (
    ALLURE_COLLECTOR_OUTPUT,
    WORKFLOWRESULT,
    NOTIFICATION,
    validate_schema,
    make_status_response,
    subscribe,
    unsubscribe,
    make_app,
    run_app,
    make_dispatchqueue,
    make_event,
)

########################################################################
## Constants

REWRITTEN_MARK = 'opentestfactory.org/rewritten'


########################################################################


def aco(_, body, workflow_id):
    """Generate a WorkflowResult event from an AllureCollectorOutput one."""
    with_ = body.get('with') or {}
    if report := with_.get('allureReport'):
        if os.path.exists(report):
            DISPATCH_QUEUE.put(
                make_event(
                    WORKFLOWRESULT,
                    metadata={
                        'name': 'WorkflowResult generated from AllureCollectorOutput',
                        'workflow_id': workflow_id,
                        REWRITTEN_MARK: True,
                    },
                    attachments=[report],
                )
            )
        else:
            DISPATCH_QUEUE.put(
                make_event(
                    NOTIFICATION,
                    metadata={
                        'name': 'Rewriter: allure-report.tar not found',
                        'workflow_id': workflow_id,
                        REWRITTEN_MARK: True,
                    },
                    spec={
                        'logs': [
                            f'[ERROR] Attachment {report} from AllureCollectorOutput not found.'
                        ]
                    },
                ),
            )


########################################################################
## Main

app = make_app(
    name='rewriter',
    description='Create and start a rewriter plugin.',
    descriptor='plugin.yaml',
)

DISPATCH_QUEUE = make_dispatchqueue(app)

SUBSCRIPTIONS = {
    ALLURE_COLLECTOR_OUTPUT: aco,
}


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new result message."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict):
        return make_status_response(
            'BadRequest', 'Could not parse body, was expecting a dictionary.'
        )

    if 'kind' not in body or 'apiVersion' not in body:
        return make_status_response('BadRequest', 'Missing kind or apiVersion.')

    event = f'{body["apiVersion"]}/{body["kind"]}'
    if event not in SUBSCRIPTIONS:
        return make_status_response('BadRequest', f'Unexpected event {event}.')
    valid, extra = validate_schema(event, body)
    if not valid:
        return make_status_response(
            'BadRequest', f'Not a valid {body["kind"]} request: {extra}'
        )

    if REWRITTEN_MARK not in body['metadata']:
        workflow_id = body['metadata']['workflow_id']
        SUBSCRIPTIONS[event](event, body, workflow_id)

    return make_status_response('OK', '')


def main():
    """Start the Local Publisher service."""
    try:
        app.logger.debug('Subscribing to %s.', ', '.join(SUBSCRIPTIONS))
        uuids = [
            subscribe(kind=event, target='/inbox', app=app) for event in SUBSCRIPTIONS
        ]
    except Exception as err:
        app.logger.error('Could not subscribe to eventbus: %s', str(err))
        sys.exit(2)

    try:
        app.logger.debug('Starting service.')
        run_app(app)
    finally:
        for uuid in uuids:
            unsubscribe(uuid, app=app)


if __name__ == '__main__':
    main()
