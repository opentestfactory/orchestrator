# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An automata plugin (skf).

Provides default implementation for skf actions.

# Usage

```
python3 -m opentf.plugins.skf.main [--context context] [--config configfile]
```


# Endpoints

This module exposes one endpoint:

- /inbox (POST)

Whenever calling this endpoint, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

from opentf.toolkit import make_plugin, run_plugin

from .implementation import KNOWN_CATEGORIES


########################################################################
## Main

plugin = make_plugin(
    name='skf',
    description='Create and start a skf provider.',
    providers=KNOWN_CATEGORIES,
)


if __name__ == '__main__':
    run_plugin(plugin)
