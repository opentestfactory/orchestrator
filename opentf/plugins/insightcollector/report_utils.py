# Copyright (c) 2024 Henix, Henix.fr

"""Common methods and constants for XML and HTML report generators."""

import logging
import os

from typing import Any, Callable, Dict, List, Optional, Tuple

from collections import defaultdict

from opentf.plugins.insightcollector.exceptions import InsightCollectorError
from opentf.commons import WORKFLOWRESULT, make_event, make_uuid

########################################################################
## Constants

FAILURE = 'failure'
ERROR = 'error'
SKIPPED = 'skipped'
SUCCESS = 'success'
TOTAL = 'total count'

STATUSES_KO = (FAILURE, ERROR, SKIPPED)

STATUSES_ORDER = (SUCCESS, FAILURE, ERROR, SKIPPED)

RESOURCE_CSS = 'css'
RESOURCE_JS = 'js'

RESOURCES_PATHS = {RESOURCE_CSS: 'stylesheet.css', RESOURCE_JS: 'scripts.html'}

########################################################################
## Methods

# Handle resources (CSS & scripts)


def get_resources(resource_kind: str) -> str:
    """Read file from resource and return its content."""
    resource = ''
    try:
        script_path = os.path.abspath(__file__)
        resource_path = os.path.join(
            os.path.dirname(script_path), f'resources/{RESOURCES_PATHS[resource_kind]}'
        )
        with open(resource_path, 'r', encoding='utf-8') as file:
            resource = file.read()
    except Exception as err:
        logging.debug('Failed to import report resource file: %s.', str(err))
    return resource


# Handle test results


def _get_path(src: Dict[str, Any], path: List[str]) -> Any:
    if not path:
        return src
    try:
        return _get_path(src[path[0]], path[1:])
    except (KeyError, TypeError) as err:
        raise KeyError(f"Invalid path: {'.'.join(path)}") from err


def get_sorted_testcases(
    testcase_metadata: Dict[str, Any], path: List[str]
) -> Dict[str, Any]:
    sorted_testcases = {}
    for testcase, data in testcase_metadata.items():
        sorted_testcases.setdefault(_get_path(data, path), {})[testcase] = data
    return sorted_testcases


def parse_testcase_name(full_name: str) -> Tuple[str, str]:
    """Parse test case name from testResults notification.

    full_name is a string: classname#testcase name

    # Returned value

    A tuple of two strings: suite and test case name. If one
    of strings is empty, returns not empty element value instead.
    """
    suite, _, name = full_name.partition('#')
    return suite or name, name or suite


def _get_sum_for_status(testcases: Dict[str, Any], status: str) -> int:
    return sum(
        1 for testcase in testcases.values() if testcase['test']['outcome'] == status
    )


def summarize_test_results(testcases: Dict[str, Any]) -> Dict[str, Any]:
    """Summarize test results for a test cases dictionary."""
    successes, failures, errors, skipped = [
        _get_sum_for_status(testcases, status) for status in STATUSES_ORDER
    ]
    return {
        SUCCESS: successes,
        FAILURE: failures,
        ERROR: errors,
        SKIPPED: skipped,
        TOTAL: len(testcases),
    }


def _evaluate_test_results(jobs_testcases: Dict[str, Any]) -> Dict[str, Dict[str, int]]:
    """Summarize job testcases.

    # Returned value

    A dictionary with one entry per job (a dictionary with keys being
    statuses and values being counts).
    """
    summaries = {}
    for job, testcases in jobs_testcases.items():
        successes, failures, errors, skipped = [
            _get_sum_for_status(testcases, status) for status in STATUSES_ORDER
        ]
        summaries[job] = {
            SUCCESS: successes,
            FAILURE: failures,
            ERROR: errors,
            SKIPPED: skipped,
            TOTAL: len(testcases),
        }
    return summaries


def evaluate_test_results(jobs_testcases: Dict[str, Any]) -> Dict[str, Any]:
    """Summarize job testcases.

    # Returned value

    A dictionary with one entry per job (a dictionary with keys being
    statuses and values being counts), with an additional 'TOTAL' entry
    summing per statuses.
    """
    summaries = _evaluate_test_results(jobs_testcases)
    total = defaultdict(int)
    for summary in summaries.values():
        for k, v in summary.items():
            total[k] += v
    summaries[TOTAL] = total
    return summaries


# Handle content stream


def make_workflowresult_event(
    content: Callable,
    name: str,
    report_type: str,
    extension: str,
    workflow_id: str,
    request_id: Optional[str],
) -> Dict[str, Any]:
    """Creates a file from content and makes a WorkflowResult event."""
    uuid = make_uuid()
    target = f'/tmp/{workflow_id}-{uuid}_WR_{name}.{extension}'
    try:
        with open(target, 'w', encoding='utf-8') as f:
            content(f)

        metadata = {
            'name': name,
            'workflow_id': workflow_id,
            'request_id': request_id,
            'attachments': {target: {'type': report_type, 'uuid': uuid}},
        }
        return make_event(WORKFLOWRESULT, metadata=metadata, attachments=[target])
    except Exception as err:
        raise InsightCollectorError(
            f'Failed to publish report {target} for workflow {workflow_id}: {err}.'
        )
