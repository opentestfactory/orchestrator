# Copyright (c) 2023-2024 Henix, Henix.fr

"""A simple report generator for machines that can read xml."""

from typing import Any, Dict, List, Optional

import xml.etree.ElementTree as ET

from opentf.plugins.insightcollector.report_utils import (
    make_workflowresult_event,
    evaluate_test_results,
    TOTAL,
    FAILURE,
    ERROR,
    SKIPPED,
    STATUSES_KO,
)
from opentf.plugins.insightcollector.exceptions import XmlReportError


########################################################################
## Constants and helpers

XML_REPORT_TYPE = 'application/vnd.opentestfactory.executionreport+xml'


########################################################################
## XML report


def _evaluate_durations(jobs_testcases: Dict[str, Any]) -> Dict[str, int]:
    """Collect job and total durations."""
    durations = {'total_time': 0}
    for job, testcases in jobs_testcases.items():
        durations[job] = 0
        for data in testcases.values():
            time = data['execution'].get('duration')
            if isinstance(time, int):
                durations[job] += time
        durations['total_time'] += durations[job]
    return durations


def _format_counter(nr: int):
    formatted = str(nr)
    if nr < 10:
        formatted = f'00{nr}'
    elif nr < 100:
        formatted = f'0{nr}'
    return f'[{formatted}] '


def _get_job_suite_testcases(testcases: Dict[str, Any]) -> Dict[str, Any]:
    """Collect jobsuites metadata.

    `testcases` is a dict of entries like:

    ```python
    {
        '<<<uuid>>>': {
            'name': '<<<Test Statuses#Successful Test 1>>>',
            'status': 'SUCCESS',
            'duration': 1,
            'test': {
                'job': '<<<job>>>',
                'uses': 'robotframework/execute@v1',
                'technology': 'robotframework',
                'runs-on': ["robotframework", "junit", "linux", "cypress"],
                'managed': False
            },
            'errorsList': [...],
        }
    }
    ```

    Recompose `testcases` as:

    ```python
    {
        '<<<job_id>>>.<<<job>>>.<<<suite>>>': {
            uuid: { **testcases value, 'short_name': <<<short_name>>>},
            ...
        }
    }
    ```

    Where `suite` is first part (before '#') of testcase name and
    `short_name` is the rest.
    """
    job_suite_testcase = {}
    for nr, (uuid, data) in enumerate(testcases.items()):
        test = data['test']
        suite, name = test['suiteName'], test['testCaseName']
        uuid_job_suite = f'{data["metadata"]["job_id"]}.{test["job"]}.{suite}'
        job_suite_testcase.setdefault(uuid_job_suite, {})[uuid] = data
        job_suite_testcase[uuid_job_suite][uuid]['short_name'] = name
        job_suite_testcase[uuid_job_suite][uuid]['counter'] = _format_counter(nr)
    return job_suite_testcase


def _maybe_get_robot_errorslist(job_suite_testcases: Dict[str, Any]) -> Dict[str, List]:
    return {
        job: data['execution']['errorsList']
        for job, testcases in job_suite_testcases.items()
        for data in testcases.values()
        if data['test']['technology'] == 'robotframework'
        and data['execution'].get('errorsList')
    }


########################################################################
# XML helpers


def _make_testsuites_element(
    workflow_id: str, results: Dict[str, Any], durations: Dict[str, Any]
) -> ET.Element:
    return ET.Element(
        'testsuites',
        {
            'id': workflow_id,
            'name': f'Test results for the workflow {workflow_id}',
            'tests': str(results[TOTAL][TOTAL]),
            'failures': str(results[TOTAL][FAILURE]),
            'errors': str(results[TOTAL][ERROR]),
            'skipped': str(results[TOTAL][SKIPPED]),
            'time': str(durations.get('total_time', 0) / 1000),
        },
    )


def _append_testsuite_element(
    testsuites: ET.Element, job: str, results: Dict[str, Any], durations: Dict[str, Any]
) -> ET.Element:
    job_id, _, name = job.partition('.')
    return ET.SubElement(
        testsuites,
        'testsuite',
        {
            'id': job_id,
            'name': name,
            'tests': str(results[job][TOTAL]),
            'failures': str(results[job][FAILURE]),
            'errors': str(results[job][ERROR]),
            'skipped': str(results[job][SKIPPED]),
            'time': str(durations[job] / 1000),
        },
    )


def _append_testcase_element(
    testsuite: ET.Element, uuid: str, data: Dict[str, Any], job: str
) -> ET.Element:
    testcase = ET.SubElement(
        testsuite,
        'testcase',
        {
            'id': uuid,
            'name': data['short_name'],
            'classname': f'{data["counter"]}{job.partition(".")[2]}',
            'timestamp': data['metadata']['creationTimestamp'],
        },
    )
    time = data['execution'].get('duration')
    if isinstance(time, int) and time != 0:
        testcase.set('time', str(time / 1000))
    return testcase


def _append_failure_element(
    data: Dict[str, Any],
    robot_errorslist: Dict[str, List],
    job: str,
    testcase: ET.Element,
    status: str,
):
    if data['test']['technology'] == 'robotframework' and robot_errorslist.get(job):
        for error in robot_errorslist[job]:
            failure = ET.SubElement(
                testcase,
                'failure',
                {
                    'message': 'Some general error occured during Robot Framework execution',
                    'type': 'GeneralFailure',
                },
            )
            failure.text = error.get('message', 'Unable to get error message')
    if failure_details := (
        data['execution'].get('failureDetails')
        or data['execution'].get('errorDetails')
        or data['execution'].get('warningDetails')
    ):
        failure = ET.SubElement(
            testcase,
            'failure',
            {
                'message': failure_details.get('message', ''),
                'type': failure_details.get('type', ''),
            },
        )
        failure.text = failure_details.get('text', '')
    else:
        failure = ET.SubElement(
            testcase,
            'failure',
            {'message': 'Test failed', 'type': status},
        )
        failure.text = f'Test failed, status: {status}. No failure details retrieved from test report.'


def _create_report_xml(
    job_suite_testcases: Dict[str, Any], workflow_id: str
) -> ET.Element:
    results = evaluate_test_results(job_suite_testcases)
    durations = _evaluate_durations(job_suite_testcases)
    robot_errorslist = _maybe_get_robot_errorslist(job_suite_testcases)
    testsuites = _make_testsuites_element(workflow_id, results, durations)

    for job, testcases in job_suite_testcases.items():
        testsuite = _append_testsuite_element(testsuites, job, results, durations)
        for uuid, data in testcases.items():
            testcase = _append_testcase_element(testsuite, uuid, data, job)
            status = data['test']['outcome']
            if status in STATUSES_KO:
                _append_failure_element(data, robot_errorslist, job, testcase, status)
    return testsuites


########################################################################
# exported function


def prepare_xml_report_event(
    observer_proxy,
    model: Dict[str, Any],
    workflow_id: str,
    request_id: Optional[str],
    default_scope: str,
) -> Dict[str, Any]:
    """Prepare XML execution report event to be published."""
    scope = model.get('spec', {}).get('scope', default_scope)
    testcases = observer_proxy.get_datasource('testcases', scope=scope)
    if not testcases or testcases.get('EMPTY_ITEMS'):
        raise XmlReportError(
            f'No testcase metadata retrieved for the workflow {workflow_id}, not publishing the Surefire execution report.'
        )

    job_suite_testcases = _get_job_suite_testcases(testcases)
    testsuites = _create_report_xml(job_suite_testcases, workflow_id)

    return make_workflowresult_event(
        lambda x: x.write(ET.tostring(testsuites, 'unicode', xml_declaration=True)),
        model['name'],
        XML_REPORT_TYPE,
        'xml',
        workflow_id,
        request_id,
    )
