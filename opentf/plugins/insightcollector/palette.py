# Copyright (c) 2023-2024 Henix, Henix.fr

"""Refinements from zabel palette."""

from typing import Any, Dict, Iterable, List, Tuple


from zabel.palette.html import (
    CHART_DATASET_COLORS,
    DOUGHNUT_COLORS,
    make_table as _make_table,
    make_doughnut as _make_doughnut,
    make_barchart as _make_barchart,
    make_block,
    make_element,
    make_dialog,
    make_action,
)

from zabel.palette.text import safe

## Wrappers


def make_table(source, cols, large=True, searchable=None, width=None, sortable=False):
    style = ''
    if width is not None:
        style += 'width: %dem;' % width
    return _make_table(
        source, cols, large=large, searchable=searchable, width=width, sortable=sortable
    ).replace('<div id="table_', f'<div style="overflow-x: auto; {style}" id="table_')


def make_doughnut(
    title, subtitle, counts, labels, link, colors=None, legend: bool = False
):
    doughnut = _make_doughnut(
        title, subtitle, counts, labels, link, colors=colors, legend=legend
    )
    return (
        doughnut.replace('<script>', '<script>\nsetTimeout(function() {\n')
        .replace('</script>', '}, 100);\n</script>')
        .replace(f"'label': '{title}'", "'label': ' '")
    )


def make_barchart(
    xaxis,
    datasets,
    colors,
    legend: bool = False,
    stacked: bool = False,
    template: Dict[str, Any] = {},
):
    barchart = _make_barchart(
        xaxis, datasets, colors=colors, legend=legend, stacked=stacked
    )
    orientation = 'maintainAspectRatio: false'
    if template.get('orientation', 'horizontal') == 'vertical':
        orientation += ", indexAxis: 'y'"

    barchart = (
        barchart.replace('maintainAspectRatio: false', orientation)
        .replace('x: {', _complete_chart_axis('x', template.get('primaryAxis', {})))
        .replace('y: {', _complete_chart_axis('y', template['secondaryAxis']))
        .replace("'y': {", _maybe_add_min_max('y', template['secondaryAxis']))
    )
    return barchart


########################################################################

### Charts

CHART_TITLE_TICKS_TMPL = '''
    title: {{
        display: {show_title},
        align: 'center',
        text: '{title}'
    }},
    ticks: {{
        display: {show_ticks}
    }},
'''


def _complete_chart_axis(axis: str, conf: Dict[str, Any]) -> str:
    what = f"'{axis}': {{"
    show_title = False
    show_ticks = True
    if title := conf.get('title', ''):
        show_title = True
    if conf.get('ticks') == 'hidden':
        show_ticks = False
    what += CHART_TITLE_TICKS_TMPL.format(
        show_title=str(show_title).lower(),
        title=safe(title),
        show_ticks=str(show_ticks).lower(),
    )
    return what


def _maybe_add_min_max(axis: str, conf: Dict[str, Any]) -> str:
    what = f"'{axis}': {{"
    if min_value := conf.get('min'):
        what += f' min: {min_value},'
    if max_value := conf.get('max'):
        what += f' max: {max_value},'
    return what


########################################################################

SCATTER_DATASET_TMPL = '''{{
    label: "{title}",
    backgroundColor: "{c}",
    borderColor: "{c}",
    pointBackgroundColor: "{c}",
    pointBorderColor: "#fff",
    pointHoverBackgroundColor: "#fff",
    pointHoverBorderColor: "rgba(220,220,220,1)",
    fill: true,
    data: {data}
}}'''

SCATTERCHART_TMPL = '''<div style="width: 700px; height: 250px"><canvas id="{id}"></canvas></div>
       <script>
       var data = {{datasets: [{datasets}]}};
       var yTicks = {y_ticks}
       var ctx = document.getElementById("{id}").getContext("2d"); var myLineChart = new Chart(ctx, {{
           type: 'scatter',
           data: data,
           options: {{
               maintainAspectRatio: false,
               plugins: {{
                    legend: {{display: {legend}}},
                }},
               scales: {{
                    x: {{
                        type: 'linear',
                        position: 'bottom',
                    }},
                    y: {{
                        ticks: {{
                            callback: function(value, index, ticks){{
                                return yTicks[value] !== undefined ? yTicks[value] : '';
                            }},
                        stepSize: 1
                        }}
                    }}
               }}
           }}
       }});
       </script>'''

_scatterchart_cpt = 0


def make_scatterchart(
    datasets: List[Tuple[str, Iterable]],
    y_ticks: List[str],
    template: Dict[str, Any],
    colors: List[Tuple[str, None]],
    legend: bool = False,
) -> str:
    """Return HTML-ready representation for scatter chart.

    - `datasets` is an iterable of (title, data) pairs

        `title` is the dataset title
        `data` is a list of numerical values, in order

    - `y_ticks` is a list of y-axis ticks
    - `template` is a dictionary containing primary axis parameters.

    It is up to the caller to sanitize the parameters, if needed.
    """
    global _scatterchart_cpt

    _scatterchart_cpt += 1
    data = ",".join(
        SCATTER_DATASET_TMPL.format(title=title, data=data, c=c, h=h)
        for (title, data), (c, h) in zip(datasets, colors * len(datasets))
    )
    scatterchart = SCATTERCHART_TMPL.format(
        datasets=data,
        y_ticks=y_ticks,
        id="scatterchart_%d" % _scatterchart_cpt,
        legend=str(legend).lower(),
    )
    return scatterchart.replace(
        'x: {', _maybe_add_min_max('x', template['primaryAxis'])
    ).replace("'x': {", _complete_chart_axis('x', template['primaryAxis']))
