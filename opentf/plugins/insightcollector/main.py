# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A simple workflow insights collector plugin.

Joins workflow execution reports to workflows.

By default generates three reports: `executionlog.txt`,
`executionreport.html`, and `executionreport.xml`.

# Inputs

`Workflow`, `WorkflowCompleted`, and `WorkflowCanceled` events.

# Endpoints

This module exposes two endpoints:

- /inbox (POST)
- /workflows/{workflow_id}/insights (POST)

Whenever calling those endpoints, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

from typing import Any, Callable, Iterable

import fnmatch
import os
import sys
import threading

from collections import defaultdict
from datetime import datetime, timedelta
from queue import Queue
from time import sleep, time

import requests
import yaml

from flask import request, Response


from opentf.commons import (
    INSIGHT_COLLECTOR,
    NOTIFICATION,
    WORKFLOW,
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
    WORKFLOWRESULT,
    authorizer,
    validate_schema,
    read_and_validate,
    is_uuid,
    make_status_response,
    make_uuid,
    publish,
    subscribe,
    unsubscribe,
    make_app,
    make_event,
    run_app,
    get_context_service,
    get_context_parameter,
)

from opentf.commons.expressions import evaluate_bool

from opentf.toolkit import watch_file

from opentf.plugins.insightcollector.exceptions import (
    InsightCollectorError,
    ScopeError,
    XmlReportError,
)
from opentf.plugins.insightcollector.htmlreport import prepare_html_report_event
from opentf.plugins.insightcollector.xmlreport import prepare_xml_report_event
from opentf.plugins.insightcollector.executionlog import (
    prepare_executionlog_event,
    OUTPUTCONTEXT_PARAMETERS,
)
from opentf.plugins.insightcollector.report_utils import RESOURCE_CSS, get_resources


type Contexts = dict[str, Any]
type Event = dict[str, Any]
type Model = dict[str, Any]
type Spec = dict[str, Any]
type ReportsTask = tuple[str, str | None, Contexts, list[Model], str | None]
type WorkflowResult = dict[str, Any]

########################################################################
## Constants

SUBSCRIPTIONS = (WORKFLOW, WORKFLOWCOMPLETED, WORKFLOWCANCELED)
WORKFLOWS_META_EVENTS: dict[str, list[Event]] = defaultdict(list)


DEFAULT_TIMEOUT = 10  # for requests
OBSERVER_PROXY_TIMEOUT = 15 * 60
OBSERVER_PROXY_WAIT = 5
REPORTS_CLEANUP_DELAY = 3600  # in seconds
WORKFLOWNOTDONE_DELAY = 5  # in seconds

TIMEOUT_SENTINEL = '__timeout sentinel__'

PENDING_REPORTS: dict[str, float] = {}
WORKERS: dict[str, str] = {}

PENDING_LOCK = threading.Lock()

PENDING_REPORTSTASKS: Queue[ReportsTask] = Queue()

ERROR_LOG_TYPE = 'application/vnd.opentestfactory.errorlog+text'

EXECUTIONLOG_NAME = 'executionlog'
HTML_REPORT_NAME = 'executionreport'
XML_REPORT_NAME = 'executionreport'

EXECUTIONLOG_KIND = 'ExecutionLog'
EXECUTIONLOG_DEFAULTS: Model = {
    'name': EXECUTIONLOG_NAME,
    'kind': EXECUTIONLOG_KIND,
    'spec': OUTPUTCONTEXT_PARAMETERS,
}

HTML_REPORT_KIND = 'SummaryReport'
DEFAULT_HTML_REPORT_SPEC: Spec = {
    'template': [
        {'name': 'Summary', 'kind': 'SummaryBlock'},
        {'name': 'Jobs', 'kind': 'Cloud', 'datasource': 'jobs'},
        {'name': 'Tags', 'kind': 'Table', 'datasource': 'tags'},
        {'name': 'Test Cases', 'kind': 'Table', 'datasource': 'testcases'},
        {
            'name': 'Execution Times by Technology',
            'kind': 'ProgressBar',
            'datasource': 'testcases',
            'groupBy': 'test.technology',
            'data': 'execution.duration',
            'aggregateFunction': 'sum',
        },
        {
            'name': 'Test Cases Execution Time',
            'kind': 'BarChart',
            'datasource': 'testcases',
            'secondaryAxis': {'data': 'execution.duration'},
            'primaryAxis': {
                'ticks': 'hidden',
                'title': 'Test Cases',
                'data': 'test.testCaseName',
            },
        },
    ]
}
HTML_REPORT_DEFAULTS: Model = {
    'name': HTML_REPORT_NAME,
    'kind': HTML_REPORT_KIND,
    'spec': DEFAULT_HTML_REPORT_SPEC,
}

XML_REPORT_KIND = 'SurefireXmlReport'
XML_REPORT_DEFAULTS: Model = {'name': XML_REPORT_NAME, 'kind': XML_REPORT_KIND}

KINDS_DEFAULT_SPECS: dict[str, Spec] = {
    EXECUTIONLOG_KIND: OUTPUTCONTEXT_PARAMETERS,
    HTML_REPORT_KIND: DEFAULT_HTML_REPORT_SPEC,
}

KINDS_DEFAULT_MODELS: dict[str, Model] = {
    EXECUTIONLOG_KIND: EXECUTIONLOG_DEFAULTS,
    HTML_REPORT_KIND: HTML_REPORT_DEFAULTS,
    XML_REPORT_KIND: XML_REPORT_DEFAULTS,
}


KINDS_PREPARE_EVENT: dict[str, Callable[..., WorkflowResult]] = {
    EXECUTIONLOG_KIND: prepare_executionlog_event,
    HTML_REPORT_KIND: prepare_html_report_event,
    XML_REPORT_KIND: prepare_xml_report_event,
}

IMG_INSIGHTS: list[Model] = []
IMG_CSS: list[str] = []


########################################################################
## Helpers


def info(msg: str, *args: Any) -> None:
    """Log info message."""
    plugin.logger.info(msg, *args)


def debug(msg: str, *args: Any) -> None:
    """Log debug message."""
    plugin.logger.debug(msg, *args)


def warning(msg: str, *args: Any) -> None:
    """Log warning message."""
    plugin.logger.warning(msg, *args)


def error(msg: str, *args: Any) -> None:
    """Log error message."""
    plugin.logger.error(msg, *args)


########################################################################
## Configuration files


def _filter_listdir(path: str, kinds: tuple[str, ...]) -> list[str]:
    """listdir-like, filtering for files with specified extensions."""
    files = [
        f
        for f in os.listdir(path)
        if os.path.isfile(os.path.join(path, f)) and f.endswith(kinds)
    ]
    if not files:
        debug('No %s files provided in %s.', ', '.join(kinds), path)
    return sorted(files)


def _deduplicate(models: Iterable[Model]) -> list[Model]:
    """Remove duplicates, if any.

    The first definition of a model wins.

    # Required parameters

    - models: a list of dictionaries (models), in decreasing priority
      order.

    # Returned value

    A list of models with no duplicates.
    """
    seen, duplicate, unique_models = set(), set(), []
    for model in models:
        identifier = (model['name'], model['kind'])
        if identifier not in seen:
            seen.add(identifier)
            unique_models.append(model)
        else:
            duplicate.add(identifier)
    if duplicate:
        for item in duplicate:
            warning(
                'Duplicate insight definitions found for %s "%s", only the definition with the highest priority will be used.',
                item[1],
                item[0],
            )
    return unique_models


def _read_insights(configfile: str, schema: str) -> list[dict[str, Any]] | None:
    """Return insights from configuration file.

    Insights from file, if any, are returned in reverse order, so that
    the last definition in a file has the highest priority..
    """
    try:
        insights = read_and_validate(configfile, schema)
    except ValueError as err:
        error(
            'Invalid Insight Collector configuration file "%s": %s.  Ignoring.',
            configfile,
            str(err),
        )
        return None

    return list(reversed(insights['insights']))


def _load_image_models() -> None:
    """Load insights models from `CONFIG_PATH` directory.

    Entries in IMG_INSIGHTS are de-duplicated.
    """
    models: list[Model] = []
    for config_file in reversed(_filter_listdir(CONFIG_PATH, ('.yaml', '.yml'))):
        filepath = os.path.join(CONFIG_PATH, config_file)
        try:
            if not (insights := _read_insights(filepath, INSIGHT_COLLECTOR)):
                continue
            debug('Loading models from configuration file "%s".', config_file)
            models.extend(_deduplicate(insights))
        except Exception as err:
            error(
                'Failed to load models from configuration file "%s": %s.',
                config_file,
                str(err),
            )
            sys.exit(2)

    for defaults in KINDS_DEFAULT_MODELS.values():
        models.append(defaults.copy())

    IMG_INSIGHTS.extend(_deduplicate(models))


def _load_image_css() -> None:
    """Fill IMG_CSS with available CSS.

    Scan `{CONFIG_PATH}/css` for CSS files, read them and append their
    content to IMG_CSS.
    """
    try:
        IMG_CSS.append(get_resources(RESOURCE_CSS))
        css_path = os.path.join(CONFIG_PATH, 'css')
        if os.path.isdir(css_path):
            for css_file in _filter_listdir(css_path, ('.css',)):
                filepath = os.path.join(css_path, css_file)
                debug('Reading styles from CSS file "%s".', css_file)
                with open(filepath, 'r', encoding='utf-8') as f:
                    IMG_CSS.append(f.read())
        else:
            debug(
                '"%s" is not an existing directory, cannot retrieve external CSS for HTML reports.',
                css_path,
            )
    except Exception as err:
        error('Could not retrieve CSS for HTML reports: %s.', str(err))


def _load_image_configuration() -> None:
    """Exit with code 2 if CONFIG_PATH is not a directory."""
    if not os.path.isdir(CONFIG_PATH):
        error(
            'Configuration files path "%s" not found or not a directory.', CONFIG_PATH
        )
        sys.exit(2)

    _load_image_models()
    _load_image_css()


def _refresh_configuration(_, configfile: str | None, schema: str) -> None:
    """Read Insight collector configuration.

    Storing insight collector configuration in .config['CONFIG'], using
    the following entry:

    - insights: a dictionary of reports models
    """
    try:
        if not configfile:
            return

        config = plugin.config['CONFIG']
        if old := 'insights' in config:
            del config['insights']

        if not (insights := _read_insights(configfile, schema)):
            return

        if old:
            info('Replacing Insight Collector configuration using "%s".', configfile)
        else:
            info('Reading Insight Collector configuration from "%s".', configfile)

        config['insights'] = _deduplicate(insights)
    except Exception as err:
        error(
            'Error while reading "%s" Insight Collector configuration: %s.',
            configfile,
            str(err),
        )


########################################################################
## Insight Collector

## Handle empty model list


def _format_noinsights_msg(models: list[Model], workflow_id: str) -> str:
    max_name_length = max(len('Name'), max(len(model['name']) for model in models)) + 2
    max_kind_length = max(len('Kind'), max(len(model['kind']) for model in models)) + 2
    max_condition_length = (
        max(len('Condition'), max(len(model.get('if', 'None')) for model in models)) + 2
    )
    header = f'{'Name':<{max_name_length}}\t{'Kind':<{max_kind_length}}\t{'Condition':<{max_condition_length}}'
    rows = [
        f'{model['name']:<{max_name_length}}\t{model['kind']:<{max_kind_length}}\t{model.get('if', 'None'):<{max_condition_length}}'
        for model in models
    ]
    output = '\n'.join([header] + rows)
    return f'No insights were published for workflow {workflow_id}.  Most probably, no provided insight matched its "if" condition.\n===\nProvided insights were:\n\n{output}'


def _publish_noinsights(
    models: list[Model], workflow_id: str, request_id: str | None
) -> None:
    """Publish WorkflowResult event with no insights published attachment when condition-filtered models list is empty."""
    uuid = make_uuid()
    target = f'/tmp/{workflow_id}-{uuid}_WR_NO-INSIGHTS-PUBLISHED.txt'
    content = _format_noinsights_msg(models, workflow_id)
    try:
        with open(target, 'w', encoding='utf-8') as file:
            file.write(content)
        metadata = {
            'name': 'No insights published',
            'workflow_id': workflow_id,
            'request_id': request_id,
            'attachments': {target: {'type': ERROR_LOG_TYPE, 'uuid': uuid}},
        }
        publish(
            make_event(WORKFLOWRESULT, metadata=metadata, attachments=[target]),
            plugin.config['CONTEXT'],
        )
    except Exception as err:
        error('Error publishing error log: %s.', str(err))


## Handle models


def _maybe_set_model_defaults(models: list[Model]) -> list[Model]:
    for model in models:
        if model['kind'] in (EXECUTIONLOG_KIND, HTML_REPORT_KIND):
            model.setdefault('spec', KINDS_DEFAULT_SPECS[model['kind']])
        if model['kind'] == HTML_REPORT_KIND and not model['spec'].get('template'):
            model['spec']['template'] = HTML_REPORT_DEFAULTS['spec']['template']
    return models


def _filter_models_on_condition(models: list[Model], contexts: Contexts) -> list[Model]:
    status: str = contexts['workflow']['status']
    special_functions = {
        'success': status == 'success',
        'failure': status != 'success',
        'cancelled': status == 'cancelled',
        'always': True,
    }
    return [
        item
        for item in models
        if evaluate_bool(
            item.get('if', 'always()'), contexts, special_functions=special_functions
        )
    ]


def _adjust_models(labels: dict[str, str], models: list[Model]) -> list[Model]:
    """Get report models, adjusted with default models, Docker image models
    and label overrides."""
    models_copy = models.copy()
    for model in IMG_INSIGHTS:
        models_copy.append(model.copy())
    what: list[Model] = []
    for item in _deduplicate(models_copy):
        specific_labels = {
            k.partition('/')[2]: v
            for k, v in labels.items()
            if k.startswith(f'{item['name']}/')
        }
        if specific_labels:
            item = item.copy()
            item['spec'] = item.get('spec', {}).copy()
            item['spec'].update(specific_labels)
        what.append(item)
    return what


## Handle contexts


def _get_workflow_creationtimestamp(workflow_id: str) -> str | None:
    try:
        if workflow_event := [
            event
            for event in WORKFLOWS_META_EVENTS[workflow_id]
            if event['kind'] == 'Workflow'
        ]:
            return workflow_event[0]['metadata']['creationTimestamp']
    except Exception as err:
        warning('Could not retrieve Workflow event, got: %s.', err)
    warning(
        'Could not retrieve workflow creation timestamp, respective insight conditions may not be applied.'
    )
    return None


def _make_workflow_context(metadata: dict[str, Any], status: str) -> dict[str, Any]:
    return {
        'workflow_id': metadata['workflow_id'],
        'name': metadata['name'],
        'namespace': metadata.get('namespace'),
        'status': status,
        'completionTimestamp': metadata.get('creationTimestamp'),
    }


def _fetch_workflow_context(workflow_id: str) -> Contexts:
    """Fetch contexts for POST request triggered reports only."""
    events = [
        event
        for event in WORKFLOWS_META_EVENTS.get(workflow_id, [])
        if event['kind'] != 'Workflow'
    ]
    if len(events) != 1:
        raise InsightCollectorError(f'Cannot fetch workflow {workflow_id} context.')
    kind = events[0]['kind']
    metadata = events[0]['metadata']
    return _make_workflow_context(
        metadata, 'success' if kind == 'WorkflowCompleted' else 'failure'
    )


def _make_contexts(metadata: dict[str, Any] | None, status: str) -> Contexts:
    """Create contexts from metadata.

    # Returned value

    A dictionary containing a 'workflow' context and a timeout sentinel.
    """
    contexts: dict[str, Any] = {
        TIMEOUT_SENTINEL: datetime.now() + timedelta(seconds=WORKERS_MAX_WAIT_SECONDS)
    }
    if metadata:
        contexts['workflow'] = _make_workflow_context(metadata, status)
    return contexts


## Handle publishing


class ObserverProxy:
    """An Observer proxy class."""

    def __init__(
        self,
        datasources_url: str,
        events_url: str,
        workflow_id: str,
        headers: dict[str, Any] | None,
    ) -> None:
        self.datasources_url = datasources_url
        self.events_url = events_url
        self.workflow_id = workflow_id
        self.headers = headers
        self.datasources = {'testcases': {}, 'tags': {}, 'jobs': {}}
        self.workflow_event = []
        self.events = []

    def get_datasource(self, kind: str, scope: str = 'true') -> dict[str, Any]:
        """Get data source items."""
        source = self.datasources.get(kind, {}).get(scope)
        if not source:
            source_url = f'{self.datasources_url}/{kind}'
            source = {item['uuid']: item for item in self._query(source_url, scope)}
            self.datasources[kind][scope] = source
        return source

    def get_events(self) -> list[Event]:
        """Get workflow events."""
        if not self.events:
            self.events = self._query(self.events_url)
        return self.events

    def get_workflow_event(self) -> list[Event]:
        """Get Workflow event."""
        if not self.workflow_event:
            self.workflow_event = self._query(self.events_url, 'true', 'kind=Workflow')
        return self.workflow_event

    def _query(
        self, url: str, scope: str = 'true', fieldselector: str | None = None
    ) -> list[Event]:
        """Get workflow events or data source items.

        # Required arguments

        - url: a string, observer's endpoint URL

        # Returned value

        - events: a possibly empty list of workflow events or data
        source items
        """
        params = {'scope': scope, 'fieldSelector': fieldselector}
        try:
            start_time = time()
            while True:
                response = requests.get(
                    url, headers=self.headers, timeout=DEFAULT_TIMEOUT, params=params
                )
                if response.status_code == 202:
                    if time() - start_time > OBSERVER_PROXY_TIMEOUT:
                        raise TimeoutError(
                            'Datasource events caching timeout, aborting'
                        )
                    sleep(OBSERVER_PROXY_WAIT)
                    continue
                if response.status_code == 422 and 'scope_error' in response.json().get(
                    'details'
                ):
                    raise ScopeError(response.json().get('message'))
                if response.status_code == 422 and 'data_error' in response.json().get(
                    'details'
                ):
                    debug(
                        'Could not retrieve datasource data for workflow %s: %s',
                        self.workflow_id,
                        response.json().get('message'),
                    )
                    return [{'uuid': 'EMPTY_ITEMS'}]
                if response.status_code != 200:
                    debug(
                        'Could not retrieve events or datasource for workflow %s: (%d) %s.',
                        self.workflow_id,
                        response.status_code,
                        response.text,
                    )
                    return []
                break

            events = response.json()['details']['items']
            if not events:
                return [{'uuid': 'EMPTY_ITEMS'}]
            try:
                while 'next' in response.links:
                    next_url = (
                        url + '?' + response.links['next']['url'].partition('?')[2]
                    )
                    response = requests.get(
                        next_url, headers=self.headers, timeout=DEFAULT_TIMEOUT
                    )
                    events += response.json()['details']['items']
                return events
            except (ValueError, KeyError) as err:
                error('Could not deserialize observer response: %s.', err)
                debug(response.text)
                return []
        except ScopeError:
            raise
        except Exception as err:
            error(
                'Could not obtain data source items for workflow %s from observer: %s.',
                self.workflow_id,
                err,
            )
            return []


def _maybe_cleanup_pending_reports() -> None:
    """Clean up pending reports dictionary if cleanup delay passed."""
    for workflow_id, timestamp in PENDING_REPORTS.copy().items():
        if timestamp + REPORTS_CLEANUP_DELAY < time():
            for collection in (PENDING_REPORTS, WORKERS, WORKFLOWS_META_EVENTS):
                try:
                    del collection[workflow_id]
                except KeyError:
                    pass


def _publish_worker_status(workflow_id: str, worker: str, status: str) -> None:
    """Publishes worker status notification to a workflow."""
    metadata = {
        'name': 'Insight Collector report publisher',
        'workflow_id': workflow_id,
    }
    spec = {'worker': {'worker_id': worker, 'status': status}}
    try:
        publish(
            make_event(NOTIFICATION, metadata=metadata, spec=spec),
            plugin.config['CONTEXT'],
        )
    except Exception as err:
        debug(
            'Failed to publish worker %s %s notification for workflow %s: %s.',
            worker,
            status,
            workflow_id,
            str(err),
        )


def _maybe_register_worker(workflow_id: str) -> None:
    if workflow_id not in WORKERS:
        WORKERS[workflow_id] = worker_id = make_uuid()
        _publish_worker_status(workflow_id, worker_id, 'setup')


def _publish_insight(
    observer_proxy: ObserverProxy,
    model: Model,
    workflow_id: str,
    contexts: Contexts,
    request_id: str | None,
) -> None:
    """Generate and publish report based on model."""
    args: list[Any] = [observer_proxy, model, workflow_id, request_id]
    kind = model['kind']
    if kind == HTML_REPORT_KIND:
        args.append(contexts)
        args.append(IMG_CSS)
    args.append(DEFAULT_SCOPE)
    try:
        debug(
            'Generating insight %s of kind %s for workflow %s...',
            model['name'],
            kind,
            workflow_id,
        )
        publish(KINDS_PREPARE_EVENT[kind](*args), plugin.config['CONTEXT'])
    except XmlReportError as e:
        info(e.msg)
    except InsightCollectorError as e:
        error(e.msg)


def workflow_is_completed(
    workflow_id: str,
    token: str | None,
    contexts: Contexts,
    models: list[Model],
    request_id: str | None,
    response: requests.Response,
) -> bool:
    """Ensure workflow status is either DONE or FAILED.

    Will defer check if workflow is still running.
    """
    if response.status_code not in (200, 202):
        error(
            'Could not get status for workflow %s, skipping collection: (%d) %s.',
            workflow_id,
            response.status_code,
            response.text,
        )
        return False

    try:
        body = response.json()
    except Exception as err:
        error(
            'Could not parse status body for workflow %s, skipping collection: %s.',
            workflow_id,
            err,
        )
        return False

    if 'details' not in body or 'status' not in body['details']:
        error(
            'Missing .details.status entry in body for workflow %s, skipping collection.',
            workflow_id,
        )
        return False

    details = body['details']
    if details['status'] == 'ONGOING':
        if (
            details.get('workers_count', 0) != 1
            or not details.get('handled', False)
            or response.status_code == 202
        ):
            if details.get('workers_count', 0) != 1:
                if contexts[TIMEOUT_SENTINEL] < datetime.now():
                    warning(
                        'Timeout while waiting for workflow %s workers completion, will attempt to generate insights.',
                        workflow_id,
                    )
                    return True
            info(
                'Workflow %s not completed yet, will retry in %d seconds: status is %s.',
                workflow_id,
                WORKFLOWNOTDONE_DELAY,
                body['details']['status'],
            )
            sleep(WORKFLOWNOTDONE_DELAY)
            PENDING_REPORTSTASKS.put((workflow_id, token, contexts, models, request_id))
            return False
    return True


## Handle datasources retrieval from observer


def collect_and_publish_insights() -> None:
    """Publish result to all matching executions."""
    observer = get_context_service(plugin, 'observer')
    while True:
        workflow_id = '<unknown>'
        worker_id: str | None = None
        try:
            task = PENDING_REPORTSTASKS.get()
            workflow_id, token, contexts, models, request_id = task
            debug('Generating insights for workflow %s.', workflow_id)
            headers = {'Authorization': token} if token else None
            url = f'{observer['endpoint']}/workflows/{workflow_id}/datasources'
            response = requests.get(
                f'{url}/testcases',
                headers=headers,
                timeout=DEFAULT_TIMEOUT,
                params={'page': 1, 'per_page': 1},
            )

            if not workflow_is_completed(*task, response=response):
                continue

            if 'workflow' not in contexts:
                contexts['workflow'] = _fetch_workflow_context(workflow_id)
            contexts['workflow']['creationTimestamp'] = _get_workflow_creationtimestamp(
                workflow_id
            )

            if not request_id:
                worker_id = WORKERS.get(workflow_id)
            if not (filtered_models := _filter_models_on_condition(models, contexts)):
                debug(
                    'Filtered models list is empty, not generating reports for workflow %s.',
                    workflow_id,
                )
                _publish_noinsights(models, workflow_id, request_id)
                if worker_id:
                    _publish_worker_status(workflow_id, worker_id, 'teardown')
                continue

            datasources = ObserverProxy(
                url,
                f'{observer['endpoint']}/workflows/{workflow_id}/status',
                workflow_id,
                headers,
            )
            for model in _maybe_set_model_defaults(filtered_models):
                _publish_insight(datasources, model, workflow_id, contexts, request_id)
        except Exception as err:
            error(
                'Internal error while publishing insights for workflow %s: (%s) %s.',
                workflow_id,
                err.__class__.__name__,
                str(err),
            )
        finally:
            if worker_id:
                _publish_worker_status(workflow_id, worker_id, 'teardown')
        _maybe_cleanup_pending_reports()


def _get_request_insights_definition() -> dict[str, Any]:
    """Get insight definition from request."""
    if request.is_json:
        try:
            return request.get_json() or {}
        except Exception as err:
            error('Could not parse insight definition: %s.', str(err))
    if request.mimetype in ('application/x-yaml', 'text/yaml'):
        if request.content_length and request.content_length < 1000000:
            return yaml.safe_load(request.data)
        raise InsightCollectorError(
            'Content-size must be specified and less than 1000000 for YAML insight definitions.'
        )
    if 'insights' in request.files:
        return yaml.safe_load(request.files['insights'].read())
    raise InsightCollectorError('Could not parse insight definition from file.')


def _maybe_generate_reports(
    workflow_id: str, models: list[Model], pattern: str
) -> Response:
    """Generate reports if the incoming request is valid.

    # Returned value

    A Response object.

    HTTP 200 + details dictionary containing two entries:

    - `request_id`: an UUID
    - `expected`: a list of tuples, each tuple containing name and kind
      of expected report
    """
    models = _deduplicate(
        model for model in reversed(models) if fnmatch.fnmatch(model['name'], pattern)
    )
    request_id = make_uuid()
    if models:
        PENDING_REPORTSTASKS.put(
            (
                workflow_id,
                request.headers.get('Authorization'),
                _make_contexts(None, 'dummy'),
                models,
                request_id,
            )
        )
    return make_status_response(
        'OK',
        f'{len(models)} expected insight{'s' if len(models) != 1 else ''}',
        {
            'request_id': request_id,
            'expected': [(model['name'], model['kind']) for model in models],
        },
    )


########################################################################
## Main

plugin = make_app(
    name='insightcollector',
    description='Create and start an insight collector service.',
    descriptor='plugin.yaml',
)

WORKERS_MAX_WAIT_SECONDS = get_context_parameter(plugin, 'workers_max_wait_seconds')
DEFAULT_SCOPE = get_context_parameter(plugin, 'default_scope')
CONFIG_PATH = get_context_parameter(plugin, 'configurations_path')


@plugin.route('/workflows/<workflow_id>/insights', methods=['POST'])
@authorizer(resource='insights', verb='create')
def process_insight_definition(workflow_id: str) -> Response:
    """Handle insight definition from request.

    # Required query parameter

    - insight: a string, the pattern of the requested insight(s)
    """
    if not is_uuid(workflow_id):
        return make_status_response(
            'Invalid', f'This parameter is not a valid UUID: {workflow_id}.'
        )

    if workflow_id not in WORKFLOWS_META_EVENTS:
        return make_status_response('Invalid', f'Workflow {workflow_id} not found.')

    if not (pattern := request.args.get('insight')):
        return make_status_response('Invalid', 'Missing insight name pattern.')

    try:
        insights = _get_request_insights_definition()
        valid, extra = validate_schema(INSIGHT_COLLECTOR, insights)
        if not valid:
            raise InsightCollectorError(f'Not a valid insight definition: {extra}')
    except Exception as err:
        return make_status_response(
            'Invalid', str(err), details={'workflow_id': workflow_id}
        )

    return _maybe_generate_reports(workflow_id, insights['insights'], pattern)


@plugin.route('/inbox', methods=['POST'])
def process_inbox() -> Response:
    """Process a new result message."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict):
        return make_status_response('BadRequest', 'Body must be a JSON object.')

    if 'apiVersion' not in body or 'kind' not in body:
        return make_status_response('BadRequest', 'Missing apiVersion or kind.')

    schema = f'{body['apiVersion']}/{body['kind']}'
    if schema in SUBSCRIPTIONS:
        valid, extra = validate_schema(schema, body)
        if not valid:
            return make_status_response(
                'BadRequest', f'Not a valid {body['kind']} request: {extra}.'
            )
    else:
        return make_status_response(
            'BadRequest', f'Unexpected kind: {body.get('kind')}.'
        )

    metadata: dict[str, Any] = body['metadata']
    labels = metadata.get('labels', {})
    workflow_id: str = metadata['workflow_id']

    if schema == WORKFLOW:
        _maybe_register_worker(workflow_id)
        WORKFLOWS_META_EVENTS[workflow_id].append(body)
        return make_status_response('OK', '')

    token = request.headers.get('Authorization')
    with PENDING_LOCK:
        if workflow_id in PENDING_REPORTS:
            debug('Reports already generated for workflow %s.', workflow_id)
            return make_status_response(
                'OK', 'Report already generated for this workflow.'
            )
        PENDING_REPORTS[workflow_id] = time()

    contexts = _make_contexts(
        metadata, 'success' if schema == WORKFLOWCOMPLETED else 'failure'
    )
    models = _adjust_models(labels, plugin.config['CONFIG'].get('insights', []))
    WORKFLOWS_META_EVENTS[workflow_id].append(body)
    PENDING_REPORTSTASKS.put((workflow_id, token, contexts, models, None))
    return make_status_response('OK', '')


def main(svc):
    """Start the Insight Collector service."""
    _ = get_context_service(svc, 'observer')

    _load_image_configuration()

    if os.environ.get('INSIGHTCOLLECTOR_CONFIGURATION'):
        watch_file(
            plugin,
            os.environ['INSIGHTCOLLECTOR_CONFIGURATION'],
            _refresh_configuration,
            INSIGHT_COLLECTOR,
        )

    try:
        debug('Starting insights collection thread.')
        threading.Thread(target=collect_and_publish_insights, daemon=True).start()
    except Exception as err:
        error('Could not start insights collection thread: %s.', str(err))
        sys.exit(2)

    try:
        debug(f'Subscribing to {", ".join(SUBSCRIPTIONS)}.')
        subs = [
            subscribe(kind=schema, target='/inbox', app=svc) for schema in SUBSCRIPTIONS
        ]
    except Exception as err:
        error('Could not subscribe to eventbus: %s.', str(err))
        sys.exit(2)

    try:
        debug('Starting service.')
        run_app(svc)
    finally:
        for sub in subs:
            unsubscribe(sub, app=svc)


if __name__ == '__main__':
    main(plugin)
