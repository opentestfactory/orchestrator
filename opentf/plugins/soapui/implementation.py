# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the soapui provider.

The following categories are provided:

- execute
- params
- soapui
"""

from shlex import quote

from opentf.toolkit import core


########################################################################
## Constants

EXECUTE_CATEGORY = 'soapui/execute'
PARAM_CATEGORY = 'soapui/params'
SOAPUI_CATEGORY = 'soapui/soapui'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'

REPORT_TYPE = 'application/vnd.opentestfactory.soapui-surefire+xml'

TEMP_REPORT_FILES = 'temp_report_files'
NO_REPORT_ERROR_MESSAGE = 'No xml report generated.'

SOAPUI_ACTION = 'soapui'

########################################################################
## Local Toolkit


def testsuite_name_filter(testsuite_name: str) -> str:
    """Filter test suite name"""
    new_testsuite_name = ''.join(
        x
        for x in testsuite_name.replace(' ', '_')
        if (x.isalnum() or x == '_') and x != '²'
    )
    return new_testsuite_name


def _handle_no_xml_report(action: str, file: str, folder: str = '') -> str:
    """Check existing file in folder"""
    if folder == '':
        folder = '.'
    else:
        folder = core.normalize_path(folder)
    if core.runner_on_windows():
        exit_or_not = ' & exit 1' if action == SOAPUI_ACTION else ''
        ret = f'''@echo off
              if exist {TEMP_REPORT_FILES} del /f/q {TEMP_REPORT_FILES}
              for /f "tokens=*" %%G in ('dir /b /a:-D "{folder}\\{file}"') do (
              echo %%G>> {TEMP_REPORT_FILES}
              )
             if not exist {TEMP_REPORT_FILES} (
             echo ::error::{NO_REPORT_ERROR_MESSAGE}{exit_or_not}
             )'''
    else:
        exit_or_not = 'exit 1;' if action == SOAPUI_ACTION else ''
        ret = f'if test -z "$(find {folder} -maxdepth 1 -name "{file}" -print -quit)"; then echo "::error::{NO_REPORT_ERROR_MESSAGE}";{exit_or_not}fi'
    return ret


########################################################################
## 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: soapui/execute@v1
      with:
        test: path/to/test.xml#testsuiteName#testcaseName
    ```

    ```yaml
    - uses: soapui/execute@v1
      with:
        test: path/to/test.xml#testsuiteName#
    ```
    """
    test_reference = inputs['test']

    parts = test_reference.partition('#')
    datasource = parts[0]
    if parts[2].rstrip():
        parts = parts[2].partition('#')
        testsuite = parts[0]
        if parts[2].rstrip():
            testcase_arg = f'-c"{parts[2]}" '
        else:
            testcase_arg = ''
    else:
        testsuite = testsuite_arg = testcase_arg = ''

    if testsuite == '':
        testsuite_arg = ''
        testsuite = '*'
    else:
        testsuite_arg = f'-s"{testsuite}" '
        testsuite = testsuite_name_filter(testsuite)

    if core.runner_on_windows():
        run_cmd = f'testrunner.bat -j %SOAPUI_GLOBAL_PROPERTIES% %SOAPUI_PROJECT_PROPERTIES% {testsuite_arg}{testcase_arg}"{datasource}" || echo ::error::SoapUI failed with rc:%ERRORLEVEL% & exit 0'
    else:
        testsuite_arg = testsuite_arg.replace('"', "'")
        testcase_arg = testcase_arg.replace('"', "'")
        expr = f"testrunner.sh -j $SOAPUI_GLOBAL_PROPERTIES $SOAPUI_PROJECT_PROPERTIES {testsuite_arg}{testcase_arg}'{datasource}'"
        run_cmd = f'eval "{expr}" || echo "::error::SoapUI failed with rc:$?"'

    steps = [
        {'run': f"{core.delete_file(f'TEST-{testsuite}.xml')}\n{run_cmd}"},
        {'run': _handle_no_xml_report('execute', f'TEST-{testsuite}.xml')},
    ]

    if testsuite == '*':
        steps += [
            {
                'uses': 'actions/get-files@v1',
                'with': {
                    'pattern': f'TEST-{testsuite}.xml',
                    'type': REPORT_TYPE,
                    'warn-if-not-found': 'No attachment with SoapUI reports for Allure',
                },
            },
        ]
    else:
        steps += [
            {
                'run': core.attach_file(f'TEST-{testsuite}.xml', type=REPORT_TYPE),
                'continue-on-error': True,
            }
        ]

    return steps


########################################################################
# 'params' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: soapui/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    core.validate_params_inputs(inputs)
    data = inputs['data']

    if core.runner_on_windows():
        global_properties = (
            ' '.join(
                f'''-G"{key}={value.replace('\"', '\\\"')}"'''
                for key, value in data.get('global', {}).items()
            )
            or ' '
        )
        project_properties = (
            ' '.join(
                f'''-P"{key}={value.replace('\"', '\\\"')}"'''
                for key, value in data.get('test', {}).items()
            )
            or ' '
        )
    else:
        global_properties = (
            ' '.join(
                f'-G{key}={quote(value)}'
                for key, value in data.get('global', {}).items()
            )
            or ' '
        )
        project_properties = (
            ' '.join(
                f'-P{key}={quote(value)}' for key, value in data.get('test', {}).items()
            )
            or ' '
        )

    return core.export_variables(
        {
            'SOAPUI_GLOBAL_PROPERTIES': global_properties,
            'SOAPUI_PROJECT_PROPERTIES': project_properties,
        },
        verbatim=True,
    )


########################################################################
# 'soapui' action


def soapui_action(inputs):
    """Process 'soapui' action.

    `soapui` actions have a mandatory `project` input:

    ```yaml
    - uses: soapui/soapui@v1
      with:
        project: foo
    ```

    ```yaml
    - uses: soapui/soapui@v1
      with:
        project: foo
        testcase: bar
        testsuite: baz
        user: user
        host: host:port
        endpoint: https://host:port/foo
        system-properties:
          key1: value1
          key2: value2
        global-properties:
          key3: value3
          key4: value4
        project-properties:
          key5: value5
          key6: value6
        target: foo/bar

        extra-options: any other option
    ```

    If the action is used more than once in a job, it is up to the
    caller to ensure no previous test execution results remains before
    executing a new test.

    It is also up to the caller to attach the relevant reports so that
    publishers can do their job too, by using the `actions/get-files@v1`
    action or some other means.

    `extra-options` is optional, allowing to specify any other option.
    """
    project = inputs['project']

    args = '-j '
    if testcase := inputs.get('testcase'):
        args += f'-c"{testcase}" '
    if testsuite := inputs.get('testsuite'):
        args += f'-s"{testsuite}" '
    else:
        testsuite = '*'

    if user := inputs.get('user'):
        args += f'-u{user} '
    if host := inputs.get('host'):
        args += f'-h{host} '
    if endpoint := inputs.get('endpoint'):
        args += f'-e{endpoint} '

    if system_properties := inputs.get('system-properties'):
        for prop, value in system_properties.items():
            args += f'-D{prop}="{value}" '
    if global_properties := inputs.get('global-properties'):
        for prop, value in global_properties.items():
            args += f'-G{prop}="{value}" '
    if project_properties := inputs.get('project-properties'):
        for prop, value in project_properties.items():
            args += f'-P{prop}="{value}" '

    if value := inputs.get('target'):
        report_target = core.normalize_path(value.replace(' ', '_'))
        args += f'-f"{report_target}" '
        if core.runner_on_windows():
            report_folder = f'{report_target}\\'
        else:
            report_folder = f'{report_target}/'
    else:
        report_folder = ''

    if extra_options := inputs.get('extra-options'):
        args += f'{extra_options} '

    if core.runner_on_windows():
        steps = [
            {
                'run': f'testrunner.bat {args}"{project}" '
                '|| echo ::error::SoapUI failed with rc:%ERRORLEVEL% & exit 0'
            },
        ]
    else:
        steps = [
            {
                'run': f'testrunner.sh {args}"{project}" '
                '|| echo "::error::SoapUI failed with rc:$?"'
            },
        ]

    if testsuite == '*':
        if report_folder:
            steps += [
                {
                    'run': _handle_no_xml_report(
                        SOAPUI_ACTION, f'TEST-{testsuite}.xml', report_folder
                    )
                },
                {
                    'uses': 'actions/get-files@v1',
                    'with': {
                        'pattern': f'TEST-{testsuite}.xml',
                        'type': REPORT_TYPE,
                        'warn-if-not-found': 'No attachment with SoapUI reports for Allure',
                    },
                    'working-directory': report_target,
                },
            ]
        else:
            steps += [
                {'run': _handle_no_xml_report(SOAPUI_ACTION, f'TEST-{testsuite}.xml')},
                {
                    'uses': 'actions/get-files@v1',
                    'with': {
                        'pattern': f'TEST-{testsuite}.xml',
                        'type': REPORT_TYPE,
                        'warn-if-not-found': 'No attachment with SoapUI reports for Allure',
                    },
                },
            ]
    else:
        testsuite = testsuite_name_filter(testsuite)
        steps += [
            {
                'run': _handle_no_xml_report(
                    SOAPUI_ACTION, f'TEST-{testsuite}.xml', report_folder
                )
            },
            {
                'run': core.attach_file(
                    f'{report_folder}TEST-{testsuite}.xml', type=REPORT_TYPE
                ),
            },
        ]

    return steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    SOAPUI_CATEGORY: soapui_action,
}
