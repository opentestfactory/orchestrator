# Copyright (c) Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the actions provider.

The following categories are provided:

- checkout@v2
- checkout@v3
- create-archive@v1
- createfile@v1
- delete-file@v1
- get-file
- get-files@v1
- prepare-inception@v1
- put-file@v1
- touch-file@v1
- upload-artifact@v1
- download-artifact@v1
"""

from .checkout import (
    checkoutv2_action,
    CHECKOUTV2_CATEGORY,
    checkoutv3_action,
    CHECKOUTV3_CATEGORY,
)
from .files import (
    createfile_action,
    createarchive_action,
    downloadartifact_action,
    touchfile_action,
    deletefile_action,
    getfiles_action,
    getfile_action,
    prepareinception_action,
    putfile_action,
    uploadartifact_action,
    CREATEFILE_CATEGORY,
    CREATEARCHIVE_CATEGORY,
    TOUCHFILE_CATEGORY,
    DELETEFILE_CATEGORY,
    GETFILES_CATEGORY,
    GETFILE_CATEGORY,
    PREPAREINCEPTION_CATEGORY,
    PUTFILE_CATEGORY,
    UPLOADARTIFACT_CATEGORY,
    DOWNLOADARTIFACT_CATEGORY,
)

KNOWN_CATEGORIES = {
    CHECKOUTV2_CATEGORY: checkoutv2_action,
    CHECKOUTV3_CATEGORY: checkoutv3_action,
    CREATEARCHIVE_CATEGORY: createarchive_action,
    CREATEFILE_CATEGORY: createfile_action,
    TOUCHFILE_CATEGORY: touchfile_action,
    DELETEFILE_CATEGORY: deletefile_action,
    GETFILE_CATEGORY: getfile_action,
    GETFILES_CATEGORY: getfiles_action,
    PREPAREINCEPTION_CATEGORY: prepareinception_action,
    PUTFILE_CATEGORY: putfile_action,
    UPLOADARTIFACT_CATEGORY: uploadartifact_action,
    DOWNLOADARTIFACT_CATEGORY: downloadartifact_action,
}
