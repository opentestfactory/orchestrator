# Copyright (c) 2021-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An inception execution environment plugin."""

from typing import Any, Dict, List, Tuple

import fnmatch
import shutil
import sys

from flask import request

from opentf.commons import (
    SERVICECONFIG,
    EXECUTIONCOMMAND,
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
    EXECUTIONRESULT,
    validate_schema,
    make_status_response,
    subscribe,
    unsubscribe,
    publish,
    make_app,
    run_app,
    make_uuid,
    make_event,
)

from opentf.toolkit.channels import CHANNEL_REQUEST, DEFAULT_CHANNEL_LEASE


########################################################################
## Constants

FILE_PREFIXES = ('%%', '$')
LHS_PATTERNS = [
    ('(@for %%i in (', 14, ')'),
    ('(@for /f %%i in (', 35, '"\')'),
    ('if test -z "$(find . -maxdepth 1 -name ', 40, "'"),
    ('if test -z "$(find .', 28, "'"),
]

########################################################################
## Helpers

CHANNELS: Dict[str, str] = {}  # keys are channel_id, values are workflow_id
CACHE: Dict[str, Dict[str, Tuple[str, str]]] = {}  # keys are workflow_id


def _maybe_make_offer(command: Dict[str, Any], metadata: Dict[str, Any], job_id: str):
    """Make a job offer if `inception` is in requested tags."""
    tags = command['runs-on']
    if 'inception' not in tags:
        return make_status_response(
            'OK', f'Not providing execution environment matching {tags}.'
        )

    metadata.update(
        channel_id=make_uuid(),
        channel_lease=DEFAULT_CHANNEL_LEASE,
        channel_os='windows' if 'windows' in tags else 'linux',
        channel_tags=tags,
        channel_temp='/tmp',
    )
    CHANNELS[metadata['channel_id']] = metadata['workflow_id']
    CACHE.setdefault(metadata['workflow_id'], {})
    publish(
        make_event(EXECUTIONRESULT, metadata=metadata, status=0),
        context=app.config['CONTEXT'],
    )
    return make_status_response('OK', f'Making an offer for job {job_id}.')


def _attach_files(
    files: List[str], parameters: Dict[str, str], event: Dict[str, Any]
) -> None:
    """Fill event with attachments."""
    metadata = event['metadata']
    workflow_id = metadata['workflow_id']
    job_id = metadata['job_id']
    step_sequence_id = metadata['step_sequence_id']
    for name in files:
        remote_path, uuid = CACHE[workflow_id][name]
        app.logger.debug('%s found in cache as %s.', name, remote_path)
        local_path = f'/tmp/{job_id}-{uuid}_{step_sequence_id}_{name}'
        shutil.copy(src=remote_path, dst=local_path)
        event.setdefault('attachments', []).append(local_path)
        metadata.setdefault('attachments', {})[local_path] = {
            **parameters,
            'uuid': uuid,
        }


def _maybe_attach(item: str, event: Dict[str, Any]) -> None:
    """Attach files to event if needed."""
    workflow_id = event['metadata']['workflow_id']
    while '::attach' in item:
        lhs, _, item = item.partition('::attach')
        parameters, _, item = item.partition('::')
        pattern = item.partition('\n')[0]
        pattern = pattern.rsplit('/', 1)[-1].rsplit('\\', 1)[-1].strip('"')
        if pattern.startswith(FILE_PREFIXES):
            app.logger.debug('Attempting to find pattern in: %s.', lhs)
            for pat, start, end in LHS_PATTERNS:
                if lhs.startswith(pat):
                    pattern = lhs[start:].split(end)[0]
                    break
        if files := fnmatch.filter(CACHE[workflow_id], pattern):
            params = {
                param.partition('=')[0].strip(): param.partition('=')[2].strip()
                for param in parameters.split(',')
            }
            _attach_files(files, params, event)
        else:
            app.logger.warning('Could not find files matching %s in cache.', pattern)


def _feed_cache(item: str, workflow_id: str) -> None:
    """Feed the cache with a new entry.

    `item` is a string of the form

        ::inception::<workflow_id>::<name>::<target>
    """
    _, _, wid, name, target = item.split('::')
    if wid == workflow_id:
        CACHE[wid][name] = target, make_uuid()
        app.logger.debug('%s added to cache as %s.', name, target)
    else:
        app.logger.warning('Invalid workflow_id %s in ::inception:: command.', wid)


def _process_command(command: Dict[str, Any], metadata: Dict[str, Any], job_id: str):
    workflow_id = metadata['workflow_id']
    prepare = make_event(EXECUTIONRESULT, metadata=metadata, status=0)
    for item in command['scripts']:
        app.logger.debug('>>> %s', item)
        if item.startswith('::inception::'):
            _feed_cache(item, workflow_id)
        else:
            _maybe_attach(item, prepare)

    if metadata['step_sequence_id'] == -2:
        try:
            del CHANNELS[metadata['channel_id']]
        except KeyError:
            pass

    publish(prepare, context=app.config['CONTEXT'])

    return make_status_response(
        'OK', f'Step #{metadata["step_sequence_id"]} added to job {job_id} queue.'
    )


def _maybe_release_workflow(workflow_id: str):
    channels = [channel for channel, wid in CHANNELS.items() if wid == workflow_id]
    if not channels:
        return make_status_response(
            'OK', f'Workflow {workflow_id} not handled by this channel.'
        )

    try:
        del CACHE[workflow_id]
    except KeyError:
        pass

    msg = f'Inception channel released for workflow {workflow_id}.'
    app.logger.debug(msg)
    return make_status_response('OK', msg)


def _route_request(schema: str, body: Dict[str, Any]):
    metadata = body['metadata']

    if schema in (WORKFLOWCOMPLETED, WORKFLOWCANCELED):
        return _maybe_release_workflow(metadata['workflow_id'])

    if metadata['step_sequence_id'] == CHANNEL_REQUEST:
        return _maybe_make_offer(body, metadata, metadata['job_id'])

    if metadata.get('channel_id') not in CHANNELS:
        return make_status_response(
            'OK', f'Job {metadata["job_id"]} not handled by this channel plugin.'
        )

    return _process_command(body, metadata, metadata['job_id'])


########################################################################
## Main

SUBSCRIPTIONS = (EXECUTIONCOMMAND, WORKFLOWCOMPLETED, WORKFLOWCANCELED)


app = make_app(
    name='inceptionee',
    description='Create and start an inception execution environment plugin.',
    schema=SERVICECONFIG,
    descriptor='plugin.yaml',
)


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict) or 'kind' not in body or 'apiVersion' not in body:
        return make_status_response(
            'Invalid', 'Not an object or no kind or no apiVersion specified.'
        )

    if (schema := f'{body["apiVersion"]}/{body["kind"]}') not in SUBSCRIPTIONS:
        return make_status_response('OK', 'Unexpected event received.')

    try:
        channel_id = body['metadata'].get('channel_id')
        if channel_id and channel_id not in CHANNELS:
            return make_status_response('OK', 'Job not handled by this channel plugin.')
    except KeyError:
        return make_status_response(
            'BadRequest', f'Not a valid {schema} request: Missing metadata section.'
        )

    valid, extra = validate_schema(schema, body)
    if not valid:
        return make_status_response(
            'BadRequest', f'Not a valid {schema} request: {extra}.'
        )

    return _route_request(schema, body)


def main():
    """Start the Inception channel service."""
    app.logger.debug('Subscribing to %s.', ', '.join(SUBSCRIPTIONS))
    try:
        subs = [
            subscribe(kind=kind, target='inbox', app=app)
            for kind in (WORKFLOWCOMPLETED, WORKFLOWCANCELED)
        ]
        subs.append(
            subscribe(
                kind=EXECUTIONCOMMAND,
                fieldexpressions=[
                    {
                        'key': 'runs-on',
                        'operator': 'ContainsAll',
                        'values': ['inception'],
                    }
                ],
                target='inbox',
                app=app,
            )
        )
    except Exception as err:
        app.logger.error('Could not subscribe to eventbus: %s.', str(err))
        sys.exit(2)

    try:
        app.logger.info("Providing environments for the following tags: {'inception'}.")
        app.logger.debug('Starting service.')
        run_app(app)
    finally:
        for sub in subs:
            unsubscribe(sub, app=app)


if __name__ == '__main__':
    main()
