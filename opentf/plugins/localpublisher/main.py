# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A dummy publisher plugin.

This publisher plugin copies published documents to a local repository.

# Usage

```
python3 -m opentf.plugins.localpublisher.main [--context context] [--config configfile]
```


# Endpoints

This module exposes one endpoint:

- /inbox (POST)

Whenever calling this endpoint, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

import os
import shutil
import sys

from flask import request

from opentf.commons import (
    EXECUTIONRESULT,
    WORKFLOWRESULT,
    validate_schema,
    make_status_response,
    subscribe,
    unsubscribe,
    make_app,
    run_app,
)

########################################################################
## Main

app = make_app(
    name='localpublisher',
    description='Create and start a local publisher plugin.',
    descriptor='plugin.yaml',
)


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new result message."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict):
        return make_status_response('BadRequest', 'Body must be a JSON object.')

    if 'apiVersion' not in body or 'kind' not in body:
        return make_status_response('BadRequest', 'Missing apiVersion or kind')

    schema = f'{body["apiVersion"]}/{body["kind"]}'
    if schema in (EXECUTIONRESULT, WORKFLOWRESULT):
        valid, extra = validate_schema(schema, body)
        if not valid:
            return make_status_response(
                'BadRequest', f'Not a valid {body["kind"]} request: {extra}'
            )
    else:
        return make_status_response(
            'BadRequest', f'No kind or invalid kind: {body.get("kind")}.'
        )

    workflow_id = body['metadata']['workflow_id']
    if 'attachments' in body:
        copy_to_localfilesystem(body['attachments'], workflow_id)

    return make_status_response('OK', '')


def copy_to_localfilesystem(attachments, workflow_id):
    """Copy attachments to configured target directory."""
    target = app.config['CONTEXT']['target']
    try:
        if not os.path.isdir(os.path.join(target, workflow_id)):
            os.mkdir(os.path.join(target, workflow_id))
        for output in attachments:
            app.logger.debug('Saving attachment : ' + output)
            shutil.copy(
                output, os.path.join(target, workflow_id, output.split('/')[-1])
            )
    except FileNotFoundError:
        app.logger.error(f'Target path {target} does not exist, ignoring.')


def main(plugin):
    """Start the Local Publisher service."""
    try:
        plugin.logger.info(f'Subscribing to {EXECUTIONRESULT}.')
        uuid_e = subscribe(kind=EXECUTIONRESULT, target='/inbox', app=plugin)
        plugin.logger.info(f'Subscribing to {WORKFLOWRESULT}.')
        uuid_w = subscribe(kind=WORKFLOWRESULT, target='/inbox', app=plugin)
    except Exception as err:
        plugin.logger.error('Could not subscribe to eventbus: %s', str(err))
        sys.exit(2)

    try:
        plugin.logger.info('Starting local publisher service.')
        run_app(plugin)
    finally:
        unsubscribe(uuid_w, app=plugin)
        unsubscribe(uuid_e, app=plugin)


if __name__ == '__main__':
    main(app)
