# EventBus Architecture

The EventBus is an event broker service that manages the flow of subscriptions and
publications between OpenTestFactory orchestrator services and plugins. It receives
event publications and dispatches them to the appropriate subscribers. Details on how
subscribers interact with the EventBus for subscribing and publishing can be found
in the [OpenTestFactory EventBus specification](https://opentestfactory.org/specification/eventbus.html). The present
document describes service architecture.

## Subscription Handling

A service subscribing to publications sends a `Subscription` event to the EventBus
via the `POST /subscriptions` endpoint. If the subscription is a valid JSON document and conforms
to the [`Subscription` schema](https://opentestfactory.org/specification/schemas/subscription.html), its field and/or label selectors are compiled
to be matched against publications. When the validity check or selector compilation fails,
a `BadRequest` or `Invalid` response is returned.

A valid subscription is then processed: subscription metadata is completed, the subscription
is assigned a UUID as an identifier, and the subscription itself is added to the subscriptions store.
Its selectors and authorization token are also added to respective data structures.
Finally, a dispatcher is created for the subscription, storing an asynchronous dispatch queue and a
Requests session. A `Created` (201) response is returned, containing the subscription UUID, to indicate
that the subscription was successfully handled.

If a subscription is no longer relevant, a service can cancel its subscription by sending
a request to the `DELETE /subscriptions/{subscription_id}` endpoint of the EventBus.
If no subscription matching the provided `subscription_id` is found, a `NotFound`
response is returned. Otherwise, the subscription, its token, and the selectors mapped
to the subscription UUID are cleaned up, and the subscription dispatcher is released.
An `OK` status response is then returned to the service.

The EventBus service also exposes the `GET /subscriptions` endpoint, which returns an `OK`
response along with a `SubscriptionsList` JSON document filtered based on the query
parameters `fieldSelector` and `labelSelector`. If the selectors cannot be compiled,
an `Invalid` response is returned. The `SubscriptionsList` is derived from the items
in the subscriptions store. These items include subscription metadata and status information,
such as the publication count, last publication timestamp, and publication status summary,
all mapped to subscription UUID.

Each `SubscriptionsList` item or subscribription manifest has the following structure:

```json
{
  "apiVersion": "opentestfactory.org/v1alpha1",
  "kind": "Subscription",
  "metadata": {
    "name": "string",
    "annotations": {
      "opentestfactory.org/fieldselector": "string",
      "opentestfactory.org/labelselector": "string"
    },
    "creationTimestamp": "timestamp",
    "subscription_id": "uuid"
  },
  "spec": {
    "selector": "object",
    "subscriber": {
      "endpoint": "string"
    }
  },
  "status": {
    "lastPublicationTimestamp": "timestamp or null",
    "publicationCount": "integer",
    "publicationStatusSummary": "object"
  }
}
```

## Publication Handling

Services send publications to the EventBus via the `POST /publications` endpoint.
If the publication is a valid JSON document, it is processed. Otherwise, a `BadRequest`
response is returned.

For each received publication, the EventBus generates a list of interested recipients
based on subscriptions label and field selectors. If no matching subscription
is found, an `OK` response is returned along with respective message. Alternatively, the
publication is forwarded to the dispatcher of each recipient, and an `OK` response is
returned.

For each subscription, an asynchronous dispatch queue is created to handle publication
dispatching. Before a publication is posted to a subscriber, it is amended with headers
that include the subscription and publication UUIDs, the `application/json` content type,
and an authorization token if required. The publication is then posted using the dispatcher's
Request session. If the post request is not successful and response status code is `5xx` or `429`,
the publication is reposted, with the delay between attempts gradually increasing from 2 to 60 seconds.

```mermaid
sequenceDiagram
box
    participant Services
    participant EventBus
end
box EventBus
    participant subscriptions
    participant dispatchers
end
box
    participant Subscribers
end
Services->>EventBus: Publication
Note over EventBus: Is valid JSON?
alt not valid
    EventBus-->>Services: BadRequest
else valid
    EventBus->>subscriptions: Publication
    Note over subscriptions: Matching subscriptions found?
end
alt no subscriptions
    subscriptions-->>EventBus: No matching subscriptions
    EventBus-->>Services: OK, No matching subscriptions
else subscriptions found
    subscriptions-->>EventBus: [subscriptions]
    EventBus->>dispatchers: Publication
    EventBus-->>Services: OK, Publication handled
    Note over dispatchers: Amending publication headers
    dispatchers->>Subscribers: Publication
end
loop Publication dispatched?
    alt
        Subscribers-->>dispatchers: 5xx or 429
        dispatchers->>dispatchers: Init or increase delay (max 60 seconds)
        dispatchers->>dispatchers: Wait for delay
        dispatchers->>Subscribers: Publication
    else
        break
            Subscribers-->>dispatchers: OK (200) or not 5xx, 429
            dispatchers->>dispatchers: Publish debug message
        end
    end
end
```
