# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Some quality gate unit tests."""


import json
import logging
import os
import requests
import sys
import time
import unittest

from unittest.mock import MagicMock, patch, mock_open
from uuid import UUID


sys.argv = ['dummy', '--trusted-authorities', os.devnull]

from opentf.plugins.qualitygate import main as qualitygate

########################################################################
# Datasets

QUALITYGATE = {
    'quaLItygAte': {
        'rules': [
            {
                'name': 'allTests',
                'rule': {
                    'scope': "(test.technology == 'cypress')",
                    'threshold': '100%',
                    'failure-status': ['failure'],
                },
            }
        ],
        'scope': 'true',
    }
}

STRICT_QUALITYGATE = {
    'strict': {
        'rules': [
            {
                'name': 'Default strict quality gate',
                'rule': {
                    'scope': 'true',
                    'threshold': '100%',
                    'failure-status': ['failure'],
                },
            }
        ],
        'scope': 'true',
    }
}

PASSING_QUALITYGATE = {
    'passing': {
        'rules': [
            {
                'name': 'Default passing quality gate',
                'rule': {
                    'scope': 'true',
                    'threshold': '0%',
                    'failure-status': ['failure'],
                },
            }
        ],
        'scope': 'true',
    }
}

QUALITYGATES_DICT = {
    'qualitygates': [
        {
            'name': 'tesTqG',
            'rules': [
                {
                    'name': 'nOtAllTesTs',
                    'rule': {
                        'scope': '(test.technology == "rRroBot")',
                        'threshold': '100%',
                        'failure-status': ['failure', 'error'],
                    },
                }
            ],
        }
    ]
}

QUALITYGATES_WITH_DUPLICATES = {
    'qualitygates': [
        {
            'name': 'DoUblE',
            'rules': [
                {
                    'name': 'rule3',
                    'rule': {
                        'scope': '(test.name == "nAmE")',
                        'threshold': '0%',
                    },
                }
            ],
        },
        {
            'name': 'DoUblE',
            'rules': [
                {
                    'name': 'rule3',
                    'rule': {
                        'scope': '(test.technology == "rRroBot")',
                        'threshold': '100%',
                    },
                }
            ],
        },
        {
            'name': 'triPle',
            'rules': [
                {
                    'name': 'rule1',
                    'rule': {
                        'scope': '(test.technology == "JUNit")',
                        'threshold': '100%',
                    },
                },
                {
                    'name': 'rule1',
                    'rule': {
                        'scope': '(test.technology == "tEcHnO")',
                        'threshold': '10%',
                    },
                },
            ],
        },
    ]
}

TC_METADATA = [
    {
        'name': 'nAmE',
        'status': 'SUCCESS',
        'test': {
            'technology': 'cypress',
        },
        'uuid': 'caseId',
    }
]


UUID_OK = 'bb2e5737-1aee-4fad-97e8-2d46c1984b3e'

########################################################################
# Helpers


def build_observer_payload_mock(
    observer_status="DONE", tests_status=None, namespace='default'
):
    if tests_status is None:
        tests_status = {}
    status_objects = [
        {
            'metadata': {'namespace': namespace},
            'test': {'outcome': status, 'technology': 'cypress'},
            'status': status,
            'uuid': test_id,
            'name': 'nAmE',
        }
        for test_id, status in tests_status.items()
    ]
    return {
        "details": {
            "status": observer_status,
            "items": status_objects,
        }
    }


def build_observer_mock(http_status=200, observer_status="DONE", tests_status=None):
    response = requests.Response()
    response.status_code = http_status
    response.json = MagicMock(
        return_value=build_observer_payload_mock(observer_status, tests_status)
    )
    return response


########################################################################
# Tests


class QualityGateTests(unittest.TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        plugin = qualitygate.plugin
        plugin.config['CONTEXT']['services'] = {
            'observer': {'endpoint': 'http://localhost'}
        }
        plugin.config['CONTEXT']['enable_insecure_login'] = True
        plugin.config['CONFIG']['qualitygates'] = {
            'passing': {},
            'strict': {},
            'foobar': {},
            'barfoo': {},
            'bar': {},
            'foo': {},
        }
        self.plugin = plugin.test_client()
        self.assertFalse(plugin.debug)
        self.sleep_patcher = patch('time.sleep', return_value=None)
        self.mock_sleep = self.sleep_patcher.start()

    def tearDown(self):
        self.sleep_patcher.stop()
        logging.disable(logging.NOTSET)

    # observer response codes

    def test_respond_422_when_uuid_is_invalid(self):
        response = self.plugin.get('/workflows/notaUUiD/qualitygate')
        self.assertEqual(response.status_code, 422)
        self.assertEqual(response.json['message'], 'Not a valid UUID: notaUUiD.')

    def test_respond_422_when_mode_is_unknown(self):
        response = self.plugin.get(f'/workflows/{UUID_OK}/qualitygate?mode=invalid')
        self.assertEqual(response.status_code, 422)
        self.assertEqual(
            response.json['message'],
            'Quality gate "invalid" not found in quality gate definitions.',
        )

    def test_respond_422_when_observer_said_422(self):
        mockreponse = requests.Response()
        mockreponse.status_code = 422
        with patch('requests.get', MagicMock(return_value=mockreponse)):
            response = self.plugin.get(f'/workflows/{UUID_OK}/qualitygate?mode=bar')
        self.assertEqual(response.status_code, 422)

    def test_respond_500_when_observer_said_418(self):
        mockreponse = requests.Response()
        mockreponse.status_code = 418
        with (
            patch('requests.get', MagicMock(return_value=mockreponse)),
            patch('opentf.plugins.qualitygate.main.error') as mock_error,
        ):
            response = self.plugin.get(f'/workflows/{UUID_OK}/qualitygate?mode=foobar')
        self.assertEqual(response.status_code, 500)
        mock_error.assert_called_once_with('Unexpected observer response: %d.', 418)

    def test_respond_404_when_observer_said_404(self):
        mockresponse = requests.Response()
        mockresponse.status_code = 404
        with patch('requests.get', MagicMock(return_value=mockresponse)):
            response = self.plugin.get(f'/workflows/{UUID_OK}/qualitygate?mode=foo')
            self.assertEqual(response.status_code, 404)

    def test_respond_401_when_observer_said_401(self):
        mockresponse = requests.Response()
        mockresponse.status_code = 401
        with patch('requests.get', MagicMock(return_value=mockresponse)):
            response = self.plugin.get(f'/workflows/{UUID_OK}/qualitygate?mode=foobar')
            self.assertEqual(response.status_code, 401)

    def test_respond_403_when_observer_said_403(self):
        mockresponse = requests.Response()
        mockresponse.status_code = 403
        with patch('requests.get', MagicMock(return_value=mockresponse)):
            response = self.plugin.get(f'/workflows/{UUID_OK}/qualitygate?mode=bar')
            self.assertEqual(response.status_code, 403)

    def test_respond_200_when_observer_said_200(self):
        mockresponse = requests.Response()
        mockresponse.status_code = 200
        mockresponse.json = lambda: {
            'details': {
                'status': 'DONE',
                'items': [{'metadata': {'namespace': 'foo'}}],
            }
        }
        mock_handle = MagicMock()
        with (
            patch('requests.get', MagicMock(return_value=mockresponse)),
            patch(
                'opentf.plugins.qualitygate.main.STATUS_HANDLER', {'DONE': mock_handle}
            ),
        ):
            response = self.plugin.get(f'/workflows/{UUID_OK}/qualitygate?mode=barfoo')
            mock_handle.assert_called_once()
            self.assertEqual(response.status_code, 200)

    def test_respond_200_when_observer_said_202_then_200(self):
        mock_json = lambda: {
            'details': {
                'status': 'DONE',
                'items': [{'metadata': {'namespace': 'foo'}}],
            }
        }
        mockresponse = requests.Response()
        mockresponse.status_code = 202
        mockresponse.json = mock_json
        mockresponse2 = requests.Response()
        mockresponse2.status_code = 200
        mockresponse2.json = mock_json
        mock_handle = MagicMock()
        with (
            patch('time.time', MagicMock(return_value=1)),
            patch('time.sleep'),
            patch('requests.get', MagicMock(side_effect=[mockresponse, mockresponse2])),
            patch(
                'opentf.plugins.qualitygate.main.STATUS_HANDLER', {'DONE': mock_handle}
            ),
        ):
            response = self.plugin.get(f'/workflows/{UUID_OK}/qualitygate?mode=barfoo')
            print(response.text)
            mock_handle.assert_called_once()
            self.assertEqual(response.status_code, 200)

    def test_handle_workflow_when_workflow_in_cache(self):
        with (
            patch(
                'opentf.plugins.qualitygate.main.TESTCASES_CACHES',
                {UUID_OK: [{'some': 'cache'}]},
            ),
            patch(
                'opentf.plugins.qualitygate.main.handle_completed_workflow'
            ) as mock_handle,
        ):
            self.plugin.get(f'/workflows/{UUID_OK}/qualitygate?mode=foo')
            mock_handle.assert_called_once()
            self.assertEqual(UUID_OK, mock_handle.call_args[0][0])

    # quality gate response codes for workflow statuses

    def test_respond_FAILURE_when_workflow_is_invalid(self):
        app = qualitygate.plugin
        with app.app_context():
            response = build_observer_mock(
                200, observer_status="UNEXPECTED", tests_status={'dummytest': 'FAILURE'}
            )
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", 8, 'tkn', {'foo': 'bar'}
            )
            self.assertEqual(422, quality_gate_response.status_code)

    def test_respond_FAILURE_when_workflow_is_failure(self):
        app = qualitygate.plugin
        with app.app_context():
            response = build_observer_mock(
                200, observer_status="FAILED", tests_status={'dummytest': 'SUCCESS'}
            )
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", 8, 'tkn', {'foo': 'bar'}
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]["status"]
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual("FAILURE", quality_gate_status)

    def test_respond_RUNNING_when_workflow_is_pending(self):
        app = qualitygate.plugin
        with app.app_context():
            response = build_observer_mock(200, observer_status="PENDING")
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", 8, 'tkn', {'foo': 'bar'}
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]["status"]
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual("RUNNING", quality_gate_status)

    def test_respond_RUNNING_when_workflow_is_running(self):
        app = qualitygate.plugin
        with app.app_context():
            response = build_observer_mock(200, observer_status="RUNNING")
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", 8, 'tkn', {'foo': 'bar'}
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]["status"]
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual("RUNNING", quality_gate_status)

    def test_respond_NOTEST_when_no_test_results(self):
        app = qualitygate.plugin
        testcases = {'woRfkloW': [{'EMPTY_ITEMS': {'zoo'}}]}
        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main.TESTCASES_CACHES', testcases),
        ):
            response = build_observer_mock(200)
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", 8, 'tkn', {'foo': 'bar'}
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]["status"]
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual("NOTEST", quality_gate_status)

    def test_respond_INVALID_when_error(self):
        app = qualitygate.plugin
        testcases = {'woRfkloW': [{'exception_error': {'moo'}}]}
        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main.TESTCASES_CACHES', testcases),
        ):
            response = build_observer_mock(200)
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", 8, 'tkn', {'foo': 'bar'}
            )
            response_json = quality_gate_response.json
            self.assertEqual(422, quality_gate_response.status_code)

    # evaluate quality gate

    def test_evaluate_qualitygate_ok(self):
        tc_metadata = [
            {
                'name': 'cAsEName',
                'status': 'SUCCESS',
                'test': {
                    'technology': 'cypress',
                },
                'uuid': 'caSeId',
            }
        ]

        qg_result = qualitygate.evaluate_qualitygate(
            'quaLItygAte', tc_metadata, QUALITYGATE
        )
        self.assertEqual('SUCCESS', qg_result[0]['allTests']['result'])

    def test_evaluate_qualitygate_ko(self):
        tc_metadata = [
            {
                'name': 'cAsEName',
                'status': 'FAILURE',
                'test': {
                    'technology': 'cypress',
                },
                'uuid': 'caSeId',
            }
        ]
        qg_result = qualitygate.evaluate_qualitygate(
            'quaLItygAte', tc_metadata, QUALITYGATE
        )
        self.assertEqual('FAILURE', qg_result[0]['allTests']['result'])

    def test_evaluate_qualitygate_notest(self):
        tc_metadata = [
            {
                'name': 'cAsE',
                'status': 'SUCCESS',
                'test': {'technology': 'junit'},
                'uuid': 'caSeId',
            }
        ]
        qg_result = qualitygate.evaluate_qualitygate(
            'quaLItygAte', tc_metadata, QUALITYGATE
        )
        self.assertEqual('NOTEST', qg_result[0]['allTests']['result'])

    def test_evaluate_qualitygate_mode_ko(self):
        with self.assertRaises(Exception) as ex:
            qualitygate.evaluate_qualitygate('nOmOde', [], QUALITYGATE)
            self.assertEqual('Quality gate nOmOde not defined.', str(ex.exception))

    def test_evaluate_qualitygate_treshold_ko(self):
        qg_ko_treshold = {
            'tresholdKo': {
                'rules': [
                    {
                        'name': 'tTests',
                        'rule': {
                            'scope': "(test.technology == 'cypress')",
                            'threshold': 'one hundred and one percent',
                            'failure-status': ['failure'],
                        },
                    }
                ],
                'scope': 'true',
            }
        }
        tc_metadata = [
            {
                'name': 'cAsEName',
                'status': 'FAILURE',
                'test': {
                    'technology': 'cypress',
                },
                'uuid': 'caSeId',
            }
        ]
        with self.assertRaises(Exception) as ex:
            qualitygate.evaluate_qualitygate('tresholdKo', tc_metadata, qg_ko_treshold)
        self.assertIn(
            'Qualitygate threshold one hundred and one percent is invalid.',
            str(ex.exception),
        )

    def test_evaluate_qualitygate_scope_datakey_ko(self):
        tc_metadata = [
            {
                'name': 'cAsEName',
                'status': 'FAILURE',
                'test': {'name': 'tesT', 'data': {'DSNAME': 'Dataset not Cypress'}},
                'uuid': 'caSeId',
            }
        ]
        qg_nodata = {
            'quaLItygAte': {
                'rules': [
                    {
                        'name': 'noTests',
                        'rule': {
                            'scope': "(test.data.DSNAME == 'Dataset Cypress')",
                            'threshold': '100%',
                            'failure-status': ['failure'],
                        },
                    }
                ],
                'scope': 'true',
            }
        }
        qg_result = qualitygate.evaluate_qualitygate(
            'quaLItygAte', tc_metadata, qg_nodata
        )
        self.assertEqual('NOTEST', qg_result[0]['noTests']['result'])

    def test_evaluate_qualitygate_scope_throws_value_error(self):
        qg_invalid_conditional = {
            'quaLItygAte': {
                'rules': [
                    {
                        'name': 'noTests',
                        'rule': {
                            'scope': 'foo bar',
                            'threshold': '100%',
                            'failure-status': ['failure'],
                        },
                    }
                ],
                'scope': 'true',
            }
        }
        qg_result = qualitygate.evaluate_qualitygate(
            'quaLItygAte', TC_METADATA, qg_invalid_conditional
        )
        self.assertEqual('INVALID_SCOPE', qg_result[0]['noTests']['result'])
        self.assertIn('Invalid conditional foo bar', qg_result[0]['noTests']['message'])

    def test_evaluate_qualitygate_scope_throws_key_error(self):
        tc_metadata = [{'test': {}, 'uuid': 'caSeId'}]
        qg_invalid_property = {
            'quaLItygAte': {
                'rules': [
                    {
                        'name': 'noTests',
                        'rule': {
                            'scope': "test.use.path=='path'",
                            'threshold': '100%',
                            'failure-status': ['failure'],
                        },
                    }
                ],
                'scope': 'true',
            }
        }
        qg_result = qualitygate.evaluate_qualitygate(
            'quaLItygAte', tc_metadata, qg_invalid_property
        )
        self.assertEqual('INVALID_SCOPE', qg_result[0]['noTests']['result'])
        self.assertIn(
            'Nonexisting context entry in expression test.use.path',
            qg_result[0]['noTests']['message'],
        )

    def test_evaluate_qualitygate_rule_without_name(self):
        tc_metadata = [
            {
                'name': 'cAsEName',
                'status': 'SUCCESS',
                'test': {
                    'name': 'tesT',
                },
                'uuid': 'caSeId',
            }
        ]
        qg_norulename = {
            'quaLItygAte': {
                'rules': [
                    {
                        'rule': {
                            'scope': "(test.technology == 'cypress')",
                            'threshold': '100%',
                            'failure-status': ['failure'],
                        },
                    }
                ],
                'scope': 'true',
            }
        }
        qg_result = qualitygate.evaluate_qualitygate(
            'quaLItygAte', tc_metadata, qg_norulename
        )
        keys = [key for key in qg_result[0].keys()]
        self.assertEqual(1, len(keys))
        self.assertTrue(UUID(keys[0]))

    # handle completed workflow

    def test_handle_completed_workflow_ok(self):
        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        eqg_mock = MagicMock(return_value=({'rUle': {'result': 'SUCCESS'}}, None))
        response = build_observer_mock(200, tests_status={'caSeId': 'SUCCESS'})
        testcases = response.json()['details']['items']
        with (
            app.app_context(),
            patch(
                'opentf.plugins.qualitygate.main.evaluate_qualitygate',
                eqg_mock,
            ),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response'),
            patch(
                'opentf.plugins.qualitygate.main.TESTCASES_CACHES',
                {'woRfkloW': testcases},
            ),
        ):
            qualitygate.handle_completed_workflow(
                'woRfkloW', 'strict', 8, 'tkn', {'strict': {'foo': 'bar'}}
            )
            eqg_mock.assert_called_once()
            metadata = eqg_mock.call_args[0][1][0]
            self.assertEqual('nAmE', metadata['name'])
            self.assertEqual('cypress', metadata['test']['technology'])

    def test_handle_completed_workflow_cachemiss(self):
        def mock_put(workflow_id, _):
            mock_cache[workflow_id] = testcases

        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        eqg_mock = MagicMock(return_value=({'rUle': {'result': 'SUCCESS'}}, None))
        response = build_observer_mock(200, tests_status={'caSeId': 'SUCCESS'})
        testcases = response.json()['details']['items']
        mock_cache = {}
        mock_queue = MagicMock()
        mock_queue.put = mock_put
        with (
            app.app_context(),
            patch(
                'opentf.plugins.qualitygate.main.evaluate_qualitygate',
                eqg_mock,
            ),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response'),
            patch('opentf.plugins.qualitygate.main.TESTCASES_CACHES', mock_cache),
        ):
            qualitygate.handle_completed_workflow(
                'woRfkloW', 'strict', 1, 'tkn', {'foo': 'bar'}
            )
            eqg_mock.assert_not_called()

    def test_handle_completed_workflow_cachemiss_empty(self):
        def mock_put(arg):
            mock_cache[arg[0]] = [{'EMPTY_ITEMS': True}]

        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        eqg_mock = MagicMock(return_value=({'rUle': {'result': 'SUCCESS'}}, None))
        mock_cache = {}
        mock_queue = MagicMock()
        mock_queue.put = mock_put
        with (
            app.app_context(),
            patch(
                'opentf.plugins.qualitygate.main.evaluate_qualitygate',
                eqg_mock,
            ),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response'),
            patch('opentf.plugins.qualitygate.main.TESTCASES_CACHES', mock_cache),
            patch('opentf.plugins.qualitygate.main.TESTCASES_QUEUE', mock_queue),
        ):
            qualitygate.handle_completed_workflow(
                'woRfkloW', 'strict', 1, 'tkn', {'foo': 'bar'}
            )
            eqg_mock.assert_not_called()

    def test_handle_completed_workflow_cachemiss_notempty(self):
        def mock_put(arg):
            mock_cache[arg[0]] = [{}]

        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        eqg_mock = MagicMock(return_value=({'rUle': {'result': 'SUCCESS'}}, None))
        mock_cache = {}
        mock_queue = MagicMock()
        mock_queue.put = mock_put
        with (
            app.app_context(),
            patch(
                'opentf.plugins.qualitygate.main.evaluate_qualitygate',
                eqg_mock,
            ),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response'),
            patch('opentf.plugins.qualitygate.main.TESTCASES_CACHES', mock_cache),
            patch('opentf.plugins.qualitygate.main.TESTCASES_QUEUE', mock_queue),
        ):
            qualitygate.handle_completed_workflow(
                'woRfkloW',
                'strict',
                1,
                'tkn',
                {'strict': {'foo': 'bar', 'scope': 'true'}},
            )
            eqg_mock.assert_called_once()

    # def test_handle_completed_workflow_ok_paginated(self):
    #     resp1 = MagicMock()
    #     resp1.json.return_value = {'details': {'items': [{'dummytest': 'failure'}]}}
    #     resp1.links = {'next': {'url': 'foobar'}}
    #     resp1.request.headers = {}
    #     resp2 = MagicMock()
    #     resp2.json.return_value = {'details': {'items': [{'dummytest2': 'success'}]}}
    #     resp2.links = {}
    #     resp2.request.headers = {}
    #     mock_get = MagicMock(side_effect=resp2)
    #     mock_eqg = MagicMock(side_effect=Exception('Quit'))
    #     with patch('opentf.plugins.qualitygate.main.requests.get', mock_get), patch(
    #         'opentf.plugins.qualitygate.main.evaluate_qualitygate', mock_eqg
    #     ):
    #         self.assertRaisesRegex(
    #             Exception,
    #             'Quit',
    #             qualitygate.handle_completed_workflow,
    #             'wf',
    #             resp1,
    #             'strict',
    #             {},
    #         )
    #     mock_get.assert_called_once()
    #     mock_eqg.assert_called_once()

    # def test_handle_completed_workflow_ko_exception(self):
    #     resp1 = MagicMock()
    #     resp1.json.return_value = {'details': {'items': [{'dummytest': 'failure'}]}}
    #     resp1.links = {'next': {'url': 'foobar'}}
    #     resp1.request.headers = {}
    #     mock_get = MagicMock(side_effect=ValueError('Boo'))
    #     with patch('opentf.plugins.qualitygate.main.requests.get', mock_get), patch(
    #         'opentf.plugins.qualitygate.main.error'
    #     ) as mock_error, patch(
    #         'opentf.plugins.qualitygate.main.make_status_response'
    #     ) as mock_msr:
    #         qualitygate.handle_completed_workflow('wf', resp1, 'passing', {})
    #     mock_get.assert_called_once()
    #     mock_error.assert_called_once_with(
    #         'Could not deserialize observer response: %s.', 'Boo'
    #     )
    #     mock_msr.assert_called_once_with(
    #         'Invalid', 'Error while querying datasources: Boo.'
    #     )

    def test_handle_completed_workflow_with_duplicate_warnings(self):
        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        qg_rule_double = {
            'triPle': {
                'rules': QUALITYGATES_WITH_DUPLICATES['qualitygates'][2]['rules'],
                'scope': 'true',
            }
        }
        msr_mock = MagicMock()
        response = build_observer_mock(200, tests_status={'caSeId': 'SUCCESS'})
        testcases = response.json()['details']['items']
        patch(
            'opentf.plugins.qualitygate.main.TESTCASES_CACHES', {'woRfkloW': testcases}
        )
        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main.make_status_response', msr_mock),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response'),
            patch(
                'opentf.plugins.qualitygate.main.TESTCASES_CACHES',
                {'woRfkloW': testcases},
            ),
        ):
            qualitygate.handle_completed_workflow(
                'woRfkloW', 'triPle', 8, 'tkn', qg_rule_double, ['triPle dOuble']
            )
            msr_mock.assert_called_once()
            warnings = msr_mock.call_args[0][2]['warnings']
            self.assertEqual('triPle dOuble', warnings[0])
            self.assertIn(
                'Duplicate rule name(s) found in the quality gate triPle: rule1.',
                warnings[1],
            )

    def test_handle_completed_workflow_general_result_success(self):
        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        qg_rule_success = {
            'rUlE': {
                'rules': QUALITYGATES_WITH_DUPLICATES['qualitygates'][0]['rules'],
                'scope': 'true',
            }
        }
        msr_mock = MagicMock()
        response = build_observer_mock(200, tests_status={'caSeId': 'SUCCESS'})
        testcases = response.json()['details']['items']
        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main.make_status_response', msr_mock),
            patch(
                'opentf.plugins.qualitygate.main.in_scope', MagicMock(return_value=True)
            ),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response'),
            patch(
                'opentf.plugins.qualitygate.main.TESTCASES_CACHES',
                {'woRfkloW': testcases},
            ),
        ):
            qualitygate.handle_completed_workflow(
                'woRfkloW', 'rUlE', 8, 'tkn', qg_rule_success, None
            )
            msr_mock.assert_called_once()
            self.assertTrue(msr_mock.call_args[0][2]['rules'].get('rule3'))
            self.assertEqual('SUCCESS', msr_mock.call_args[0][2]['status'])

    def test_handle_completed_workflow_no_filtered_testcases(self):
        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        qg_rule_success = {
            'rUlE': {
                'rules': QUALITYGATES_WITH_DUPLICATES['qualitygates'][0]['rules'],
                'scope': 'test.technology==foo',
            }
        }
        msr_mock = MagicMock()
        response = build_observer_mock(200, tests_status={'caSeId': 'SUCCESS'})
        testcases = response.json()['details']['items']
        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main.make_status_response', msr_mock),
            patch(
                'opentf.plugins.qualitygate.main.in_scope',
                MagicMock(return_value=False),
            ),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response'),
            patch(
                'opentf.plugins.qualitygate.main.TESTCASES_CACHES',
                {'woRfkloW': testcases},
            ),
        ):
            qualitygate.handle_completed_workflow(
                'woRfkloW', 'rUlE', 8, 'tkn', qg_rule_success, None
            )
        msr_mock.assert_called_once()
        self.assertEqual('NOTEST', msr_mock.call_args[0][2]['status'])

    def test_handle_completed_workflow_scope_error(self):
        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        qg_rule_success = {
            'rUlE': {
                'rules': QUALITYGATES_WITH_DUPLICATES['qualitygates'][0]['rules'],
                'scope': 'test.technology=foo',
            }
        }
        msr_mock = MagicMock()
        response = build_observer_mock(200, tests_status={'caSeId': 'SUCCESS'})
        testcases = response.json()['details']['items']
        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main.make_status_response', msr_mock),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response'),
            patch(
                'opentf.plugins.qualitygate.main.TESTCASES_CACHES',
                {'woRfkloW': testcases},
            ),
        ):
            qualitygate.handle_completed_workflow(
                'woRfkloW', 'rUlE', 8, 'tkn', qg_rule_success, None
            )
        msr_mock.assert_called_once()
        self.assertEqual('INVALID_SCOPE', msr_mock.call_args[0][2]['status'])

    # default quality gates

    def test_respond_SUCCESS_with_passing_strategy(self):
        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        mock_ar = lambda x, **_: x
        response = build_observer_mock(
            200, tests_status={"id1": "SUCCESS", "id2": "FAILURE"}
        )
        testcases = response.json()['details']['items']
        mock_img_definitions = {}
        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response', mock_ar),
            patch(
                'opentf.plugins.qualitygate.main.TESTCASES_CACHES',
                {'woRfkloW': testcases},
            ),
            patch(
                'opentf.plugins.qualitygate.main.IMG_DEFINITIONS', mock_img_definitions
            ),
            patch('opentf.plugins.qualitygate.main._filter_listdir', return_value=[]),
        ):
            qualitygate._load_image_qualitygates()
            quality_gate_response = qualitygate.handle_completed_workflow(
                'woRfkloW', 'passing', 8, 'tkn', PASSING_QUALITYGATE
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]  # type: ignore
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual(
                'SUCCESS',
                quality_gate_status['rules']['Default passing quality gate']['result'],
            )

    def test_respond_FAILURE_with_strict_strategy_and_a_test_failure(self):
        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        mock_ar = lambda x, **_: x
        response = build_observer_mock(
            200, tests_status={"id1": "FAILURE", "id2": "SUCCESS", "id3": "SUCCESS"}
        )
        testcases = response.json()['details']['items']
        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response', mock_ar),
            patch(
                'opentf.plugins.qualitygate.main.TESTCASES_CACHES',
                {'woRfkloW': testcases},
            ),
        ):
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", 8, 'tkn', STRICT_QUALITYGATE
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]  # type: ignore
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual(
                'FAILURE',
                quality_gate_status['rules']['Default strict quality gate']['result'],
            )

    def test_respond_FAILURE_with_strict_strategy_and_a_test_error(self):
        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        mock_ar = lambda x, **_: x
        response = build_observer_mock(
            200, tests_status={"id1": "FAILURE", "id2": "SUCCESS", "id3": "SUCCESS"}
        )
        testcases = response.json()['details']['items']
        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response', mock_ar),
            patch(
                'opentf.plugins.qualitygate.main.TESTCASES_CACHES',
                {'woRfkloW': testcases},
            ),
        ):
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, 'strict', 8, 'tkn', STRICT_QUALITYGATE
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]  # type: ignore
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual(
                'FAILURE',
                quality_gate_status['rules']['Default strict quality gate']['result'],
            )

    def test_respond_SUCCESS_with_strict_strategy_and_a_test_success(self):
        app = qualitygate.plugin
        req = MagicMock()
        req.args = {}
        mock_ar = lambda x, **_: x
        response = build_observer_mock(
            200, tests_status={"id1": "SUCCESS", "id2": "SUCCESS", "id3": "SUCCESS"}
        )
        testcases = response.json()['details']['items']
        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main.request', req),
            patch('opentf.plugins.qualitygate.main.annotate_response', mock_ar),
            patch(
                'opentf.plugins.qualitygate.main.TESTCASES_CACHES',
                {'woRfkloW': testcases},
            ),
        ):
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, 'strict', 8, 'tkn', STRICT_QUALITYGATE
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]  # type: ignore
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual(
                'SUCCESS',
                quality_gate_status['rules']['Default strict quality gate']['result'],
            )

    # read quality gate definition

    def test_read_quality_gate_definition_ok(self):
        app = qualitygate.plugin

        with (
            app.app_context(),
            patch(
                'opentf.plugins.qualitygate.main._read_qualitygates',
                MagicMock(return_value=QUALITYGATES_DICT['qualitygates']),
            ),
            patch(
                'opentf.plugins.qualitygate.main.validate_schema',
                MagicMock(return_value=(True, 'bar')),
            ),
        ):
            qualitygate._refresh_configuration(app, 'foo', 'bar')
            qg = app.config['CONFIG'].get('qualitygates')
            self.assertIn('tesTqG', qg.keys())
            self.assertEqual('nOtAllTesTs', qg['tesTqG']['rules'][0]['name'])

        del app.config['CONFIG']['qualitygates']

    def test_read_quality_gate_definition_ko(self):
        app = qualitygate.plugin
        mock_read = mock_open(read_data=None)
        mock_logger = MagicMock()
        mock_logger.error = MagicMock()
        app.logger = mock_logger

        with (
            app.app_context(),
            patch('builtins.open', mock_read),
            patch(
                'opentf.plugins.qualitygate.main.validate_schema',
                MagicMock(return_value=(True, 'bar')),
            ),
        ):
            qualitygate._refresh_configuration(app, 'foo', 'bar')
            mock_read.assert_called_once()
            mock_logger.error.assert_called_once()
            self.assertIn(
                'Invalid quality gate definition file "%s": %s.  Ignoring.',
                mock_logger.error.call_args[0][0],
            )

    def test_read_quality_gate_definition_invalid_file(self):
        app = qualitygate.plugin
        mock_logger = MagicMock()
        mock_logger.error = MagicMock()
        app.logger = mock_logger

        with (
            app.app_context(),
            patch(
                'opentf.plugins.qualitygate.main.read_and_validate',
                MagicMock(side_effect=ValueError('meow')),
            ),
        ):
            qualitygate._refresh_configuration(app, 'foo', 'bar')
        mock_logger.error.assert_called_once_with(
            'Invalid quality gate definition file "%s": %s.  Ignoring.', 'foo', 'meow'
        )

    def test_read_quality_gate_definition_ko_with_exception(self):
        app = qualitygate.plugin
        mock_read = mock_open(read_data='dAtA')
        mock_read.side_effect = FileNotFoundError('File not found')
        mock_logger = MagicMock()
        mock_logger.error = MagicMock()
        app.logger = mock_logger
        vs_mock = MagicMock()

        with (
            app.app_context(),
            patch('builtins.open', mock_read),
            patch('opentf.plugins.qualitygate.main.validate_schema', vs_mock),
        ):
            qualitygate._refresh_configuration(app, 'foo', 'bar')
        mock_read.assert_called_once()
        vs_mock.assert_not_called()
        mock_logger.error.assert_called_once_with(
            'Error while reading "%s" quality gate definition: %s.',
            'foo',
            'File not found',
        )

    def test_read_quality_gate_definition_with_duplicates(self):
        app = qualitygate.plugin
        mock_warning = MagicMock()
        msg = (
            'Duplicate quality gate name(s) found in the configuration file foo: DoUblE'
        )

        with (
            app.app_context(),
            patch(
                'opentf.plugins.qualitygate.main._read_qualitygates',
                MagicMock(return_value=QUALITYGATES_WITH_DUPLICATES['qualitygates']),
            ),
            patch(
                'opentf.plugins.qualitygate.main.validate_schema',
                MagicMock(return_value=(True, 'bar')),
            ),
            patch('opentf.plugins.qualitygate.main.warning', mock_warning),
        ):
            qualitygate._refresh_configuration(app, 'foo', 'bar')
            mock_warning.assert_called_once()
            self.assertIn(msg, mock_warning.call_args[0][0])
            self.assertIn(
                msg, list(app.config['CONFIG'].get('qualitygate_warnings'))[0]
            )

    # start qualitygate app

    def test_start_qualitygate_with_definition_file(self):
        mock_wf = MagicMock()
        mock_ra = MagicMock()
        mock_svc = MagicMock()
        mock_svc.config = {
            'CONTEXT': {'services': {'observer': 'SOMETHING'}},
            'CONFIG': {'qualitygates': {}},
        }

        with (
            patch('opentf.plugins.qualitygate.main.watch_file', mock_wf),
            patch.dict('os.environ', {'QUALITYGATE_DEFINITIONS': '/high/way/to'}),
            patch('opentf.plugins.qualitygate.main.run_app', mock_ra),
            patch('opentf.plugins.qualitygate.main.CONFIG_PATH', 'path'),
            patch(
                'opentf.plugins.qualitygate.main._load_image_configuration'
            ) as mock_load,
        ):
            qualitygate.main(mock_svc)
        mock_load.assert_called_once_with()
        mock_wf.assert_called_once()
        self.assertEqual('/high/way/to', mock_wf.call_args[0][1])
        mock_ra.assert_called_once_with(mock_svc)

    def test_start_qualitygate_without_definition_file(self):
        app = qualitygate.plugin
        mock_rqd = MagicMock()
        mock_ra = MagicMock()
        mock_svc = MagicMock()

        with (
            app.app_context(),
            patch('opentf.plugins.qualitygate.main._read_qualitygates', mock_rqd),
            patch('opentf.plugins.qualitygate.main.run_app', mock_ra),
            patch('opentf.plugins.qualitygate.main.CONFIG_PATH', 'path'),
            patch(
                'opentf.plugins.qualitygate.main._load_image_configuration'
            ) as mock_load,
        ):
            qualitygate.main(mock_svc)
        mock_rqd.assert_not_called()
        mock_ra.assert_called_once_with(mock_svc)
        mock_load.assert_called_once_with()

    # handle quality gate definition

    def test_handle_qualitygate_definition_json_ok(self):
        json_data = QUALITYGATES_DICT
        meq_mock = MagicMock(return_value='oK')
        with patch(
            'opentf.plugins.qualitygate.main._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.plugin.post(
                '/workflows/00000000-0000-0000-0000-000000000009/qualitygate',
                data=json.dumps(json_data),
                headers={'Content-Type': 'application/json'},
            )
        meq_mock.assert_called_once()
        self.assertEqual(
            '00000000-0000-0000-0000-000000000009', meq_mock.call_args[0][0]
        )
        self.assertIn('tesTqG', meq_mock.call_args[0][1])
        self.assertIn('oK', response.text)

    def test_handle_qualitygate_definition_json_ko(self):
        json_data = '1+2+3'
        meq_mock = MagicMock()
        with patch(
            'opentf.plugins.qualitygate.main._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.plugin.post(
                '/workflows/00000000-0000-0000-0000-000000000000/qualitygate',
                data=json_data,
                headers={'Content-Type': 'application/json'},
            )
        self.assertTrue(
            response.json['message'].startswith(
                'Could not parse quality gate definition:'
            ),
        )
        meq_mock.assert_not_called()

    def test_handle_qualitygate_definition_yaml_ok(self):
        yaml_data = QUALITYGATES_DICT
        meq_mock = MagicMock(return_value='oKy')
        with patch(
            'opentf.plugins.qualitygate.main._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.plugin.post(
                '/workflows/00000000-0000-0000-0000-000000000001/qualitygate',
                data=json.dumps(yaml_data),
                headers={'Content-Type': 'application/x-yaml'},
            )
        meq_mock.assert_called_once()
        self.assertEqual(
            '00000000-0000-0000-0000-000000000001', meq_mock.call_args[0][0]
        )
        self.assertIn('tesTqG', meq_mock.call_args[0][1])
        self.assertIn('oKy', response.text)

    def test_handle_qualitygate_definition_yaml_ko(self):
        meq_mock = MagicMock()
        with patch(
            'opentf.plugins.qualitygate.main._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.plugin.post(
                '/workflows/00000000-0000-0000-0000-000000000000/qualitygate',
                data='',
                headers={'Content-Type': 'application/x-yaml'},
            )
        meq_mock.assert_not_called()
        self.assertIn('YAML quality gate definitions', response.json['message'])

    def test_handle_qualitygate_definition_file_ok(self):
        meq_mock = MagicMock(return_value='oKf')
        with patch(
            'opentf.plugins.qualitygate.main._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.plugin.post(
                '/workflows/00000000-0000-0000-0000-000000000002/qualitygate',
                data={
                    'qualitygates': open(
                        'tests/python/resources/qualitygate.yaml', 'rb'
                    )
                },
            )
        meq_mock.assert_called_once()
        self.assertEqual(
            '00000000-0000-0000-0000-000000000002', meq_mock.call_args[0][0]
        )
        self.assertIn('my.quality.gate', meq_mock.call_args[0][1])
        self.assertIn('oKf', response.text)

    def test_handle_qualitygate_definition_file_not_found(self):
        meq_mock = MagicMock(return_value='oKf')
        with patch(
            'opentf.plugins.qualitygate.main._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.plugin.post(
                '/workflows/00000000-0000-0000-0000-000000000000/qualitygate',
                data={'quality': open('tests/python/resources/qualitygate.yaml', 'rb')},
            )
            self.assertEqual(
                'Could not parse quality gate definition from file.',
                response.json['message'],
            )

    def test_handle_qualitygate_definition_invalid_schema(self):
        meq_mock = MagicMock(return_value='noTOk')
        vs_mock = MagicMock(return_value=(False, 'nope'))
        with (
            patch(
                'opentf.plugins.qualitygate.main._maybe_evaluate_qualitygate', meq_mock
            ),
            patch('opentf.plugins.qualitygate.main.validate_schema', vs_mock),
        ):
            response = self.plugin.post(
                '/workflows/00000000-0000-0000-0000-000000000000/qualitygate',
                data={
                    'qualitygates': open(
                        'tests/python/resources/qualitygate.yaml', 'rb'
                    )
                },
            )
        meq_mock.assert_not_called()
        self.assertEqual(
            'Not a valid quality gate definition: nope', response.json['message']
        )

    def test_handle_qualitygate_definition_invalid_workflow_id(self):
        vs_mock = MagicMock(return_value=(True, 'yep'))
        with patch('opentf.plugins.qualitygate.main.validate_schema', vs_mock):
            response = self.plugin.post(
                '/workflows/faLse_wF_Id/qualitygate?mode=my.quality.gate',
                data={
                    'qualitygates': open(
                        'tests/python/resources/qualitygate.yaml', 'rb'
                    )
                },
            )
        self.assertEqual(422, response.json['code'])
        self.assertEqual('Not a valid UUID: faLse_wF_Id.', response.json['message'])

    def test_handle_qualitygate_definition_with_name_duplicates(self):
        json_data = QUALITYGATES_WITH_DUPLICATES
        meq_mock = MagicMock(return_value='oK')
        with patch(
            'opentf.plugins.qualitygate.main._maybe_evaluate_qualitygate', meq_mock
        ):
            self.plugin.post(
                '/workflows/00000000-0000-0000-0000-000000000000/qualitygate',
                data=json.dumps(json_data),
                headers={'Content-Type': 'application/json'},
            )
        meq_mock.assert_called_once()
        self.assertIn(
            'Duplicate quality gate name(s) found: DoUblE', meq_mock.call_args[0][2][0]
        )

    # _get_testcases_from_observer

    def test_gettestcasesfromobserver_badresponse(self):
        mock_fdi = MagicMock()
        with patch('opentf.plugins.qualitygate.main._find_datasource_items', mock_fdi):
            what = qualitygate._get_testcases_from_observer('foo', 'bar')
        self.assertEqual(len(what), 1)
        self.assertIn('exception_error', what[0])

    def test_gettestcasesfromobserver_200_notc(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = MagicMock(return_value={'details': {'items': []}})
        mock_fdi = MagicMock(return_value=mock_response)
        with patch('opentf.plugins.qualitygate.main._find_datasource_items', mock_fdi):
            what = qualitygate._get_testcases_from_observer('foo', 'bar')
        self.assertEqual(len(what), 1)
        self.assertIn('EMPTY_ITEMS', what[0])

    def test_gettestcasesfromobserver_200_tc(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = MagicMock(return_value={'details': {'items': ['foo']}})
        mock_fdi = MagicMock(return_value=mock_response)
        with patch('opentf.plugins.qualitygate.main._find_datasource_items', mock_fdi):
            what = qualitygate._get_testcases_from_observer('foo', 'bar')
        self.assertEqual(len(what), 1)
        self.assertIn('foo', what)

    def test_gettestcasesfromobserver_200_keyerror(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = MagicMock(return_value={'details': {'items': ['foo']}})
        mock_fdi = MagicMock(side_effect=KeyError('boom'))
        with patch('opentf.plugins.qualitygate.main._fetch_testcases', mock_fdi):
            what = qualitygate._get_testcases_from_observer('foo', 'bar')
        self.assertEqual(len(what), 1)
        self.assertIn('exception_error', what[0])
        self.assertEqual(what[0]['exception_error'], "'boom'")

    def test_gettestcasesfromobserver_200_timeout(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = MagicMock(return_value={'details': {'items': ['foo']}})
        mock_fdi = MagicMock(side_effect=TimeoutError('boom'))
        with patch('opentf.plugins.qualitygate.main._fetch_testcases', mock_fdi):
            what = qualitygate._get_testcases_from_observer('foo', 'bar')
        self.assertEqual(len(what), 1)
        self.assertIn('exception_error', what[0])

    # testcases_cache_worker

    def test_testcasescacheworker_hit(self):
        mock_cache = {'foo': True}
        with (
            patch('opentf.plugins.qualitygate.main.TESTCASES_CACHES', mock_cache),
            patch(
                'opentf.plugins.qualitygate.main._get_testcases_from_observer'
            ) as mock_gtfo,
        ):
            qualitygate.testcases_cache_worker('foo', None)
        mock_gtfo.assert_not_called()

    def test_testcasescacheworker_miss(self):
        mock_cache = {}
        with (
            patch('opentf.plugins.qualitygate.main.TESTCASES_CACHES', mock_cache),
            patch(
                'opentf.plugins.qualitygate.main._get_testcases_from_observer'
            ) as mock_gtfo,
        ):
            qualitygate.testcases_cache_worker('foo', None)
        mock_gtfo.assert_called_once_with('foo', None)
        self.assertIn('foo', mock_cache)

    # cleanup_cache

    def test_cleanupcache(self):
        mock_pending = MagicMock()
        mock_pending.get = MagicMock(return_value=(time.time() - 1000, 'wOrkflow_id'))
        mock_pending.put = MagicMock(side_effect=Exception('boom'))
        mock_error = MagicMock(side_effect=SystemExit(1))
        with (
            patch('opentf.plugins.qualitygate.main.error', mock_error),
            patch('opentf.plugins.qualitygate.main.CLEANUP_PENDING', mock_pending),
        ):
            self.assertRaises(SystemExit, qualitygate.cleanup_cache)

    # _load_qg_definitions

    def test_load_qg_definitions_ok(self):
        with (
            patch('os.listdir', return_value=['1.yml', '2.yaml', '3.yaml']),
            patch('os.path.isfile', return_value=True),
            patch(
                'opentf.plugins.qualitygate.main._read_qualitygates',
                side_effect=[{'a': 'b'}, {'c': 'd'}, None],
            ) as mock_dict,
            patch('opentf.plugins.qualitygate.main._add_to_conf') as mock_add,
            patch('opentf.plugins.qualitygate.main.debug') as mock_debug,
        ):
            qualitygate._load_image_qualitygates()
        self.assertEqual(3, mock_dict.call_count)
        self.assertEqual(2, mock_debug.call_count)
        self.assertEqual(2, mock_add.call_count)
        self.assertEqual('1.yml', mock_add.call_args_list[0][1]['source'])
        self.assertEqual('2.yaml', mock_add.call_args_list[1][1]['source'])

    def test_load_qg_definitions_ko(self):
        with (
            patch(
                'opentf.plugins.qualitygate.main._filter_listdir', return_value=['a']
            ),
            patch(
                'opentf.plugins.qualitygate.main._read_qualitygates',
                side_effect=Exception('Boom'),
            ),
            patch('opentf.plugins.qualitygate.main.error') as mock_error,
        ):
            self.assertRaisesRegex(
                SystemExit, '2', qualitygate._load_image_qualitygates
            )
        mock_error.assert_called_once()
        self.assertEqual('Boom', mock_error.call_args[0][2])

    def test_load_qg_definitions_no_files(self):
        with (
            patch('os.listdir', return_value=[]),
            patch('opentf.plugins.qualitygate.main._read_qualitygates') as mock_dict,
            patch('opentf.plugins.qualitygate.main.debug') as mock_debug,
        ):
            qualitygate._load_image_qualitygates()
        mock_dict.assert_not_called()
        mock_debug.assert_called_once()


if __name__ == "__main__":
    unittest.main()
