# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Action Channel unit tests."""

import logging
import os
import unittest
import sys

from datetime import datetime, timedelta
from collections import defaultdict
from queue import Queue
from unittest.mock import MagicMock, patch, mock_open, call

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.agentchannel import main

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PREPARE_COMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ExecutionCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
        'channel_id': 'chAnnel_Id',
        'step_sequence_id': 123,
    },
    "scripts": [],
    "job": {
        "name": "JOBNAME",
        "runs-on": "linux",
    },
}

AGENT_NAMESPACED_MANIFEST = {
    'metadata': {'name': 'nAme', 'namespaces': 'a,c'},
    'spec': {'tags': ['abc', 'def']},
    'status': {'phase': 'IDLE'},
}


########################################################################


class TestAgentChannel(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def test_listjobsteps(self):
        JOB_ID = 'foo'
        _jobs = {}
        with patch('opentf.plugins.agentchannel.main.JOBS', _jobs):
            self.assertFalse(main.list_job_steps(JOB_ID))
        self.assertEqual(len(_jobs), 1)
        self.assertIn(JOB_ID, _jobs)

    def test_notify_available_channels(self):
        mock_dispatch = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.DISPATCH_QUEUE', mock_dispatch),
            patch('opentf.plugins.agentchannel.main.Timer') as mock_timer,
        ):
            main.notify_available_channels()
            main.notify_available_channels()
            self.assertTrue(mock_timer.called)
            self.assertEqual(main.notify_available_channels.t.start.call_count, 2)
            mock_dispatch.put.assert_not_called()
            call_it = mock_timer.call_args[0][1]
            call_it()
            mock_dispatch.put.assert_called_once()

    # debounce

    @patch('opentf.plugins.agentchannel.main.Timer')
    def test_debounce_called_once_after_wait(self, MockTimer):
        # Create a mock function to decorate
        mock_func = MagicMock()

        # Decorate it with the debounce decorator
        debounced_func = main.debounce(1)(mock_func)

        # Call the debounced function
        debounced_func()

        # Assert the Timer is started
        self.assertTrue(MockTimer.called)

        # Ensure the function is not called immediately
        mock_func.assert_not_called()

        # Simulate the timer finishing by calling the function manually
        timer_call_it = MockTimer.call_args[0][1]
        timer_call_it()

        # Ensure the function is called after the wait period
        mock_func.assert_called_once()

    @patch('opentf.plugins.agentchannel.main.Timer')
    def test_debounce_resets_timer_on_quick_successive_calls(self, MockTimer):
        mock_func = MagicMock()

        # Decorate the function with debounce
        debounced_func = main.debounce(1)(mock_func)

        # Call the function multiple times quickly
        debounced_func()
        debounced_func()
        debounced_func()

        # Ensure the Timer was restarted for each call
        self.assertEqual(MockTimer.call_count, 3)

        # Ensure the function has not been called yet
        mock_func.assert_not_called()

        # Simulate time passing to trigger the final function call
        timer_call_it = MockTimer.call_args[0][1]
        timer_call_it()

        # Ensure the function is called only once after the debounce period
        mock_func.assert_called_once()

    @patch('opentf.plugins.agentchannel.main.Timer')
    def test_debounce_cancels_previous_timer(self, MockTimer):
        mock_func = MagicMock()

        # Decorate the function with debounce
        debounced_func = main.debounce(1)(mock_func)

        # Call the function multiple times
        debounced_func()
        debounced_func()

        # Ensure the previous timer is canceled
        self.assertTrue(debounced_func.t.cancel.called)

        # Simulate the final call after the debounce period
        timer_call_it = MockTimer.call_args[0][1]
        timer_call_it()

        # Ensure the function is called once
        mock_func.assert_called_once()

    @patch('opentf.plugins.agentchannel.main.Timer')
    def test_first_call_debounced(self, MockTimer):
        mock_func = MagicMock()

        # Decorate the function with debounce
        debounced_func = main.debounce(1)(mock_func)

        # Call the function
        debounced_func()

        # Ensure that the function is not called immediately
        mock_func.assert_not_called()

        # Simulate the passage of the debounce window
        timer_call_it = MockTimer.call_args[0][1]
        timer_call_it()

        # Ensure the function is called after the debounce time
        mock_func.assert_called_once()

    @patch('opentf.plugins.agentchannel.main.Timer')
    def test_timer_cancel_on_multiple_calls(self, MockTimer):
        mock_func = MagicMock()

        # Decorate the function with debounce
        debounced_func = main.debounce(1)(mock_func)

        # Call the function twice in quick succession
        debounced_func()
        debounced_func()

        timer_call_it = MockTimer.call_args[0][1]

        # Assert that the timer was restarted
        self.assertTrue(debounced_func.t.cancel.called)

        # Simulate time passing
        timer_call_it()

        # Ensure that the function is called only once after the debounce window
        mock_func.assert_called_once()

    # make_offers

    def test_make_offers_none(self):
        mock_info = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.DISPATCH_QUEUE') as mock_queue,
            patch('opentf.plugins.agentchannel.main.info', mock_info),
        ):
            main.make_offers({'job_id': 'jOb_iD'}, [])
        mock_queue.put.assert_not_called()
        mock_info.assert_called_once()

    def test_make_offers_some(self):
        _agents = {
            'aGenT2_Id': {
                'status': {
                    'lastCommunicationTimestamp': datetime.now().isoformat(),
                    'phase': 'IDLE',
                },
                'spec': {'tags': ['windows']},
            },
        }
        mock_dispatch = MagicMock()
        mock_warning = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.DISPATCH_QUEUE', mock_dispatch),
            patch('opentf.plugins.agentchannel.main.warning', mock_warning),
            patch('opentf.plugins.agentchannel.main.AGENTS', _agents),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                MagicMock(),
            ),
        ):
            main.make_offers({'job_id': 'jOb_iD'}, ['aGenT_Id', 'aGenT2_Id'])
        mock_dispatch.put.assert_called_once()
        mock_warning.assert_not_called()

    def test_make_offers_with_hooks(self):
        _agents = {
            'aGenT2_Id': {
                'status': {
                    'lastCommunicationTimestamp': datetime.now().isoformat(),
                    'phase': 'IDLE',
                },
                'spec': {'tags': ['windows']},
            }
        }
        _hooks = {'CONFIG': {'hooks': {'event': 'eVeNt'}}, 'CONTEXT': {}}
        mock_dispatch = MagicMock()
        mock_warning = MagicMock()
        mock_me = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.DISPATCH_QUEUE', mock_dispatch),
            patch(
                'opentf.plugins.agentchannel.main.is_available',
                MagicMock(return_value=True),
            ),
            patch(
                'opentf.plugins.agentchannel.main.is_alive',
                MagicMock(return_value=True),
            ),
            patch('opentf.plugins.agentchannel.main.warning', mock_warning),
            patch('opentf.plugins.agentchannel.main.AGENTS', _agents),
            patch('opentf.plugins.agentchannel.main.plugin.config', _hooks),
            patch('opentf.plugins.agentchannel.main.make_event', mock_me),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                MagicMock(),
            ),
        ):
            main.make_offers({'job_id': 'jOb_iD'}, ['aGenT2_Id'])
        self.assertEqual(mock_me.call_count, 1)
        metadata = mock_me.call_args_list[0][1]['metadata']
        self.assertEqual({'event': 'eVeNt'}, metadata['annotations']['hooks'])
        mock_dispatch.put.assert_called_once()
        mock_warning.assert_not_called()

    # _get_phase

    def test_get_phase_unknown(self):
        with patch(
            'opentf.plugins.agentchannel.main.is_alive', MagicMock(return_value=True)
        ):
            self.assertEqual(main._get_phase('uNkNown'), 'IDLE')

    def test_get_phase_unreachable(self):
        with (
            patch(
                'opentf.plugins.agentchannel.main.is_alive',
                MagicMock(return_value=False),
            ),
            patch(
                'opentf.plugins.agentchannel.main.UNREACHABLE_SEEN',
                {'aGenT': datetime.now()},
            ),
        ):
            self.assertEqual(main._get_phase('aGenT'), 'UNREACHABLE')

    def test_get_phase_busy(self):
        host_id = 'hOsT_Id'
        with (
            patch('opentf.plugins.agentchannel.main.JOBS_AGENTS', {'job_Id': host_id}),
            patch(
                'opentf.plugins.agentchannel.main.is_alive',
                MagicMock(return_value=True),
            ),
        ):
            self.assertEqual(main._get_phase(host_id), 'BUSY')

    def test_get_phase_longrunning_busy(self):
        host_id = 'hOsT_Id'
        with (
            patch('opentf.plugins.agentchannel.main.JOBS_AGENTS', {'job_Id': host_id}),
            patch(
                'opentf.plugins.agentchannel.main.is_alive',
                MagicMock(return_value=False),
            ),
        ):
            self.assertEqual(main._get_phase(host_id), 'BUSY')

    def test_get_phase_pending_expired(self):
        host_id = 'hOsT_Id'
        with (
            patch('opentf.plugins.agentchannel.main.JOBS_AGENTS', {}),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS_LEASES',
                {host_id: datetime.now() - timedelta(seconds=1)},
            ),
            patch(
                'opentf.plugins.agentchannel.main.is_alive',
                MagicMock(return_value=True),
            ),
        ):
            self.assertEqual(main._get_phase(host_id), 'IDLE')

    def test_get_phase_pending_notexpired(self):
        host_id = 'hOsT_Id'
        with (
            patch('opentf.plugins.agentchannel.main.JOBS_AGENTS', {}),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS_LEASES',
                {host_id: datetime.now() + timedelta(seconds=10)},
            ),
            patch(
                'opentf.plugins.agentchannel.main.is_alive',
                MagicMock(return_value=True),
            ),
        ):
            self.assertEqual(main._get_phase(host_id), 'PENDING')

    # get_matching_agents

    def test_get_matching_agents_empty(self):
        with patch('opentf.plugins.agentchannel.main.AGENTS', {}):
            self.assertFalse(main.get_matching_agents({}, 'namespace'))

    def test_get_matching_agents_notempty(self):
        with patch(
            'opentf.plugins.agentchannel.main.AGENTS',
            {'foo': {'spec': {'tags': ['tag']}, 'metadata': {'namespaces': 'ns'}}},
        ):
            self.assertTrue(main.get_matching_agents({'tag'}, 'ns'))

    # process_inbox

    # def test_process_inbox_norequest(self):
    #     mock_msr = MagicMock(return_value='foo')
    #     with patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr):
    #         self.assertEqual(main.process_inbox(), 'foo')
    #     mock_msr.assert_called_once()

    def test_process_inbox_noagents(self):
        request_body = {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'ExecutionCommand',
            'metadata': {
                'name': 'foo',
                'workflow_id': 'n/a',
                'job_id': 'JOB_ID',
                'job_origin': [],
                'step_id': 'STEP_ID',
                'step_origin': [],
                'step_sequence_id': -1,
            },
            'runs-on': ['foo'],
            'scripts': [],
        }
        with (
            patch('opentf.plugins.agentchannel.main.REQUESTS_QUEUE') as mock_rq,
            patch(
                'opentf.plugins.agentchannel.main.prepare_executioncommand'
            ) as mock_pc,
        ):
            main.process_executioncommand(request_body)
        mock_rq.put.assert_not_called()
        mock_pc.assert_not_called()

    def test_process_inbox_duplicates(self):
        request_body = {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'ExecutionCommand',
            'metadata': {
                'name': 'foo',
                'workflow_id': 'n/a',
                'job_id': 'JOB_ID',
                'job_origin': [],
                'step_id': 'STEP_ID',
                'step_origin': [],
                'step_sequence_id': 0,
                'channel_id': 'cHannel_id',
            },
            'runs-on': ['foo'],
            'scripts': [],
        }
        with (
            patch('opentf.plugins.agentchannel.main.REQUESTS_QUEUE') as mock_rq,
            patch(
                'opentf.plugins.agentchannel.main.prepare_executioncommand'
            ) as mock_pc,
            patch(
                'opentf.plugins.agentchannel.main.list_job_steps',
                MagicMock(return_value=[0]),
            ),
            patch('opentf.plugins.agentchannel.main.warning') as mock_warning,
            patch('opentf.plugins.agentchannel.main.CHANNELS_AGENTS', ['cHannel_id']),
        ):
            main.process_executioncommand(request_body)
        mock_rq.put.assert_not_called()
        mock_pc.assert_not_called()
        mock_warning.assert_called_once_with('Step 0 already received for job JOB_ID.')

    # register_agent

    def test_register_agent_badjson(self):
        mock_request = MagicMock()
        mock_request.is_json = True
        mock_request.get_json = lambda: {
            {
                'apiVersion': 'opentestfactory.org/v1alpha1',
                'kind': 'AgentRegistration',
                'metadata': {
                    'name': 'aN_aGenT',
                },
                'spec': {'tags': ['foo', 'windows']},
            }
        }
        mock_msr = MagicMock()
        mock_notify = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                mock_notify,
            ),
        ):
            main.register_agent()
        mock_msr.assert_called_once()

    def test_register_agent_wo_os_tag(self):
        mock_request = MagicMock()
        mock_request.is_json = True
        mock_request.get_json = lambda: {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'AgentRegistration',
            'metadata': {
                'name': 'aN_aGenT',
            },
            'spec': {'tags': ['foo', 'notSoFoo']},
        }
        mock_msr = MagicMock()
        mock_notify = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                mock_notify,
            ),
        ):
            main.register_agent()
        mock_msr.assert_called_once_with(
            'Invalid',
            'AgentRegistration manifest tags must include one and only one of "%s"'
            % '\", \"'.join(main.RUNNER_OS),
        )

    def test_register_agent(self):
        mock_request = MagicMock()
        mock_request.is_json = True
        mock_request.get_json = lambda: {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'AgentRegistration',
            'metadata': {
                'name': 'aN_aGenT',
            },
            'spec': {'tags': ['foo', 'windows']},
        }
        mock_msr = MagicMock()
        mock_notify = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                mock_notify,
            ),
        ):
            main.register_agent()
        mock_msr.assert_called_once()

    def test_register_agent_noaccess(self):
        mock_request = MagicMock()
        mock_request.is_json = True
        mock_request.get_json = lambda: {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'AgentRegistration',
            'metadata': {
                'name': 'aN_aGenT',
            },
            'spec': {'tags': ['foo', 'windows']},
        }
        mock_msr = MagicMock()
        mock_notify = MagicMock()
        mock_can_use = MagicMock(return_value=False)
        with (
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                mock_notify,
            ),
            patch('opentf.plugins.agentchannel.main.can_use_namespace', mock_can_use),
        ):
            main.register_agent()
        mock_msr.assert_called_once()

    # list_agents

    def test_list_agents_empty(self):
        mock_ar = MagicMock()
        mock_cun = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.args = {}
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_mr = MagicMock(return_value=mock_response)
        with (
            patch('opentf.plugins.agentchannel.main.AGENTS', {}),
            patch('opentf.plugins.agentchannel.main.can_use_namespace', mock_cun),
            patch('opentf.plugins.agentchannel.main.make_response', mock_mr),
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.annotate_response', mock_ar),
        ):
            _ = main.list_agents()
        self.assertFalse(mock_response.headers)
        mock_mr.assert_called_once_with(
            {'apiVersion': 'v1', 'kind': 'AgentRegistrationsList', 'items': []}
        )
        mock_ar.assert_called_once_with(
            mock_response, processed=['fieldSelector', 'labelSelector']
        )

    def test_list_agents_nomatch(self):
        mock_ar = MagicMock()
        mock_request = MagicMock()
        mock_request.args = {}
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_mr = MagicMock(return_value=mock_response)
        with (
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {'a1': {'metadata': {'namespaces': 'a,b'}}},
            ),
            patch(
                'opentf.plugins.agentchannel.main.can_use_namespace',
                lambda n, **_: n == 'c',
            ),
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_response', mock_mr),
            patch('opentf.plugins.agentchannel.main.annotate_response', mock_ar),
        ):
            _ = main.list_agents()
        mock_mr.assert_called_once_with(
            {'apiVersion': 'v1', 'kind': 'AgentRegistrationsList', 'items': []}
        )
        mock_ar.assert_called_once()

    def test_list_agents_starmatch(self):
        mock_ar = MagicMock()
        mock_request = MagicMock()
        mock_request.args = {}
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_mr = MagicMock(return_value=mock_response)
        with (
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {
                    'a1': {'metadata': {'namespaces': 'a,b'}},
                    'a2': {'metadata': {'namespaces': '*'}},
                },
            ),
            patch(
                'opentf.plugins.agentchannel.main.can_use_namespace',
                lambda n, **_: n == 'c',
            ),
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_response', mock_mr),
            patch('opentf.plugins.agentchannel.main.annotate_response', mock_ar),
        ):
            _ = main.list_agents()
        items = mock_mr.call_args_list[0][0][0]['items']
        self.assertEqual(len(items), 1)
        self.assertEqual(items[0]['metadata']['namespaces'], '*')
        mock_ar.assert_called_once()

    def test_list_agents_twomatch(self):
        mock_ar = MagicMock()
        mock_request = MagicMock()
        mock_request.args = {}
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_mr = MagicMock(return_value=mock_response)
        with (
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {
                    'a1': {'metadata': {'namespaces': 'a,b'}},
                    'a2': {'metadata': {'namespaces': '*'}},
                    'a3': {'metadata': {'namespaces': 'c'}},
                },
            ),
            patch(
                'opentf.plugins.agentchannel.main.can_use_namespace',
                lambda n, **_: n == 'c',
            ),
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_response', mock_mr),
            patch('opentf.plugins.agentchannel.main.annotate_response', mock_ar),
        ):
            _ = main.list_agents()
        items = mock_mr.call_args_list[0][0][0]['items']
        self.assertEqual(len(items), 2)
        mock_ar.assert_called_once()
        self.assertEqual(
            mock_ar.call_args.kwargs['processed'], ['fieldSelector', 'labelSelector']
        )

    def test_list_agents_twomatch_restricted(self):
        mock_ar = MagicMock()
        mock_request = MagicMock()
        mock_request.args = {'fieldSelector': 'metadata.namespaces=c'}
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_mr = MagicMock(return_value=mock_response)
        with (
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {
                    'a1': {'metadata': {'namespaces': 'a,b'}},
                    'a2': {'metadata': {'namespaces': '*'}},
                    'a3': {'metadata': {'namespaces': 'c'}},
                },
            ),
            patch(
                'opentf.plugins.agentchannel.main.can_use_namespace',
                lambda n, **_: n == 'c',
            ),
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_response', mock_mr),
            patch('opentf.plugins.agentchannel.main.annotate_response', mock_ar),
        ):
            _ = main.list_agents()
        items = mock_mr.call_args_list[0][0][0]['items']
        self.assertEqual(len(items), 1)
        mock_ar.assert_called_once()

    def test_list_agents_twomatch_badselector(self):
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.args = {'fieldSelector': 'metadata.namespaces>c'}
        with (
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {
                    'a1': {'metadata': {'namespaces': 'a,b'}},
                    'a2': {'metadata': {'namespaces': '*'}},
                    'a3': {'metadata': {'namespaces': 'c'}},
                },
            ),
            patch(
                'opentf.plugins.agentchannel.main.can_use_namespace',
                lambda n, **_: n == 'c',
            ),
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
        ):
            main.list_agents()
        mock_msr.assert_called_once_with(
            'Invalid', 'Invalid expression metadata.namespaces>c.'
        )

    # cancel_agent

    def test_cancel_agent_notfound(self):
        mock_msr = MagicMock()
        with patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr):
            _ = main.cancel_agent('abc')
        mock_msr.assert_called_once_with('NotFound', 'Agent abc not known.')

    def test_cancel_agent_notallns(self):
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {'abc': AGENT_NAMESPACED_MANIFEST},
            ),
            patch(
                'opentf.plugins.agentchannel.main.can_use_namespace',
                lambda n, **_: n == 'a',
            ),
        ):
            _ = main.cancel_agent('abc')
        mock_msr.assert_called_once_with(
            'Forbidden',
            'Token not allowed to deregister channel: not in accessible namespaces.',
        )

    def test_cancel_agent_allns(self):
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {'abc': AGENT_NAMESPACED_MANIFEST},
            ),
            patch(
                'opentf.plugins.agentchannel.main.can_use_namespace',
                lambda n, **_: n in ('a', 'c'),
            ),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                MagicMock(),
            ),
        ):
            _ = main.cancel_agent('abc')
        mock_msr.assert_called_once_with('OK', 'Agent abc de-registered.')

    def test_cancel_agent_star(self):
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {
                    'abc': {
                        'metadata': {'name': 'nAme', 'namespaces': '*'},
                        'spec': {'tags': ['windows']},
                    }
                },
            ),
            patch(
                'opentf.plugins.agentchannel.main.can_use_namespace',
                lambda n, **_: n in ('a', 'c', '*'),
            ),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                MagicMock(),
            ),
        ):
            _ = main.cancel_agent('abc')
        mock_msr.assert_called_once_with('OK', 'Agent abc de-registered.')

    # release_resources

    def test_release_resources(self):
        with patch(
            'opentf.plugins.agentchannel.main.notify_available_channels', MagicMock()
        ):
            main.release_resources('aGenT_Id', 'jOb_iD', 'chAnnel_Id')

    # get_command

    def test_get_command_unknown(self):
        mock_msr = MagicMock()
        with patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr):
            main.get_command('aGenT_Id')
        mock_msr.assert_called_once_with('NotFound', 'Unknown agent aGenT_Id.')

    def test_get_command_empty(self):
        mock_msr = MagicMock()
        agent_id = 'aGenT_Id'
        steps_queues = {agent_id: Queue()}
        agents = {
            agent_id: {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.touch_agent', MagicMock()),
            patch('opentf.plugins.agentchannel.main.STEPS_QUEUES', steps_queues),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.get_command(agent_id)
        mock_msr.assert_called_once_with(
            'NoContent', 'Nothing to do, please try again later.'
        )
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['204'], 1
        )

    def test_get_command_simple(self):
        mock_msr = MagicMock()
        agent_id = 'aGenT_Id'
        steps_queues = {agent_id: Queue()}
        steps_queues[agent_id].put('foo')
        agents = {
            agent_id: {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.touch_agent', MagicMock()),
            patch('opentf.plugins.agentchannel.main.STEPS_QUEUES', steps_queues),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.get_command(agent_id)
        mock_msr.assert_called_once_with('OK', 'Command available.', details='foo')
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['200'], 1
        )

    def test_get_command_deferred(self):
        mock_msr = MagicMock()
        mock_ppr = MagicMock()
        agent_id = 'aGenT_Id'
        agents = {
            agent_id: {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        steps_queues = {agent_id: Queue()}
        steps_queues[agent_id].put(main.DEFERRED_PROCESSING_SENTINEL)
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.touch_agent', MagicMock()),
            patch('opentf.plugins.agentchannel.main.STEPS_QUEUES', steps_queues),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
            patch('opentf.plugins.agentchannel.main.publish_preparedresult', mock_ppr),
        ):
            main.get_command(agent_id)
        mock_ppr.assert_called_once()

    # publish_preparedresult

    def test_publish_prepared_ready(self):
        _agent_id = 'aGenT_Id'
        _deferred_result = {_agent_id: {'metadata': {'id': 'fOo'}}}
        mock_publish = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.DEFERRED_RESULT', _deferred_result),
            patch('opentf.plugins.agentchannel.main.publish_and_hold', mock_publish),
            patch('opentf.plugins.agentchannel.main.LOCALSTORE', {}),
            patch('opentf.plugins.agentchannel.main.PENDINGSTORE', {}),
        ):
            main.publish_preparedresult(_agent_id)
        mock_publish.assert_called_once()

    def test_publish_prepared_with_upload(self):
        _agent_id = 'aId'
        _deferred_result = {_agent_id: {'metadata': {'id': 'fOo', 'upload': 0}}}
        mock_publish = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.DEFERRED_RESULT', _deferred_result),
            patch('opentf.plugins.agentchannel.main.publish_and_hold', mock_publish),
            patch('opentf.plugins.agentchannel.main.LOCALSTORE', {}),
            patch('opentf.plugins.agentchannel.main.PENDINGSTORE', {}),
            patch(
                'opentf.plugins.agentchannel.main.process_upload', return_value='foo'
            ) as mock_upload,
            patch('opentf.plugins.agentchannel.main.DISPATCH_QUEUE') as mock_queue,
        ):
            main.publish_preparedresult(_agent_id)
        mock_publish.assert_called_once()
        mock_upload.assert_called_once_with({'metadata': {'id': 'fOo', 'upload': 0}})
        mock_queue.put.assert_called_once_with('foo')

    # set_execution_result

    def test_set_execution_result_unknown(self):
        mock_msr = MagicMock()
        with patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr):
            main.set_execution_result('aGenT_Id')
        mock_msr.assert_called_once_with('NotFound', 'Unknown agent aGenT_Id.')

    def test_set_execution_result_badrequest(self):
        mock_msr = MagicMock()
        mock_req = MagicMock()
        mock_req.get_json = MagicMock(side_effect=Exception('ooOps'))
        agent_id = 'aGenT_Id'
        agents = {
            agent_id: {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.touch_agent', MagicMock()),
            patch('opentf.plugins.agentchannel.main.request', mock_req),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.set_execution_result(agent_id)
        mock_msr.assert_called_once_with('BadRequest', 'Could not parse body: ooOps.')
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['400'], 1
        )

    def test_set_execution_result_invalid(self):
        mock_msr = MagicMock()
        mock_req = MagicMock()
        mock_req.get_json = MagicMock(return_value='ooOps')
        agent_id = 'aGenT_Id'
        agents = {
            agent_id: {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.touch_agent', MagicMock()),
            patch('opentf.plugins.agentchannel.main.request', mock_req),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.set_execution_result(agent_id)
        mock_msr.assert_called_once_with('Invalid', 'Body is not an object.')
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['422'], 1
        )

    def test_set_execution_result_error(self):
        mock_msr = MagicMock()
        mock_req = MagicMock()
        mock_req.get_json = MagicMock(return_value={'details': {'msg': 'ooOops'}})
        agent_id = 'aGenT_Id'
        agents = {
            agent_id: {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.touch_agent', MagicMock()),
            patch('opentf.plugins.agentchannel.main.request', mock_req),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
            patch('opentf.plugins.agentchannel.main.DISPATCH_QUEUE'),
            patch(
                'opentf.plugins.agentchannel.main.STEPS',
                {agent_id: {'metadata': 'meTaDatA'}},
            ),
            patch(
                'opentf.plugins.agentchannel.main.STEPS_QUEUES', {agent_id: 'aqueue'}
            ),
        ):
            main.set_execution_result(agent_id)
        mock_msr.assert_called_once_with('OK', 'Propagating execution error.')
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['200'], 1
        )

    def test_set_execution_result_noattach(self):
        mock_msr = MagicMock()
        mock_req = MagicMock()
        mock_req.get_json = MagicMock(return_value={'foo': 'yaDa'})
        agent_id = 'aGenT_Id'
        agents = {
            agent_id: {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.touch_agent', MagicMock()),
            patch('opentf.plugins.agentchannel.main.request', mock_req),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
            patch(
                'opentf.plugins.agentchannel.main.STEPS',
                {agent_id: {'metadata': 'meTaDatA'}},
            ),
            patch('opentf.plugins.agentchannel.main.prepare_result', MagicMock()),
        ):
            main.set_execution_result(agent_id)
        mock_msr.assert_called_once_with('OK', 'Results published.')
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['200'], 1
        )

    def test_set_execution_result_attach(self):
        mock_msr = MagicMock()
        mock_req = MagicMock()
        mock_req.get_json = MagicMock(return_value={'foo': 'yaDa'})
        mock_sem = MagicMock()
        mock_sem.acquire = MagicMock()
        agent_id = 'aGenT_Id'
        semaphores = {agent_id: mock_sem}
        agents = {
            agent_id: {'status': {'communicationStatusSummary': defaultdict(int)}}
        }

        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.touch_agent', MagicMock()),
            patch('opentf.plugins.agentchannel.main.request', mock_req),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
            patch(
                'opentf.plugins.agentchannel.main.STEPS',
                {agent_id: {'metadata': 'meTaDatA'}},
            ),
            patch(
                'opentf.plugins.agentchannel.main.LOCALSTORE',
                {agent_id: {}},
            ),
            patch(
                'opentf.plugins.agentchannel.main.STEPS_QUEUES',
                {agent_id: Queue()},
            ),
            patch(
                'opentf.plugins.agentchannel.main.PENDINGSTORE',
                {agent_id: {}},
            ),
            patch(
                'opentf.plugins.agentchannel.main.prepare_result',
                MagicMock(return_value=False),
            ),
            patch('opentf.plugins.agentchannel.main.AGENTS_EVENTS', semaphores),
        ):
            main.set_execution_result(agent_id)
        mock_msr.assert_called_once_with('OK', 'Awaiting attachments.')
        mock_sem.acquire.assert_not_called()

    # publish_and_hold

    def test_publish_and_hold_reuse(self):
        agent_id = 'aGenT_Id'
        mock_evt = MagicMock()
        mock_evt.is_set = MagicMock(return_value=True)
        mock_evt.clear = MagicMock()
        with patch(
            'opentf.plugins.agentchannel.main.AGENTS_EVENTS', {agent_id: mock_evt}
        ):
            main.publish_and_hold('yada', agent_id)
        mock_evt.is_set.assert_called_once()
        mock_evt.clear.assert_called_once()

    def test_publish_and_hold_new(self):
        agent_id = 'aGenT_Id'
        mock_evt = MagicMock()
        mock_evt.is_set = MagicMock(return_value=False)
        mock_evt.clear = MagicMock()
        mock_event = MagicMock()
        agents_events = {agent_id: mock_evt}
        with (
            patch('opentf.plugins.agentchannel.main.AGENTS_EVENTS', agents_events),
            patch('opentf.plugins.agentchannel.main.Event', mock_event),
        ):
            main.publish_and_hold('yada', agent_id)
        mock_evt.is_set.assert_called_once()
        mock_evt.clear.assert_not_called()
        mock_event.assert_called_once()
        self.assertNotEqual(agents_events[agent_id], mock_evt)

    # prepare_command

    def test_prepare_command(self):
        mock_evt = MagicMock()
        mock_evt.set = MagicMock()
        with (
            patch(
                'opentf.plugins.agentchannel.main.CHANNELS_AGENTS',
                {'chAnnel_Id': 'aGenT_Id'},
            ),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {'aGenT_Id': {'spec': {}, 'metadata': {}}},
            ),
            patch('opentf.plugins.agentchannel.main.get_os', lambda _: 'linux'),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS_EVENTS',
                {'aGenT_Id': mock_evt},
            ),
        ):
            main.prepare_executioncommand(PREPARE_COMMAND)
        mock_evt.set.assert_called_once()

    def test_prepare_command_new(self):
        mock_sem = MagicMock()
        mock_sem.release = MagicMock()
        with (
            patch(
                'opentf.plugins.agentchannel.main.CHANNELS_AGENTS',
                {'chAnnel_Id': ['aGenT_Id']},
            ),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS', {'aGenT_Id': {'spec': {}}}
            ),
            patch('opentf.plugins.agentchannel.main.get_os', lambda _: 'linux'),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS_EVENTS',
                {'aGenT_Id': mock_sem},
            ),
        ):
            main.prepare_executioncommand(PREPARE_COMMAND)
        mock_sem.release.assert_not_called()

    # prepare_result

    def test_prepare_result_deferred(self):
        agent_id = 'aGenT_Id'
        steps = {agent_id: {'metadata': {'job_id': 'job_Id', 'step_sequence_id': 1}}}
        mock_po = MagicMock(return_value={'attachments': True})
        mock_queue = MagicMock()
        mock_queue.put = MagicMock()
        steps_queues = {agent_id: mock_queue}
        body = {'exit_status': 0, 'stdout': ['stdout'], 'stderr': []}
        deferred_result = {}
        with (
            patch('opentf.plugins.agentchannel.main.process_output', mock_po),
            patch('opentf.plugins.agentchannel.main.STEPS', steps),
            patch('opentf.plugins.agentchannel.main.STEPS_QUEUES', steps_queues),
            patch('opentf.plugins.agentchannel.main.DEFERRED_RESULT', deferred_result),
        ):
            result = main.prepare_result(body, 'aGenT_Id')
        self.assertFalse(result)
        mock_po.assert_called_once()
        mock_queue.put.assert_called_once()

    def test_prepare_result_hold(self):
        agent_id = 'aGenT_Id'
        steps = {agent_id: {'metadata': {'job_id': 'job_Id', 'step_sequence_id': 1}}}
        mock_po = MagicMock(return_value={})
        mock_queue = MagicMock()
        mock_queue.put = MagicMock()
        steps_queues = {agent_id: mock_queue}
        body = {'exit_status': 0, 'stdout': ['stdout'], 'stderr': []}
        mock_pah = MagicMock()
        deferred_result = {}
        localstore = {agent_id: []}
        with (
            patch('opentf.plugins.agentchannel.main.process_output', mock_po),
            patch('opentf.plugins.agentchannel.main.STEPS', steps),
            patch('opentf.plugins.agentchannel.main.STEPS_QUEUES', steps_queues),
            patch('opentf.plugins.agentchannel.main.DEFERRED_RESULT', deferred_result),
            patch('opentf.plugins.agentchannel.main.publish_and_hold', mock_pah),
            patch('opentf.plugins.agentchannel.main.LOCALSTORE', localstore),
        ):
            result = main.prepare_result(body, 'aGenT_Id')
        self.assertTrue(result)
        mock_po.assert_called_once()
        mock_queue.put.assert_not_called()
        mock_pah.assert_called_once()

    def test_prepare_result_release_nohold(self):
        agent_id = 'aGenT_Id'
        steps = {agent_id: {'metadata': {'job_id': 'job_Id', 'step_sequence_id': -2}}}
        mock_po = MagicMock(return_value={})
        mock_queue = MagicMock()
        steps_queues = {agent_id: mock_queue}
        body = {'exit_status': 0, 'stdout': ['stdout'], 'stderr': []}
        mock_pah = MagicMock()
        deferred_result = {}
        localstore = {agent_id: []}
        mock_dispatch = MagicMock()
        mock_rr = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.process_output', mock_po),
            patch('opentf.plugins.agentchannel.main.STEPS', steps),
            patch('opentf.plugins.agentchannel.main.STEPS_QUEUES', steps_queues),
            patch('opentf.plugins.agentchannel.main.DEFERRED_RESULT', deferred_result),
            patch('opentf.plugins.agentchannel.main.publish_and_hold', mock_pah),
            patch('opentf.plugins.agentchannel.main.LOCALSTORE', localstore),
            patch('opentf.plugins.agentchannel.main.DISPATCH_QUEUE', mock_dispatch),
            patch('opentf.plugins.agentchannel.main.release_resources', mock_rr),
        ):
            result = main.prepare_result(body, 'aGenT_Id')
        self.assertTrue(result)
        mock_po.assert_called_once()
        mock_queue.put.assert_not_called()
        mock_pah.assert_not_called()
        mock_dispatch.put.assert_called_once()
        mock_rr.assert_called_once()

    # touch_agent

    def test_touch_agent_unknown(self):
        with patch('opentf.plugins.agentchannel.main.AGENTS', {}):
            self.assertIsNone(main.touch_agent('aGenT_Id'))

    def test_touch_agent_known(self):
        agents = {
            'aGenT_Id': {
                'status': {'lastCommunicationTimestamp': 'foo', 'communicationCount': 0}
            }
        }
        with patch('opentf.plugins.agentchannel.main.AGENTS', agents):
            self.assertIsNone(main.touch_agent('aGenT_Id'))
        self.assertNotEqual(
            agents['aGenT_Id']['status']['lastCommunicationTimestamp'], 'foo'
        )
        self.assertEqual(agents['aGenT_Id']['status']['communicationCount'], 1)

    # add_file

    def test_add_file_unknown_agent(self):
        mock_dummy = MagicMock()
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.touch_agent', mock_dummy),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
        ):
            main.add_file('aGenT_Id', 'fIle_Id')
        mock_msr.assert_called_with(
            'NotFound', 'No attachment expected from agent_id aGenT_Id.'
        )

    def test_add_file_no_pending(self):
        mock_dummy = MagicMock()
        mock_msr = MagicMock()
        agents = {
            'aGenT_Id': {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.touch_agent', mock_dummy),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.PENDINGSTORE', {'aGenT_Id': []}),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.add_file('aGenT_Id', 'fIle_Id')
        mock_msr.assert_called_with(
            'NotFound', 'Attachment fIle_Id not found for agent aGenT_Id.'
        )
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['404'], 1
        )

    def test_add_file_fine(self):
        mock_dummy = MagicMock()
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.stream.read = lambda _: ''
        pendingstore = {'aGenT_Id': {'fIle_Id': 'foo'}}
        agents = {
            'aGenT_Id': {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.touch_agent', mock_dummy),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.PENDINGSTORE', pendingstore),
            patch('builtins.open', mock_open()),
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.add_file('aGenT_Id', 'fIle_Id')
        mock_msr.assert_called_with('OK', 'Content received for fIle_Id.')
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['200'], 1
        )

    def test_add_file_readfail(self):
        mock_dummy = MagicMock()
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.stream.read = MagicMock(side_effect=Exception('ooOps'))
        pendingstore = {'aGenT_Id': {'fIle_Id': 'foo'}}
        agents = {
            'aGenT_Id': {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.touch_agent', mock_dummy),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.PENDINGSTORE', pendingstore),
            patch('builtins.open', mock_open()),
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.add_file('aGenT_Id', 'fIle_Id')
        mock_msr.assert_called_with(
            'BadRequest', 'Failed to get attachment fIle_Id: ooOps.'
        )
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['400'], 1
        )

    # get_file

    def test_get_file_unknown_agent(self):
        mock_dummy = MagicMock()
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.touch_agent', mock_dummy),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
        ):
            main.get_file('aGenT_Id', 'fIle_Id')
        mock_msr.assert_called_with('NotFound', 'No file for agent_id aGenT_Id.')

    def test_get_file_no_pending(self):
        mock_dummy = MagicMock()
        mock_msr = MagicMock()
        agents = {
            'aGenT_Id': {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.touch_agent', mock_dummy),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.LOCALSTORE', {'aGenT_Id': []}),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.get_file('aGenT_Id', 'fIle_Id')
        mock_msr.assert_called_with(
            'NotFound', 'File fIle_Id not found for agent aGenT_Id.'
        )
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['404'], 1
        )

    def test_get_file_baddata(self):
        mock_dummy = MagicMock()
        mock_msr = MagicMock()
        localstore = {'aGenT_Id': {'fIle_Id': {'bad': 12}}}
        agents = {
            'aGenT_Id': {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.touch_agent', mock_dummy),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.LOCALSTORE', localstore),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.get_file('aGenT_Id', 'fIle_Id')
        mock_msr.assert_called_with(
            'InternalError',
            'Invalid data in LOCALSTORE: contains neither "content" nor "filepath" key.',
        )
        self.assertNotIn('fIle_Id', localstore['aGenT_Id'])
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['500'], 1
        )

    def test_get_file_badcontent(self):
        mock_dummy = MagicMock()
        mock_msr = MagicMock()
        localstore = {'aGenT_Id': {'fIle_Id': {'content': '12', 'filename': 'fOO'}}}
        agents = {
            'aGenT_Id': {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.touch_agent', mock_dummy),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.LOCALSTORE', localstore),
            patch(
                'opentf.plugins.agentchannel.main.send_file',
                MagicMock(side_effect=OSError('ooOps')),
            ),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.get_file('aGenT_Id', 'fIle_Id')
        mock_msr.assert_called_with(
            'InternalError', 'Could not deliver file fIle_Id: ooOps.'
        )
        self.assertNotIn('fIle_Id', localstore['aGenT_Id'])
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['500'], 1
        )

    def test_get_file_badfilepath(self):
        mock_dummy = MagicMock()
        mock_msr = MagicMock()
        localstore = {'aGenT_Id': {'fIle_Id': {'filepath': '12', 'filename': 'fOO'}}}
        agents = {
            'aGenT_Id': {'status': {'communicationStatusSummary': defaultdict(int)}}
        }
        with (
            patch('opentf.plugins.agentchannel.main.touch_agent', mock_dummy),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch('opentf.plugins.agentchannel.main.LOCALSTORE', localstore),
            patch(
                'opentf.plugins.agentchannel.main.send_file',
                MagicMock(side_effect=OSError('ooOps')),
            ),
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
        ):
            main.get_file('aGenT_Id', 'fIle_Id')
        mock_msr.assert_called_with(
            'InternalError', 'Could not deliver file fIle_Id: ooOps.'
        )
        self.assertNotIn('fIle_Id', localstore['aGenT_Id'])
        self.assertEqual(
            agents['aGenT_Id']['status']['communicationStatusSummary']['500'], 1
        )

    # get_job_agent

    def test_get_job_agent_unknownchannelid(self):
        self.assertIsNone(main.get_job_agent('jOb_Id', 'wOrKflow_iD', 'chAnnel_Id'))

    def test_get_job_agent_alreadyknown(self):
        with patch(
            'opentf.plugins.agentchannel.main.JOBS_AGENTS', {'jOb_Id': 'aGenT_Id'}
        ):
            self.assertEqual(
                'aGenT_Id', main.get_job_agent('jOb_Id', 'wOrKflow_iD', 'chAnnel_Id')
            )

    def test_get_job_agent_otherjob(self):
        with patch(
            'opentf.plugins.agentchannel.main.CHANNELS_AGENTS',
            {'chAnnel_Id': 'aGenT_Id'},
        ):
            self.assertIsNone(main.get_job_agent('jOb_Id', 'wOrKflow_iD', 'chAnnel_Id'))

    def test_get_job_agent_candidatenolease(self):
        with patch(
            'opentf.plugins.agentchannel.main.CHANNELS_AGENTS',
            {'chAnnel_Id': ['aGenT_Id']},
        ):
            self.assertIsNone(main.get_job_agent('jOb_Id', 'wOrKflow_iD', 'chAnnel_Id'))

    def test_get_job_agent_candidatelease(self):
        _agents = {
            'aGenT_Id': {
                'metadata': {'name': 'aGenT_nAme'},
                'status': {'phase': 'PENDING'},
            }
        }
        _jobsagents = {}
        with (
            patch(
                'opentf.plugins.agentchannel.main.CHANNELS_AGENTS',
                {'chAnnel_Id': ['baDAgenT_Id', 'aGenT_Id']},
            ),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS_LEASES',
                {
                    'aGenT_Id': datetime.now()
                    + timedelta(seconds=main.DEFAULT_CHANNEL_LEASE)
                },
            ),
            patch('opentf.plugins.agentchannel.main.AGENTS', _agents),
            patch('opentf.plugins.agentchannel.main.JOBS_AGENTS', _jobsagents),
            patch('opentf.plugins.agentchannel.main.notify_available_channels'),
        ):
            self.assertEqual(
                'aGenT_Id', main.get_job_agent('jOb_Id', 'wOrKflow_iD', 'chAnnel_Id')
            )
        self.assertEqual(_agents['aGenT_Id']['status']['currentJobID'], 'jOb_Id')
        self.assertEqual(_jobsagents['jOb_Id'], 'aGenT_Id')

    # is_available

    def test_is_available_dummy(self):
        self.assertTrue(main.is_available('aGenT_Id'))

    def test_is_available_busy(self):
        _jobsagents = {'jOb_Id': 'aGenT_Id'}
        with patch('opentf.plugins.agentchannel.main.JOBS_AGENTS', _jobsagents):
            self.assertFalse(main.is_available('aGenT_Id'))

    # is_alive

    def test_is_alive_dummy(self):
        self.assertFalse(main.is_alive('aGenT_Id'))

    def test_is_alive_unreachable(self):
        _agents = {
            'Tux': {
                'status': {'lastCommunicationTimestamp': '2023-05-02T12:00:20.663245'},
                'spec': {'liveness_probe': 4},
            }
        }
        with patch('opentf.plugins.agentchannel.main.AGENTS', _agents):
            self.assertFalse(main.is_alive('Tux'))

    # refresh_agents

    def test_refresh(self):
        mock_agents = {
            'aGenT_Id': {
                'metadata': {'name': 'aGenT_nAme'},
                'status': {
                    'lastCommunicationTimestamp': 'foo',
                    'communicationCount': 123,
                },
            }
        }
        mock_request = MagicMock()
        mock_request.is_json = True
        mock_request.get_json = MagicMock(return_value={'aGenT_Id': {}})
        with (
            patch('opentf.plugins.agentchannel.main.AGENTS', mock_agents),
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_status_response') as mock_msr,
        ):
            _ = main.refresh_agents()
        self.assertEqual(mock_agents['aGenT_Id']['status']['communicationCount'], 124)
        self.assertNotEqual(
            mock_agents['aGenT_Id']['status']['lastCommunicationTimestamp'], 'foo'
        )
        mock_msr.assert_called_once_with('OK', 'Agents status refreshed.')

    def test_refresh_bad(self):
        mock_request = MagicMock()
        mock_request.is_json = False
        with (
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_status_response') as mock_msr,
        ):
            _ = main.refresh_agents()
        mock_msr.assert_called_once_with('BadRequest', 'Not a JSON document.')

    # telemetry

    def test_set_metrics_get_job_agent(self):
        _agents = {
            'aGenT_Id': {
                'metadata': {'name': 'aGenT_nAme'},
                'status': {'phase': 'PENDING'},
            }
        }
        _jobsagents = {}
        with (
            patch(
                'opentf.plugins.agentchannel.main.CHANNELS_AGENTS',
                {'chAnnel_Id': ['baDAgenT_Id', 'aGenT_Id']},
            ),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS_LEASES',
                {
                    'aGenT_Id': datetime.now()
                    + timedelta(seconds=main.DEFAULT_CHANNEL_LEASE)
                },
            ),
            patch('opentf.plugins.agentchannel.main.AGENTS', _agents),
            patch('opentf.plugins.agentchannel.main.JOBS_AGENTS', _jobsagents),
            patch('opentf.plugins.agentchannel.main.notify_available_channels'),
            patch('opentf.plugins.agentchannel.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            self.assertEqual(
                'aGenT_Id', main.get_job_agent('jOb_Id', 'wOrKflow_iD', 'chAnnel_Id')
            )
        self.assertEqual(2, mock_manager.set_metric.call_count)
        calls = mock_manager.set_metric.call_args_list
        assert call(main.AgentChannelMetrics.CHANNELS_PENDING_GAUGE, -1) in calls
        assert call(main.AgentChannelMetrics.CHANNELS_BUSY_GAUGE, 1) in calls

    def test_set_metrics_touch_agent(self):
        agents = {
            'aGenT_Id': {
                'status': {'lastCommunicationTimestamp': 'foo', 'communicationCount': 0}
            }
        }
        seen = {'aGenT_Id': 'foo'}
        with (
            patch('opentf.plugins.agentchannel.main.AGENTS', agents),
            patch('opentf.plugins.agentchannel.main.UNREACHABLE_SEEN', seen),
            patch('opentf.plugins.agentchannel.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.touch_agent('aGenT_Id')
        self.assertEqual({}, seen)
        mock_manager.set_metric.assert_called_once()
        assert (
            call(main.AgentChannelMetrics.CHANNELS_UNREACHABLE_GAUGE, -1)
            in mock_manager.set_metric.call_args_list
        )

    def test_set_metrics_get_phase(self):
        seen = {}
        with (
            patch(
                'opentf.plugins.agentchannel.main.is_alive',
                MagicMock(return_value=False),
            ),
            patch('opentf.plugins.agentchannel.main.UNREACHABLE_SEEN', seen),
            patch('opentf.plugins.agentchannel.main.TELEMETRY_MANAGER') as mock_manager,
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {'aGenT': {'status': {'phase': 'IDLE'}}},
            ),
        ):
            self.assertEqual(main._get_phase('aGenT'), 'UNREACHABLE')
        self.assertIn('aGenT', seen)
        self.assertEqual(2, mock_manager.set_metric.call_count)
        calls = mock_manager.set_metric.call_args_list
        assert call(main.AgentChannelMetrics.CHANNELS_IDLE_GAUGE, -1) in calls
        assert call(main.AgentChannelMetrics.CHANNELS_UNREACHABLE_GAUGE, 1) in calls

    def test_set_metrics_make_offers(self):
        _agents = {
            'aGenT4_Id': {
                'status': {
                    'lastCommunicationTimestamp': datetime.now().isoformat(),
                    'phase': 'IDLE',
                },
                'spec': {'tags': ['windows']},
            },
        }
        mock_dispatch = MagicMock()
        mock_warning = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.DISPATCH_QUEUE', mock_dispatch),
            patch('opentf.plugins.agentchannel.main.warning', mock_warning),
            patch('opentf.plugins.agentchannel.main.AGENTS', _agents),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                MagicMock(),
            ),
            patch('opentf.plugins.agentchannel.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.make_offers({'job_id': 'jOb_iD_2'}, ['aGenT_Id', 'aGenT4_Id'])
        self.assertEqual(3, mock_manager.set_metric.call_count)
        calls = mock_manager.set_metric.call_args_list
        assert call(main.AgentChannelMetrics.CHANNELS_IDLE_GAUGE, -1) in calls
        assert call(main.AgentChannelMetrics.CHANNEL_OFFERS_COUNTER, 1) in calls
        assert call(main.AgentChannelMetrics.CHANNELS_PENDING_GAUGE, 1) in calls

    def test_set_metrics_release_resources(self):
        with (
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                MagicMock(),
            ),
            patch('opentf.plugins.agentchannel.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.release_resources('aGenT_Id', 'jOb_iD', 'chAnnel_Id')
        self.assertEqual(2, mock_manager.set_metric.call_count)
        calls = mock_manager.set_metric.call_args_list
        assert call(main.AgentChannelMetrics.CHANNELS_IDLE_GAUGE, 1) in calls
        assert call(main.AgentChannelMetrics.CHANNELS_BUSY_GAUGE, -1) in calls

    def test_set_metrics_process_executioncommand(self):
        request_body = {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'ExecutionCommand',
            'metadata': {
                'name': 'foo',
                'workflow_id': 'n/a',
                'job_id': 'JOB_ID',
                'job_origin': [],
                'step_id': 'STEP_ID',
                'step_origin': [],
                'step_sequence_id': -1,
            },
            'runs-on': ['foo'],
            'scripts': [],
        }
        with (
            patch('opentf.plugins.agentchannel.main.REQUESTS_QUEUE'),
            patch('opentf.plugins.agentchannel.main.prepare_executioncommand'),
            patch('opentf.plugins.agentchannel.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.process_executioncommand(request_body)
        mock_manager.set_metric.assert_called_once()
        assert (
            call(main.AgentChannelMetrics.CHANNEL_REQUESTS_COUNTER, 1)
            in mock_manager.set_metric.call_args_list
        )

    def test_set_metrics_register_agent(self):
        mock_request = MagicMock()
        mock_request.is_json = True
        mock_request.get_json = lambda: {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'AgentRegistration',
            'metadata': {
                'name': 'aN_aGenT',
            },
            'spec': {'tags': ['foo', 'windows']},
        }
        mock_msr = MagicMock()
        mock_notify = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.request', mock_request),
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                mock_notify,
            ),
            patch('opentf.plugins.agentchannel.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.register_agent()
        self.assertEqual(5, mock_manager.set_metric.call_count)
        calls = mock_manager.set_metric.call_args_list
        assert call(main.AgentChannelMetrics.REGISTRATIONS_COUNTER, 1) in calls
        assert call(main.AgentChannelMetrics.REGISTRATIONS_CURRENT_GAUGE, 1) in calls
        assert call(main.AgentChannelMetrics.CHANNELS_IDLE_GAUGE, 1) in calls
        assert (
            call(main.AgentChannelMetrics.CHANNELS_TAGS_GAUGE, 1, {'tag': 'foo'})
            in calls
        )
        assert (
            call(main.AgentChannelMetrics.CHANNELS_TAGS_GAUGE, 1, {'tag': 'windows'})
            in calls
        )

    def test_set_metrics_deregister_agent(self):
        mock_msr = MagicMock()
        seen = {'abc': 'foobar'}
        with (
            patch('opentf.plugins.agentchannel.main.make_status_response', mock_msr),
            patch(
                'opentf.plugins.agentchannel.main.AGENTS',
                {'abc': AGENT_NAMESPACED_MANIFEST},
            ),
            patch(
                'opentf.plugins.agentchannel.main.can_use_namespace',
                lambda n, **_: n in ('a', 'c'),
            ),
            patch(
                'opentf.plugins.agentchannel.main.notify_available_channels',
                MagicMock(),
            ),
            patch('opentf.plugins.agentchannel.main.UNREACHABLE_SEEN', seen),
            patch('opentf.plugins.agentchannel.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            _ = main.cancel_agent('abc')
        self.assertEqual({}, seen)
        self.assertEqual(6, mock_manager.set_metric.call_count)
        calls = mock_manager.set_metric.call_args_list
        assert call(main.AgentChannelMetrics.DEREGISTRATIONS_COUNTER, 1) in calls
        assert call(main.AgentChannelMetrics.REGISTRATIONS_CURRENT_GAUGE, -1) in calls
        assert call(main.AgentChannelMetrics.CHANNELS_IDLE_GAUGE, -1)
        assert call(main.AgentChannelMetrics.CHANNELS_TAGS_GAUGE, -1, {'tag': 'abc'})
        assert call(main.AgentChannelMetrics.CHANNELS_TAGS_GAUGE, -1, {'tag': 'def'})
        assert call(main.AgentChannelMetrics.CHANNELS_UNREACHABLE_GAUGE, -1)

    # main

    def test_main(self):
        mock_runplugin = MagicMock()
        mock_watch = MagicMock()
        with (
            patch('opentf.plugins.agentchannel.main.Thread'),
            patch('opentf.plugins.agentchannel.main.run_plugin', mock_runplugin),
            patch('opentf.plugins.agentchannel.main.watch_and_notify', mock_watch),
        ):
            main.main()
        mock_runplugin.assert_called_once()
        mock_watch = MagicMock()


if __name__ == '__main__':
    unittest.main()
