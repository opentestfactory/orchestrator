# Copyright (c) 2021, 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Receptionist unit tests."""

import logging
import os
import unittest
import sys

from unittest.mock import MagicMock, patch

from requests import Response

import yaml


mockresponse = Response()
mockresponse.status_code = 200

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.core import receptionist

########################################################################

BAD_YAML_WORKFLOW = '''jobs:
    launchGenerator:
      generator: tm.squashtest.org/tm.generator@v1
      with:
        foo: bar
      runs-on:
      - linux
kind: Workflow
    name: Workflow avec generator
    namespace: default
'''

########################################################################

with open('tests/python/resources/order_1.yaml') as f:
    ORDER_1 = f.read()


class TestReceptionist(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        app = receptionist.app
        app.config['CONTEXT']['enable_insecure_login'] = True
        self.app = app.test_client()
        self.assertFalse(app.debug)

    def test_ping(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows?ping',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Workflow",
                    "metadata": {"name": "foo"},
                    "jobs": {"tests": {"generator": "xxx"}},
                },
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json['message'], 'Pong!')
        mock.assert_not_called()

    # low-level tests

    def test_get_workflow_badjson(self):
        mock_request = MagicMock()
        mock_request.is_json = True
        mock_request.get_json = MagicMock(side_effect=Exception('bOOm'))
        with patch('opentf.core.receptionist.request', mock_request):
            self.assertRaises(
                receptionist.InvalidRequestError, receptionist._get_workflow
            )

    def test_get_workflow_yamlwithnolength(self):
        mock_request = MagicMock()
        mock_request.is_json = False
        mock_request.mimetype = 'text/yaml'
        mock_request.content_length = 0
        with patch('opentf.core.receptionist.request', mock_request):
            self.assertRaises(
                receptionist.InvalidRequestError, receptionist._get_workflow
            )

    def test_get_workflow_multipartduplicates(self):
        mock_request = MagicMock()
        mock_request.is_json = False
        mock_request.mimetype = 'multipart/form-data'
        mock_duplicate = MagicMock(return_value='foo bar')
        with (
            patch('opentf.core.receptionist.request', mock_request),
            patch('opentf.core.receptionist._list_duplicates', mock_duplicate),
        ):
            self.assertRaises(
                receptionist.InvalidRequestError, receptionist._get_workflow
            )
        mock_duplicate.assert_called_once()

    # high-level tests

    def test_simpleworkflow(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Workflow",
                    "metadata": {"name": "foo"},
                    "jobs": {"tests": {"generator": "xxx"}},
                },
            )
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(response.json['details'].get('workflow_id'))
        mock.assert_called_once()

    def test_simpleworkflow_dryrun(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows?dryRun',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Workflow",
                    "metadata": {"name": "foo"},
                    "jobs": {"tests": {"generator": "xxx"}},
                },
            )
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(response.json['details'].get('workflow_id'))
        mock.assert_not_called()

    def test_workflow_bad_yaml(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data=BAD_YAML_WORKFLOW,
                headers={'Content-type': 'application/x-yaml'},
            )
        self.assertEqual(response.status_code, 422)
        mock.assert_not_called()

    def test_workflow_yaml(self):
        workflow = yaml.safe_load(ORDER_1)

        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data=yaml.safe_dump(workflow),
                headers={'Content-type': 'application/x-yaml'},
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()

    def test_workflow_json(self):
        workflow = yaml.safe_load(ORDER_1)

        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                json=workflow,
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()

    def test_workflow_json_unauthorizedns(self):
        workflow = yaml.safe_load(ORDER_1)

        mock = MagicMock(return_value=mockresponse)
        mock_can_use = MagicMock(return_value=False)
        with (
            patch('opentf.core.receptionist.publish', mock),
            patch('opentf.core.receptionist.can_use_namespace', mock_can_use),
        ):
            response = self.app.post(
                '/workflows',
                json=workflow,
            )
        self.assertEqual(response.status_code, 401)
        mock.assert_not_called()

    def test_workflow_nojobs(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Workflow",
                    "metadata": {"name": "foo"},
                },
            )
        self.assertEqual(response.status_code, 422)
        mock.assert_not_called()

    def test_workflow_circular(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Workflow",
                    "metadata": {"name": "foo"},
                    "jobs": {
                        "test1": {"needs": ["test2"], "generator": "xxx"},
                        "test2": {"needs": ["test1"], "generator": "xxx"},
                    },
                },
            )
        self.assertEqual(response.status_code, 422)
        mock.assert_not_called()

    def test_workflow_with_nofiles(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={'variables': 'A=B'},
                # files={'workflow': open('tests/python/resources/order_1.yaml', 'rb')}
            )
        self.assertEqual(response.status_code, 422)
        mock.assert_not_called()

    def test_workflow_from_file(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': 'A=B',
                    'workflow': open('tests/python/resources/order_1.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()
        self.assertIn('variables', mock.call_args.args[0])
        self.assertIn('A', mock.call_args.args[0]['variables'])
        self.assertEqual(mock.call_args.args[0]['variables']['A'], 'B')

    def test_workflow_varsfrom_file(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': open('tests/python/resources/vars', 'rb'),
                    'workflow': open('tests/python/resources/order_1.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()
        self.assertIn('variables', mock.call_args.args[0])
        self.assertIn('A', mock.call_args.args[0]['variables'])
        self.assertEqual(mock.call_args.args[0]['variables']['A'], 'B')
        self.assertIn('B', mock.call_args.args[0]['variables'])
        self.assertEqual(mock.call_args.args[0]['variables']['B'], 'DDDD')
        self.assertIn('C', mock.call_args.args[0]['variables'])
        self.assertEqual(mock.call_args.args[0]['variables']['C'], 'hi there!')
        self.assertNotIn('', mock.call_args.args[0]['variables'])
        self.assertEqual(mock.call_args.args[0]['variables']['SERVER'], 'nonono')

    def test_workflow_inputsfrom_file_missing(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': open('tests/python/resources/vars', 'rb'),
                    'workflow': open('tests/python/resources/inputs_1.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 422)
        resp = response.json
        self.assertIn('message', resp)
        self.assertIn('Mandatory input "input-1"', resp['message'])
        mock.assert_not_called()

    def test_workflow_inputsfrom_file_onlyrequired(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': open('tests/python/resources/vars', 'rb'),
                    'inputs': open('tests/python/resources/inputs_ok', 'rb'),
                    'workflow': open('tests/python/resources/inputs_1.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()
        workflow = mock.call_args.args[0]
        self.assertIn('opentestfactory.org/inputs', workflow['metadata']['annotations'])
        inputs = workflow['metadata']['annotations']['opentestfactory.org/inputs']
        self.assertIn('input-1', inputs)
        self.assertEqual(inputs['input-1'], 'One')
        self.assertNotIn('input-2', inputs)

    def test_workflow_inputsfrom_file_both(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': open('tests/python/resources/vars', 'rb'),
                    'inputs': open('tests/python/resources/inputs_full', 'rb'),
                    'workflow': open('tests/python/resources/inputs_1.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()
        workflow = mock.call_args.args[0]
        self.assertIn('opentestfactory.org/inputs', workflow['metadata']['annotations'])
        inputs = workflow['metadata']['annotations']['opentestfactory.org/inputs']
        self.assertIn('input-1', inputs)
        self.assertEqual(inputs['input-1'], 'One')
        self.assertIn('input-2', inputs)

    def test_workflow_inputsfrom_file_toomany(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': open('tests/python/resources/vars', 'rb'),
                    'inputs': open('tests/python/resources/inputs_toomany', 'rb'),
                    'workflow': open('tests/python/resources/inputs_1.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 422)
        resp = response.json
        self.assertIn('message', resp)
        self.assertIn('Unexpected input "input-3"', resp['message'])
        mock.assert_not_called()

    def test_workflow_with_missingfiles(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': 'A=B',
                    'workflow': open('tests/python/resources/filedemo.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 422)
        mock.assert_not_called()

    def test_workflow_with_nomissingfiles(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': 'A=B',
                    'workflow': open('tests/python/resources/filedemo.yaml', 'rb'),
                    'report': open('tests/python/resources/filedemo.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()


if __name__ == '__main__':
    unittest.main()
