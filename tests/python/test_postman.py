# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Postman Provider unit tests.

- execute tests
- param tests
- postman tests
"""

import os
import unittest
import sys

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.postman import main, implementation

import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    'contexts': {'runner': {'os': 'linux'}},
    'step': {'uses': 'foo'},
    'runs-on': ['linux'],
}

PROVIDERMANIFESTCACHE = {
    ('postman', 'execute', None): ({'test': {'required': True}}, False),
    ('postman', 'params', None): (
        {
            'data': {
                'description': 'The data to use for the automated test.',
                'required': True,
            },
            'format': {
                'description': 'The format to use for the automated test data.',
                'required': True,
            },
        },
        True,
    ),
}


########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestPostman(unittest.TestCase):
    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONTEXT'][opentf.toolkit.INPUTS_KEY] = PROVIDERMANIFESTCACHE
        app.config['CONTEXT'][opentf.toolkit.OUTPUTS_KEY] = {}
        self.app = app.test_client()
        self.assertFalse(app.debug)

    # execute

    def test_execute_windows_collection_ok(self):
        """
        Checks the runner command with a collection item in the test definition
        when execute action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/execute',
                'with': {'test': 'postmanProject/path/to/collection.json'},
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 8)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json"  -g _opentf_global_params.json -e _opentf_environment_params.json --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[4]['run'],
        )
        self.assertTrue(steps[7]['continue-on-error'])

    def test_execute_linux_collection_ok(self):
        """
        Checks the runner command with a collection item in the test definition
        when execute action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/execute',
                'with': {'test': 'postmanProject/path/to/collection.json'},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 8)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json"  -g _opentf_global_params.json -e _opentf_environment_params.json --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[4]['run'],
        )
        self.assertTrue(steps[7]['continue-on-error'])

    def test_execute_windows_extra_options_ok(self):
        """
        Checks that the runner command contains POSTMAN_EXTRA_OPTIONS
        environment variable when execute action is called on a Windows
        execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/execute',
                'with': {'test': 'postmanProject/path/to/collection.json'},
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 8)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json"  -g _opentf_global_params.json -e _opentf_environment_params.json --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html %POSTMAN_EXTRA_OPTIONS%',
            steps[4]['run'],
        )

    def test_execute_linux_extra_options_ok(self):
        """
        Checks that the runner command contains POSTMAN_EXTRA_OPTIONS
        environment variable when execute action is called on a Linux
        execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/execute',
                'with': {'test': 'postmanProject/path/to/collection.json'},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 8)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json"  -g _opentf_global_params.json -e _opentf_environment_params.json --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html $POSTMAN_EXTRA_OPTIONS',
            steps[4]['run'],
        )

    def test_execute_linux_folder_ok(self):
        """
        Checks the runner command with collection and folder items
        in the test definition when execute action is called
        on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/execute',
                'with': {'test': 'postmanProject/path/to/collection.json#testFolder'},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 8)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --folder "testFolder" --bail folder  -g _opentf_global_params.json -e _opentf_environment_params.json --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[4]['run'],
        )

    def test_execute_windows__special_characters_folder_ok(self):
        """
        Checks the runner command with collection and folder ( with special characters) items
        in the test definition when execute action is called
        on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/execute',
                'with': {
                    'test': "postmanProject/path/to/collection.json#Dossier \\ fidélité ! : [\"5,2%\"]{^2+1-3*4=0} > l'@d_min; < (en € & $ ?)."
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 8)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --folder "Dossier \\ fidélité ! : ["5,2%%"]{^2+1-3*4=0} > l\'@d_min; < (en € & $ ?)." --bail folder  -g _opentf_global_params.json -e _opentf_environment_params.json --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[4]['run'],
        )

    # param

    def test_param_windows_ok(self):
        """
        Checks the runner command with global and test params
        when param action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.postman.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/params',
                'with': {
                    'data': {
                        'global': {'gid': '29428053'},
                        'test': {'id': '31338079'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        step_create_global_file = steps[1]
        step_create_envir_file = steps[2]
        self.assertEqual(
            step_create_global_file['with']['data']['values'][0],
            {'key': 'gid', 'value': '29428053', 'enabled': True},
        )
        self.assertEqual(
            step_create_envir_file['with']['data']['values'][0],
            {'key': 'id', 'value': '31338079', 'enabled': True},
        )

    def test_param_global_linux_ok(self):
        """
        Checks the runner command with global params when param action is called
        on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.postman.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/params',
                'with': {
                    'data': {
                        'global': {'gid': '29428053'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        step_create_file = steps[1]
        self.assertEqual(
            step_create_file['with']['data']['values'][0],
            {'key': 'gid', 'value': '29428053', 'enabled': True},
        )

    def test_param_test_linux_ok(self):
        """
        Checks the runner command with test params when param action is called
        on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.postman.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/params',
                'with': {
                    'data': {
                        'test': {'id': '29428053'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        step_create_file = steps[2]
        self.assertEqual(
            step_create_file['with']['data']['values'][0],
            {'key': 'id', 'value': '29428053', 'enabled': True},
        )

    def test_param_invalid_format_nok(self):
        """
        Checks the runner command with an invalid format param
        when param action is called.
        """
        with patch(
            'opentf.plugins.postman.implementation.core.validate_params_inputs',
            MagicMock(side_effect=SystemExit(2)),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'invalid_format/params@v1',
                },
            }
            self.assertRaisesRegex(
                SystemExit, '2', _dispatch, implementation.param_action, command
            )

    # postman

    def test_postman_linux_collection_ok(self):
        """
        Checks the runner command with a collection input
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {'collection': 'postmanProject/path/to/collection.json'},
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_folder_ok(self):
        """
        Checks the runner command with collection and folder inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'folder': 'testFolder',
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --folder "testFolder" --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_environment_ok(self):
        """
        Checks the runner command with collection and environment file inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'environment': 'path/to/environment_params.json',
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" -e "path/to/environment_params.json" --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_globals_ok(self):
        """
        Checks the runner command with collection and globals file inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'globals': 'path/to/globals_params.json',
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" -g "path/to/globals_params.json" --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_iteration_data_ok(self):
        """
        Checks the runner command with collection and iteration-data file inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'iteration-data': 'path/to/iteration_data.csv',
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" -d "path/to/iteration_data.csv" --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_iteration_count_ok(self):
        """
        Checks the runner command with collection and iteration-count number inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'iteration-count': '3',
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" -n 3 --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_delay_request_ok(self):
        """
        Checks the runner command with collection and delay-request number inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'delay-request': '1001',
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --delay-request 1001 --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_timeout_request_ok(self):
        """
        Checks the runner command with collection and timeout-request number inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'timeout-request': '1234',
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --timeout-request 1234 --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_bail_folder_ok(self):
        """
        Checks the runner command with collection and bail folder inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'bail': 'folder',
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --bail folder --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_bail_failure_ok(self):
        """
        Checks the runner command with collection and bail failure inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'bail': 'failure',
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --bail failure --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_bail_ok(self):
        """
        Checks the runner command with collection and bail inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'bail': True,
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --bail --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_extra_options_ok(self):
        """
        Checks the runner command with collection and extra-options inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'extra-options': '--verbose --env-var "id=36483577" --global-var "gid=31338079"',
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --verbose --env-var "id=36483577" --global-var "gid=31338079" --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_linux_special_characters_folder_ok(self):
        """
        Checks the runner command with collection and special characters in folder inputs
        when postman action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'folder': "postmanProject/path/to/collection.json#Dossier \\ fidélité ! : [\"5,2%\"]{^2+1-3*4=0} > l'@d_min; < (en € & $ ?).",
                },
            }
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --folder "postmanProject/path/to/collection.json#Dossier \\ fidélité ! : [\\\"5,2%\\\"]{^2+1-3*4=0} > l\'@d_min; < (en € & $ ?)." --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )

    def test_postman_windows_special_characters_folder_ok(self):
        """
        Checks the runner command with collection and special characters in folder inputs
        when postman action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'postman/postman',
                'with': {
                    'collection': 'postmanProject/path/to/collection.json',
                    'folder': "postmanProject/path/to/collection.json#Dossier \\ fidélité ! : [\"5,2%\"]{^2+1-3*4=0} > l'@d_min; < (en € & $ ?).",
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.postman_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'newman run "postmanProject/path/to/collection.json" --folder "postmanProject/path/to/collection.json#Dossier \\ fidélité ! : [\\\"5,2%%\\\"]{^2+1-3*4=0} > l\'@d_min; < (en € & $ ?)." --reporters junit,html --reporter-junit-export newman-run-report.xml --reporter-html-export newman-run-report.html',
            steps[0]['run'],
        )


if __name__ == '__main__':
    unittest.main()
