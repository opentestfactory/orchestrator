# Copyright (c) 2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
from unittest.mock import MagicMock, patch

from queue import Queue

from opentf.core import channelallocator as ac

########################################################################


class TestChannelAllocator(unittest.TestCase):
    def setUp(self):
        ac.JOBS_TIMESTAMPS.clear()
        ac.PENDING_JOBS.clear()

    def handle_all_events(self, events: Queue):
        while not events.empty():
            ac._handle_event({})

    def test_add_one_old_and_one_new_job_to_jobs_list(self):
        publish_mock = MagicMock()
        callback = MagicMock()
        workflow_A = [
            ('job_A1', ['tag1'], 'job_A1', [], 'default'),
            ('job_A2', ['tag1'], 'job_A2', [], 'default'),
            ('job_A3', ['tag2'], 'job_A3', [], 'default'),
        ]
        workflow_B = [('job_B1', ['tag3'], 'job_B1', [], 'other')]
        workflow_C = [('job_C1', ['tag4'], 'job_C1', [], 'default')]
        workflow_A_1 = [
            ('job_A1', ['tag1'], 'job_A1', [], 'default'),
            ('job_A4', ['tagX'], 'job_A4', [], 'default'),
        ]
        events = Queue()
        pendingjobs = dict()
        timestamps = dict()
        with (
            patch('opentf.core.channelallocator.publish', publish_mock),
            patch('opentf.core.channelallocator.EVENTS_QUEUE', events),
            patch('opentf.core.channelallocator.PENDING_JOBS', pendingjobs),
            patch('opentf.core.channelallocator.JOBS_TIMESTAMPS', timestamps),
        ):
            ac.require_channels('workflow_A', workflow_A, callback, 2)
            ac.require_channels('workflow_B', workflow_B, callback, 1)
            ac.require_channels('workflow_C', workflow_C, callback, 1)
            ac.process_candidate('workflow_A', {'metadata': {'job_id': 'job_A2'}})
            ac.process_candidate('workflow_A', {'metadata': {'job_id': 'job_A3'}})
            self.assertEqual(5, events.qsize())
            self.handle_all_events(events)
            self.assertIn('workflow_A', pendingjobs)
            self.assertEqual(0, pendingjobs['workflow_A'].max_parallel)
            sorted_jobs = ac.sorted_job_list()
            self.assertNotIn('job_A1', sorted_jobs)
            ac.require_channels('workflow_A', workflow_A_1, callback, 2)
            self.handle_all_events(events)
            sorted_jobs = ac.sorted_job_list()
            self.assertEqual('job_A1', sorted_jobs[0])

    # TO REVIEW !!! (Same problem with last assertions...)
    def test_add_new_job_when_old_job_cancelled(self):
        publish_mock = MagicMock()
        callback = MagicMock()
        workflow_A = [
            ('job_A1', ['tag1'], 'job_A1', [], 'default'),
            ('job_A2', ['tag1'], 'job_A2', [], 'default'),
            ('job_A3', ['tag2'], 'job_A3', [], 'default'),
        ]
        workflow_B = [('job_B1', ['tag3'], 'job_B1', [], 'default')]
        workflow_C = [('job_C1', ['tag4'], 'job_C1', [], 'default')]
        workflow_A_1 = [('job_A4', ['tagX'], 'job_A4', [], 'default')]
        events = Queue()

        with (
            patch('opentf.core.channelallocator.publish', publish_mock),
            patch('opentf.core.channelallocator.EVENTS_QUEUE', events),
        ):
            ac.require_channels('workflow_A', workflow_A, callback, 2)
            self.handle_all_events(events)
            self.assertIn('workflow_A', ac.PENDING_JOBS)
            ac.require_channels('workflow_B', workflow_B, callback, 1)
            self.handle_all_events(events)
            self.assertIn('workflow_A', ac.PENDING_JOBS)
            self.assertIn('workflow_B', ac.PENDING_JOBS)
            ac.require_channels('workflow_C', workflow_C, callback, 1)
            self.handle_all_events(events)
            self.assertIn('workflow_A', ac.PENDING_JOBS)
            self.assertIn('workflow_B', ac.PENDING_JOBS)
            self.assertIn('workflow_C', ac.PENDING_JOBS)
            ac.process_candidate('workflow_A', {'metadata': {'job_id': 'job_A2'}})
            ac.process_candidate('workflow_A', {'metadata': {'job_id': 'job_A3'}})
            self.handle_all_events(events)
            self.assertIn('workflow_A', ac.PENDING_JOBS)
            self.assertEqual(0, ac.PENDING_JOBS['workflow_A'].max_parallel)
            ac.require_channels('workflow_A', workflow_A_1, callback, 2)
            self.handle_all_events(events)
            self.assertIn('workflow_A', ac.PENDING_JOBS)
            self.assertNotIn('job_A1', ac.JOBS_TIMESTAMPS)
            self.assertIn('job_A4', ac.JOBS_TIMESTAMPS)
            sorted_jobs = ac.sorted_job_list()
            self.assertEqual('job_B1', sorted_jobs[0])

    # init

    def test_init_ok(self):
        mock_debug = MagicMock()
        mock_thread_instance = MagicMock()
        mock_thread = MagicMock(return_value=mock_thread_instance)
        with (
            patch('opentf.core.channelallocator.debug', mock_debug),
            patch('opentf.core.channelallocator.threading.Thread', mock_thread),
        ):
            ac.init({})
        self.assertEqual(2, mock_debug.call_count)
        self.assertEqual(2, mock_thread_instance.start.call_count)

    def test_init_ko_handle_events(self):
        mock_fatal = MagicMock(side_effect=SystemExit())
        mock_thread_instance = MagicMock()
        mock_thread_instance.start.side_effect = ValueError('Err')
        mock_thread = MagicMock(return_value=mock_thread_instance)
        with (
            patch('opentf.core.channelallocator.fatal', mock_fatal),
            patch('opentf.core.channelallocator.threading.Thread', mock_thread),
        ):
            self.assertRaises(SystemExit, ac.init, {})
        mock_thread.assert_called_once_with(
            target=ac.handle_events_queue, daemon=True, args=({},)
        )
        mock_fatal.assert_called_once_with(
            'Could not start channel requests and offers managing thread: Err.'
        )

    def test_init_ko_handle_requests(self):
        mock_fatal = MagicMock(side_effect=SystemExit())
        mock_thread_instance = MagicMock()
        mock_thread_instance.start.side_effect = [None, ValueError('Err')]
        mock_thread = MagicMock(return_value=mock_thread_instance)
        with (
            patch('opentf.core.channelallocator.fatal', mock_fatal),
            patch('opentf.core.channelallocator.threading.Thread', mock_thread),
        ):
            self.assertRaises(SystemExit, ac.init, {})
        self.assertEqual(2, mock_thread.call_count)
        mock_fatal.assert_called_once_with(
            'Could not start channel requests handler thread: Err.'
        )

    # handle_requests

    def test_handle_requests_ok(self):
        workflow_A = [('job_A4', ['tagX'], 'job_A4', 'nameSpace', [])]
        callback = MagicMock()
        ac.PENDING_JOBS['workflow_A'] = ac.PendingJobs(
            'workflow_A', callback, workflow_A[0], 1
        )
        mock_order = MagicMock()
        mock_sleep = MagicMock(side_effect=Exception('Err'))
        with (
            patch('opentf.core.channelallocator.publish_channel_requests', mock_order),
            patch('opentf.core.channelallocator.sleep', mock_sleep),
        ):
            self.assertRaisesRegex(Exception, 'Err', ac.handle_refresh_requests, {})
        mock_order.assert_called_once_with({})
        mock_sleep.assert_called_once()

    # handle_events_queue

    def test_handle_events_queue(self):
        mock_handle = MagicMock(side_effect=[None, Exception('Boom')])
        mock_error = MagicMock()
        with (
            patch('opentf.core.channelallocator._handle_event', mock_handle),
            patch('opentf.core.channelallocator.error', mock_error),
        ):
            self.assertRaisesRegex(Exception, 'Boom', ac.handle_events_queue, {})
        self.assertEqual(2, mock_handle.call_count)
        mock_error.assert_called_once_with('Internal error while handling event: Boom.')

    # handle_require_channels

    def test_handle_require_channels_ok_with_previous_jobs(self):
        callback = MagicMock()
        workflow_A = [
            ('job_A1', ['tag1'], 'job_A1', [], 'default'),
            ('job_A2', ['tag1'], 'job_A2', [], 'default'),
            ('job_A3', ['tag2'], 'job_A3', [], 'default'),
        ]
        workflow_A_1 = [
            ('job_A1', ['tag1'], 'job_A1', [], 'default'),
            ('job_A4', ['tagX'], 'job_A4', [], 'default'),
        ]
        events = Queue()
        with (
            patch('opentf.core.channelallocator.EVENTS_QUEUE', events),
            patch('opentf.core.channelallocator.publish'),
        ):
            ac.require_channels('workflow_A', workflow_A, callback, 1)
            self.handle_all_events(events)
            ac.require_channels('workflow_A', workflow_A_1, callback, 1)
            self.handle_all_events(events)
        self.assertIn('job_A1', ac.JOBS_TIMESTAMPS)
        self.assertNotIn('job_A2', ac.JOBS_TIMESTAMPS)

    def test_handle_require_channels_keyerror_pass(self):
        callback = MagicMock()
        workflow_A = [
            ('job_A1', ['tag1'], 'job_A1', [], 'default'),
            ('job_A2', ['tag1'], 'job_A2', [], 'default'),
            ('job_A3', ['tag2'], 'job_A3', [], 'default'),
        ]
        workflow_A_1 = [
            ('job_A1', ['tag1'], 'job_A1', [], 'default'),
            ('job_A4', ['tagX'], 'job_A4', [], 'default'),
        ]
        events = Queue()
        with (
            patch('opentf.core.channelallocator.EVENTS_QUEUE', events),
            patch('opentf.core.channelallocator.publish'),
        ):
            ac.require_channels('workflow_A', workflow_A, callback, 1)
            self.handle_all_events(events)
            del ac.JOBS_TIMESTAMPS['job_A2']
            ac.require_channels('workflow_A', workflow_A_1, callback, 1)
            self.handle_all_events(events)
        self.assertIn('job_A1', ac.JOBS_TIMESTAMPS)
        self.assertNotIn('job_A3', ac.JOBS_TIMESTAMPS)

    # publish_channel & handle_candidate

    def test_publish_channel_request_ko(self):
        mock_event = MagicMock(side_effect=Exception('Another one'))
        with (
            patch('opentf.core.channelallocator.make_event', mock_event),
            patch('opentf.core.channelallocator.error') as mock_error,
        ):
            ac._publish_channel_request(
                'wFiD', 'jobId', 'nAme', 'nAmEspace', [], [], {}
            )
        mock_error.assert_called_once_with(
            'Error while managing requests: %s.', 'Another one'
        )

    def test_handle_candidate_not_pending(self):
        ac.JOBS_TIMESTAMPS = {'job_1': {}}
        ac.handle_candidate('wFiD', {})
        self.assertIn('job_1', ac.JOBS_TIMESTAMPS)

    def test_handle_candidate_keyerror_return(self):
        callback = MagicMock()
        ac.JOBS_TIMESTAMPS = {'job2': {}}
        ac.PENDING_JOBS['wfid'] = ac.PendingJobs(
            'wfid', callback, (('job1', ['tagX'], 'job1', []),), 1
        )
        ac.handle_candidate('wfid', {'metadata': {'job_id': 'job1'}})
        self.assertEqual(1, len(ac.JOBS_TIMESTAMPS))
