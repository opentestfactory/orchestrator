# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Insight Collector unit tests."""

import json
import logging
import os
import sys
import unittest

from unittest.mock import MagicMock, mock_open, patch, call

from requests import Response


sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.insightcollector import main

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates and helpers


EVENT_WF = {
    'kind': 'Workflow',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {
        'name': 'workFlow',
        'namespace': 'default',
        'creationTimestamp': 'smth',
        'workflow_id': 'wOrKflow_iD',
    },
}


EVENT_WC = {
    'kind': 'WorkflowCompleted',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {
        'name': 'workFlow',
        'namespace': 'default',
        'creationTimestamp': 'smth',
        'workflow_id': 'wOrKflow_iD',
    },
}

EVENT_WC_WITH_LABELS = {
    'kind': 'WorkflowCompleted',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {
        'name': 'workFlow',
        'namespace': 'default',
        'creationTimestamp': 'smth',
        'workflow_id': 'wOrKflow_iD',
        'labels': {
            'executionlog/step-depth': '0',
            'executionlog/job-depth': '0',
            'executionlog/max-command-length': '0',
        },
    },
}

EVENT_OTHER = {
    'kind': 'FooBar',
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'metadata': {'workflow_id': 'wOrKflow_iD'},
}

EXECUTIONLOG_PARAMS = {
    'step-depth': 60,
    'job-depth': 70,
    'max-command-length': 80,
}

RAW_CONFIG = {
    'insights': [
        {
            'name': 'executionlog',
            'kind': 'ExecutionLog',
            'spec': {'step-depth': 100, 'job-depth': 200, 'max-command-length': 300},
        }
    ]
}

DUPLICATE_CONFIG = {
    'insights': [
        {
            'name': 'executionlog',
            'kind': 'ExecutionLog',
            'spec': {'step-depth': 0, 'job-depth': 0, 'max-command-length': 0},
        },
        {
            'name': 'executionlog',
            'kind': 'ExecutionLog',
            'spec': {'step-depth': 1, 'job-depth': 1, 'max-command-length': 1},
        },
        {
            'name': 'executionlog',
            'kind': 'ExecutionLog',
            'spec': {'step-depth': 100, 'job-depth': 200, 'max-command-length': 300},
        },
    ]
}

INSIGHT_DICT = {'insights': [{'name': 'report', 'kind': 'SummaryReport'}]}

IMG_INSIGHT_1 = [
    {'name': 'executionlog', 'kind': 'ExecutionLog', 'spec': {'step-depth': 1}},
    {
        'name': 'executionreport',
        'kind': 'SummaryReport',
        'title': 'Default HTML report',
    },
]

IMG_INSIGHT_2 = [
    {'name': 'executionlog', 'kind': 'ExecutionLog', 'spec': {'step-depth': 2}},
]


IMG_INSIGHTS = [
    {'name': 'executionreport-2', 'kind': 'SummaryReport'},
    {'name': 'executionlog', 'kind': 'ExecutionLog', 'spec': {'step-depth': 2}},
    {
        'name': 'executionreport',
        'kind': 'SummaryReport',
        'title': 'Default HTML report',
    },
    {'name': 'executionlog', 'kind': 'ExecutionLog', 'spec': {'step-depth': 1}},
]

########################################################################


class TestInsightCollector(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)
        plugin = main.plugin
        plugin.config['CONTEXT']['services'] = {
            'observer': {'endpoint': 'http://localhost'}
        }
        plugin.config['CONTEXT']['enable_insecure_login'] = True
        cls.plugin = plugin.test_client()

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def test_process_inbox_badjson(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(side_effect=Exception('ooOps'))
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()
        mock_msr.assert_called_once_with('BadRequest', 'Could not parse body: ooOps.')

    def test_process_inbox_wc(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WC
        mock_msr = MagicMock()
        reports = {}
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
            patch('opentf.plugins.insightcollector.main.PENDING_REPORTS', reports),
        ):
            main.process_inbox()
        mock_msr.assert_called_once_with('OK', '')

    def test_process_inbox_workflow(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WF
        mock_msr = MagicMock()
        reports = {}
        events = {'wOrKflow_iD': []}
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
            patch('opentf.plugins.insightcollector.main.PENDING_REPORTS', reports),
            patch('opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS', events),
            patch(
                'opentf.plugins.insightcollector.main.validate_schema',
                return_value=(True, ''),
            ),
            patch(
                'opentf.plugins.insightcollector.main._maybe_register_worker'
            ) as mock_worker,
        ):
            main.process_inbox()
        mock_msr.assert_called_once_with('OK', '')
        mock_worker.assert_called_once_with('wOrKflow_iD')
        self.assertEqual(1, len(events['wOrKflow_iD']))

    def test_process_inbox_notdict(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: 'foo'
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()
        mock_msr.assert_called_once_with('BadRequest', 'Body must be a JSON object.')

    def test_process_inbox_nokind(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: {'apiVersion': 'foobar'}
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()
        mock_msr.assert_called_once_with('BadRequest', 'Missing apiVersion or kind.')

    def test_process_inbox_other(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_OTHER
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()
        mock_msr.assert_called_once_with('BadRequest', 'Unexpected kind: FooBar.')

    def test_process_inbox_add_labels_and_default_reports(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WC_WITH_LABELS
        mock_msr = MagicMock()
        mock_queue = MagicMock()
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONFIG': {
                'insights': [
                    {
                        'name': 'executionlog',
                        'kind': 'ExecutionLog',
                        'spec': EXECUTIONLOG_PARAMS,
                    }
                ]
            },
            'CONTEXT': 'context',
        }
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
            patch(
                'opentf.plugins.insightcollector.main.PENDING_REPORTSTASKS', mock_queue
            ),
            patch('opentf.plugins.insightcollector.main.plugin', mock_plugin),
        ):
            main.process_inbox()
        mock_msr.assert_called_once_with('OK', '')
        mock_queue.put.assert_called_once()
        execlog = mock_queue.put.call_args[0][0][3][0]
        summaryreport = mock_queue.put.call_args[0][0][3][1]
        self.assertEqual('0', execlog['spec']['step-depth'])
        self.assertIsNotNone(summaryreport['spec']['template'])

    def test_process_inbox_report_already_generated(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WC
        mock_msr = MagicMock()
        mock_contexts = MagicMock()
        reports = {'wOrKflow_iD': 100}
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
            patch('opentf.plugins.insightcollector.main.PENDING_REPORTS', reports),
            patch(
                'opentf.plugins.insightcollector.main._make_contexts',
                mock_contexts,
            ),
        ):
            main.process_inbox()
        mock_msr.assert_called_once_with(
            'OK', 'Report already generated for this workflow.'
        )
        mock_contexts.assert_not_called()

    def test_can_collect_nojson(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = lambda: 1 / 0
        mock_plugin = MagicMock()
        with patch('opentf.plugins.insightcollector.main.plugin', mock_plugin):
            self.assertFalse(
                main.workflow_is_completed(
                    'wOrKflow_iD', 'toKen', {}, [{}], None, mock_response
                )
            )

    def test_can_collect_baddict(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = lambda: {}
        mock_plugin = MagicMock()
        with patch('opentf.plugins.insightcollector.main.plugin', mock_plugin):
            self.assertFalse(
                main.workflow_is_completed(
                    'wOrKflow_iD', 'toKen', {}, [{}], None, mock_response
                )
            )

    def test_can_collect_done(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = lambda: {'details': {'status': 'DONE'}}
        mock_plugin = MagicMock()
        with patch('opentf.plugins.insightcollector.main.plugin', mock_plugin):
            self.assertTrue(
                main.workflow_is_completed(
                    'wOrKflow_iD', 'toKen', {}, [{}], None, mock_response
                )
            )

    def test_can_collect_ongoing(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = lambda: {'details': {'status': 'ONGOING'}}
        mock_plugin = MagicMock()
        mock_sleep = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.plugin', mock_plugin),
            patch('opentf.plugins.insightcollector.main.sleep', mock_sleep),
            patch(
                'opentf.plugins.insightcollector.main.PENDING_REPORTSTASKS', MagicMock()
            ),
        ):
            self.assertFalse(
                main.workflow_is_completed(
                    'wOrKflow_iD',
                    'toKen',
                    main._make_contexts(None, ''),
                    [{}],
                    None,
                    mock_response,
                )
            )
        mock_sleep.assert_called_once()

    def test_can_collect_unauthorized(self):
        mock_response = MagicMock()
        mock_response.status_code = 401
        mock_plugin = MagicMock()
        with patch('opentf.plugins.insightcollector.main.plugin', mock_plugin):
            self.assertFalse(
                main.workflow_is_completed(
                    'wOrKflow_iD', 'toKen', {}, [{}], None, mock_response
                )
            )

    def test_main_mock(self):
        mock_svc = MagicMock()
        mock_threading = MagicMock()
        mock_runapp = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.threading', mock_threading),
            patch('opentf.plugins.insightcollector.main.subscribe', MagicMock()),
            patch('opentf.plugins.insightcollector.main.unsubscribe', MagicMock()),
            patch('opentf.plugins.insightcollector.main.run_app', mock_runapp),
            patch('opentf.plugins.insightcollector.main._load_image_models'),
            patch('opentf.plugins.insightcollector.main.CONFIG_PATH', '/some/path'),
            patch('opentf.plugins.insightcollector.main._load_image_css'),
            patch('opentf.plugins.insightcollector.main._load_image_configuration'),
        ):
            main.main(mock_svc)
        mock_threading.assert_not_called()
        mock_runapp.assert_called_once()

    def test_main_badsubscribe(self):
        mock_svc = MagicMock()
        mock_threading = MagicMock()
        mock_runapp = MagicMock()
        mock_subscribe = MagicMock(side_effect=Exception('ooOps'))
        with (
            patch('opentf.plugins.insightcollector.main.threading', mock_threading),
            patch('opentf.plugins.insightcollector.main.subscribe', mock_subscribe),
            patch('opentf.plugins.insightcollector.main.unsubscribe', MagicMock()),
            patch('opentf.plugins.insightcollector.main.run_app', mock_runapp),
        ):
            self.assertRaises(SystemExit, main.main, mock_svc)
        mock_threading.assert_not_called()
        mock_runapp.assert_not_called()

    def test_main_with_config_file(self):
        mock_svc = MagicMock()
        mock_threading = MagicMock()
        mock_runapp = MagicMock()
        mock_watch = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.threading', mock_threading),
            patch('opentf.plugins.insightcollector.main.subscribe', MagicMock()),
            patch('opentf.plugins.insightcollector.main.unsubscribe', MagicMock()),
            patch('opentf.plugins.insightcollector.main.run_app', mock_runapp),
            patch.dict(
                'os.environ', {'INSIGHTCOLLECTOR_CONFIGURATION': '/some/short/path'}
            ),
            patch('opentf.plugins.insightcollector.main.watch_file', mock_watch),
            patch('opentf.plugins.insightcollector.main._load_image_models'),
            patch('opentf.plugins.insightcollector.main.CONFIG_PATH', '/some/path'),
            patch('opentf.plugins.insightcollector.main._load_image_css'),
            patch('opentf.plugins.insightcollector.main._load_image_configuration'),
        ):
            main.main(mock_svc)
        mock_watch.assert_called_once()
        calls = mock_watch.call_args[0]
        self.assertEqual('/some/short/path', calls[1])
        self.assertEqual('opentestfactory.org/v1alpha1/InsightCollector', calls[-1])
        mock_threading.assert_not_called()
        mock_runapp.assert_called_once()

    def test_main_without_config_file(self):
        mock_svc = MagicMock()
        mock_threading = MagicMock()
        mock_runapp = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.threading', mock_threading),
            patch('opentf.plugins.insightcollector.main.subscribe', MagicMock()),
            patch('opentf.plugins.insightcollector.main.unsubscribe', MagicMock()),
            patch('opentf.plugins.insightcollector.main.run_app', mock_runapp),
            patch.dict(
                'os.environ', {'NOT_INSIGHTCOLLECTOR_CONFIGURATION': '/some/short/path'}
            ),
            patch('opentf.plugins.insightcollector.main._load_image_models'),
            patch('opentf.plugins.insightcollector.main.CONFIG_PATH', '/some/path'),
            patch('opentf.plugins.insightcollector.main._load_image_css'),
            patch('opentf.plugins.insightcollector.main._load_image_configuration'),
        ):
            main.main(mock_svc)
        mock_threading.assert_not_called()
        mock_runapp.assert_called_once()

    def test_main_load_default_from_img(self):
        mock_svc = MagicMock()
        mock_threading = MagicMock()
        mock_runapp = MagicMock()
        plugin = main.plugin
        if 'insights' in plugin.config['CONFIG']:
            del plugin.config['CONFIG']['insights']
        with (
            patch('opentf.plugins.insightcollector.main.threading', mock_threading),
            patch('opentf.plugins.insightcollector.main.subscribe', MagicMock()),
            patch('opentf.plugins.insightcollector.main.unsubscribe', MagicMock()),
            patch('opentf.plugins.insightcollector.main.run_app', mock_runapp),
            patch.dict(
                'os.environ', {'NOT_INSIGHTCOLLECTOR_CONFIGURATION': '/some/short/path'}
            ),
            patch('opentf.plugins.insightcollector.main.IMG_INSIGHTS', IMG_INSIGHTS),
            patch('opentf.plugins.insightcollector.main._load_image_models'),
            patch('opentf.plugins.insightcollector.main.CONFIG_PATH', '/some/path'),
            patch('os.path.isdir', return_value=True),
        ):
            main.main(mock_svc)
        self.assertNotIn('insights', plugin.config['CONFIG'])

    def test_main_config_path_ko(self):
        mock_svc = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.get_context_service'),
            patch(
                'opentf.plugins.insightcollector.main.get_context_parameter',
                return_value='/some/path',
            ),
            patch('os.path.isdir', return_value=False),
            patch('opentf.plugins.insightcollector.main.error') as mock_error,
        ):
            self.assertRaisesRegex(SystemExit, '2', main.main, mock_svc)
        mock_error.assert_called_once()
        self.assertIn('not found or not a directory', mock_error.call_args[0][0])

    # _read_configuration

    def test_read_configuration_ok(self):
        plugin = main.plugin
        if 'insights' in plugin.config['CONFIG']:
            del plugin.config['CONFIG']['insights']
        mock_error = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.main.read_and_validate',
                MagicMock(return_value=RAW_CONFIG),
            ),
            patch('opentf.plugins.insightcollector.main.error', mock_error),
        ):
            main._refresh_configuration(plugin, 'foo', 'bar')
        mock_error.assert_not_called()
        self.assertIsNotNone(plugin.config['CONFIG'].get('insights'))
        self.assertEqual(
            plugin.config['CONFIG'],
            RAW_CONFIG,
        )

    def test_read_configuration_not_dict(self):
        plugin = main.plugin
        plugin.config['CONFIG']['insights'] = {}
        mock_read = mock_open(read_data='data')
        mock_error = MagicMock()
        with (
            patch('builtins.open', mock_read),
            patch('opentf.plugins.insightcollector.main.error', mock_error),
        ):
            main._refresh_configuration(plugin, 'foo', 'bar')
        mock_read.assert_called_once()
        mock_error.assert_called_once_with(
            'Invalid Insight Collector configuration file "%s": %s.  Ignoring.',
            'foo',
            'Config file is not an object.',
        )

    def test_read_configuration_not_valid(self):
        plugin = main.plugin
        plugin.config['CONFIG']['insights'] = {}
        mock_error = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.main.read_and_validate',
                MagicMock(side_effect=ValueError('bar')),
            ),
            patch('opentf.plugins.insightcollector.main.error', mock_error),
        ):
            main._refresh_configuration(plugin, 'foo', 'bar')
        mock_error.assert_called_once_with(
            'Invalid Insight Collector configuration file "%s": %s.  Ignoring.',
            'foo',
            'bar',
        )

    def test_read_configuration_replace(self):
        plugin = main.plugin
        plugin.config['CONFIG']['insights'] = [{'step-depth': 1, 'job-depth': 2}]
        mock_error = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.main.read_and_validate',
                MagicMock(return_value=RAW_CONFIG),
            ),
            patch('opentf.plugins.insightcollector.main.error', mock_error),
        ):
            main._refresh_configuration(plugin, 'foo', 'bar')
        mock_error.assert_not_called()
        self.assertIsNotNone(plugin.config['CONFIG'].get('insights'))
        self.assertEqual(
            plugin.config['CONFIG'],
            RAW_CONFIG,
        )

    def test_read_configuration_ko(self):
        plugin = main.plugin
        plugin.config['CONFIG']['insights'] = {}
        mock_read = MagicMock(side_effect=Exception('Boooooo!'))
        mock_error = MagicMock()
        with (
            patch('builtins.open', mock_read),
            patch('opentf.plugins.insightcollector.main.error', mock_error),
        ):
            main._refresh_configuration(plugin, 'foo', 'bar')
        mock_read.assert_called_once()
        mock_error.assert_called_once_with(
            'Error while reading "%s" Insight Collector configuration: %s.',
            'foo',
            'Boooooo!',
        )

    def test_read_configuration_no_configfile(self):
        plugin = main.plugin
        mock_read = mock_open(read_data='data')
        with patch('builtins.open', mock_read):
            main._refresh_configuration(plugin, '', 'bar')
        mock_read.assert_not_called()

    def test_read_configuration_duplicate_insights(self):
        plugin = main.plugin
        if 'insights' in plugin.config['CONFIG']:
            del plugin.config['CONFIG']['insights']
        mock_error = MagicMock()
        mock_warning = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.main.read_and_validate',
                MagicMock(return_value=DUPLICATE_CONFIG),
            ),
            patch('opentf.plugins.insightcollector.main.error', mock_error),
            patch('opentf.plugins.insightcollector.main.warning', mock_warning),
        ):
            main._refresh_configuration(plugin, 'foo', 'bar')
        mock_error.assert_not_called()
        mock_warning.assert_called_once_with(
            'Duplicate insight definitions found for %s "%s", only the definition with the highest priority will be used.',
            'ExecutionLog',
            'executionlog',
        )
        self.assertIsNotNone(plugin.config['CONFIG'].get('insights'))
        self.assertEqual(
            plugin.config['CONFIG'],
            RAW_CONFIG,
        )

    # _adjust_models

    def test_adjust_models_no_html_report(self):
        mock_insights = [m.copy() for m in main.KINDS_DEFAULT_MODELS.values()]
        plugin = main.plugin
        models = plugin.config['CONFIG']['insights'] = [
            {'name': 'exe', 'kind': 'ExecutionLog', 'spec': {'a': 'b'}}
        ]
        with (
            patch(
                'opentf.plugins.insightcollector.main.evaluate_bool',
                MagicMock(return_value=True),
            ),
            patch('opentf.plugins.insightcollector.main.IMG_INSIGHTS', mock_insights),
        ):
            result = main._adjust_models({}, models)
        self.assertEqual(4, len(result))
        self.assertEqual('exe', result[0]['name'])
        self.assertEqual('executionreport', result[2]['name'])

    def test_adjust_models_no_execution_log(self):
        mock_insights = [m.copy() for m in main.KINDS_DEFAULT_MODELS.values()]
        plugin = main.plugin
        models = plugin.config['CONFIG']['insights'] = [
            {'name': 'html', 'kind': 'SummaryReport', 'spec': {'a': 'b'}}
        ]
        with (
            patch(
                'opentf.plugins.insightcollector.main.evaluate_bool',
                MagicMock(return_value=True),
            ),
            patch('opentf.plugins.insightcollector.main.IMG_INSIGHTS', mock_insights),
        ):
            result = main._adjust_models({}, models)
        self.assertEqual(4, len(result))
        self.assertEqual('html', result[0]['name'])
        self.assertEqual('executionlog', result[1]['name'])

    def test_adjust_models_override_with_labels(self):
        mock_insights = [m.copy() for m in main.KINDS_DEFAULT_MODELS.values()]
        models = [{'name': 'html', 'kind': 'SummaryReport', 'spec': {'a': 'b'}}]
        with (
            patch(
                'opentf.plugins.insightcollector.main.evaluate_bool',
                MagicMock(return_value=True),
            ),
            patch('opentf.plugins.insightcollector.main.IMG_INSIGHTS', mock_insights),
        ):
            result = main._adjust_models({'executionlog/job-depth': '33'}, models)
        self.assertEqual(4, len(result))
        self.assertEqual('33', result[1]['spec']['job-depth'])

    def test_adjust_models_with_labels_no_config_override(self):
        models = [
            {'name': 'executionlog', 'kind': 'ExecutionLog', 'spec': {'job-depth': 0}}
        ]
        with patch(
            'opentf.plugins.insightcollector.main.evaluate_bool',
            MagicMock(return_value=True),
        ):
            result = main._adjust_models({'executionlog/job-depth': '10'}, models)
        self.assertEqual('10', result[0]['spec']['job-depth'])
        self.assertEqual(0, models[0]['spec']['job-depth'])

    def test_adjust_models_append_img_models(self):
        models = [{'name': 'exe', 'kind': 'ExecutionLog', 'spec': {'a': 'b'}}]
        with (
            patch(
                'opentf.plugins.insightcollector.main.evaluate_bool',
                MagicMock(return_value=True),
            ),
            patch('opentf.plugins.insightcollector.main.IMG_INSIGHTS', IMG_INSIGHTS),
        ):
            result = main._adjust_models({}, models)
        self.assertIn('executionreport-2', [r['name'] for r in result])

    def test_adjust_models_handle_duplicates(self):
        insights = IMG_INSIGHTS.copy()
        insights = [
            {'name': 'executionreport-2', 'kind': 'SummaryReport', 'title': 'Report 01'}
        ] + insights
        models = [
            {
                'name': 'executionreport-2',
                'kind': 'SummaryReport',
                'title': 'Report Env',
            }
        ]
        with (
            patch(
                'opentf.plugins.insightcollector.main.evaluate_bool', return_value=True
            ),
            patch('opentf.plugins.insightcollector.main.IMG_INSIGHTS', insights),
        ):
            result = main._adjust_models({}, models)
        res = [item for item in result if item['name'] == 'executionreport-2']
        self.assertEqual(1, len(res))
        self.assertEqual('Report Env', res[0]['title'])

    # _filter_models_on_condition

    def test_filter_models_on_condition_ok(self):
        result = main._filter_models_on_condition(
            [{'model': 'one'}], {'workflow': {'status': 'success'}}
        )
        self.assertEqual([{'model': 'one'}], result)

    # _get_workflow_creationtimestamp

    def test_get_workflow_creationtimestamp_ok(self):
        wme = {
            'wfid': [
                {'metadata': {'creationTimestamp': 'yesterday'}, 'kind': 'Workflow'}
            ]
        }
        with patch('opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS', wme):
            ts = main._get_workflow_creationtimestamp('wfid')
        self.assertEqual('yesterday', ts)

    def test_get_workflow_creationtimestamp_ko(self):
        mock_response = MagicMock()
        mock_response.json.return_value = {'details': {'items': None}}
        mock_request = MagicMock()
        mock_request.get.return_value = mock_response
        mock_warning = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.requests', mock_request),
            patch('opentf.plugins.insightcollector.main.warning', mock_warning),
        ):
            ts = main._get_workflow_creationtimestamp('wfid')
        self.assertIsNone(ts)
        mock_warning.assert_called_once_with(
            'Could not retrieve workflow creation timestamp, respective insight conditions may not be applied.'
        )

    def test_get_workflow_creationtimestamp_exception(self):
        with (
            patch('opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS', {}),
            patch('opentf.plugins.insightcollector.main.warning') as mock_warning,
        ):
            ts = main._get_workflow_creationtimestamp('wfid')
        self.assertIsNone(ts)
        self.assertEqual(2, mock_warning.call_count)

    # _maybe_cleanup_pending_reports

    def test_maybe_cleanup_pending_reports_ok(self):
        reports = {'wf': 60}
        with (
            patch('opentf.plugins.insightcollector.main.PENDING_REPORTS', reports),
            patch('time.time', MagicMock(return_value=3600)),
        ):
            main._maybe_cleanup_pending_reports()
        self.assertEqual({}, reports)

    # _publish_insight

    def test_publish_insight_executionlog(self):
        mock_prepare = MagicMock(return_value={'e': 'f'})
        mock_events = {'ExecutionLog': mock_prepare}
        mock_publish = MagicMock()
        proxy = main.ObserverProxy('url_1', 'url_2', 'wf_id', {})
        with (
            patch(
                'opentf.plugins.insightcollector.main.make_uuid',
                MagicMock(return_value='123'),
            ),
            patch('builtins.open', MagicMock()),
            patch(
                'opentf.plugins.insightcollector.main.KINDS_PREPARE_EVENT', mock_events
            ),
            patch('opentf.plugins.insightcollector.main.publish', mock_publish),
            patch('opentf.plugins.insightcollector.main.make_event', MagicMock()),
        ):
            main._publish_insight(
                proxy,
                {'kind': 'ExecutionLog', 'name': 'ExLog', 'spec': {'c': 'd'}},
                'wfid',
                {},
                None,
            )
        mock_prepare.assert_called_once()
        mock_publish.assert_called_once()
        self.assertEqual({'e': 'f'}, mock_publish.call_args[0][0])

    def test_publish_insight_htmlreport(self):
        plugin = main.plugin
        plugin.config['CONTEXT'] = {'a': 'b'}
        mock_prepare = MagicMock(return_value={'event': 'some'})
        mock_events = {'SummaryReport': mock_prepare}
        mock_publish = MagicMock()
        mock_proxy = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.main.KINDS_PREPARE_EVENT', mock_events
            ),
            patch('opentf.plugins.insightcollector.main.publish', mock_publish),
            patch('opentf.plugins.insightcollector.main.IMG_CSS', ['style']),
        ):
            main._publish_insight(
                mock_proxy,
                {'kind': 'SummaryReport', 'name': 'HTML', 'spec': {'c': 'd'}},
                'wfid',
                {'e': 'f'},
                None,
            )
        mock_prepare.assert_called_once_with(
            mock_proxy,
            {'kind': 'SummaryReport', 'name': 'HTML', 'spec': {'c': 'd'}},
            'wfid',
            None,
            {'e': 'f'},
            ['style'],
            'true',
        )
        mock_publish.assert_called_once_with({'event': 'some'}, {'a': 'b'})

    def test_publish_insight_catch_errorevent(self):
        mock_prepare = MagicMock(side_effect=main.InsightCollectorError('o_O'))
        mock_events = {'SummaryReport': mock_prepare}
        mock_error = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.main.KINDS_PREPARE_EVENT',
                mock_events,
            ),
            patch('opentf.plugins.insightcollector.main.error', mock_error),
        ):
            main._publish_insight(
                main.ObserverProxy('url1', 'url2', 'wfid', {}),
                {'kind': 'SummaryReport', 'name': 'abcdef'},
                'wfid',
                {},
                None,
            )
            mock_prepare.assert_called_once()
            mock_error.assert_called_once_with('o_O')

    # _publish_worker_status

    def test_publish_worker_status_ok(self):
        mock_publish = MagicMock()
        mock_event = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.publish', mock_publish),
            patch('opentf.plugins.insightcollector.main.make_event', mock_event),
        ):
            main._publish_worker_status('wFId', 'wRkId', 'sTaTus')
        mock_event.assert_called_once()
        call = mock_event.call_args
        self.assertEqual('wFId', call[1]['metadata']['workflow_id'])
        self.assertEqual('wRkId', call[1]['spec']['worker']['worker_id'])
        mock_publish.assert_called_once()

    def test_publish_worker_status_ko(self):
        mock_publish = MagicMock(side_effect=Exception('Boom.'))
        mock_debug = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.publish', mock_publish),
            patch('opentf.plugins.insightcollector.main.make_event', MagicMock()),
            patch('opentf.plugins.insightcollector.main.debug', mock_debug),
        ):
            main._publish_worker_status('wFId', 'wRkId', 'sTaTus')
        mock_publish.assert_called_once()
        mock_debug.assert_called_once_with(
            'Failed to publish worker %s %s notification for workflow %s: %s.',
            'wRkId',
            'sTaTus',
            'wFId',
            'Boom.',
        )

    # _maybe_register_worker

    def test_maybe_registerworker_ok(self):
        workers = {}
        with (
            patch('opentf.plugins.insightcollector.main.WORKERS', workers),
            patch('opentf.plugins.insightcollector.main._publish_worker_status'),
        ):
            main._maybe_register_worker('wFId')
        self.assertEqual(1, len(workers))
        self.assertIn('wFId', workers)

    def test_maybe_registerworker_ko(self):
        workers = {'wFId': 'a'}
        with (
            patch('opentf.plugins.insightcollector.main.WORKERS', workers),
            patch('opentf.plugins.insightcollector.main._publish_worker_status'),
        ):
            main._maybe_register_worker('wFId')
        self.assertEqual(1, len(workers))
        self.assertIn('wFId', workers)

    # process_insight_definition

    def test_process_insight_definition_ok(self):
        mock_uuid = '00000000-0000-0000-0000-000000000001'
        mock_request = MagicMock()
        mock_request.args = {'insight': 'rgrg'}
        mock_report = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.main._get_request_insights_definition',
                MagicMock(return_value={'insights': 'b'}),
            ),
            patch(
                'opentf.plugins.insightcollector.main.validate_schema',
                MagicMock(return_value=(True, 'wroom')),
            ),
            patch(
                'opentf.plugins.insightcollector.main._maybe_generate_reports',
                mock_report,
            ),
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response'
            ) as mock_msr,
            patch(
                'opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS',
                {mock_uuid: []},
            ),
        ):
            main.process_insight_definition(mock_uuid)
        mock_report.assert_called_once_with(mock_uuid, 'b', 'rgrg')
        mock_msr.assert_not_called()

    def test_process_insight_definition_schema_ko(self):
        mock_uuid = '00000000-0000-0000-0000-000000000001'
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.args = {'insight': 'rgrg'}
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main._get_request_insights_definition',
                MagicMock(return_value={'insights': 'b'}),
            ),
            patch(
                'opentf.plugins.insightcollector.main.validate_schema',
                MagicMock(return_value=(False, 'wroom')),
            ),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
            patch(
                'opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS',
                {mock_uuid: []},
            ),
        ):
            main.process_insight_definition(mock_uuid)
        mock_msr.assert_called_once_with(
            'Invalid',
            'Not a valid insight definition: wroom',
            details={'workflow_id': mock_uuid},
        )

    def test_process_insight_definition_ko_exception(self):
        mock_uuid = '00000000-0000-0000-0000-000000000001'
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.args = {'insight': 'rgrg'}
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main._get_request_insights_definition',
                MagicMock(side_effect=Exception('Brr.')),
            ),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
            patch(
                'opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS',
                {mock_uuid: []},
            ),
        ):
            main.process_insight_definition(mock_uuid)
        mock_msr.assert_called_once_with(
            'Invalid', 'Brr.', details={'workflow_id': mock_uuid}
        )

    # _maybe_generate_reports

    def test_maybe_generate_report_ok(self):
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.args = {'insight': 'rgrg'}
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
            patch(
                'opentf.plugins.insightcollector.main.is_uuid',
                MagicMock(return_value=True),
            ),
            patch(
                'opentf.plugins.insightcollector.main.make_uuid',
                MagicMock(return_value='uuid_0'),
            ),
        ):
            main._maybe_generate_reports(
                'WFFF0', [{'name': 'rgrg', 'kind': 'kind'}], 'rgrg'
            )
        mock_msr.assert_called_once_with(
            'OK',
            '1 expected insight',
            {'request_id': 'uuid_0', 'expected': [('rgrg', 'kind')]},
        )

    def test_maybe_generate_report_ok_zero_models(self):
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.args = {'insight': 'c'}
        with (
            patch('opentf.plugins.insightcollector.main.request', mock_request),
            patch(
                'opentf.plugins.insightcollector.main.make_status_response', mock_msr
            ),
            patch(
                'opentf.plugins.insightcollector.main.is_uuid',
                MagicMock(return_value=True),
            ),
            patch(
                'opentf.plugins.insightcollector.main.make_uuid',
                MagicMock(return_value='uuid'),
            ),
        ):
            main._maybe_generate_reports('WFFFA', [{'name': 'rgrg'}], 'c')
        mock_msr.assert_called_once_with(
            'OK', '0 expected insights', {'request_id': 'uuid', 'expected': []}
        )

    # _get_request_insights_definition

    def test_process_insight_definition_json_ok(self):
        mock_uuid = '00000000-0000-0000-0000-000000000001'
        json_data = INSIGHT_DICT
        mgr_mock = MagicMock(return_value='oK')
        with (
            patch(
                'opentf.plugins.insightcollector.main._maybe_generate_reports', mgr_mock
            ),
            patch(
                'opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS',
                {mock_uuid: []},
            ),
        ):
            self.plugin.post(
                f'/workflows/{mock_uuid}/insights?insight=rgrg',
                data=json.dumps(json_data),
                headers={'Content-Type': 'application/json'},
            )
        mgr_mock.assert_called_once_with(
            mock_uuid, [{'name': 'report', 'kind': 'SummaryReport'}], 'rgrg'
        )

    def test_process_insight_definition_yaml_ok(self):
        mock_uuid = '00000000-0000-0000-0000-000000000001'
        yaml_data = INSIGHT_DICT
        mgr_mock = MagicMock(return_value='oK')
        with (
            patch(
                'opentf.plugins.insightcollector.main._maybe_generate_reports', mgr_mock
            ),
            patch(
                'opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS',
                {mock_uuid: []},
            ),
        ):
            self.plugin.post(
                f'/workflows/{mock_uuid}/insights?insight=rgrg',
                data=json.dumps(yaml_data),
                headers={'Content-Type': 'application/x-yaml'},
            )
        mgr_mock.assert_called_once_with(
            mock_uuid, [{'name': 'report', 'kind': 'SummaryReport'}], 'rgrg'
        )

    def test_process_insight_definition_from_file(self):
        mock_uuid = '00000000-0000-0000-0000-000000000001'
        mgr_mock = MagicMock(return_value='oK')
        with (
            patch(
                'opentf.plugins.insightcollector.main._maybe_generate_reports', mgr_mock
            ),
            patch(
                'opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS',
                {mock_uuid: []},
            ),
        ):
            self.plugin.post(
                f'/workflows/{mock_uuid}/insights?insight=rgrg',
                data={'insights': open('tests/python/resources/insights.yaml', 'rb')},
            )
        mgr_mock.assert_called_once_with(
            mock_uuid, [{'name': 'my-report', 'kind': 'SummaryReport'}], 'rgrg'
        )

    def test_process_insight_definition_ko_cannot_parse_json(self):
        mock_uuid = '00000000-0000-0000-0000-000000000001'
        json_data = '1+2+3'
        mock_error = MagicMock(return_value='oK')
        with (
            patch('opentf.plugins.insightcollector.main.error', mock_error),
            patch(
                'opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS',
                {mock_uuid: []},
            ),
        ):
            response = self.plugin.post(
                f'/workflows/{mock_uuid}/insights?insight=rgrg',
                data=json_data,
                headers={'Content-Type': 'application/json'},
            )
        mock_error.assert_called_once()
        self.assertEqual('Could not parse insight definition from file.', response.json['message'])  # type: ignore

    def test_process_insight_definition_ko_contentsize(self):
        mock_uuid = '00000000-0000-0000-0000-000000000001'
        with patch(
            'opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS',
            {mock_uuid: []},
        ):
            response = self.plugin.post(
                f'/workflows/{mock_uuid}/insights?insight=rgrg',
                data='',
                headers={'Content-Type': 'application/x-yaml'},
            )
        self.assertIn('Content-size must be specified', response.json['message'])  # type: ignore

    def test_process_insight_definition_ko_unknownworkflow(self):
        mock_uuid = '00000000-0000-0000-0000-000000000001'
        response = self.plugin.post(
            f'/workflows/{mock_uuid}/insights?insight=rgrg',
            data='',
            headers={'Content-Type': 'application/x-yaml'},
        )
        self.assertIn(f'Workflow {mock_uuid} not found.', response.json['message'])  # type: ignore

    # _fetch_contexts

    def test_fetch_contexts_wf_done(self):
        workflow_id = 'wOrKflow_iD'
        wme = {
            workflow_id: [
                {
                    'metadata': {
                        'name': 'WorkflowCompleted',
                        'workflow_id': workflow_id,
                    },
                    'kind': 'WorkflowCompleted',
                }
            ]
        }
        mock_context = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.WORKFLOWS_META_EVENTS', wme),
            patch(
                'opentf.plugins.insightcollector.main._make_workflow_context',
                mock_context,
            ),
        ):
            main._fetch_workflow_context(workflow_id)
        mock_context.assert_called_once_with(
            {'name': 'WorkflowCompleted', 'workflow_id': workflow_id}, 'success'
        )

    # _maybe_set_model_defaults

    def test_maybe_set_model_defaults_ok(self):
        models = [
            {
                'name': 'executionreport',
                'kind': 'SummaryReport',
                'spec': {'scope': 'true'},
            }
        ]
        result = main._maybe_set_model_defaults(models)
        self.assertEqual(1, len(result))
        self.assertIsNotNone(result[0]['spec'].get('template'))

    # _publish_noinsights

    def test_publish_no_insights_msg_ok(self):
        models = [{'name': 'ModelName', 'kind': 'ModelKind', 'if': 'sun is shining'}]
        file_mock = mock_open()
        file_mock().write = MagicMock()
        mock_event = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.main.make_uuid',
                MagicMock(return_value='123'),
            ),
            patch('builtins.open', file_mock),
            patch('opentf.plugins.insightcollector.main.publish'),
            patch('opentf.plugins.insightcollector.main.make_event', mock_event),
        ):
            main._publish_noinsights(models, 'wf_id', None)
        mock_event.assert_called_once()
        self.assertEqual(
            '/tmp/wf_id-123_WR_NO-INSIGHTS-PUBLISHED.txt',
            mock_event.call_args[1]['attachments'][0],
        )
        file_mock().write.assert_called_once()
        self.assertIn('sun is shining', file_mock().write.call_args[0][0])

    def test_publish_no_insights_msg_ko(self):
        mock_error = MagicMock()
        models = [{'name': 'ModelName', 'kind': 'ModelKind', 'if': 'sun is shining'}]
        with (
            patch('opentf.plugins.insightcollector.main.make_uuid'),
            patch('opentf.plugins.insightcollector.main.error', mock_error),
            patch(
                'builtins.open',
                MagicMock(side_effect=Exception('2 lazy 2 open a file')),
            ),
        ):
            main._publish_noinsights(models, 'wffff', None)
        mock_error.assert_called_once_with(
            'Error publishing error log: %s.', '2 lazy 2 open a file'
        )

    # collect_and_publish_insights

    def test_collect_and_publish_insights_ok_then_ko(self):
        workflow_id = 'wOrKflow_iD'
        mock_response = MagicMock()
        mock_response.json.return_value = {'some': 'content'}
        mock_requests = MagicMock()
        mock_requests.get = MagicMock(side_effect=[mock_response, Exception('ooOps')])
        mock_plugin = MagicMock()
        mock_plugin.logger.error = MagicMock()
        mock_queue = MagicMock()
        mock_queue.get = MagicMock(
            return_value=(
                workflow_id,
                'tOkEn',
                {'workflow': {'some': 'context'}},
                {'some': 'model'},
                None,
            )
        )
        mock_cleanup = MagicMock(side_effect=Exception('Boom.'))
        mock_worker = MagicMock()
        mock_workers = {workflow_id: 'uuId'}
        mock_publish = MagicMock()
        mock_filter = MagicMock(side_effect=['some', 'thing', 'else'])
        mock_proxy = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.requests', mock_requests),
            patch('opentf.plugins.insightcollector.main.plugin', mock_plugin),
            patch(
                'opentf.plugins.insightcollector.main.PENDING_REPORTSTASKS', mock_queue
            ),
            patch(
                'opentf.plugins.insightcollector.main._maybe_cleanup_pending_reports',
                mock_cleanup,
            ),
            patch(
                'opentf.plugins.insightcollector.main.workflow_is_completed',
                MagicMock(return_value=True),
            ),
            patch(
                'opentf.plugins.insightcollector.main._get_workflow_creationtimestamp',
                MagicMock(return_value='today'),
            ),
            patch(
                'opentf.plugins.insightcollector.main.make_uuid',
                MagicMock(return_value='uuid'),
            ),
            patch(
                'opentf.plugins.insightcollector.main._publish_worker_status',
                mock_worker,
            ),
            patch(
                'opentf.plugins.insightcollector.main._filter_models_on_condition',
                mock_filter,
            ),
            patch(
                'opentf.plugins.insightcollector.main._maybe_set_model_defaults',
                MagicMock(return_value=[{'model': 'one'}]),
            ),
            patch(
                'opentf.plugins.insightcollector.main._publish_insight', mock_publish
            ),
            patch('opentf.plugins.insightcollector.main.ObserverProxy', mock_proxy),
            patch('opentf.plugins.insightcollector.main.WORKERS', mock_workers),
        ):
            self.assertRaisesRegex(
                Exception, 'Boom.', main.collect_and_publish_insights
            )
        self.assertEqual(1, mock_worker.call_count)
        mock_publish.assert_called_once()
        self.assertEqual(mock_proxy(), mock_publish.call_args[0][0])
        self.assertEqual({'model': 'one'}, mock_publish.call_args[0][1])
        self.assertEqual(
            'today', mock_publish.call_args[0][3]['workflow']['creationTimestamp']
        )
        mock_cleanup.assert_called_once()

    def test_collect_and_publish_insights_no_contexts_no_models_then_ko(self):
        workflow_id = 'wOrKflow_iD'
        mock_response = MagicMock()
        mock_response.json.return_value = {
            'details': {'status': 'DONE'},
            'some': 'content',
        }
        mock_requests = MagicMock()
        mock_requests.get = MagicMock(side_effect=[mock_response, Exception('ooOps')])
        mock_plugin = MagicMock()
        mock_plugin.logger.error = MagicMock()
        mock_plugin.logger.debug = MagicMock()
        mock_queue = MagicMock()
        mock_queue.get = MagicMock(
            return_value=(workflow_id, 'tOkEn', {}, {'some': 'model'}, None)
        )
        mock_cleanup = MagicMock(side_effect=Exception('Boom.'))
        mock_contexts = MagicMock(return_value={'workflow': {}})
        mock_worker = MagicMock()
        mock_workers = {'wOrKflow_iD': 'uuiD'}
        mock_publish = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.requests', mock_requests),
            patch('opentf.plugins.insightcollector.main.plugin', mock_plugin),
            patch(
                'opentf.plugins.insightcollector.main.PENDING_REPORTSTASKS', mock_queue
            ),
            patch(
                'opentf.plugins.insightcollector.main._maybe_cleanup_pending_reports',
                mock_cleanup,
            ),
            patch(
                'opentf.plugins.insightcollector.main.workflow_is_completed',
                MagicMock(return_value=True),
            ),
            patch(
                'opentf.plugins.insightcollector.main._fetch_workflow_context',
                mock_contexts,
            ),
            patch(
                'opentf.plugins.insightcollector.main._get_workflow_creationtimestamp',
                MagicMock(return_value='today'),
            ),
            patch(
                'opentf.plugins.insightcollector.main.make_uuid',
                MagicMock(return_value='uuid'),
            ),
            patch(
                'opentf.plugins.insightcollector.main._publish_worker_status',
                mock_worker,
            ),
            patch(
                'opentf.plugins.insightcollector.main._publish_noinsights',
                mock_publish,
            ),
            patch(
                'opentf.plugins.insightcollector.main._filter_models_on_condition',
                MagicMock(return_value=None),
            ),
            patch('opentf.plugins.insightcollector.main.WORKERS', mock_workers),
        ):
            self.assertRaisesRegex(
                Exception, 'Boom.', main.collect_and_publish_insights
            )
        mock_contexts.assert_called_once()
        self.assertEqual(2, mock_worker.call_count)
        self.assertEqual(2, mock_requests.get.call_count)
        self.assertEqual(3, mock_plugin.logger.debug.call_count)
        specific_call = (
            'Filtered models list is empty, not generating reports for workflow %s.',
            'wOrKflow_iD',
        )
        call_found = False
        for callargs in mock_plugin.logger.debug.call_args_list:
            if callargs == call(*specific_call):
                call_found = True
                break
        self.assertTrue(call_found)
        mock_publish.assert_called_once_with({'some': 'model'}, 'wOrKflow_iD', None)
        mock_plugin.logger.error.assert_called_once()
        mock_cleanup.assert_called_once()

    def test_collect_and_publish_insights_wf_not_completed_then_ko(self):
        mock_requests = MagicMock()
        mock_requests.get = MagicMock(side_effect=[{}, {}, Exception('ooOps')])
        mock_plugin = MagicMock()
        mock_plugin.logger.error = MagicMock()
        mock_queue = MagicMock()
        mock_queue.get = MagicMock(return_value=('wOrKflow_iD', 'tOkEn', {}, {}, None))
        mock_cleanup = MagicMock(side_effect=Exception('Boom.'))
        with (
            patch('opentf.plugins.insightcollector.main.requests', mock_requests),
            patch('opentf.plugins.insightcollector.main.plugin', mock_plugin),
            patch(
                'opentf.plugins.insightcollector.main.PENDING_REPORTSTASKS', mock_queue
            ),
            patch(
                'opentf.plugins.insightcollector.main._maybe_cleanup_pending_reports',
                mock_cleanup,
            ),
            patch(
                'opentf.plugins.insightcollector.main.workflow_is_completed',
                MagicMock(side_effect=[False, True]),
            ),
        ):
            self.assertRaisesRegex(
                Exception, 'Boom.', main.collect_and_publish_insights
            )
        self.assertEqual(2, mock_requests.get.call_count)
        mock_plugin.logger.error.assert_called_once()
        mock_cleanup.assert_called_once()

    def test_collect_and_publish_insights_ko(self):
        mock_requests = MagicMock()
        mock_requests.get = MagicMock(side_effect=Exception('ooOps'))
        mock_plugin = MagicMock()
        mock_plugin.logger.error = MagicMock()
        mock_queue = MagicMock()
        mock_queue.get = MagicMock(return_value=('wOrKflow_iD', 'tOkEn', {}, {}, None))
        mock_cleanup = MagicMock(side_effect=Exception('Boom.'))
        with (
            patch('opentf.plugins.insightcollector.main.requests', mock_requests),
            patch('opentf.plugins.insightcollector.main.plugin', mock_plugin),
            patch(
                'opentf.plugins.insightcollector.main.PENDING_REPORTSTASKS', mock_queue
            ),
            patch(
                'opentf.plugins.insightcollector.main._maybe_cleanup_pending_reports',
                mock_cleanup,
            ),
        ):
            self.assertRaisesRegex(
                Exception, 'Boom.', main.collect_and_publish_insights
            )
        mock_plugin.logger.error.assert_called_once()
        mock_cleanup.assert_called_once()

    # ObserverProxy

    ## get_datasource

    def test_observer_proxy_get_datasource_ok(self):
        datasource = [{'uuid': 'id1', 'foo': 'bar'}]
        mock_query = MagicMock(return_value=datasource)
        with patch(
            'opentf.plugins.insightcollector.main.ObserverProxy._query', mock_query
        ):
            proxy = main.ObserverProxy('url1', 'url2', 'wF', {})
            source = proxy.get_datasource('testcases', 'true')
        self.assertEqual({'id1': datasource[0]}, source)

    def test_observer_proxy_get_datasource_ko_scopeerror(self):
        mock_query = MagicMock(side_effect=main.ScopeError('Error.'))
        with patch(
            'opentf.plugins.insightcollector.main.ObserverProxy._query', mock_query
        ):
            proxy = main.ObserverProxy('url1', 'url2', 'wF', {})
            self.assertRaisesRegex(
                main.ScopeError, 'Error.', proxy.get_datasource, 'testcases', 'true'
            )

    ## get_events

    def test_observer_proxy_get_events_ok(self):
        mock_query = MagicMock(return_value=['some', 'small', 'events'])
        with patch(
            'opentf.plugins.insightcollector.main.ObserverProxy._query', mock_query
        ):
            proxy = main.ObserverProxy('url1', 'url2', 'wF', {})
            events = proxy.get_workflow_event()
        self.assertEqual(['some', 'small', 'events'], events)

    ## _query

    def test_observer_proxy_query_events_ok(self):
        mock_resp = MagicMock()
        mock_resp.json.return_value = {'details': {'items': [{'item': 'one'}]}}
        mock_resp.links = {'next': {'url': 'foo?bar'}}
        mock_resp.status_code = 200
        mock_resp2 = MagicMock()
        mock_resp2.json.return_value = {'details': {'items': [{'item': 'two'}]}}
        mock_resp2.links = {'not next': {'url': 'foo?bar'}}
        mock_resp2.status_code = 200
        mock_request = MagicMock()
        mock_request.get = MagicMock(side_effect=[mock_resp, mock_resp2])
        with patch('opentf.plugins.insightcollector.main.requests', mock_request):
            proxy = main.ObserverProxy('url1', 'url2', 'wf', {})
            result = proxy._query(proxy.events_url)
        self.assertEqual(2, mock_request.get.call_count)
        self.assertEqual([{'item': 'one'}, {'item': 'two'}], result)

    def test_observer_proxy_query_ok_202_then_200(self):
        mock_resp = MagicMock()
        mock_resp.status_code = 202
        mock_resp2 = MagicMock()
        mock_resp2.json.return_value = {'details': {'items': [{'item': 'three'}]}}
        mock_resp2.links = {'not next': {'url': 'foo?bar'}}
        mock_resp2.status_code = 200
        mock_request = MagicMock()
        mock_request.get = MagicMock(side_effect=[mock_resp, mock_resp2])
        with (
            patch('opentf.plugins.insightcollector.main.requests', mock_request),
            patch('time.time', side_effect=[1, 10, 33444]),
            patch('opentf.plugins.insightcollector.main.sleep', return_value=None),
        ):
            proxy = main.ObserverProxy('url1', 'url2', 'wf', {})
            result = proxy._query(proxy.events_url)
        self.assertEqual(2, mock_request.get.call_count)
        self.assertEqual([{'item': 'three'}], result)

    def test_observer_proxy_query_dataerror(self):
        mock_response = MagicMock()
        mock_response.status_code = 422
        mock_response.text = 'Data data error'
        mock_response.json = lambda: {
            'message': 'Data error',
            'details': {'data_error': 'data error'},
        }
        mock_requests = MagicMock(return_value=mock_response)
        with (
            patch('opentf.plugins.insightcollector.main.requests.get', mock_requests),
            patch('opentf.plugins.insightcollector.main.debug') as mock_debug,
        ):
            proxy = main.ObserverProxy('url1', 'url2', 'wfId', {})
            result = proxy._query(proxy.datasources_url, 'true')
            mock_debug.assert_called_once_with(
                'Could not retrieve datasource data for workflow %s: %s',
                'wfId',
                'Data error',
            )
            self.assertEqual([{'uuid': 'EMPTY_ITEMS'}], result)

    def test_observer_proxy_query_valueerror(self):
        mock_resp = MagicMock()
        mock_resp.text = 'fOOBAR'
        mock_resp.json = MagicMock(side_effect=ValueError('yaDa'))
        mock_resp.status_code = 200
        mock_error = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.main.requests.get',
                MagicMock(return_value=mock_resp),
            ),
            patch('opentf.plugins.insightcollector.main.error', mock_error),
        ):
            proxy = main.ObserverProxy('url1', 'url2', 'wfId', {})
            result = proxy._query(proxy.datasources_url)
        self.assertEqual([], result)
        mock_error.assert_called_once()

    def test_observer_proxy_query_ko(self):
        mock_response = MagicMock()
        mock_response.status_code = 404
        mock_response.text = 'Incredibly not found'
        mock_requests = MagicMock(return_value=mock_response)
        mock_debug = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.main.requests.get', mock_requests),
            patch('opentf.plugins.insightcollector.main.debug', mock_debug),
        ):
            proxy = main.ObserverProxy('url1', 'url2', 'wfId', {})
            proxy._query(proxy.datasources_url, 'true')
        mock_requests.assert_called_once_with(
            'url1',
            headers={},
            timeout=10,
            params={'scope': 'true', 'fieldSelector': None},
        )
        mock_debug.assert_called_once_with(
            'Could not retrieve events or datasource for workflow %s: (%d) %s.',
            'wfId',
            404,
            'Incredibly not found',
        )

    def test_observer_proxy_query_ko_scopeerror(self):
        mock_response = MagicMock()
        mock_response.status_code = 422
        mock_response.text = 'Incredibly not found'
        mock_response.json = lambda: {
            'message': 'Scope error',
            'details': {'scope_error': 'scope error'},
        }
        mock_requests = MagicMock(return_value=mock_response)
        with patch('opentf.plugins.insightcollector.main.requests.get', mock_requests):
            proxy = main.ObserverProxy('url1', 'url2', 'wfId', {})
            self.assertRaisesRegex(
                main.ScopeError,
                'Scope error',
                proxy._query,
                proxy.datasources_url,
                'true',
            )
        mock_requests.assert_called_once_with(
            'url1',
            headers={},
            timeout=10,
            params={'scope': 'true', 'fieldSelector': None},
        )

    # _load_image_models

    def test_load_insights_models_ok(self):
        insights = []
        with (
            patch('opentf.plugins.insightcollector.main.IMG_INSIGHTS', insights),
            patch('os.path.isdir', return_value=True),
            patch('os.path.isfile', return_value=True) as mock_dir,
            patch('os.listdir', return_value=['1.yml', '2.yaml']) as mock_listdir,
            patch(
                'opentf.plugins.insightcollector.main._read_insights',
                side_effect=[IMG_INSIGHT_1, IMG_INSIGHT_2],
            ),
            patch('opentf.plugins.insightcollector.main.debug') as mock_debug,
            patch('opentf.plugins.insightcollector.main.CONFIG_PATH', '/some/path'),
        ):
            main._load_image_models()
        self.assertEqual(2, mock_dir.call_count)
        self.assertEqual(1, mock_listdir.call_count)
        self.assertEqual(2, mock_debug.call_count)
        self.assertEqual('SummaryReport', insights[1]['kind'])
        self.assertEqual('ExecutionLog', insights[0]['kind'])

    def test_load_insights_models_ko_ok(self):
        insights = []
        with (
            patch('opentf.plugins.insightcollector.main.IMG_INSIGHTS', insights),
            patch(
                'opentf.plugins.insightcollector.main._filter_listdir',
                return_value=['01_conf.yaml', '02_conf.yaml'],
            ),
            patch(
                'opentf.plugins.insightcollector.main._read_insights',
                side_effect=[{}, {'insights': {'a': 'b'}}],
            ),
            patch(
                'opentf.plugins.insightcollector.main._deduplicate',
                return_value=[{'a': 'b'}],
            ),
            patch('opentf.plugins.insightcollector.main.debug') as mock_debug,
            patch('opentf.plugins.insightcollector.main.CONFIG_PATH', '/some/path'),
        ):
            main._load_image_models()
        self.assertEqual([{'a': 'b'}], insights)
        mock_debug.assert_called_once()

    def test_load_insights_models_no_file(self):
        with (
            patch('os.path.isfile', return_value=False),
            patch('os.listdir', return_value=['1', '2']),
            patch('opentf.plugins.insightcollector.main.debug') as mock_debug,
        ):
            res = main._load_image_models()
        self.assertIsNone(res)
        mock_debug.assert_called_once()
        self.assertIn('No %s files provided in %s', mock_debug.call_args[0][0])

    def test_load_insights_models_exception(self):
        with (
            patch('os.path.isdir', return_value=True),
            patch('os.path.isfile', return_value=True),
            patch('os.listdir', return_value=['1.yml', '2.yaml']),
            patch('opentf.plugins.insightcollector.main.error') as mock_error,
            patch(
                'opentf.plugins.insightcollector.main._read_insights',
                side_effect=Exception('Boo'),
            ),
        ):
            self.assertRaises(SystemExit, main._load_image_models)
        self.assertEqual(1, mock_error.call_count)
        self.assertEqual('Boo', mock_error.call_args_list[0][0][2])

    # _load_image_configuration

    def test_ensure_config_path_exists(self):
        with (
            patch(
                'opentf.plugins.insightcollector.main.get_context_parameter',
                return_value='/some/path',
            ),
            patch('os.path.isdir') as mock_isdir,
            patch(
                'opentf.plugins.insightcollector.main._load_image_models'
            ) as mock_lim,
            patch('opentf.plugins.insightcollector.main._load_image_css') as mock_lic,
        ):
            main._load_image_configuration()
            mock_isdir.assert_called_once()
            mock_lim.assert_called_once()
            mock_lic.assert_called_once()

    # _load_image_css

    def test_load_css_ok(self):
        mock_file = mock_open()
        mock_file.return_value.read.side_effect = [
            'style',
            'style1',
            'style2',
            'style3',
        ]
        with (
            patch('opentf.plugins.insightcollector.main.IMG_CSS', []) as mock_css,
            patch(
                'opentf.plugins.insightcollector.main.os.listdir',
                return_value=[
                    '01_style.css',
                    '03_style.css',
                    '04_style.css',
                    '02_style.css',
                ],
            ) as mock_dir,
            patch('opentf.plugins.insightcollector.main.os.path.isfile'),
            patch('opentf.plugins.insightcollector.main.debug') as mock_debug,
            patch('builtins.open', mock_file),
            patch(
                'opentf.plugins.insightcollector.main.get_resources',
                return_value='built-in style',
            ) as mock_res,
            patch('os.path.isdir', return_value=True),
        ):
            main._load_image_css()
        self.assertEqual(5, len(mock_css))
        self.assertEqual('built-in style', mock_css[0])
        self.assertEqual('style3', mock_css[-1])
        self.assertEqual(4, mock_debug.call_count)
        self.assertEqual('04_style.css', mock_debug.call_args_list[-1][0][1])
        mock_dir.assert_called_once()
        mock_res.assert_called_once()

    def test_load_css_no_files(self):
        with (
            patch('opentf.plugins.insightcollector.main.IMG_CSS', []) as mock_css,
            patch(
                'opentf.plugins.insightcollector.main._filter_listdir', return_value=[]
            ),
            patch(
                'opentf.plugins.insightcollector.main.get_resources',
                return_value='built-in style',
            ) as mock_res,
        ):
            main._load_image_css()
        self.assertEqual(['built-in style'], mock_css)
        mock_res.assert_called_once()

    def test_load_css_no_directory(self):
        with (
            patch('opentf.plugins.insightcollector.main.IMG_CSS', []) as mock_css,
            patch(
                'opentf.plugins.insightcollector.main._filter_listdir', return_value=[]
            ),
            patch(
                'opentf.plugins.insightcollector.main.get_resources',
                return_value='built-in style',
            ) as mock_res,
            patch('opentf.plugins.insightcollector.main.debug') as mock_debug,
            patch('os.path.isdir', return_value=False),
        ):
            main._load_image_css()
        self.assertEqual(['built-in style'], mock_css)
        mock_res.assert_called_once()
        mock_debug.assert_called_once()
        self.assertIn('not an existing directory', mock_debug.call_args[0][0])


if __name__ == '__main__':
    unittest.main()
