# Copyright (c) 2022-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Receptionist unit tests."""

import logging
import os
import unittest
import sys

from datetime import datetime, timedelta, timezone
from unittest.mock import MagicMock, patch, mock_open

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
logging.disable(logging.CRITICAL)
from opentf.core import observer

logging.disable(logging.NOTSET)


########################################################################

CHANNELS_EMPTY = {}

CHANNELS_1_EMPTY = {'provider_1': []}
CHANNELS_1_NOTEMPTY = {'provider_1': [{'something'}]}

CHANNELS_2_EMPTY = {'provider_1': [], 'provider_2': []}
CHANNELS_2_NOTEMPTY = {'provider_1': [{'something'}], 'provider_2': [{'another'}]}

CHANNELS_3_FOR_FILTERS = {
    'provider_1': [{'something': 'anything', 'metadata': {'namespaces': '*'}}],
    'provider_2': [{'something': 'nothing', 'metadata': {'namespaces': '*'}}],
}

CHANNELS_4_FOR_FILTERS = {
    'provider_1': [{'something': 'anything', 'metadata': {'namespaces': '*'}}],
    'provider_2': [{'something': 'nothing', 'metadata': {'namespaces': '*'}}],
    'provider_3': [{'something': 'nothing', 'metadata': {'namespaces': 'test,foo'}}],
    'provider_4': [{'something': 'nothing', 'metadata': {'namespaces': 'foo,test'}}],
}

CHANNELS_5_FOR_FILTERS = {
    'provider_1': [{'something': 'anything', 'metadata': {'namespaces': '*'}}],
    'provider_2': [{'something': 'nothing', 'metadata': {'namespaces': '*'}}],
    'provider_3': [{'something': 'nothing', 'metadata': {'namespaces': 'test,foo'}}],
    'provider_4': [{'something': 'nothing', 'metadata': {'namespaces': 'foo,test'}}],
    'provider_5': [{'something': 'nothing', 'metadata': {'namespaces': 'bar'}}],
    'provider_6': [
        {'something': 'nothing', 'metadata': {'namespaces': 'foo,test,bar'}}
    ],
}

WORKFLOW_STATUS_FOR_FILTERS = {
    'workflow': [
        {'kind': 'ExecutionResult', 'something': 'being'},
        {'kind': 'ProviderCommand', 'something': 'nothingness0'},
        {'kind': 'ProviderCommand', 'something': 'nothingness1'},
    ]
}

TESTCASES = {
    "c94d7ea6-9add-4791-a179-81b7a7559ff7": {
        "apiVersion": "testing.opentestfactory.org/v1alpha1",
        "execution": {
            "duration": 1,
            "endTime": "2024-08-23T17:15:32.399656",
            "startTime": "2024-08-23T17:15:30.884244",
        },
        "kind": "TestCase",
        "metadata": {
            "creationTimestamp": "2024-08-23T17:15:30.842905",
            "execution_id": "8f2db7d7-4859-4590-90b5-41fd66ceb9c0",
            "id": "c94d7ea6-9add-4791-a179-81b7a7559ff7",
            "job_id": "PrettyJob",
            "name": "Test Backslashes Reporter#Test  single",
            "namespace": "default",
            "workflow_id": "8ff734b8-320e-4e0b-8470-e6db432ada5f",
        },
        "status": "FAILURE",
        "test": {
            "job": "robot-2-execute-test",
            "managed": False,
            "outcome": "failure",
            "runs-on": ["linux", "cypress"],
            "suiteName": "Test Backslashes Reporter",
            "technology": "robotframework",
            "test": "robotframeworktestsamples/test_backslashes_reporter.robot",
            "testCaseName": "Test  single",
            "uses": "robotframework/execute@v1",
        },
        "uuid": "c94d7ea6-9add-4791-a179-81b7a7559ff7",
    },
    "1e09191e-030f-4043-bcb6-8ab254129173": {
        "apiVersion": "testing.opentestfactory.org/v1alpha1",
        "execution": {
            "duration": 0,
            "endTime": "2024-08-23T17:15:32.399656",
            "startTime": "2024-08-23T17:15:30.884244",
        },
        "kind": "TestCase",
        "metadata": {
            "creationTimestamp": "2024-08-23T17:15:30.842905",
            "execution_id": "8f2db7d7-4859-4590-90b5-41fd66ceb9c0",
            "id": "1e09191e-030f-4043-bcb6-8ab254129173",
            "job_id": "PrettyJob",
            "name": "Test Backslashes Reporter#Test \\ double",
            "namespace": "default",
            "workflow_id": "8ff734b8-320e-4e0b-8470-e6db432ada5f",
        },
        "status": "SUCCESS",
        "test": {
            "job": "robot-2-execute-test",
            "managed": False,
            "outcome": "success",
            "runs-on": ["linux", "cypress"],
            "suiteName": "Test Backslashes Reporter",
            "technology": "robotframework",
            "test": "robotframeworktestsamples/test_backslashes_reporter.robot",
            "testCaseName": "Test \\ double",
            "uses": "robotframework/execute@v1",
        },
        "uuid": "1e09191e-030f-4043-bcb6-8ab254129173",
    },
}

JOBS_ZERO = {
    "PrettyJob": {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "Job",
        "metadata": {
            "creationTimestamp": "2024-08-23T17:15:30.798933",
            "id": "PrettyJob",
            "name": "robot-2-execute-test",
            "namespace": "default",
            "workflow_id": "8ff734b8-320e-4e0b-8470-e6db432ada5f",
        },
        "spec": {"runs-on": ["linux", "cypress"], "variables": {}},
        "status": {
            "duration": 1515.412,
            "endTime": "2024-08-23T17:15:32.399656",
            "phase": "SUCCEEDED",
            "requestTime": "2024-08-23T17:15:30.810283",
            "startTime": "2024-08-23T17:15:30.884244",
            "testCaseCount": 0,
            "testCaseStatusSummary": {
                "cancelled": 0,
                "error": 0,
                "failure": 0,
                "skipped": 0,
                "success": 0,
            },
        },
        "uuid": "PrettyJob",
    }
}

TAGS_ZERO = {
    "cypress": {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "Tag",
        "metadata": {
            "name": "cypress",
            "namespace": "default",
            "workflow_id": "8ff734b8-320e-4e0b-8470-e6db432ada5f",
        },
        "status": {
            "jobCount": 0,
            "testCaseCount": 0,
            "testCaseStatusSummary": {
                "cancelled": 0,
                "error": 0,
                "failure": 0,
                "skipped": 0,
                "success": 0,
            },
        },
        "uuid": "cypress",
    },
    "linux": {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "Tag",
        "metadata": {
            "name": "linux",
            "namespace": "default",
            "workflow_id": "8ff734b8-320e-4e0b-8470-e6db432ada5f",
        },
        "status": {
            "jobCount": 2,
            "testCaseCount": 217,
            "testCaseStatusSummary": {
                "cancelled": 0,
                "error": 0,
                "failure": 205,
                "skipped": 0,
                "success": 12,
            },
        },
        "uuid": "linux",
    },
}

########################################################################


class TestObserver(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def test_clean_workflows_nocompletedworkflows(self):
        with patch('opentf.core.observer.WORKFLOWS_LASTSEEN', {}):
            observer.clean_workflows()
            self.assertFalse(observer.WORKFLOWS_LASTSEEN)

    def test_clean_workflows_notexpired(self):
        workflows = {'notexpired': {'metadata': {}}}
        lastseen = {'notexpired': datetime.now(timezone.utc)}
        with (
            patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            observer.clean_workflows()
            self.assertEqual(len(observer.WORKFLOWS_LASTSEEN), 1)
            self.assertIn('notexpired', observer.WORKFLOWS_LASTSEEN)

    def test_clean_workflows_expired(self):
        workflows = {
            'expired': {
                'metadata': {
                    'completionTimestamp': datetime(
                        year=2000, month=12, day=3, tzinfo=timezone.utc
                    ).isoformat()
                }
            }
        }
        lastseen = {
            'expired': datetime(year=2000, month=12, day=3, tzinfo=timezone.utc)
        }
        with (
            patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            observer.clean_workflows()
            self.assertEqual(len(observer.WORKFLOWS_LASTSEEN), 0)
            self.assertNotIn('expired', observer.WORKFLOWS_LASTSEEN)

    def test_clean_workflows_mixed(self):
        workflows = {
            'expired': {
                'metadata': {
                    'completionTimestamp': datetime(
                        year=2000, month=12, day=3, tzinfo=timezone.utc
                    ).isoformat()
                }
            },
            'notexpired': {
                'metadata': {
                    'completionTimestamp': datetime.now(timezone.utc).isoformat()
                }
            },
        }
        lastseen = {
            'expired': datetime(year=2000, month=12, day=3, tzinfo=timezone.utc),
            'notexpired': datetime.now(timezone.utc),
        }
        with (
            patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            observer.clean_workflows()
            self.assertEqual(len(observer.WORKFLOWS_LASTSEEN), 1)
            self.assertNotIn('expired', observer.WORKFLOWS_LASTSEEN)
            self.assertIn('notexpired', observer.WORKFLOWS_LASTSEEN)

    # _read_rp_definition

    def test_read_rp_definition_invalid(self):
        rp = ['foo']
        with (
            patch('opentf.core.observer.open', mock_open(read_data='{}')),
            patch('opentf.core.observer.RETENTIONPOLICY', rp),
        ):
            observer._read_rp_definition('dummy', 'dummyfile')
            self.assertEqual(observer.RETENTIONPOLICY, [])

    def test_read_rp_definition_ok_empty(self):
        rp = ['foo']
        new_rp = '{"retentionPolicy": []}'
        with (
            patch('opentf.core.observer.open', mock_open(read_data=new_rp)),
            patch('opentf.core.observer.RETENTIONPOLICY', rp),
        ):
            observer._read_rp_definition('dummy', 'dummyfile')
            self.assertEqual(observer.RETENTIONPOLICY, [])

    def test_read_rp_definition_ok_two(self):
        rp = ['foo']
        new_rp = '''
retentionPolicy:
- name: foo
  scope: workflow.namespace == 'default'
  retentionPeriod: forever
- name: bar
  scope: workflow.namespace == 'junk'
  retentionPeriod: 3d
  weight: 100
'''
        with (
            patch('opentf.core.observer.open', mock_open(read_data=new_rp)),
            patch('opentf.core.observer.RETENTIONPOLICY', rp),
        ):
            observer._read_rp_definition('dummy', 'dummyfile')
            self.assertEqual(len(observer.RETENTIONPOLICY), 2)
            policy_1 = observer.RETENTIONPOLICY[0]
            self.assertEqual(policy_1['name'], 'foo')
            self.assertIsNone(policy_1['timedelta'])
            policy_2 = observer.RETENTIONPOLICY[1]
            self.assertEqual(policy_2['name'], 'bar')
            self.assertIsNotNone(policy_2['timedelta'])

    def test_read_rp_definition_bad_yaml(self):
        rp = ['foo']
        new_rp = '''
retentionPoli
- name: foo
'''
        with (
            patch('opentf.core.observer.open', mock_open(read_data=new_rp)),
            patch('opentf.core.observer.RETENTIONPOLICY', rp),
        ):
            observer._read_rp_definition('dummy', 'dummyfile')
            self.assertEqual(observer.RETENTIONPOLICY, [])

    # _get_cleanup_candidates

    def test_get_cleanup_candidates_no_rp(self):
        with patch('opentf.core.observer.RETENTIONPOLICY', []):
            self.assertEqual(observer._get_cleanup_candidates(), [])

    def test_get_cleanup_candidates_maxcount_norp(self):
        lastseen = {
            'weight_1': datetime.now(timezone.utc),
            'weight_2': datetime.now(timezone.utc),
            'weight_3': datetime.now(timezone.utc),
            'weight_4': datetime.now(timezone.utc),
        }
        workflows = {
            'weight_1': {'metadata': {}},
            'weight_2': {'metadata': {}},
            'weight_3': {'metadata': {}},
            'weight_4': {'metadata': {}},
        }
        with (
            patch('opentf.core.observer.RETENTIONPOLICY', []),
            patch('opentf.core.observer.MAX_RETENTION_WORKFLOW_COUNT', 2),
            patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            self.assertEqual(
                observer._get_cleanup_candidates(), ['weight_1', 'weight_2']
            )

    def test_get_cleanup_candidates_maxcount_rp(self):
        lastseen = {
            'weight_1': datetime.now(timezone.utc),
            'weight_2': datetime.now(timezone.utc),
            'weight_3': datetime.now(timezone.utc),
            'weight_4': datetime.now(timezone.utc),
        }
        workflows = {
            'weight_1': {'metadata': {'namespace': 'junk'}},
            'weight_2': {'metadata': {}},
            'weight_3': {'metadata': {'namespace': 'junk'}},
            'weight_4': {'metadata': {}},
        }
        rp = [
            {
                'name': 'notjunk',
                'scope': "workflow.namespace != 'junk'",
                'weight': 10,
                'retentionPeriod': '3d',
                'timedelta': timedelta(days=3),
            }
        ]
        with (
            patch('opentf.core.observer.RETENTIONPOLICY', rp),
            patch('opentf.core.observer.MAX_RETENTION_WORKFLOW_COUNT', 2),
            patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            self.assertEqual(
                observer._get_cleanup_candidates(), ['weight_1', 'weight_3']
            )

    def test_get_cleanup_candidates_maxcount_rp_expired(self):
        lastseen = {
            'weight_1': datetime.now(timezone.utc),
            'weight_2': datetime.now(timezone.utc),
            'weight_3': datetime.now(timezone.utc),
            'weight_4': datetime(year=2000, month=12, day=3, tzinfo=timezone.utc),
        }
        workflows = {
            'weight_1': {'metadata': {'namespace': 'junk'}},
            'weight_2': {'metadata': {}},
            'weight_3': {'metadata': {'namespace': 'junk'}},
            'weight_4': {'metadata': {}},
        }
        rp = [
            {
                'name': 'notjunk',
                'scope': "workflow.namespace != 'junk'",
                'weight': 10,
                'retentionPeriod': '3d',
                'timedelta': timedelta(days=3),
            }
        ]
        with (
            patch('opentf.core.observer.RETENTIONPOLICY', rp),
            patch('opentf.core.observer.MAX_RETENTION_WORKFLOW_COUNT', 2),
            patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            self.assertEqual(
                observer._get_cleanup_candidates(), ['weight_4', 'weight_1']
            )

    def test_get_cleanup_candidates_maxcount_rp_expired_oldestfirst(self):
        lastseen = {
            'weight_1': datetime.now(timezone.utc),
            'weight_2': datetime.now(timezone.utc),
            'weight_3': datetime.now(timezone.utc) - timedelta(minutes=5),
            'weight_4': datetime(year=2000, month=12, day=3, tzinfo=timezone.utc),
        }
        workflows = {
            'weight_1': {'metadata': {'namespace': 'junk'}},
            'weight_2': {'metadata': {}},
            'weight_3': {'metadata': {'namespace': 'junk'}},
            'weight_4': {'metadata': {}},
        }
        rp = []
        with (
            patch('opentf.core.observer.RETENTIONPOLICY', rp),
            patch('opentf.core.observer.MAX_RETENTION_WORKFLOW_COUNT', 2),
            patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            self.assertEqual(
                observer._get_cleanup_candidates(), ['weight_4', 'weight_3']
            )

    # list_namespaces

    def test_list_namespaces_ok(self):
        namespaces = ['foo', 'bAr']
        mock_lan = MagicMock(return_value=namespaces)
        with (
            patch('opentf.core.observer.make_status_response') as mock_msr,
            patch('opentf.core.observer.list_accessible_namespaces', mock_lan),
            patch('opentf.core.observer.request', MagicMock()),
            patch('opentf.core.observer.annotate_response') as mock_ar,
        ):
            observer.list_namespaces()
        mock_msr.assert_called_once()
        self.assertIn('details', mock_msr.call_args.kwargs)
        self.assertEqual(mock_msr.call_args.kwargs['details']['items'], namespaces)
        mock_ar.assert_called_once()
        self.assertEqual(mock_ar.call_args.kwargs['processed'], ['resource', 'verb'])

    def test_list_namespaces_notboth(self):
        namespaces = ['foo', 'bAr']
        mock_lan = MagicMock(return_value=namespaces)
        mock_request = MagicMock()
        mock_request.args.get = lambda x, **_: {'resource': 'workflows'}.get(x)
        with (
            patch('opentf.core.observer.make_status_response') as mock_msr,
            patch('opentf.core.observer.list_accessible_namespaces', mock_lan),
            patch('opentf.core.observer.request', mock_request),
        ):
            observer.list_namespaces()
            mock_msr.assert_called_once_with(
                'Invalid',
                'resource and verb must be both provided or not provided at all.',
            )

    def test_list_namespaces_badtype(self):
        namespaces = ['foo', 'bAr']
        mock_lan = MagicMock(return_value=namespaces)
        mock_request = MagicMock()
        mock_request.args.get = MagicMock(side_effect=ValueError())
        with (
            patch('opentf.core.observer.make_status_response') as mock_msr,
            patch('opentf.core.observer.list_accessible_namespaces', mock_lan),
            patch('opentf.core.observer.request', mock_request),
        ):
            observer.list_namespaces()
            mock_msr.assert_called_once_with(
                'Invalid', 'resource and verb, if provided, must be strings.'
            )

    # list_workflows

    def test_list_workflows_empty(self):
        mock_request = MagicMock()
        mock_request.args.get = MagicMock(return_value=None)
        mock_msr = MagicMock()
        mock_ar = MagicMock()
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            observer.list_workflows()
        mock_msr.assert_called_once()
        self.assertIn('details', mock_msr.call_args.kwargs)
        self.assertFalse(mock_msr.call_args.kwargs['details']['items'])
        mock_ar.assert_called_once()

    def test_list_workflows_manifests(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            'manifest' if x == 'expand' else None
        )
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_msr = MagicMock(return_value=mock_response)
        mock_ar = MagicMock()
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.can_use_namespace'),
            patch('opentf.core.observer.WORKFLOWS', {'wf': {'metadata': {}}}),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', {'wf': 'default'}),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            what = observer.list_workflows()
        mock_msr.assert_called_once()
        self.assertIn('details', mock_msr.call_args.kwargs)
        self.assertIn('wf', mock_msr.call_args.kwargs['details']['items'])
        self.assertIn('metadata', mock_msr.call_args.kwargs['details']['items']['wf'])
        mock_ar.assert_called_once_with(
            mock_response, processed=['expand', 'fieldSelector', 'labelSelector']
        )

    def test_list_workflows_selectors(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            'metadata' if x == 'fieldSelector' else None
        )
        mock_msr = MagicMock()
        mock_ar = MagicMock()
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.can_use_namespace'),
            patch('opentf.core.observer.WORKFLOWS', {'wf': {'metadata': {}}}),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', {'wf': 'default'}),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            observer.list_workflows()
        mock_msr.assert_called_once()
        self.assertIn('details', mock_msr.call_args.kwargs)
        self.assertIn('wf', mock_msr.call_args.kwargs['details']['items'])
        self.assertIsInstance(mock_msr.call_args.kwargs['details']['items'], list)
        mock_ar.assert_called_once()

    def test_list_workflows_nomanifests(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: None
        mock_msr = MagicMock()
        mock_ar = MagicMock()
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.can_use_namespace'),
            patch('opentf.core.observer.WORKFLOWS', {'wf': {'metadata': {}}}),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', {'wf': 'default'}),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            observer.list_workflows()
        mock_msr.assert_called_once()
        self.assertIn('details', mock_msr.call_args.kwargs)
        self.assertIn('wf', mock_msr.call_args.kwargs['details']['items'])
        self.assertIsInstance(mock_msr.call_args.kwargs['details']['items'], list)
        mock_ar.assert_called_once()
        self.assertEqual(
            {'expand', 'fieldSelector', 'labelSelector'},
            set(mock_ar.call_args.kwargs['processed']),
        )

    # _get_workflow_items

    def test_get_workflow_items_badfieldselector(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            'badselector>2' if x == 'fieldSelector' else None
        )
        with (
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.WORKFLOWS_STATUS', {'wf': []}),
        ):
            self.assertRaises(ValueError, observer._get_workflow_items, 'wf', 1, 10)

    def test_get_workflow_items_badlabelselector(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            'badselector>2' if x == 'labelSelector' else None
        )
        with (
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.WORKFLOWS_STATUS', {'wf': []}),
        ):
            self.assertRaises(ValueError, observer._get_workflow_items, 'wf', 1, 10)

    def test_get_workflow_items_selectors(self):
        mock_events = MagicMock()
        mock_events.get_filtered_item = MagicMock(return_value=('items', 432))
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            x if x in ('labelSelector', 'fieldSelector') else None
        )
        with (
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.WORKFLOWS_STATUS', {'wf': mock_events}),
        ):
            result = observer._get_workflow_items('wf', 1, 10)
        self.assertEqual(result[0], 'items')
        self.assertEqual(result[1], 44)
        self.assertEqual(
            result[2], '&fieldSelector=fieldSelector&labelSelector=labelSelector'
        )

    # _get_datasource_items

    def test_get_datasource_items_testcases_ok(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            x if x in ('labelSelector', 'fieldSelector') else None
        )
        mock_testcases = {
            'datasource_wf': {
                'testcases': {
                    'uuid1': {'testcase': 'data'},
                    'uuid2': {'another_testcase': 'other_data'},
                }
            }
        }
        with (
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.LIVE_DATASOURCE_CACHES', mock_testcases),
            patch('opentf.core.observer.in_scope'),
            patch('opentf.core.observer.match_selectors'),
        ):
            result = observer._get_datasource_items('datasource_wf', 'testcases', 1, 1)
        self.assertEqual(1, len(result[0]))
        self.assertEqual({'uuid': 'uuid1', 'testcase': 'data'}, result[0][0])
        self.assertEqual(2, result[1])
        self.assertIn('fieldSelector', result[2])

    def test_get_datasource_items_jobs_ok(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            x if x in ('labelSelector', 'fieldSelector') else None
        )
        mock_jobs = {
            'datasource_wf': {
                'jobs': {
                    'job1': {
                        'status': {
                            'testCaseCount': 0,
                            'testCaseStatusSummary': {'success': 0},
                        }
                    },
                },
                'testcases': {
                    'uuid1': {
                        'metadata': {'job_id': 'job1'},
                        'test': {'outcome': 'success'},
                    }
                },
            }
        }
        with (
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.LIVE_DATASOURCE_CACHES', mock_jobs),
            patch('opentf.core.observer.in_scope'),
            patch('opentf.core.observer.match_selectors'),
        ):
            result = observer._get_datasource_items('datasource_wf', 'jobs', 1, 1)
        self.assertEqual(
            {
                'uuid': 'job1',
                'status': {'testCaseCount': 1, 'testCaseStatusSummary': {'success': 1}},
            },
            result[0][0],
        )

    def test_get_datasource_items_tags_ok(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            x if x in ('labelSelector', 'fieldSelector') else None
        )
        mock_tags = {
            'datasource_wf': {
                'jobs': {'job_1': {}},
                'tags': {
                    'tag1': {
                        'status': {
                            'testCaseCount': 0,
                            'testCaseStatusSummary': {'success': 0},
                        }
                    },
                },
                'testcases': {
                    'uuid1': {
                        'metadata': {'job_id': 'job1'},
                        'test': {'outcome': 'success', 'runs-on': ['tag1']},
                    }
                },
            }
        }
        with (
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.LIVE_DATASOURCE_CACHES', mock_tags),
            patch('opentf.core.observer.in_scope'),
            patch('opentf.core.observer.match_selectors'),
        ):
            result = observer._get_datasource_items('datasource_wf', 'tags', 1, 1)
        self.assertEqual(
            {
                'uuid': 'tag1',
                'status': {'testCaseCount': 1, 'testCaseStatusSummary': {'success': 1}},
            },
            result[0][0],
        )

    def test_get_datasource_items_cachenotready(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            x if x in ('labelSelector', 'fieldSelector') else None
        )
        mock_tags = {}
        with (
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.LIVE_DATASOURCE_CACHES', mock_tags),
            patch('opentf.core.observer.in_scope'),
            patch('opentf.core.observer.match_selectors'),
        ):
            self.assertRaises(
                observer.CacheNotReady,
                observer._get_datasource_items,
                'datasource_wf',
                'tags',
                1,
                1,
            )

    def test_get_datasource_items_scopeerror(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            x if x in ('labelSelector', 'fieldSelector') else None
        )
        mock_tags = {'datasource_wf': {'tags': {'uuid1': {}}, 'jobs': {'uuid2': {}}}}
        with (
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.LIVE_DATASOURCE_CACHES', mock_tags),
            patch('opentf.core.observer.in_scope', side_effect=Exception('Boom')),
            patch('opentf.core.observer.match_selectors'),
        ):
            self.assertRaises(
                observer.DataSourceScopeError,
                observer._get_datasource_items,
                'datasource_wf',
                'tags',
                1,
                1,
            )

    # _make_paginated_response

    def test_make_paginated_response_ko_ve(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            1 if x in ('page', 'per_page') else None
        )
        mock_generator = MagicMock(side_effect=ValueError('boom'))
        with patch('opentf.core.observer.request', mock_request):
            self.assertRaisesRegex(
                ValueError,
                'boom',
                observer._make_paginated_response,
                mock_generator,
                ['smbd'],
                uri='foo',
                msg='bar',
                details=None,
            )

    # get_workflow_logs

    def test_get_workflow_logs_unknown(self):
        mock_msr = MagicMock()
        with patch('opentf.core.observer.make_status_response', mock_msr):
            observer.get_workflow_logs('abc')
        mock_msr.assert_called_once()
        self.assertEqual(mock_msr.call_args.args[0], 'NotFound')

    def test_get_workflow_logs_ok(self):
        mock_sf = MagicMock()
        mock_cwn = MagicMock(return_value=None)
        workflow_id = 'abc'
        wfs_statuses = {workflow_id: 'DONE'}
        with (
            patch('opentf.core.observer.send_file', mock_sf),
            patch('opentf.core.observer._check_workflow_namespace', mock_cwn),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', wfs_statuses),
        ):
            observer.get_workflow_logs('abc')
        mock_sf.assert_called_once()

    # get_workflow_status

    def test_get_workflow_status(self):
        mock_msr = MagicMock()
        with patch('opentf.core.observer.make_status_response', mock_msr):
            observer.get_workflow_status('abc')
        mock_msr.assert_called_once()
        self.assertEqual(mock_msr.call_args.args[0], 'NotFound')

    def test_get_workflow_status_cannotusens(self):
        mock_msr = MagicMock()
        mock_cun = MagicMock(return_value=False)
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.can_use_namespace', mock_cun),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', {'cafebabe': 'foo'}),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'cafebabe': 'RUNNING'}),
        ):
            observer.get_workflow_status('cafebabe')
            mock_msr.assert_called_once_with(
                'Forbidden', 'Token not allowed to access workflows in namespace foo.'
            )

    def test_get_workflow_status_canusens(self):
        mock_ar = MagicMock()
        mock_msr = MagicMock()
        mock_cun = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            1 if x in ('page', 'per_page') else None
        )
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.can_use_namespace', mock_cun),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', {'cafebabe': 'foo'}),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'cafebabe': 'RUNNING'}),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            observer.get_workflow_status('cafebabe')
        mock_msr.assert_called_once_with(
            'OK',
            message='Workflow in progress',
            details={'status': 'RUNNING', 'items': []},
        )
        mock_ar.assert_called_once()

    def test_get_workflow_status_ns_pages(self):
        mock_ar = MagicMock()
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_msr = MagicMock(return_value=mock_response)
        mock_cun = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.args.get = lambda x, *_, **__: (
            2 if x in ('page', 'per_page') else None
        )
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.can_use_namespace', mock_cun),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', {'cafebabe': 'foo'}),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'cafebabe': 'RUNNING'}),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            resp = observer.get_workflow_status('cafebabe')
        mock_msr.assert_called_once_with(
            'OK',
            message='Workflow in progress',
            details={'status': 'RUNNING', 'items': []},
        )
        links = mock_ar.call_args.kwargs['links']
        self.assertTrue(any(link.endswith('rel="prev"') for link in links))

    def test_get_workflow_status_ns_pages_2(self):
        mock_ar = MagicMock()
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_msr = MagicMock(return_value=mock_response)
        mock_cun = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.args.get = lambda x, **_: (
            None if x in ('fieldSelector', 'labelSelector') else 2 if x == 'page' else 1
        )
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.can_use_namespace', mock_cun),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', {'cafebabe': 'foo'}),
            patch(
                'opentf.core.observer.WORKFLOWS_STATUSES',
                {'cafebabe': 'RUNNING'},
            ),
            patch(
                'opentf.core.observer.WORKFLOWS_STATUS',
                {'cafebabe': ['dummy_0', 'dummy_1', 'dummy_2', 'dummy_3']},
            ),
            patch('opentf.core.observer.request', mock_request),
            patch(
                'os.environ',
                {'OPENTF_OBSERVER_BASE_URL': 'yada', 'OPENTF_BASE_URL': 'yidi'},
            ),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            resp = observer.get_workflow_status('cafebabe')
        mock_msr.assert_called_once_with(
            'OK',
            message='Workflow in progress',
            details={'status': 'RUNNING', 'items': ['dummy_1']},
        )
        mock_ar.assert_called_once()
        links = mock_ar.call_args.kwargs['links']
        self.assertIn(
            '<yada/workflows/cafebabe/status?page=1&per_page=1>; rel="prev"', links
        )

    def test_get_workflow_status_ko_ve(self):
        mock_mpr = MagicMock(side_effect=ValueError('Well...'))
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.args.get = MagicMock(return_value=False)
        with (
            patch(
                'opentf.core.observer._check_workflow_namespace',
                MagicMock(return_value=None),
            ),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'wf': 'DONE'}),
            patch('opentf.core.observer._make_paginated_response', mock_mpr),
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.request', mock_request),
        ):
            observer.get_workflow_status('wf')
        mock_msr.assert_called_once()
        self.assertEqual('Well...', mock_msr.call_args[0][1])

    def test_bs_get_filtered_item_found_one(self):
        mock_td = MagicMock()
        mock_tf = MagicMock()
        mock_tf.TemporaryDirectory = MagicMock(return_value=mock_td)
        with (
            patch('opentf.core.observer.tempfile', mock_tf),
            patch('builtins.open', mock_open()),
        ):
            bs = observer.BackingStoreItems()
            bs._read = lambda n: WORKFLOW_STATUS_FOR_FILTERS['workflow'][n]
            bs._itemcount = len(WORKFLOW_STATUS_FOR_FILTERS['workflow'])
            items, items_count = bs.get_filtered_item(
                slice(0, bs._itemcount + 1),
                fieldselector='kind=ExecutionResult',
                labelselector=None,
            )
        self.assertEqual(items_count, 2)
        self.assertIn('kind', items[0])
        self.assertEqual(items[0]['kind'], 'ExecutionResult')
        mock_tf.TemporaryDirectory.assert_called_once()

    def test_bs_get_filtered_item_found_second(self):
        mock_td = MagicMock()
        mock_tf = MagicMock()
        mock_tf.TemporaryDirectory = MagicMock(return_value=mock_td)
        with (
            patch('opentf.core.observer.tempfile', mock_tf),
            patch('builtins.open', mock_open()),
        ):
            bs = observer.BackingStoreItems()
            bs._read = lambda n: WORKFLOW_STATUS_FOR_FILTERS['workflow'][n]
            bs._itemcount = len(WORKFLOW_STATUS_FOR_FILTERS['workflow'])
            items, items_count = bs.get_filtered_item(
                slice(0, bs._itemcount + 1),
                fieldselector='kind=ProviderCommand',
                labelselector=None,
            )
        self.assertEqual(items_count, 3)
        self.assertEqual(len(items), 2)
        self.assertIn('kind', items[0])
        self.assertEqual(items[0]['kind'], 'ProviderCommand')
        self.assertEqual(items[1]['kind'], 'ProviderCommand')
        mock_tf.TemporaryDirectory.assert_called_once()

    def test_bs_get_filtered_item_found_second_paginated(self):
        mock_td = MagicMock()
        mock_tf = MagicMock()
        mock_tf.TemporaryDirectory = MagicMock(return_value=mock_td)
        with (
            patch('opentf.core.observer.tempfile', mock_tf),
            patch('builtins.open', mock_open()),
        ):
            bs = observer.BackingStoreItems()
            bs._read = lambda n: WORKFLOW_STATUS_FOR_FILTERS['workflow'][n]
            bs._itemcount = len(WORKFLOW_STATUS_FOR_FILTERS['workflow'])
            items, items_count = bs.get_filtered_item(
                slice(1, 2), fieldselector='kind=ProviderCommand', labelselector=None
            )
        self.assertEqual(items_count, 3)
        self.assertEqual(len(items), 1)
        self.assertIn('kind', items[0])
        self.assertEqual(items[0]['kind'], 'ProviderCommand')
        self.assertEqual(items[0]['something'], 'nothingness1')
        mock_tf.TemporaryDirectory.assert_called_once()

    def test_bs_get_filtered_item_found_none(self):
        mock_td = MagicMock()
        mock_tf = MagicMock()
        mock_tf.TemporaryDirectory = MagicMock(return_value=mock_td)
        with (
            patch('opentf.core.observer.tempfile', mock_tf),
            patch('builtins.open', mock_open()),
        ):
            bs = observer.BackingStoreItems()
            bs._read = lambda n: WORKFLOW_STATUS_FOR_FILTERS['workflow'][n]
            bs._itemcount = len(WORKFLOW_STATUS_FOR_FILTERS['workflow'])
            items, items_count = bs.get_filtered_item(
                slice(0, bs._itemcount + 1),
                fieldselector='kind=Nothing',
                labelselector=None,
            )
        self.assertEqual(items_count, 1)
        self.assertFalse(items)
        mock_tf.TemporaryDirectory.assert_called_once()

    # refresh_channels

    def test_refresh_channels_providerisknown(self):
        observer.refresh_channels('fOo', [])
        self.assertIn('fOo', observer.CHANNELS)

    def test_refresh_channels_provideridisknown(self):
        observer.refresh_channels('bAr', [{'metadata': {}, 'status': {}}])
        self.assertIn('bAr', observer.CHANNELS)
        self.assertTrue(observer.CHANNELS['bAr'])
        self.assertEqual(
            'bAr', observer.CHANNELS['bAr'][0]['metadata']['channelhandler_id']
        )

    def test_process_inbox_invalid(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: {}
        mock_msr = MagicMock()
        with (
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.make_status_response', mock_msr),
        ):
            observer.process_inbox()

    def test_process_inbox_notification(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'Notification',
            'metadata': {'name': 'foo', 'workflow_id': 'n/a'},
            'spec': {'channel.handler': {'channelhandler_id': 'myid', 'channels': []}},
        }
        mock_msr = MagicMock()
        mock_refresh = MagicMock()
        with (
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.refresh_channels', mock_refresh),
        ):
            observer.process_inbox()
        mock_refresh.assert_called_once()

    def test_workflows_status_is_idle_if_no_workflow_processed(self):
        mock_msr = MagicMock()
        with patch('opentf.core.observer.make_status_response', mock_msr):
            observer.get_orchestrator_status()
        mock_msr.assert_called_once()
        self.assertEqual(mock_msr.call_args.kwargs['details']['status'], 'IDLE')

    def test_workflow_workers_notfound(self):
        mock_msr = MagicMock()
        with patch('opentf.core.observer.make_status_response', mock_msr):
            observer.get_workflow_workers('cafebabe')
            mock_msr.assert_called_once_with('NotFound', 'Workflow cafebabe not found.')

    def test_workflow_workers_noaccess(self):
        mock_msr = MagicMock()
        mock_cun = MagicMock(return_value=False)
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.can_use_namespace', mock_cun),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', {'cafebabe': 'foo'}),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', {'cafebabe': {}}),
        ):
            observer.get_workflow_workers('cafebabe')
            mock_msr.assert_called_once_with(
                'Forbidden', 'Token not allowed to access workflows in namespace foo.'
            )

    # get_datasource

    def test_get_datasource_ok(self):
        mock_mpr = MagicMock()
        with (
            patch(
                'opentf.core.observer._check_workflow_namespace',
                MagicMock(return_value=None),
            ),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'wf': 'DONE'}),
            patch('opentf.core.observer._make_paginated_response', mock_mpr),
        ):
            observer.get_datasource('wf', 'testcases')
        mock_mpr.assert_called_once()
        self.assertEqual(
            '/workflows/wf/datasources/testcases', mock_mpr.call_args[1]['uri']
        )

    def test_get_datasource_ok_ongoing(self):
        mock_mpr = MagicMock()
        with (
            patch(
                'opentf.core.observer._check_workflow_namespace',
                MagicMock(return_value=None),
            ),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'wf': 'RUNNING'}),
            patch('opentf.core.observer._make_paginated_response', mock_mpr),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', {'wf': {}}),
        ):
            observer.get_datasource('wf', 'testcases')
        mock_mpr.assert_called_once()
        self.assertEqual('ONGOING', mock_mpr.call_args[1]['details']['status'])
        self.assertEqual(0, mock_mpr.call_args[1]['details']['workers_count'])

    def test_get_datasource_not_handled_to_handled(self):
        mock_mpr = MagicMock()
        mock_att = {'wf': {'att1': {'handled': False, 'timestamp': 1}}}
        with (
            patch(
                'opentf.core.observer._check_workflow_namespace',
                MagicMock(return_value=None),
            ),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'wf': 'DONE'}),
            patch('opentf.core.observer._make_paginated_response', mock_mpr),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', {'wf': {}}),
            patch('opentf.core.observer.ATTACHMENTS_NOTIFICATIONS', mock_att),
            patch('time.time', return_value=22),
        ):
            observer.get_datasource('wf', 'testcases')
        mock_mpr.assert_called_once()
        self.assertEqual('COMPLETE', mock_mpr.call_args[1]['details']['status'])
        self.assertTrue(mock_mpr.call_args[1]['details']['handled'])

    def test_get_datasource_ko_wf_ns(self):
        with patch(
            'opentf.core.observer._check_workflow_namespace',
            MagicMock(return_value='Horrible error message.'),
        ):
            self.assertEqual(
                'Horrible error message.', observer.get_datasource('wf', 'dk')
            )

    def test_get_datasource_kind_ko(self):
        mock_msr = MagicMock()
        with (
            patch(
                'opentf.core.observer._check_workflow_namespace',
                MagicMock(return_value=None),
            ),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'wf': 'DONE'}),
            patch('opentf.core.observer.make_status_response', mock_msr),
        ):
            observer.get_datasource('wf', 'whatever')
        mock_msr.assert_called_once()
        self.assertIn(
            'Datasource kind `whatever` does not exist', mock_msr.call_args[0][1]
        )

    def test_get_datasource_ko_ve(self):
        mock_mpr = MagicMock(side_effect=ValueError('Bad value.'))
        mock_msr = MagicMock()
        with (
            patch(
                'opentf.core.observer._check_workflow_namespace',
                MagicMock(return_value=None),
            ),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'wf': 'DONE'}),
            patch('opentf.core.observer._make_paginated_response', mock_mpr),
            patch('opentf.core.observer.make_status_response', mock_msr),
        ):
            observer.get_datasource('wf', 'testcases')
        mock_mpr.assert_called_once()
        mock_msr.assert_called_once()
        self.assertEqual('Bad value.', mock_msr.call_args[0][1])

    def test_get_datasource_cacheexception_202(self):
        with (
            patch(
                'opentf.core.observer._check_workflow_namespace',
                MagicMock(return_value=None),
            ),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'wf': 'DONE'}),
            patch('opentf.core.observer.make_status_response') as mock_msr,
            patch(
                'opentf.core.observer._make_paginated_response',
                side_effect=observer.CacheNotReady('Bang'),
            ),
        ):
            observer.get_datasource('wf', 'testcases')
        mock_msr.assert_called_once_with(
            'Accepted', 'Bang', details={'status': 'COMPLETE'}
        )

    def test_get_datasource_scope_error_422(self):
        mock_mpr = MagicMock(side_effect=observer.DataSourceScopeError('Bad'))
        with (
            patch(
                'opentf.core.observer._check_workflow_namespace',
                MagicMock(return_value=None),
            ),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'wf': 'DONE'}),
            patch('opentf.core.observer._make_paginated_response', mock_mpr),
            patch('opentf.core.observer.make_status_response') as mock_msr,
        ):
            observer.get_datasource('wf', 'testcases')
        mock_msr.assert_called_once_with(
            'Invalid',
            'Bad',
            details={'scope_error': 'Scope error occured.'},
            silent=True,
        )

    def test_get_datasource_data_error_422(self):
        mock_mpr = MagicMock(side_effect=observer.DataSourceDataError('Bang'))
        with (
            patch(
                'opentf.core.observer._check_workflow_namespace',
                MagicMock(return_value=None),
            ),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', {'wf': 'DONE'}),
            patch('opentf.core.observer._make_paginated_response', mock_mpr),
            patch('opentf.core.observer.make_status_response') as mock_msr,
        ):
            observer.get_datasource('wf', 'testcases')
        mock_msr.assert_called_once_with(
            'Invalid',
            'Bang',
            details={'data_error': 'Data error occured.'},
            silent=True,
        )

    # get_version

    def test_get_version_ok(self):
        mock_gb = MagicMock(return_value={'image': 'foo'})
        mock_msr = MagicMock()
        with (
            patch('opentf.core.observer._get_bom', mock_gb),
            patch('opentf.core.observer.make_status_response', mock_msr),
        ):
            observer.get_version()
        mock_gb.assert_called_once()
        mock_msr.assert_called_once_with(
            'OK', 'BOM', details={'items': {'image': 'foo'}}
        )

    def test_initialize_bom_ok(self):
        mock_msr = MagicMock()
        mock_json = mock_open(read_data='{"images": [{"oNe": "tWo"}]}')
        with (
            patch('os.path.isfile'),
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('builtins.open', mock_json),
            patch('opentf.core.observer.BOM', {}),
        ):
            observer.get_version()
        mock_msr.assert_called_once()
        mock_json.assert_called_once()
        self.assertEqual(
            'tWo', mock_msr.call_args[1]['details']['items']['images'][0]['oNe']
        )

    def test_initialize_bom_json_load_ko(self):
        mock_msr = MagicMock()
        mock_json = mock_open(read_data=';{;;;')
        with (
            patch('os.path.isfile'),
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('builtins.open', mock_json),
            patch('opentf.core.observer.BOM', {}),
        ):
            observer.get_version()
        mock_msr.assert_called_once()
        mock_json.assert_called_once()
        self.assertIn(
            'Could not read \'/app/BOM.json\'',
            mock_msr.call_args[1]['details']['items']['error'],
        )

    def test_initialize_bom_no_json_file(self):
        mock_msr = MagicMock()
        mock_empty = MagicMock(return_value='')
        with (
            patch('os.path.isfile', mock_empty),
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.BOM', {}),
        ):
            observer.get_version()
        mock_empty.assert_called_once()
        mock_msr.assert_called_once()
        self.assertIn(
            'Could not find \'/app/BOM.json\'',
            mock_msr.call_args[1]['details']['items']['error'],
        )

    def test_initialize_bom_not_a_dict(self):
        mock_msr = MagicMock()
        mock_json = mock_open(read_data='["A","B","c"]')
        with (
            patch('os.path.isfile'),
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('builtins.open', mock_json),
            patch('opentf.core.observer.BOM', {}),
        ):
            observer.get_version()
        mock_msr.assert_called_once()
        mock_json.assert_called_once()
        self.assertIn(
            '\'/app/BOM.json\' is not a dictionary',
            mock_msr.call_args[1]['details']['items']['error'],
        )

    def test_list_channelhandlers_empty(self):
        mock_msr = MagicMock()
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.CHANNELS', CHANNELS_EMPTY),
        ):
            observer.list_channelhandlers()
            mock_msr.assert_called_once_with(
                'OK', 'Known channel handlers', details={'items': []}
            )

    def test_list_channelhandlers_one_provider(self):
        mock_msr = MagicMock()
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.CHANNELS', CHANNELS_1_EMPTY),
        ):
            observer.list_channelhandlers()
            mock_msr.assert_called_once_with(
                'OK', 'Known channel handlers', details={'items': ['provider_1']}
            )

    def test_list_channels_empty(self):
        mock_msr = MagicMock()
        mock_ar = MagicMock()
        mock_request = MagicMock()
        mock_request.args = MagicMock()
        mock_request.args.get = MagicMock(return_value=None)
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.CHANNELS', CHANNELS_EMPTY),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            observer.list_channels()
        mock_msr.assert_called_once_with('OK', 'Known channels', details={'items': []})
        mock_ar.assert_called_once()
        self.assertEqual(
            mock_ar.call_args.kwargs['processed'], ['fieldSelector', 'labelSelector']
        )

    def test_list_channels_empty_1(self):
        mock_ar = MagicMock()
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.args = MagicMock()
        mock_request.args.get = MagicMock(return_value=None)
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.CHANNELS', CHANNELS_1_EMPTY),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            observer.list_channels()
        mock_msr.assert_called_once_with('OK', 'Known channels', details={'items': []})
        mock_ar.assert_called_once()

    def test_list_channels_empty_2(self):
        mock_ar = MagicMock()
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.args = MagicMock()
        mock_request.args.get = MagicMock(return_value=None)
        with (
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.CHANNELS', CHANNELS_2_EMPTY),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            observer.list_channels()
        mock_msr.assert_called_once_with('OK', 'Known channels', details={'items': []})
        mock_ar.assert_called_once()

    def test_list_channels_filtered(self):
        mock_ar = MagicMock()
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_msr = MagicMock(return_value=mock_response)
        mock_cun = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.args.get = lambda x, **_: (
            'something=anything' if x == 'fieldSelector' else None
        )
        with (
            patch('opentf.core.observer.CHANNELS', CHANNELS_3_FOR_FILTERS),
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.can_use_namespace', mock_cun),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            observer.list_channels()
        mock_msr.assert_called_once()
        self.assertEqual(
            'anything', mock_msr.call_args[1]['details']['items'][0]['something']
        )
        self.assertNotEqual(
            'nothing', mock_msr.call_args[1]['details']['items'][0]['something']
        )
        mock_ar.assert_called_once()

    def test_list_channels_filtered_rbac_single(self):
        mock_ar = MagicMock()
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_msr = MagicMock(return_value=mock_response)
        mock_cun = lambda ns, **_: ns == 'foo'
        mock_request = MagicMock()
        mock_request.args.get = lambda x, **_: (
            'something=nothing' if x == 'fieldSelector' else None
        )
        mock_lan = MagicMock(return_value=['foo'])
        with (
            patch('opentf.core.observer.CHANNELS', CHANNELS_4_FOR_FILTERS),
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.can_use_namespace', mock_cun),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.annotate_response', mock_ar),
            patch('opentf.core.observer.list_accessible_namespaces', mock_lan),
        ):
            observer.list_channels()
        mock_msr.assert_called_once()
        items = mock_msr.call_args[1]['details']['items']
        for item in items:
            self.assertEqual(item['something'], 'nothing')
            self.assertEqual(item['metadata']['namespaces'], 'foo')
        mock_ar.assert_called_once()

    def test_list_channels_filtered_rbac_multi(self):
        accessible_namespaces = ['foo', 'bar']
        mock_ar = MagicMock()
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_msr = MagicMock(return_value=mock_response)
        mock_cun = lambda ns, **_: ns in accessible_namespaces
        mock_request = MagicMock()
        mock_request.args.get = lambda x, **_: (
            'something=nothing' if x == 'fieldSelector' else None
        )
        mock_lan = MagicMock(return_value=accessible_namespaces)
        with (
            patch('opentf.core.observer.CHANNELS', CHANNELS_5_FOR_FILTERS),
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.can_use_namespace', mock_cun),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.annotate_response', mock_ar),
            patch('opentf.core.observer.list_accessible_namespaces', mock_lan),
        ):
            observer.list_channels()
        mock_msr.assert_called_once()
        items = mock_msr.call_args[1]['details']['items']
        for item in items:
            self.assertEqual(item['something'], 'nothing')
            self.assertIn(
                item['metadata']['namespaces'], ('foo', 'bar', 'foo,bar', 'bar,foo')
            )
        mock_ar.assert_called_once()

    def test_list_channels_badfiltered(self):
        mock_ar = MagicMock()
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_msr = MagicMock(return_value=mock_response)
        mock_cun = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.args.get = lambda x, **_: (
            'something=anything>2' if x == 'fieldSelector' else None
        )
        with (
            patch('opentf.core.observer.CHANNELS', CHANNELS_3_FOR_FILTERS),
            patch('opentf.core.observer.make_status_response', mock_msr),
            patch('opentf.core.observer.can_use_namespace', mock_cun),
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.annotate_response', mock_ar),
        ):
            observer.list_channels()
        mock_msr.assert_called_once_with(
            'Invalid', 'Invalid expression something=anything>2.'
        )
        mock_ar.assert_not_called()

    # _maybe_set_proxyfix

    def test_maybe_set_proxyfix(self):
        mock_app = MagicMock()
        with patch('opentf.core.observer.app', mock_app):
            self.assertRaises(SystemExit, observer._maybe_set_proxyfix, '')

    def test_maybe_set_proxyfix_auto(self):
        mock_app = MagicMock()
        mock_app.wsgi_app = 'wSgI_aPP'
        mock_proxyfix = MagicMock()
        with (
            patch('opentf.core.observer.ProxyFix', mock_proxyfix),
            patch('opentf.core.observer.app', mock_app),
        ):
            observer._maybe_set_proxyfix('auto')
        mock_proxyfix.assert_called_once_with('wSgI_aPP')

    def test_maybe_set_proxyfix_three(self):
        mock_app = MagicMock()
        mock_app.wsgi_app = 'wSgI_aPP'
        mock_proxyfix = MagicMock()
        with (
            patch('opentf.core.observer.ProxyFix', mock_proxyfix),
            patch('opentf.core.observer.app', mock_app),
        ):
            observer._maybe_set_proxyfix('1,2,3')
        mock_proxyfix.assert_called_once_with('wSgI_aPP', 1, 2, 3)

    def test_maybe_set_proxyfix_notint(self):
        mock_app = MagicMock()
        mock_proxyfix = MagicMock()
        with (
            patch('opentf.core.observer.ProxyFix', mock_proxyfix),
            patch('opentf.core.observer.app', mock_app),
        ):
            self.assertRaises(SystemExit, observer._maybe_set_proxyfix, '1,a,3,4,5')

    # _record_workflow_event

    def test_record_workflow_event_workflow(self):
        statuses = {}
        workers = {}
        namespaces = {}
        workflows = {}
        workflow_id = 'wOrKflow_iD'
        with (
            patch('opentf.core.observer.WORKFLOWS_STATUSES', statuses),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', workers),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', namespaces),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            observer._record_workflow_event(
                (
                    observer.WORKFLOW,
                    workflow_id,
                    {
                        'kind': observer.WORKFLOW,
                        'metadata': {'name': 'woRkfpace', 'workflow_id': workflow_id},
                    },
                )
            )
        self.assertIn(workflow_id, statuses)
        self.assertIn(workflow_id, workers)
        self.assertIn(workflow_id, namespaces)
        self.assertIn(workflow_id, workflows)
        self.assertIn('status', workflows[workflow_id])
        self.assertEqual(workflows[workflow_id]['status']['phase'], 'RUNNING')

    def test_record_workflow_event_workflow_keep_metadata(self):
        statuses = {}
        workers = {}
        namespaces = {}
        workflow_id = 'wOrKflow_iD'
        workflows = {workflow_id: {'metadata': {'yada': 'yADa'}}}
        with (
            patch('opentf.core.observer.WORKFLOWS_STATUSES', statuses),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', workers),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', namespaces),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            observer._record_workflow_event(
                (
                    observer.WORKFLOW,
                    workflow_id,
                    {
                        'kind': observer.WORKFLOW,
                        'metadata': {'name': 'woRkfpace', 'workflow_id': workflow_id},
                    },
                )
            )
        self.assertIn(workflow_id, statuses)
        self.assertIn(workflow_id, workers)
        self.assertIn(workflow_id, namespaces)
        self.assertIn(workflow_id, workflows)
        self.assertIn('yada', workflows[workflow_id]['metadata'])
        self.assertIn('name', workflows[workflow_id]['metadata'])

    def test_record_workflow_event_workflowcanceled_keeps_metadata(self):
        statuses = {}
        workers = {}
        namespaces = {}
        lastseen = {}
        workflow_id = 'wOrKflow_iD'
        workflows = {
            workflow_id: {'metadata': {'yada': 'yADa'}, 'status': {'phase': 'RUNNING'}}
        }
        with (
            patch('opentf.core.observer.WORKFLOWS_STATUSES', statuses),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', workers),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', namespaces),
            patch('opentf.core.observer.WORKFLOWS', workflows),
            patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen),
        ):
            observer._record_workflow_event(
                (
                    observer.WORKFLOWCANCELED,
                    workflow_id,
                    {
                        'kind': observer.WORKFLOWCANCELED,
                        'metadata': {'name': 'woRkfpace', 'workflow_id': workflow_id},
                    },
                )
            )
        self.assertIn(workflow_id, statuses)
        self.assertIn(workflow_id, workers)
        self.assertIn(workflow_id, namespaces)
        self.assertIn(workflow_id, workflows)
        self.assertIn('status', workflows[workflow_id]['metadata'])
        self.assertEqual(workflows[workflow_id]['metadata']['status'], 'failure')
        self.assertIn('yada', workflows[workflow_id]['metadata'])
        self.assertIn(workflow_id, lastseen)
        self.assertEqual(workflows[workflow_id]['status']['phase'], 'FAILED')

    def test_record_workflow_event_outofband(self):
        statuses = {}
        workers = {}
        namespaces = {}
        workflows = {}
        workflow_id = 'wOrKflow_iD'
        with (
            patch('opentf.core.observer.WORKFLOWS_STATUSES', statuses),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', workers),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', namespaces),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            observer._record_workflow_event(
                (
                    observer.EXECUTIONCOMMAND,
                    workflow_id,
                    {
                        'kind': observer.EXECUTIONCOMMAND,
                        'metadata': {'name': 'woRkfpace', 'workflow_id': workflow_id},
                    },
                )
            )
        self.assertIn(workflow_id, statuses)
        self.assertIn(workflow_id, workers)
        self.assertIn(workflow_id, namespaces)
        self.assertNotIn(workflow_id, workflows)

    def test_record_workflow_event_worker_setup(self):
        statuses = {}
        workers = {}
        namespaces = {}
        workflow_id = 'wOrKflow_iD'
        with (
            patch('opentf.core.observer.WORKFLOWS_STATUSES', statuses),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', workers),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', namespaces),
        ):
            observer._record_workflow_event(
                (
                    observer.NOTIFICATION,
                    workflow_id,
                    {
                        'kind': observer.NOTIFICATION,
                        'metadata': {'name': 'woRkfpace', 'workflow_id': workflow_id},
                        'spec': {'worker': {'worker_id': 'abc', 'status': 'setup'}},
                    },
                )
            )
        self.assertIn(workflow_id, statuses)
        self.assertIn(workflow_id, workers)
        self.assertIn(workflow_id, namespaces)
        self.assertTrue(workers[workflow_id])

    def test_record_workflow_event_worker_teardown(self):
        statuses = {}
        workers = {}
        namespaces = {}
        workflows = {}
        workflow_id = 'wOrKflow_iD'
        with (
            patch('opentf.core.observer.WORKFLOWS_STATUSES', statuses),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', workers),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', namespaces),
            patch('opentf.core.observer.WORKFLOWS', workflows),
        ):
            observer._record_workflow_event(
                (
                    observer.NOTIFICATION,
                    workflow_id,
                    {
                        'kind': observer.NOTIFICATION,
                        'metadata': {'name': 'woRkfpace', 'workflow_id': workflow_id},
                        'spec': {'worker': {'worker_id': 'abc', 'status': 'teardown'}},
                    },
                )
            )
        self.assertIn(workflow_id, statuses)
        self.assertIn(workflow_id, workers)
        self.assertIn(workflow_id, namespaces)
        self.assertFalse(workers[workflow_id])
        self.assertNotIn(workflow_id, workflows)

    def test_record_workflow_event_notification(self):
        statuses = {}
        workers = {}
        namespaces = {}
        workflows = {}
        with (
            patch('opentf.core.observer.WORKFLOWS_STATUSES', statuses),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', workers),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', namespaces),
            patch('opentf.core.observer.WORKFLOWS', workflows),
            patch('opentf.core.observer._maybe_add_event_to_live_cache') as mock_cache,
        ):
            observer._record_workflow_event(
                (observer.NOTIFICATION, 'wOrKflow_iD', {'kind': 'Notification'})
            )
        mock_cache.assert_called_once_with(
            {'kind': 'Notification', 'metadata': {'namespace': ''}}
        )

    def test_record_workflow_event_with_execlog_labels(self):
        statuses = {}
        workers = {}
        namespaces = {}
        workflows = {}
        workflow_id = 'wOrKflow_iD'
        bsi = MagicMock()
        store = observer.BackingStore()
        store._store = observer.defaultdict(bsi)
        mock_iws = MagicMock()
        with (
            patch('opentf.core.observer.WORKFLOWS_STATUSES', statuses),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', workers),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', namespaces),
            patch('opentf.core.observer.WORKFLOWS', workflows),
            patch('opentf.core.observer.WORKFLOWS_STATUS', store),
            patch('opentf.core.observer._initialize_workflow_state', mock_iws),
        ):
            observer._record_workflow_event(
                (
                    observer.WORKFLOW,
                    workflow_id,
                    {
                        'kind': observer.WORKFLOW,
                        'metadata': {
                            'name': 'woRkfpace',
                            'workflow_id': workflow_id,
                            'labels': {
                                'live-executionlog/step-depth': '0',
                                'live-executionlog/job-depth': '0',
                                'live-executionlog/max-command-length': '10',
                            },
                        },
                    },
                )
            )
        self.assertIn(workflow_id, statuses)
        self.assertIn(workflow_id, workers)
        self.assertIn(workflow_id, namespaces)
        mock_iws.assert_called_once()

    def test_initialize_workflow_state(self):
        namespaces = {}
        workflows = {}
        workflow_id = 'wOrKflow_iD'
        event = {
            'kind': observer.WORKFLOW,
            'metadata': {
                'name': 'woRkfpace',
                'workflow_id': workflow_id,
                'labels': {
                    'live-executionlog/step-depth': '0',
                    'live-executionlog/job-depth': '0',
                    'live-executionlog/max-command-length': '10',
                },
            },
        }
        mock_store = MagicMock()
        mock_store.context = MagicMock()
        workflows_status = {workflow_id: mock_store}
        workflows_statuses = {workflow_id: 'rUnnIng'}
        with (
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', namespaces),
            patch('opentf.core.observer.WORKFLOWS', workflows),
            patch('opentf.core.observer.WORKFLOWS_STATUS', workflows_status),
            patch('opentf.core.observer.WORKFLOWS_STATUSES', workflows_statuses),
        ):
            observer._initialize_workflow_state(workflow_id, event)
        self.assertIn(workflow_id, namespaces)
        self.assertIn(workflow_id, workflows)
        self.assertIn('status', workflows[workflow_id])
        self.assertEqual(workflows[workflow_id]['status']['phase'], 'rUnnIng')

    def test_record_workflow_event_with_execlog_labels_not_integer(self):
        statuses = {}
        workers = {}
        namespaces = {}
        workflows = {}
        workflow_id = 'wOrKflow_iD'
        store = MagicMock(spec=observer.BackingStore())
        with (
            patch('opentf.core.observer.WORKFLOWS_STATUSES', statuses),
            patch('opentf.core.observer.WORKFLOWS_WORKERS', workers),
            patch('opentf.core.observer.WORKFLOWS_NAMESPACES', namespaces),
            patch('opentf.core.observer.WORKFLOWS', workflows),
            patch('opentf.core.observer.WORKFLOWS_STATUS', store),
            patch('opentf.core.observer.warning') as mock_warning,
        ):
            observer._record_workflow_event(
                (
                    observer.WORKFLOW,
                    workflow_id,
                    {
                        'kind': observer.WORKFLOW,
                        'metadata': {
                            'name': 'woRkfpace',
                            'workflow_id': workflow_id,
                            'labels': {
                                'live-executionlog/step-depth': 'bar',
                                'live-executionlog/job-depth': '0',
                                'live-executionlog/max-command-length': '10',
                            },
                        },
                    },
                )
            )
        mock_warning.assert_called_once()
        self.assertIn(workflow_id, statuses)
        self.assertIn(workflow_id, workers)
        self.assertIn(workflow_id, namespaces)
        self.assertIn(workflow_id, workflows)
        self.assertIn('status', workflows[workflow_id])
        self.assertEqual(workflows[workflow_id]['status']['phase'], 'RUNNING')

    # _make_workflow_response

    def test_make_workflow_response_ok(self):
        mock_request = MagicMock()
        mock_request.args.get = lambda x, **_: {'per_page': 1}.get(x)
        mock_request.base_url = 'url'
        with (
            patch('opentf.core.observer.annotate_response') as mock_annotate,
            patch('opentf.core.observer.make_status_response') as mock_msr,
            patch('opentf.core.observer.request', mock_request),
            patch('opentf.core.observer.WORKFLOWS', {'WFID': {'a': 'b'}}),
        ):
            observer._make_workflow_response('WFID', 'some message', {'str': 'Any'})
        mock_annotate.assert_called_once()
        mock_msr.assert_called_once_with(
            'OK', message='some message', details={'str': 'Any', 'items': [{'a': 'b'}]}
        )

    # _maybe_add_event_to_live_cache

    def test_maybe_add_event_to_live_cache_notification(self):
        event = {
            'kind': 'Notification',
            'metadata': {
                'workflow_id': 'wf_ID',
            },
            'spec': {'testResults': [{'attachment_origin': 'b'}]},
        }
        event_cache = {'wf_ID': []}
        att_cache = {'wf_ID': {'b': {'handled': False, 'timestamp': 'foo'}}}
        datasource_cache = {'wf_ID': {'testcases': {}}}
        testcases = {
            'uuid1': {
                'metadata': {'executions': 0},
                'executionHistory': {},
                'execution': {'foo': 'bar'},
            }
        }

        with (
            patch('opentf.core.observer.WORKFLOWS_LIVE_EVENT_CACHES', event_cache),
            patch('opentf.core.observer.LIVE_DATASOURCE_CACHES', datasource_cache),
            patch(
                'opentf.core.observer.get_testcases', return_value=testcases
            ) as mock_get_tc,
            patch('opentf.core.observer.ATTACHMENTS_NOTIFICATIONS', att_cache),
        ):
            observer._maybe_add_event_to_live_cache(event)
        mock_get_tc.assert_called_once()
        self.assertEqual({'wf_ID': []}, event_cache)
        self.assertEqual(
            1, datasource_cache['wf_ID']['testcases']['uuid1']['metadata']['executions']
        )
        self.assertTrue(att_cache['wf_ID']['b'])

    def test_maybe_add_event_to_live_cache_update_testcases(self):
        event = {
            'kind': 'Notification',
            'metadata': {
                'workflow_id': 'wf_ID',
            },
            'spec': {'testResults': [{'c': 'd'}]},
        }
        event_cache = {'wf_ID': []}
        datasource_cache = {
            'wf_ID': {
                'testcases': {
                    'uuid1': {
                        'test': {'outcome': 'failure'},
                        'status': 'failure',
                        'metadata': {'executions': 1},
                        'execution': {'1': '2'},
                        'executionHistory': [{'1': '2'}],
                    }
                }
            }
        }
        testcases = {
            'uuid1': {
                'metadata': {'executions': 1},
                'executionHistory': [{'1': '2'}],
                'execution': {'foo': 'bar'},
                'test': {'outcome': 'success'},
                'status': 'success',
            }
        }

        with (
            patch('opentf.core.observer.WORKFLOWS_LIVE_EVENT_CACHES', event_cache),
            patch('opentf.core.observer.LIVE_DATASOURCE_CACHES', datasource_cache),
            patch(
                'opentf.core.observer.get_testcases', return_value=testcases
            ) as mock_get_tc,
        ):
            observer._maybe_add_event_to_live_cache(event)
        mock_get_tc.assert_called_once()
        self.assertEqual({'wf_ID': []}, event_cache)
        tc = datasource_cache['wf_ID']['testcases']['uuid1']
        self.assertEqual(2, tc['metadata']['executions'])
        self.assertEqual([{'1': '2'}, {'foo': 'bar'}], tc['executionHistory'])
        self.assertEqual('success', tc['test']['outcome'], tc['status'])

    def test_maybe_add_event_to_live_cache_execresult(self):
        event = {
            'kind': 'ExecutionResult',
            'metadata': {'workflow_id': 'WFID', 'step_sequence_id': -2},
        }
        event_cache = {'WFID': []}
        datasource_cache = {'WFID': {}}
        with (
            patch('opentf.core.observer.WORKFLOWS_LIVE_EVENT_CACHES', event_cache),
            patch('opentf.core.observer.LIVE_DATASOURCE_CACHES', datasource_cache),
            patch('opentf.core.observer.get_jobs') as mock_jobs,
            patch('opentf.core.observer.get_tags') as mock_tags,
        ):
            observer._maybe_add_event_to_live_cache(event)
        self.assertEqual(1, len(event_cache['WFID']))
        mock_jobs.assert_called_once()
        mock_tags.assert_called_once()
        self.assertEqual(['jobs', 'tags'], list(datasource_cache['WFID']))

    def test_maybe_add_event_to_live_cache_execresult_attachment(self):
        event = {
            'kind': 'ExecutionResult',
            'attachments': ['att1'],
            'metadata': {
                'step_sequence_id': 33,
                'workflow_id': 'WFID',
                'attachments': {
                    'att1': {'type': 'vnd.opentestfactory.xml', 'uuid': 'uuid1'}
                },
            },
        }
        event_cache = {'WFID': []}
        att_cache = {'WFID': {}}
        with (
            patch('opentf.core.observer.WORKFLOWS_LIVE_EVENT_CACHES', event_cache),
            patch('opentf.core.observer.ATTACHMENTS_NOTIFICATIONS', att_cache),
        ):
            observer._maybe_add_event_to_live_cache(event)
        self.assertEqual(1, len(event_cache['WFID']))
        self.assertFalse(att_cache['WFID']['uuid1']['handled'])

    def test_maybe_add_event_to_live_cache_other_relevant(self):
        events = [
            {'kind': 'Workflow', 'metadata': {'workflow_id': 'WFID'}},
            {
                'kind': 'ExecutionCommand',
                'metadata': {'workflow_id': 'WFID', 'step_sequence_id': 0},
            },
            {
                'kind': 'ProviderCommand',
                'metadata': {'workflow_id': 'WFID', 'labels': {}},
            },
        ]
        event_cache = {'WFID': []}
        with patch('opentf.core.observer.WORKFLOWS_LIVE_EVENT_CACHES', event_cache):
            for event in events:
                observer._maybe_add_event_to_live_cache(event)
        self.assertEqual(3, len(event_cache['WFID']))
        self.assertEqual(
            ['Workflow', 'ExecutionCommand', 'ProviderCommand'],
            [event['kind'] for event in event_cache['WFID']],
        )

    def test_maybe_add_event_to_live_cache_not_relevant(self):
        event = {'kind': 'Irrelevant'}
        self.assertIsNone(observer._maybe_add_event_to_live_cache(event))

    # main

    def test_main_subscription_fail(self):
        mock_threading = MagicMock()
        mock_threading.Thread = MagicMock()
        mock_subscribe = MagicMock(side_effect=Exception('ooOps'))
        mock_unsubscribe = MagicMock()
        with (
            patch('opentf.core.observer.threading', mock_threading),
            patch('opentf.core.observer.subscribe', mock_subscribe),
            patch('opentf.core.observer.unsubscribe', mock_unsubscribe),
            patch('opentf.core.observer.run_app') as mock_run_app,
        ):
            self.assertRaises(SystemExit, observer.main)
        mock_run_app.assert_not_called()

    def test_main_preparation_thread_fail(self):
        mock_threading = MagicMock()
        mock_threading.Thread = MagicMock(side_effect=Exception('ooOps'))
        mock_fatal = MagicMock(side_effect=SystemExit('ooOps'))
        with (
            patch('opentf.core.observer.threading', mock_threading),
            patch('opentf.core.observer.fatal', mock_fatal),
        ):
            self.assertRaises(SystemExit, observer.main)
        mock_fatal.assert_called_once()

    def test_main_preparation_ok(self):
        mock_threading = MagicMock()
        mock_threading.Thread = MagicMock()
        mock_subscribe = MagicMock()
        mock_unsubscribe = MagicMock()
        mock_run_app = MagicMock()
        with (
            patch('opentf.core.observer.threading', mock_threading),
            patch('opentf.core.observer.subscribe', mock_subscribe),
            patch('opentf.core.observer.unsubscribe', mock_unsubscribe),
            patch('opentf.core.observer.run_app', mock_run_app),
        ):
            observer.main()
        mock_run_app.assert_called_once()

    def test_main_max_retention_lower_than_default(self):
        mock_threading = MagicMock()
        mock_threading.Thread = MagicMock()
        mock_subscribe = MagicMock()
        mock_unsubscribe = MagicMock()
        mock_run_app = MagicMock()
        with (
            patch('opentf.core.observer.threading', mock_threading),
            patch('opentf.core.observer.subscribe', mock_subscribe),
            patch('opentf.core.observer.unsubscribe', mock_unsubscribe),
            patch('opentf.core.observer.run_app', mock_run_app),
            patch('opentf.core.observer.MAX_RETENTION_PERIOD', timedelta(minutes=10)),
        ):
            observer.main()
        mock_run_app.assert_called_once()


if __name__ == '__main__':
    unittest.main()
