# Copyright (c) 2023-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""SKF Provider unit tests.

- SKF tests
"""

import logging
import os
import unittest
import sys

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.skf import main, implementation

import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    'contexts': {'runner': {'os': 'linux'}},
    'step': {'uses': 'foo'},
    'runs-on': ['linux'],
}

PROVIDERMANIFESTCACHE = {
    ('skf', 'execute', None): ({'test': {'required': True}}, False),
    ('skf', 'params', None): (
        {
            'data': {
                'description': 'The data to use for the automated test.',
                'required': True,
            },
            'format': {
                'description': 'The format to use for the automated test data.',
                'required': True,
            },
        },
        True,
    ),
    ('skf', 'skf', None): (
        {
            'root-project': {'required': True},
            'tests': {},
            'data': {},
            'global-parameters': {},
            'testsuite': {},
        }
    ),
}

########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestSKF(unittest.TestCase):
    """
    The class containing the SKF provider unit test methods.
    """

    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONTEXT'][opentf.toolkit.INPUTS_KEY] = PROVIDERMANIFESTCACHE
        self.app = app.test_client()
        self.assertFalse(app.debug)

    ###########################

    def test_skf_ok(self):
        """
        In a SKF context, checks that providing just a root project is fine.
        """
        with patch('opentf.toolkit.core.runner_on_windows', lambda: True):
            what = implementation.skf_action({'root-project': 'fooBar'})
        self.assertEqual(len(what), 9)
        self.assertIn('squash-ta:run', what[4]['run'])

    def test_skf_global_parameters_ok(self):
        with patch('opentf.toolkit.core.runner_on_windows', lambda: True):
            what = implementation.skf_action(
                {
                    'root-project': 'fooBar',
                    'global-parameters': {'foo': 'val1', 'bar': 'val2'},
                }
            )
        self.assertEqual(len(what), 9)
        self.assertIn('squash-ta:run', what[4]['run'])

    def test_skf_tests_ok(self):
        with patch('opentf.toolkit.core.runner_on_windows', lambda: True):
            what = implementation.skf_action(
                {
                    'root-project': 'fooBar',
                    'tests': [{'script': 'val1', 'data': 'val2'}],
                }
            )
        self.assertEqual(len(what), 9)
        self.assertIn('squash-ta:run', what[4]['run'])

    def test_skf_tests_testsuite_nok(self):
        mock = MagicMock(side_effect=Exception())
        with patch('opentf.toolkit.core.fail', mock):
            self.assertRaises(
                Exception,
                implementation.skf_action,
                {'root-project': 'fooBar', 'tests': {}, 'testsuite': 'foo'},
            )

    def test_skf_params_bad_format(self):
        mock = MagicMock(side_effect=Exception())
        with patch('opentf.toolkit.core.validate_params_inputs', mock):
            self.assertRaises(
                Exception,
                implementation.param_action,
                {
                    'data': {'global': {'key1': 'value1', 'key2': 'value2'}},
                    'format': 'nope',
                },
            )

    def test_skf_params_ok_global(self):
        with (
            patch('opentf.toolkit.core.runner_on_windows', lambda: True),
            patch(
                'opentf.plugins.skf.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            what = implementation.param_action(
                {
                    'data': {'global': {'key1': 'value1', 'key2': 'value2'}},
                    'format': 'tm.squashtest.org/params@v1',
                }
            )
        self.assertEqual(len(what), 2)

    def test_execute_ok(self):
        test_reference = 'fooBar/tests#yada'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'skf/execute',
                'with': {'test': test_reference},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 5)
        self.assertIn('squash-ta:run', msg[2]['run'].strip())

    def test_execution_ko_no_definition(self):
        test_reference = 'fooBar/tests#'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'skf/execute',
                'with': {'test': test_reference},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(1, len(msg))
        self.assertIn('You must target a ta script file', msg[0]['run'])

    def test_execution_ko_no_ecosystem(self):
        test_reference = 'fooBar/#tests'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'skf/execute',
                'with': {'test': test_reference},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(1, len(msg))
        self.assertIn('The default ecosystem must be', msg[0]['run'])


if __name__ == '__main__':
    unittest.main()
