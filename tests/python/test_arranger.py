# Copyright (c) 2021-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Arranger unit tests."""

import datetime
import json
import logging
import os
import unittest
import sys

from queue import Queue
from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
logging.disable(logging.CRITICAL)
from opentf.core import arranger

logging.disable(logging.NOTSET)

mockresponse = Response()
mockresponse.status_code = 200


def _make_copy(what):
    return json.loads(json.dumps(what, default=list))


EXECUTION_TRACE_2 = [
    {
        "metadata": {
            "name": "a generator job with two generated jobs",
            "labels": {"uuid": "yep"},
            "namespace": "default",
            "annotations": {
                "opentestfactory.org/ordering": [["job_a"], ["job_b", "job_c"]]
            },
            "creationTimestamp": "2024-09-20T13:30:16.299570",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
        },
        "hooks": [
            {
                "name": "on touch",
                "events": [{"categoryPrefix": "actions", "category": "touch-file"}],
                "before": [{"run": "echo before"}],
            },
            {
                "name": "on job",
                "events": [{"channel": "setup"}],
                "after": [{"run": "echo setup"}],
            },
            {
                "name": "on failed jobs",
                "events": [{"channel": "teardown"}],
                "if": "failure()",
                "before": [{"keep-workspace": True, "if": "failure()"}],
            },
        ],
        "jobs": {
            "job_a": {
                "generator": "core/dummygenerator@v1",
                "with": {
                    "job_1": {
                        "runs-on": "windows",
                        "steps": [
                            {"run": "echo job_1", "if": "cancelled()"},
                            {
                                "uses": "actions/touch-file@v1",
                                "with": {"path": "foo.xml"},
                            },
                        ],
                    },
                    "job_2": {"runs-on": "windows", "steps": [{"run": "echo job_2"}]},
                },
            },
            "job_b": {
                "needs": "job_a",
                "runs-on": "windows",
                "steps": [
                    {
                        "uses": "actions/touch-file@v1",
                        "with": {"path": "foo"},
                        "id": "109cd4b6-e478-4938-a2ee-5a0b04cc2c76",
                    }
                ],
            },
            "job_c": {
                "needs": "job_a",
                "runs-on": "linux",
                "if": "failure()",
                "steps": [
                    {"run": "echo oh no", "id": "6c8d91e6-8226-4cc2-8dff-52293771dc24"}
                ],
            },
        },
        "apiVersion": "opentestfactory.org/v1",
        "kind": "Workflow",
    },
    {
        "apiVersion": "opentestfactory.org/v1",
        "kind": "GeneratorResult",
        "metadata": {
            "job_origin": [],
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000001",
            "namespace": "default",
            "name": "job_a",
            "labels": {
                "opentestfactory.org/category": "dummygenerator",
                "opentestfactory.org/categoryPrefix": "core",
                "opentestfactory.org/categoryVersion": "v1",
            },
            "creationTimestamp": "2024-09-20T13:30:16.346402",
        },
        "jobs": {
            "job-1": {
                "runs-on": "windows",
                "steps": [
                    {"run": "echo job_1", "if": "cancelled()"},
                    {"uses": "actions/touch-file@v1", "with": {"path": "foo.xml"}},
                ],
            },
            "job-2": {"runs-on": "windows", "steps": [{"run": "echo job_2"}]},
        },
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "job-1",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000002",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_origin": [],
            "step_sequence_id": -1,
            "step_id": "8874b74f-9547-47fa-82e7-80ff2ca9ba7d",
            "creationTimestamp": "2024-09-20T13:30:18.040576",
            "channel_id": "7ac939f2-7abb-4414-91d5-fbedf6da8c3b",
            "channelhandler_id": "fc9e4c70-f7e8-4301-a131-be624b97a345",
            "channel_tags": ["windows", "robotframework", "junit"],
            "channel_os": "windows",
            "channel_temp": "%TEMP%",
            "channel_lease": 60,
        },
        "status": 0,
    },
    {
        "kind": "StartJob",
        "metadata": {
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000002",
        },
        "candidate": {
            "name": "job-1",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000002",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_origin": [],
            "step_sequence_id": -1,
            "step_id": "8874b74f-9547-47fa-82e7-80ff2ca9ba7d",
            "creationTimestamp": "2024-09-20T13:30:18.040576",
            "channel_id": "7ac939f2-7abb-4414-91d5-fbedf6da8c3b",
            "channelhandler_id": "fc9e4c70-f7e8-4301-a131-be624b97a345",
            "channel_tags": ["windows", "robotframework", "junit"],
            "channel_os": "windows",
            "channel_temp": "%TEMP%",
            "channel_lease": 60,
        },
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000002",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_id": "a92e8999-a35f-4b0b-0002-000000000006",
            "step_origin": [],
            "annotations": {
                "opentestfactory.org/hooks": {
                    "channel": "setup",
                    "use-workspace": "a92e8999-a35f-4b0b-b30c-000000000002",
                }
            },
            "step_sequence_id": 0,
            "channel_id": "7ac939f2-7abb-4414-91d5-fbedf6da8c3b",
            "creationTimestamp": "2024-09-20T13:30:22.255810",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": [
            "Using workspace 'a92e8999-a35f-4b0b-b30c-000000000002' on execution environment"
        ],
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run2",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000002",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_id": "a92e8999-a35f-4b0b-0002-000000000007",
            "step_origin": [],
            "annotations": {"opentestfactory.org/hooks": {"channel": "setup"}},
            "step_sequence_id": 1,
            "channel_id": "7ac939f2-7abb-4414-91d5-fbedf6da8c3b",
            "creationTimestamp": "2024-09-20T13:30:22.398621",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": ["setup"],
    },
    {
        "kind": "NextStep",
        "metadata": {
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000002",
        },
    },
    {
        "apiVersion": "opentestfactory.org/v1",
        "kind": "ProviderResult",
        "metadata": {
            "name": "actionstouch-filev1",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000002",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_id": "a92e8999-a35f-4b0b-0002-000000000005",
            "step_origin": [],
            "labels": {
                "opentestfactory.org/category": "touch-file",
                "opentestfactory.org/categoryPrefix": "actions",
                "opentestfactory.org/categoryVersion": "v1",
            },
            "creationTimestamp": "2024-09-20T13:30:22.420185",
        },
        "steps": [
            {"run": "@type nul >>foo.xml", "id": "38dcf1bb-b665-4b58-918b-e7a4e7660c18"}
        ],
        "hooks": [
            {
                "name": "Should run",
                "events": [{"categoryPrefix": "robotframework"}],
                "before": [{"run": "echo YESSSSSSSSSSSSSSSSSSSSSSSSSSSS"}],
            }
        ],
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run4",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000002",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_id": "a92e8999-a35f-4b0b-0002-000000000008",
            "step_origin": ["a92e8999-a35f-4b0b-0002-000000000005"],
            "step_sequence_id": 2,
            "channel_id": "7ac939f2-7abb-4414-91d5-fbedf6da8c3b",
            "creationTimestamp": "2024-09-20T13:30:22.526013",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": ["before"],
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run5",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000002",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_id": "38dcf1bb-b665-4b58-918b-e7a4e7660c18",
            "step_origin": ["a92e8999-a35f-4b0b-0002-000000000005"],
            "step_sequence_id": 3,
            "channel_id": "7ac939f2-7abb-4414-91d5-fbedf6da8c3b",
            "creationTimestamp": "2024-09-20T13:30:22.652298",
            "channel_os": "windows",
        },
        "status": 0,
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000002",
            "namespace": "default",
            "name": "job-1",
            "step_sequence_id": -2,
            "channel_id": "7ac939f2-7abb-4414-91d5-fbedf6da8c3b",
            "step_origin": [],
            "step_id": "a92e8999-a35f-4b0b-0002-000000000011",
            "step_count": 4,
            "annotations": {
                "opentestfactory.org/hooks": {
                    "channel": "teardown",
                    "keep-workspace": False,
                }
            },
            "creationTimestamp": "2024-09-20T13:30:24.172080",
            "channel_os": "windows",
        },
        "status": 0,
    },
    {
        'kind': 'TimeoutError',
        'metadata': {'workflow_id': 'a92e8999-a35f-4b0b-b30c-d0e88e7671fb'},
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "job-2",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000003",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_origin": [],
            "step_sequence_id": -1,
            "step_id": "b82e0213-a029-4e63-889a-2ba8ea02c226",
            "creationTimestamp": "2024-09-20T13:30:28.066211",
            "channel_id": "fdb49777-5fa0-4836-9d3f-4a9451399511",
            "channelhandler_id": "fc9e4c70-f7e8-4301-a131-be624b97a345",
            "channel_tags": ["windows", "robotframework", "junit"],
            "channel_os": "windows",
            "channel_temp": "%TEMP%",
            "channel_lease": 60,
        },
        "status": 0,
    },
    {
        "kind": "StartJob",
        "metadata": {
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000003",
        },
        "candidate": {
            "name": "job-2",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000003",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_origin": [],
            "step_sequence_id": -1,
            "step_id": "b82e0213-a029-4e63-889a-2ba8ea02c226",
            "creationTimestamp": "2024-09-20T13:30:28.066211",
            "channel_id": "fdb49777-5fa0-4836-9d3f-4a9451399511",
            "channelhandler_id": "fc9e4c70-f7e8-4301-a131-be624b97a345",
            "channel_tags": ["windows", "robotframework", "junit"],
            "channel_os": "windows",
            "channel_temp": "%TEMP%",
            "channel_lease": 60,
        },
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000003",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_id": "a92e8999-a35f-4b0b-0003-000000000013",
            "step_origin": [],
            "annotations": {
                "opentestfactory.org/hooks": {
                    "channel": "setup",
                    "use-workspace": "a92e8999-a35f-4b0b-b30c-000000000003",
                }
            },
            "step_sequence_id": 0,
            "channel_id": "fdb49777-5fa0-4836-9d3f-4a9451399511",
            "creationTimestamp": "2024-09-20T13:30:29.297016",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": [
            "Using workspace 'a92e8999-a35f-4b0b-b30c-000000000003' on execution environment"
        ],
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run2",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000003",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_id": "a92e8999-a35f-4b0b-0002-000000000007",
            "step_origin": [],
            "annotations": {"opentestfactory.org/hooks": {"channel": "setup"}},
            "step_sequence_id": 1,
            "channel_id": "fdb49777-5fa0-4836-9d3f-4a9451399511",
            "creationTimestamp": "2024-09-20T13:30:29.424692",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": ["setup"],
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run2",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000003",
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "step_id": "a92e8999-a35f-4b0b-0003-000000000012",
            "step_origin": [],
            "step_sequence_id": 2,
            "channel_id": "fdb49777-5fa0-4836-9d3f-4a9451399511",
            "creationTimestamp": "2024-09-20T13:30:29.551698",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": ["job_2"],
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "job_origin": ["a92e8999-a35f-4b0b-b30c-000000000001"],
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000003",
            "namespace": "default",
            "name": "job-2",
            "step_sequence_id": -2,
            "channel_id": "fdb49777-5fa0-4836-9d3f-4a9451399511",
            "step_origin": [],
            "step_id": "a92e8999-a35f-4b0b-0003-000000000015",
            "step_count": 3,
            "annotations": {
                "opentestfactory.org/hooks": {
                    "channel": "teardown",
                    "keep-workspace": False,
                }
            },
            "creationTimestamp": "2024-09-20T13:30:31.189969",
            "channel_os": "windows",
        },
        "status": 0,
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "job_b",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000016",
            "job_origin": [],
            "step_origin": [],
            "step_sequence_id": -1,
            "step_id": "061df055-2f32-4614-b5b3-06dba7798710",
            "creationTimestamp": "2024-09-20T13:30:33.089823",
            "channel_id": "873b52bc-254f-45c8-8393-ac8ad8e54a7a",
            "channelhandler_id": "fc9e4c70-f7e8-4301-a131-be624b97a345",
            "channel_tags": ["windows", "robotframework", "junit"],
            "channel_os": "windows",
            "channel_temp": "%TEMP%",
            "channel_lease": 60,
        },
        "status": 0,
    },
    {
        "kind": "StartJob",
        "metadata": {
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000016",
        },
        "candidate": {
            "name": "job_b",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000016",
            "job_origin": [],
            "step_origin": [],
            "step_sequence_id": -1,
            "step_id": "061df055-2f32-4614-b5b3-06dba7798710",
            "creationTimestamp": "2024-09-20T13:30:33.089823",
            "channel_id": "873b52bc-254f-45c8-8393-ac8ad8e54a7a",
            "channelhandler_id": "fc9e4c70-f7e8-4301-a131-be624b97a345",
            "channel_tags": ["windows", "robotframework", "junit"],
            "channel_os": "windows",
            "channel_temp": "%TEMP%",
            "channel_lease": 60,
        },
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000016",
            "job_origin": [],
            "step_id": "a92e8999-a35f-4b0b-0016-000000000019",
            "step_origin": [],
            "annotations": {
                "opentestfactory.org/hooks": {
                    "channel": "setup",
                    "use-workspace": "a92e8999-a35f-4b0b-b30c-000000000016",
                }
            },
            "step_sequence_id": 0,
            "channel_id": "873b52bc-254f-45c8-8393-ac8ad8e54a7a",
            "creationTimestamp": "2024-09-20T13:30:36.343807",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": [
            "Using workspace 'a92e8999-a35f-4b0b-b30c-000000000016' on execution environment"
        ],
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run2",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000016",
            "job_origin": [],
            "step_id": "a92e8999-a35f-4b0b-0002-000000000007",
            "step_origin": [],
            "annotations": {"opentestfactory.org/hooks": {"channel": "setup"}},
            "step_sequence_id": 1,
            "channel_id": "873b52bc-254f-45c8-8393-ac8ad8e54a7a",
            "creationTimestamp": "2024-09-20T13:30:36.470979",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": ["setup"],
    },
    {
        "apiVersion": "opentestfactory.org/v1",
        "kind": "ProviderResult",
        "metadata": {
            "name": "actionstouch-filev1",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000016",
            "job_origin": [],
            "step_id": "109cd4b6-e478-4938-a2ee-5a0b04cc2c76",
            "step_origin": [],
            "labels": {
                "opentestfactory.org/category": "touch-file",
                "opentestfactory.org/categoryPrefix": "actions",
                "opentestfactory.org/categoryVersion": "v1",
            },
            "creationTimestamp": "2024-09-20T13:30:36.494286",
        },
        "steps": [
            {"run": "@type nul >>foo", "id": "c7f3c397-b87f-43a8-964f-1fc125311358"}
        ],
        "hooks": [
            {
                "name": "Should run",
                "events": [{"categoryPrefix": "robotframework"}],
                "before": [{"run": "echo YESSSSSSSSSSSSSSSSSSSSSSSSSSSS"}],
            }
        ],
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run2",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000016",
            "job_origin": [],
            "step_id": "a92e8999-a35f-4b0b-0016-000000000021",
            "step_origin": ["109cd4b6-e478-4938-a2ee-5a0b04cc2c76"],
            "step_sequence_id": 2,
            "channel_id": "873b52bc-254f-45c8-8393-ac8ad8e54a7a",
            "creationTimestamp": "2024-09-20T13:30:36.629839",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": ["before"],
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run3",
            "namespace": "default",
            "workflow_id": "a92e8999-a35f-4b0b-b30c-d0e88e7671fb",
            "job_id": "a92e8999-a35f-4b0b-b30c-000000000016",
            "job_origin": [],
            "step_id": "c7f3c397-b87f-43a8-964f-1fc125311358",
            "step_origin": ["109cd4b6-e478-4938-a2ee-5a0b04cc2c76"],
            "step_sequence_id": 3,
            "channel_id": "873b52bc-254f-45c8-8393-ac8ad8e54a7a",
            "creationTimestamp": "2024-09-20T13:30:36.757961",
            "channel_os": "windows",
        },
        "status": 0,
    },
]

EXECUTION_TRACE_2_OLD = [
    {
        'metadata': {
            'name': 'a generator job with two generated jobs',
            'labels': {'uuid': 'yep'},
            'namespace': 'default',
            'annotations': {
                'opentestfactory.org/ordering': [['job_a'], ['job_b', 'job_c']]
            },
            'creationTimestamp': '2024-08-07T17:19:22.086049',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'actor': '_localhost_',
            'token': None,
        },
        'hooks': [
            {
                'name': 'on touch',
                'events': [{'categoryPrefix': 'actions', 'category': 'touch-file'}],
                'before': [{'run': 'echo before'}],
            },
            {
                'name': 'on job',
                'events': [{'channel': 'setup'}],
                'after': [{'run': 'echo setup'}],
            },
            {
                'name': 'on failed jobs',
                'events': [{'channel': 'teardown'}],
                'if': 'failure()',
                'before': [{'keep-workspace': True, 'if': 'failure()'}],
            },
        ],
        'jobs': {
            'job_a': {
                'generator': 'core/dummygenerator@v1',
                'with': {
                    'job_1': {
                        'runs-on': 'windows',
                        'steps': [
                            {'run': 'echo job_1', 'if': 'cancelled()'},
                            {
                                'uses': 'actions/touch-file@v1',
                                'with': {'path': 'foo.xml'},
                            },
                        ],
                    },
                    'job_2': {'runs-on': 'windows', 'steps': [{'run': 'echo job_2'}]},
                },
            },
            'job_b': {
                'needs': 'job_a',
                'runs-on': 'windows',
                'steps': [
                    {
                        'uses': 'actions/touch-file@v1',
                        'with': {'badpath': 'foo'},
                        'id': '109cd4b6-e478-4938-a2ee-5a0b04cc2c76',
                    }
                ],
            },
            'job_c': {
                'needs': 'job_a',
                'runs-on': 'linux',
                'if': 'failure()',
                'steps': [
                    {'run': 'echo oh no', 'id': '6c8d91e6-8226-4cc2-8dff-52293771dc24'}
                ],
            },
        },
        'apiVersion': 'opentestfactory.org/v1',
        'kind': 'Workflow',
    },
    {
        'apiVersion': 'opentestfactory.org/v1beta1',
        'kind': 'GeneratorResult',
        'metadata': {
            'job_origin': [],
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000001',
            'namespace': 'default',
            'name': 'job_a',
            'labels': {
                'opentestfactory.org/category': 'dummygenerator',
                'opentestfactory.org/categoryPrefix': 'core',
                'opentestfactory.org/categoryVersion': 'v1',
            },
            'creationTimestamp': '2024-08-07T17:19:22.256984',
        },
        'jobs': {
            'job_1': {
                'runs-on': 'windows',
                'steps': [
                    {'run': 'echo job_1', 'if': 'cancelled()'},
                    {'uses': 'actions/touch-file@v1', 'with': {'path': 'foo.xml'}},
                ],
            },
            'job_2': {'runs-on': 'windows', 'steps': [{'run': 'echo job_2'}]},
        },
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'name': 'job_1',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_origin': [],
            'step_sequence_id': -1,
            'step_id': '93c9bab2-9dfc-498a-acb6-90a7cd281a29',
            'creationTimestamp': '2024-08-07T17:19:22.411950',
            'channel_id': '9d4516d2-1050-4b18-9bab-d3a6501c9d8d',
            'channelhandler_id': '08960947-8075-4814-a73d-8ff5b2f93824',
            'channel_tags': ['windows', 'robotframework', 'junit'],
            'channel_os': 'windows',
            'channel_temp': '%TEMP%',
            'channel_lease': 60,
        },
        'status': 0,
    },
    {
        'kind': 'StartJob',
        'metadata': {
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
        },
        'candidate': {
            'name': 'job_1',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_origin': [],
            'step_sequence_id': -1,
            'step_id': '93c9bab2-9dfc-498a-acb6-90a7cd281a29',
            'creationTimestamp': '2024-08-07T17:19:22.411950',
            'channel_id': '9d4516d2-1050-4b18-9bab-d3a6501c9d8d',
            'channelhandler_id': '08960947-8075-4814-a73d-8ff5b2f93824',
            'channel_tags': ['windows', 'robotframework', 'junit'],
            'channel_os': 'windows',
            'channel_temp': '%TEMP%',
            'channel_lease': 60,
        },
    },
    {
        'kind': 'NextStep',
        'metadata': {
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
        },
    },
    {
        'apiVersion': 'opentestfactory.org/v1beta1',
        'kind': 'ProviderResult',
        'metadata': {
            'name': 'actionstouch-filev1',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_id': 'ca34c0cb-7bed-49e4-0002-000000000007',
            'step_origin': [],
            'labels': {
                'opentestfactory.org/category': 'touch-file',
                'opentestfactory.org/categoryPrefix': 'actions',
                'opentestfactory.org/categoryVersion': 'v1',
            },
            'creationTimestamp': '2024-08-07T17:19:22.519635',
        },
        'steps': [
            {'run': '@type nul >>foo.xml', 'id': 'cb1b2f9b-1448-4649-ba3a-8996bd96c282'}
        ],
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'name': 'run2',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_id': 'ca34c0cb-7bed-49e4-0002-000000000008',
            'step_origin': ['ca34c0cb-7bed-49e4-0002-000000000007'],
            'step_sequence_id': 0,
            'channel_id': '9d4516d2-1050-4b18-9bab-d3a6501c9d8d',
            'creationTimestamp': '2024-08-07T17:19:23.575826',
            'channel_os': 'windows',
        },
        'status': 0,
        'logs': ['before'],
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'name': 'run3',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_id': 'cb1b2f9b-1448-4649-ba3a-8996bd96c282',
            'step_origin': ['ca34c0cb-7bed-49e4-0002-000000000007'],
            'step_sequence_id': 1,
            'channel_id': '9d4516d2-1050-4b18-9bab-d3a6501c9d8d',
            'creationTimestamp': '2024-08-07T17:19:23.752383',
            'channel_os': 'windows',
        },
        'status': 0,
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'name': 'run4',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_id': 'ca34c0cb-7bed-49e4-0002-000000000004',
            'step_origin': [],
            'annotations': {
                'opentestfactory.org/hooks': {
                    'channel': 'setup',
                    'use-workspace': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
                }
            },
            'step_sequence_id': 2,
            'channel_id': '9d4516d2-1050-4b18-9bab-d3a6501c9d8d',
            'creationTimestamp': '2024-08-07T17:19:23.958949',
            'channel_os': 'windows',
        },
        'status': 0,
        'logs': [
            "Using workspace 'ca34c0cb-7bed-49e4-ab7a-000000000002' on execution environment"
        ],
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'name': 'run5',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_id': 'ca34c0cb-7bed-49e4-0002-000000000005',
            'step_origin': [],
            'annotations': {'opentestfactory.org/hooks': {'channel': 'setup'}},
            'step_sequence_id': 3,
            'channel_id': '9d4516d2-1050-4b18-9bab-d3a6501c9d8d',
            'creationTimestamp': '2024-08-07T17:19:24.149959',
            'channel_os': 'windows',
        },
        'status': 0,
        'logs': ['setup'],
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000002',
            'namespace': 'default',
            'name': 'job_1',
            'step_sequence_id': -2,
            'channel_id': '9d4516d2-1050-4b18-9bab-d3a6501c9d8d',
            'step_origin_status': {'ca34c0cb-7bed-49e4-0002-000000000007': 3},
            'step_origin': ['ca34c0cb-7bed-49e4-0002-000000000007'],
            'step_id': 'ca34c0cb-7bed-49e4-0002-000000000011',
            'step_count': 4,
            'annotations': {
                'opentestfactory.org/hooks': {
                    'channel': 'teardown',
                    'keep-workspace': False,
                }
            },
            'creationTimestamp': '2024-08-07T17:19:25.349817',
            'channel_os': 'windows',
        },
        'status': 0,
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'name': 'job_2',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000003',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_origin': [],
            'step_sequence_id': -1,
            'step_id': '8d9d35f3-8e02-406d-a728-a8e2afab2587',
            'creationTimestamp': '2024-08-07T17:19:29.059720',
            'channel_id': 'ec893ee7-a066-44f3-b453-5e16b2e65e67',
            'channelhandler_id': '08960947-8075-4814-a73d-8ff5b2f93824',
            'channel_tags': ['windows', 'robotframework', 'junit'],
            'channel_os': 'windows',
            'channel_temp': '%TEMP%',
            'channel_lease': 60,
        },
        'status': 0,
    },
    {
        'kind': 'StartJob',
        'metadata': {
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000003',
        },
        'candidate': {
            'name': 'job_2',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000003',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_origin': [],
            'step_sequence_id': -1,
            'step_id': '8d9d35f3-8e02-406d-a728-a8e2afab2587',
            'creationTimestamp': '2024-08-07T17:19:29.059720',
            'channel_id': 'ec893ee7-a066-44f3-b453-5e16b2e65e67',
            'channelhandler_id': '08960947-8075-4814-a73d-8ff5b2f93824',
            'channel_tags': ['windows', 'robotframework', 'junit'],
            'channel_os': 'windows',
            'channel_temp': '%TEMP%',
            'channel_lease': 60,
        },
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'name': 'run',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000003',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_id': 'ca34c0cb-7bed-49e4-0003-000000000014',
            'step_origin': [],
            'step_sequence_id': 0,
            'channel_id': 'ec893ee7-a066-44f3-b453-5e16b2e65e67',
            'creationTimestamp': '2024-08-07T17:19:31.301891',
            'channel_os': 'windows',
        },
        'status': 0,
        'logs': ['job_2'],
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'name': 'run2',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000003',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_id': 'ca34c0cb-7bed-49e4-0003-000000000012',
            'step_origin': [],
            'annotations': {
                'opentestfactory.org/hooks': {
                    'channel': 'setup',
                    'use-workspace': 'ca34c0cb-7bed-49e4-ab7a-000000000003',
                }
            },
            'step_sequence_id': 1,
            'channel_id': 'ec893ee7-a066-44f3-b453-5e16b2e65e67',
            'creationTimestamp': '2024-08-07T17:19:31.470364',
            'channel_os': 'windows',
        },
        'status': 0,
        'logs': [
            "Using workspace 'ca34c0cb-7bed-49e4-ab7a-000000000003' on execution environment"
        ],
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'name': 'run3',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000003',
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'step_id': 'ca34c0cb-7bed-49e4-0002-000000000005',
            'step_origin': [],
            'annotations': {'opentestfactory.org/hooks': {'channel': 'setup'}},
            'step_sequence_id': 2,
            'channel_id': 'ec893ee7-a066-44f3-b453-5e16b2e65e67',
            'creationTimestamp': '2024-08-07T17:19:31.645217',
            'channel_os': 'windows',
        },
        'status': 0,
        'logs': ['setup'],
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'job_origin': ['ca34c0cb-7bed-49e4-ab7a-000000000001'],
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000003',
            'namespace': 'default',
            'name': 'job_2',
            'step_sequence_id': -2,
            'channel_id': 'ec893ee7-a066-44f3-b453-5e16b2e65e67',
            'step_origin_status': {},
            'step_origin': [],
            'step_id': 'ca34c0cb-7bed-49e4-0003-000000000015',
            'step_count': 3,
            'annotations': {
                'opentestfactory.org/hooks': {
                    'channel': 'teardown',
                    'keep-workspace': False,
                }
            },
            'creationTimestamp': '2024-08-07T17:19:32.884790',
            'channel_os': 'windows',
        },
        'status': 0,
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionResult',
        'metadata': {
            'name': 'job_b',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000016',
            'job_origin': [],
            'step_origin': [],
            'step_sequence_id': -1,
            'step_id': 'af843c4e-5769-4f53-9b8d-b4d6e1f887ce',
            'creationTimestamp': '2024-08-07T17:19:34.056741',
            'channel_id': 'a4e3b4b1-5927-40c6-b7ec-55bbfc86d463',
            'channelhandler_id': '08960947-8075-4814-a73d-8ff5b2f93824',
            'channel_tags': ['windows', 'robotframework', 'junit'],
            'channel_os': 'windows',
            'channel_temp': '%TEMP%',
            'channel_lease': 60,
        },
        'status': 0,
    },
    {
        'kind': 'StartJob',
        'metadata': {
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000016',
        },
        'candidate': {
            'name': 'job_b',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000016',
            'job_origin': [],
            'step_origin': [],
            'step_sequence_id': -1,
            'step_id': 'af843c4e-5769-4f53-9b8d-b4d6e1f887ce',
            'creationTimestamp': '2024-08-07T17:19:34.056741',
            'channel_id': 'a4e3b4b1-5927-40c6-b7ec-55bbfc86d463',
            'channelhandler_id': '08960947-8075-4814-a73d-8ff5b2f93824',
            'channel_tags': ['windows', 'robotframework', 'junit'],
            'channel_os': 'windows',
            'channel_temp': '%TEMP%',
            'channel_lease': 60,
        },
    },
    {
        'apiVersion': 'opentestfactory.org/v1alpha1',
        'kind': 'ExecutionError',
        'metadata': {
            'name': 'actionstouch-filev1',
            'namespace': 'default',
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000016',
            'job_origin': [],
            'step_id': '109cd4b6-e478-4938-a2ee-5a0b04cc2c76',
            'step_origin': [],
            'labels': {
                'opentestfactory.org/category': 'touch-file',
                'opentestfactory.org/categoryPrefix': 'actions',
                'opentestfactory.org/categoryVersion': 'v1',
            },
            'creationTimestamp': '2024-08-07T17:19:34.153815',
        },
        'details': {'error': "Mandatory input 'path' not provided."},
    },
    {
        'kind': 'NextStep',
        'metadata': {
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000016',
        },
    },
    {
        'kind': 'NextStep',
        'metadata': {
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000016',
        },
    },
    {
        'kind': 'NextStep',
        'metadata': {
            'workflow_id': 'ca34c0cb-7bed-49e4-ab7a-7d005101d5f0',
            'job_id': 'ca34c0cb-7bed-49e4-ab7a-000000000016',
        },
    },
]

EXECUTION_TRACE_1 = [
    {
        "metadata": {
            "name": "a generator job with two generated jobs",
            "labels": {"uuid": "yep"},
            "namespace": "default",
            "annotations": {"opentestfactory.org/ordering": [["job_a"]]},
            "creationTimestamp": "2024-08-07T13:45:34.227578",
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "actor": "_localhost_",
            "token": None,
        },
        "jobs": {
            "job_a": {
                "generator": "core/dummygenerator@v1",
                "with": {
                    "job_1": {
                        "runs-on": "windows",
                        "steps": [
                            {"run": "echo job_1"},
                            {
                                "uses": "actions/touch-file@v1",
                                "with": {"path": "foo.xml"},
                            },
                        ],
                    },
                    "job_2": {"runs-on": "windows", "steps": [{"run": "echo job_2"}]},
                },
            }
        },
        "apiVersion": "opentestfactory.org/v1",
        "kind": "Workflow",
    },
    {
        "apiVersion": "opentestfactory.org/v1beta1",
        "kind": "GeneratorResult",
        "metadata": {
            "job_origin": [],
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000001",
            "namespace": "default",
            "name": "job_a",
            "labels": {
                "opentestfactory.org/category": "dummygenerator",
                "opentestfactory.org/categoryPrefix": "core",
                "opentestfactory.org/categoryVersion": "v1",
            },
            "creationTimestamp": "2024-08-07T13:45:34.396311",
        },
        "jobs": {
            "job_1": {
                "runs-on": "windows",
                "steps": [
                    {"run": "echo job_1"},
                    {"uses": "actions/touch-file@v1", "with": {"path": "foo.xml"}},
                ],
            },
            "job_2": {"runs-on": "windows", "steps": [{"run": "echo job_2"}]},
        },
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "job_1",
            "namespace": "default",
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000002",
            "job_origin": ["4ef2c167-1668-4970-a570-000000000001"],
            "step_origin": [],
            "step_sequence_id": -1,
            "step_id": "90bb6a84-c645-4dd3-9936-e56e765f2378",
            "creationTimestamp": "2024-08-07T13:45:34.531643",
            "channel_id": "1dd99aed-845e-453e-97f5-4d8dabb24c58",
            "channelhandler_id": "4336ad74-2512-44e3-944d-71558e4d3453",
            "channel_tags": ["windows", "robotframework", "junit"],
            "channel_os": "windows",
            "channel_temp": "%TEMP%",
            "channel_lease": 60,
        },
        "status": 0,
    },
    {
        "kind": "StartJob",
        "metadata": {
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000002",
        },
        "candidate": {
            "name": "job_1",
            "namespace": "default",
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000002",
            "job_origin": ["4ef2c167-1668-4970-a570-000000000001"],
            "step_origin": [],
            "step_sequence_id": -1,
            "step_id": "90bb6a84-c645-4dd3-9936-e56e765f2378",
            "creationTimestamp": "2024-08-07T13:45:34.531643",
            "channel_id": "1dd99aed-845e-453e-97f5-4d8dabb24c58",
            "channelhandler_id": "4336ad74-2512-44e3-944d-71558e4d3453",
            "channel_tags": ["windows", "robotframework", "junit"],
            "channel_os": "windows",
            "channel_temp": "%TEMP%",
            "channel_lease": 60,
        },
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run",
            "namespace": "default",
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000002",
            "job_origin": ["4ef2c167-1668-4970-a570-000000000001"],
            "step_id": "4ef2c167-1668-4970-0002-000000000004",
            "step_origin": [],
            "step_sequence_id": 0,
            "channel_id": "1dd99aed-845e-453e-97f5-4d8dabb24c58",
            "creationTimestamp": "2024-08-07T13:45:38.747811",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": ["job_1"],
    },
    {
        "apiVersion": "opentestfactory.org/v1beta1",
        "kind": "ProviderResult",
        "metadata": {
            "name": "actionstouch-filev1",
            "namespace": "default",
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000002",
            "job_origin": ["4ef2c167-1668-4970-a570-000000000001"],
            "step_id": "4ef2c167-1668-4970-0002-000000000005",
            "step_origin": [],
            "labels": {
                "opentestfactory.org/category": "touch-file",
                "opentestfactory.org/categoryPrefix": "actions",
                "opentestfactory.org/categoryVersion": "v1",
            },
            "creationTimestamp": "2024-08-07T13:45:38.841010",
        },
        "steps": [{"run": "@type nul >>foo.xml"}],
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run2",
            "namespace": "default",
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000002",
            "job_origin": ["4ef2c167-1668-4970-a570-000000000001"],
            "step_id": "e7d1ada3-f066-4e28-891b-e670a62c61cb",
            "step_origin": ["4ef2c167-1668-4970-0002-000000000005"],
            "step_sequence_id": 1,
            "channel_id": "1dd99aed-845e-453e-97f5-4d8dabb24c58",
            "creationTimestamp": "2024-08-07T13:45:39.057048",
            "channel_os": "windows",
        },
        "status": 0,
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "job_origin": ["4ef2c167-1668-4970-a570-000000000001"],
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000002",
            "namespace": "default",
            "name": "job_1",
            "step_sequence_id": -2,
            "channel_id": "1dd99aed-845e-453e-97f5-4d8dabb24c58",
            "step_origin_status": {"4ef2c167-1668-4970-0002-000000000005": 2},
            "step_origin": ["4ef2c167-1668-4970-0002-000000000005"],
            "step_id": "4ef2c167-1668-4970-0002-000000000008",
            "step_count": 2,
            "annotations": {
                "opentestfactory.org/hooks": {
                    "channel": "teardown",
                    "keep-workspace": False,
                }
            },
            "creationTimestamp": "2024-08-07T13:45:40.246261",
            "channel_os": "windows",
        },
        "status": 0,
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "job_2",
            "namespace": "default",
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000003",
            "job_origin": ["4ef2c167-1668-4970-a570-000000000001"],
            "step_origin": [],
            "step_sequence_id": -1,
            "step_id": "bab6d96c-17d2-4919-8820-01c903d7df8c",
            "creationTimestamp": "2024-08-07T13:45:40.701024",
            "channel_id": "f18c8d9e-a904-46c8-8da1-7c701dd03204",
            "channelhandler_id": "4336ad74-2512-44e3-944d-71558e4d3453",
            "channel_tags": ["windows", "robotframework", "junit"],
            "channel_os": "windows",
            "channel_temp": "%TEMP%",
            "channel_lease": 60,
        },
        "status": 0,
    },
    {
        "kind": "StartJob",
        "metadata": {
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000003",
        },
        "candidate": {
            "name": "job_2",
            "namespace": "default",
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000003",
            "job_origin": ["4ef2c167-1668-4970-a570-000000000001"],
            "step_origin": [],
            "step_sequence_id": -1,
            "step_id": "bab6d96c-17d2-4919-8820-01c903d7df8c",
            "creationTimestamp": "2024-08-07T13:45:40.701024",
            "channel_id": "f18c8d9e-a904-46c8-8da1-7c701dd03204",
            "channelhandler_id": "4336ad74-2512-44e3-944d-71558e4d3453",
            "channel_tags": ["windows", "robotframework", "junit"],
            "channel_os": "windows",
            "channel_temp": "%TEMP%",
            "channel_lease": 60,
        },
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "ExecutionResult",
        "metadata": {
            "name": "run",
            "namespace": "default",
            "workflow_id": "4ef2c167-1668-4970-a570-7c8b5f1eb223",
            "job_id": "4ef2c167-1668-4970-a570-000000000003",
            "job_origin": ["4ef2c167-1668-4970-a570-000000000001"],
            "step_id": "4ef2c167-1668-4970-0003-000000000009",
            "step_origin": [],
            "step_sequence_id": 0,
            "channel_id": "f18c8d9e-a904-46c8-8da1-7c701dd03204",
            "creationTimestamp": "2024-08-07T13:45:41.269364",
            "channel_os": "windows",
        },
        "status": 0,
        "logs": ["job_2"],
    },
]

WORKFLOW = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {
        "name": "TestSuite Example",
        "workflow_id": "WoRkFlOw_Id",
        "namespace": "naMeSPacE",
    },
    "variables": {"SERVER": "production", "NAME": "FOO ${{ opentf.workflow }}"},
    "defaults": {"runs-on": "dummy"},
    "resources": {
        "testmanagers": [
            {
                "testmanager": "demo",
                "type": "squashtm",
                "name": "MyDemoProject",
                "endpoint": "MySquashTMConnection",
            }
        ],
        "repositories": [
            {
                "name": "common",
                "repository": "SquashTestOrg/CommonTools",
                "type": "bitbucket",
                "endpoint": "MySquashTestOrgConnection",
            },
            {
                "name": "testsuite",
                "repository": "SquashTestOrg/DemoTestSuite",
                "type": "bitbucket",
                "endpoint": "MySquashTestOrgConnection",
            },
        ],
    },
    "jobs": {
        "prepare": {
            "runs-on": "ssh",
            "steps": [
                {
                    "uses": "actions/checkout@v2",
                    "with": {"repository": "${{ common }}"},
                },
                {"run": "ls /"},
                {
                    "run": "echo $PATH $YOLO",
                    "variables": {"SERVER": "yoloserver", "YOLO": "yada"},
                },
            ],
        },
        "ui-tests": {
            "name": "UI tests - Python  matrix.python-version ",
            "needs": "prepare",
            "strategy": {
                "matrix": {"python-version": ["3.6", "3.7", "3.8"]},
                "max-parallel": 2,
            },
            "steps": [
                {
                    "uses": "actions/setup-python@vv2",
                    "with": {"python-version": "${{ matrix.python-version }}"},
                },
                {
                    "uses": "robotframework/robot@v1",
                    "with": {
                        "datasource": "${{ testsuite }}/path/to/robot.txt",
                        "include": "fooANDbar",
                    },
                },
            ],
        },
        "persistence-tests": {
            "name": "Persistence tests",
            "needs": "ui-tests",
            "strategy": {"parallel": 4},
            "timeout-minutes": 1,
            "generator": "${{ demo }}",
            "with": {"test_plan": "foo", "test_suite": "bar", "test_iteration": "baz"},
        },
    },
}

WORKFLOW_WITH_OUTPUTS = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {
        "name": "TestSuite Example",
        "workflow_id": "WoRkFlOw_Id",
        "namespace": "naMeSPacE",
    },
    "variables": {"SERVER": "production", "NAME": "FOO ${{ opentf.workflow }}"},
    "defaults": {"runs-on": "dummy"},
    "resources": {
        "testmanagers": [
            {
                "testmanager": "demo",
                "type": "squashtm",
                "name": "MyDemoProject",
                "endpoint": "MySquashTMConnection",
            }
        ],
        "repositories": [
            {
                "name": "common",
                "repository": "SquashTestOrg/CommonTools",
                "type": "bitbucket",
                "endpoint": "MySquashTestOrgConnection",
            },
            {
                "name": "testsuite",
                "repository": "SquashTestOrg/DemoTestSuite",
                "type": "bitbucket",
                "endpoint": "MySquashTestOrgConnection",
            },
        ],
    },
    "outputs": {"abc": "def", "ghi": "jkl"},
    "jobs": {
        "prepare": {
            "runs-on": "ssh",
            "steps": [
                {
                    "uses": "actions/checkout@v2",
                    "with": {"repository": "${{ common }}"},
                },
                {"run": "ls /"},
                {
                    "run": "echo $PATH $YOLO",
                    "variables": {"SERVER": "yoloserver", "YOLO": "yada"},
                },
            ],
        },
        "ui-tests": {
            "name": "UI tests - Python  matrix.python-version ",
            "needs": "prepare",
            "strategy": {
                "matrix": {"python-version": ["3.6", "3.7", "3.8"]},
                "max-parallel": 2,
            },
            "steps": [
                {
                    "uses": "actions/setup-python@vv2",
                    "with": {"python-version": "${{ matrix.python-version }}"},
                },
                {
                    "uses": "robotframework/robot@v1",
                    "with": {
                        "datasource": "${{ testsuite }}/path/to/robot.txt",
                        "include": "fooANDbar",
                    },
                },
            ],
        },
        "persistence-tests": {
            "name": "Persistence tests",
            "needs": "ui-tests",
            "strategy": {"parallel": 4},
            "timeout-minutes": 1,
            "generator": "${{ demo }}",
            "with": {"test_plan": "foo", "test_suite": "bar", "test_iteration": "baz"},
        },
    },
}

WORKFLOW2 = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {
        "name": "TestSuite Example",
        "workflow_id": "WoRkFlOw_Id",
        "namespace": "naMeSPacE",
    },
    "variables": {
        "SERVER": "production",
        "NAME": "FOO ${{ opentf.workflow }}",
        "user": "UsEr",
        "pwd": "pASSwORD",
    },
    "defaults": {"runs-on": "dummy"},
    "resources": {
        "repositories": [
            {
                "name": "common",
                "repository": "SquashTestOrg/CommonTools",
                "type": "bitbucket",
                "endpoint": "MySquashTestOrgConnection",
            },
            {
                "name": "testsuite",
                "repository": "SquashTestOrg/DemoTestSuite",
                "type": "bitbucket",
                "endpoint": "https://${{ variables.user }}:${{ variables.pwd }}@bitbucket.org/",
            },
        ],
    },
    "jobs": {
        "prepare": {
            "runs-on": "ssh",
            "steps": [
                {
                    "uses": "actions/checkout@v2",
                    "with": {"repository": "${{ common }}"},
                },
                {"run": "ls /"},
                {
                    "run": "echo $PATH $YOLO",
                    "variables": {"SERVER": "yoloserver", "YOLO": "yada"},
                },
            ],
        },
    },
}

WORKFLOW_CHANNEL_HOOKS = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {
        "name": "TestSuite Example",
        "workflow_id": "WoRkFlOw_Id",
        "namespace": "naMeSPacE",
    },
    "variables": {"SERVER": "production", "NAME": "FOO ${{ opentf.workflow }}"},
    "defaults": {"runs-on": "dummy"},
    "jobs": {
        "prepare": {
            "runs-on": "ssh",
            "steps": [
                {
                    "uses": "actions/checkout@v2",
                    "with": {"repository": "${{ common }}"},
                },
                {"run": "ls /"},
                {
                    "run": "echo $PATH $YOLO",
                    "variables": {"SERVER": "yoloserver", "YOLO": "yada"},
                },
            ],
        },
    },
    "hooks": [
        {
            'name': 'nettoyage',
            'events': [{'channel': 'setup'}],
            'before': [
                {'run': "echo 'workflow hook 1 step 1 (before)'"},
                {'run': "echo 'workflow hook 1 step 2 (before)'"},
            ],
            'after': [
                {'run': "echo 'workflow hook 1 step 1 (after)'"},
                {'run': "echo 'workflow hook 1 step 2 (after)'"},
            ],
        },
        {
            'name': 'nettoyage2',
            'events': [{'channel': 'setup'}],
            'after': [
                {'run': "echo 'workflow hook 2 step 1 (after2)'"},
                {'run': "echo 'workflow hook 2 step 2 (after2)'"},
            ],
        },
    ],
}

WORKFLOW_GENERATOR = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {
        "name": "TestSuite Example",
        "workflow_id": "WoRkFlOw_Id",
        "namespace": "naMeSPacE",
    },
    "variables": {"SERVER": "production", "NAME": "FOO ${{ opentf.workflow }}"},
    # "defaults": {"runs-on": "dummy"},
    "jobs": {
        "persistence-tests": {
            "name": "Persistence tests",
            "strategy": {"parallel": 4},
            "timeout-minutes": 1,
            "generator": "foobaR",
            "with": {"test_plan": "foo", "test_suite": "bar", "test_iteration": "baz"},
        },
    },
}


WORKFLOW_GENERATOR_NESTED = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {
        "name": "Nested Generators Example",
        "workflow_id": "WoRkFlOw_Id",
        "namespace": "naMeSPacE",
    },
    "jobs": {
        "first_generator": {
            "generator": "my_generator",
            "with": {
                "second_generator": {
                    "generator": "my_generator",
                    "with": {
                        "first_job": {
                            "runs-on": "linux",
                            "steps": [{"run": "echo 'wroom'"}],
                        }
                    },
                }
            },
        }
    },
}

WORKFLOW_USES = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {
        "name": "Uses Job Example",
        "workflow_id": "WoRkFlOw_Id",
        "namespace": "naMeSPacE",
    },
    "jobs": {
        "uses_job": {
            "runs-on": ["env1"],
            "uses": "http://repo.url/repo#repo/wf.yaml@main",
            "with": {"input_a": "123", "input_b": ""},
        }
    },
}

WORKFLOW_SUBWORKFLOW = {
    "metadata": {"name": "Subworkflow"},
    "inputs": {"input_a": {}, "input_b": {"default": "456"}},
    "jobs": {"subwf_job": {"steps": [{"run": "echo hello"}]}},
    "outputs": {"output1": {"value": "${{ some.output.value }}"}},
}

JOB_DOWNLOAD = {
    "runs-on": [],
    "steps": [
        {
            "uses": "actions/checkout@v2",
            "with": {"repository": "http://repo.url/repo", "ref": "main"},
        },
        {"uses": "actions/get-file@v1", "with": {'path': "repo/wf.yaml"}},
    ],
    "_full_name": "uses_job .download_job",
    "name": ".download_job",
    "metadata": {
        "job_origin": ["a0dc0112-5c10-4807-a728-3c3158e709a1"],
        "download_job": True,
    },
    "needs": set(),
}

WORKFLOW_NORUNSON = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {
        "name": "TestSuite Example",
        "workflow_id": "WoRkFlOw_Id",
        "namespace": "naMeSPacE",
    },
    "variables": {"SERVER": "production", "NAME": "FOO ${{ opentf.workflow }}"},
    "jobs": {
        "prepare": {
            "steps": [
                {
                    "uses": "actions/checkout@v2",
                    "with": {"repository": "${{ common }}"},
                },
                {"run": "ls /"},
                {
                    "run": "echo $PATH $YOLO",
                    "variables": {"SERVER": "yoloserver", "YOLO": "yada"},
                },
            ],
        }
    },
}

PROVIDER_RESULT = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "ProviderResult",
    "metadata": {
        "name": "a step",
        "job_id": "jOb_Id",
        "job_origin": [],
        "step_id": "sTeP_iD",
        "step_origin": [],
        "workflow_id": "WoRkFlOw_Id",
    },
    "steps": [],
}

ACTIONSCHECKOUT_BEFORE_HOOK = [
    {
        'name': 'my pre-checkout hook',
        'events': [{'categoryPrefix': 'actions', 'category': 'checkout'}],
        'before': [{'run': 'echo about to perform a checkout'}],
    }
]

CHANNEL_OFFER_METADATA_SETUP_HOOKS = {
    'channel_id': 'channElId',
    'channel_os': 'macos',
    'channel_temp': '/tmp',
    'annotations': {
        'hooks': [
            {
                'name': 'nettoyage',
                'events': [{'channel': 'setup'}],
                'before': [
                    {'run': "echo 'channel hook 1 step 1 (before)'"},
                    {'run': "echo 'channel hook 1 step 2 (before)'"},
                ],
                'after': [
                    {'run': "echo 'channel hook 1 step 1 (after)'"},
                    {'run': "echo 'channel hook 1 step 2 (after)'"},
                ],
            },
            {
                'name': 'nettoyage2',
                'events': [{'channel': 'setup'}],
                'after': [
                    {'run': "echo 'channel hook 2 step 1 (after2)'"},
                    {'run': "echo 'channel hook 2 step 2 (after2)'"},
                ],
            },
        ]
    },
}

CHANNEL_OFFER_METADATA_SETUP_TEARDOWN_HOOKS = {
    'channel_id': 'channElId',
    'channel_os': 'macos',
    'channel_temp': '/tmp',
    'annotations': {
        'hooks': [
            {
                'name': 'nettoyage',
                'events': [{'channel': 'setup'}],
                'before': [
                    {'run': "echo 'channel hook 1 step 1 (before)'"},
                    {'run': "echo 'channel hook 1 step 2 (before)'"},
                ],
                'after': [
                    {'run': "echo 'channel hook 1 step 1 (after)'"},
                    {'run': "echo 'channel hook 1 step 2 (after)'"},
                ],
            },
            {
                'name': 'nettoyage2',
                'events': [{'channel': 'teardown'}],
                'after': [
                    {'run': "echo 'channel hook 2 step 1 (after2)'"},
                    {'run': "echo 'channel hook 2 step 2 (after2)'"},
                ],
            },
        ]
    },
}


class TestArranger(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)
        cls.patcher = patch('threading.Timer')
        cls.MockTimer = cls.patcher.start()

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)
        cls.patcher.stop()

    # Helpers tests

    def test_scl_1(self):
        metadata = {}
        arranger._set_category_labels(metadata, 'foo/bar@v0')
        self.assertTrue(metadata.get('labels'))
        self.assertEqual(
            metadata['labels']['opentestfactory.org/categoryPrefix'], 'foo'
        )
        self.assertEqual(metadata['labels']['opentestfactory.org/category'], 'bar')
        self.assertEqual(
            metadata['labels']['opentestfactory.org/categoryVersion'], 'v0'
        )

    def test_scl_2(self):
        metadata = {}
        arranger._set_category_labels(metadata, 'foo')
        self.assertTrue(metadata.get('labels'))
        self.assertEqual(metadata['labels']['opentestfactory.org/categoryPrefix'], '-')
        self.assertEqual(metadata['labels']['opentestfactory.org/category'], 'foo')
        self.assertEqual(metadata['labels']['opentestfactory.org/categoryVersion'], '-')

    def test_scl_3(self):
        metadata = {}
        arranger._set_category_labels(metadata, 'foo/bar')
        self.assertTrue(metadata.get('labels'))
        self.assertEqual(
            metadata['labels']['opentestfactory.org/categoryPrefix'], 'foo'
        )
        self.assertEqual(metadata['labels']['opentestfactory.org/category'], 'bar')
        self.assertEqual(metadata['labels']['opentestfactory.org/categoryVersion'], '-')

    def test_eitems_1(self):
        self.assertRaises(
            arranger.ExecutionError, arranger.evaluate_items, '${{ var.baz }}', {}
        )

    def test_eitems_2(self):
        response = arranger.evaluate_items({'foo': 'hello world'}, {})
        self.assertEqual(response, {'foo': 'hello world'})

    def test_eif_1(self):
        self.assertRaises(
            arranger.ExecutionError, arranger.evaluate_if, 'variables.baz == true', {}
        )

    def test_eif_2(self):
        response = arranger.evaluate_if('variables.baz', {'variables': {'baz': True}})
        self.assertTrue(response)

    def test_estr_1(self):
        self.assertRaises(
            arranger.ExecutionError, arranger.evaluate_str, '${{ var.baz }}', {}
        )

    def test_estr_2(self):
        response = arranger.evaluate_str('hello world', {})
        self.assertEqual(response, 'hello world')

    # Workflow class tests

    def test_workflow(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        self.assertEqual(wf.variables['SERVER'], 'production')
        self.assertEqual(wf.variables['NAME'], "FOO TestSuite Example")
        self.assertEqual(wf.manifest['jobs']['prepare']['name'], 'prepare')
        self.assertEqual(
            wf.manifest['jobs']['persistence-tests']['name'], 'Persistence tests'
        )

    def test_workflow_contexts(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        ctxts = wf.get_contexts()
        self.assertIn('opentf', ctxts)
        self.assertIn('resources', ctxts)

    def test_workflow_repositories(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW2))
        ctxts = wf.get_contexts()
        self.assertIn('resources', ctxts)
        self.assertIn('repositories', ctxts['resources'])
        repositories = ctxts['resources']['repositories']
        self.assertIn('testsuite', repositories)
        self.assertIn('/UsEr:', repositories['testsuite']['endpoint'])
        self.assertIn(':pASSwORD@', repositories['testsuite']['endpoint'])

    def test_workflow_workflow_id(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        self.assertEqual(wf.workflow_id, WORKFLOW['metadata']['workflow_id'])

    def test_workflow_needs_1(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        mock_job = MagicMock()
        mock_job.job_id = 'JoB_iD'
        wf.jobs_candidates = {'prepare': mock_job}
        wf.set_job_result('prepare', 'success')
        needs = wf.get_needs_context(['prepare'])
        self.assertIn('prepare', needs)
        self.assertEqual(needs['prepare']['result'], 'success')
        self.assertRaises(KeyError, wf.get_needs_context, 'unknown')

    def test_workflow_needs_2(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        mock_job = MagicMock()
        mock_job.job_id = 'JoB_iD'
        wf.jobs_candidates = {'prepare': mock_job}
        wf.set_job_result('prepare', 'success')
        wf.set_job_output('prepare', 'foo', 'bar')
        needs = wf.get_needs_context(['prepare'])
        self.assertIn('prepare', needs)
        self.assertEqual(needs['prepare']['result'], 'success')
        self.assertEqual(needs['prepare']['outputs']['foo'], 'bar')

    def test_workflow_metadata(self):
        src = WORKFLOW.copy()
        src['metadata'] = src['metadata'].copy()
        src['metadata']['actor'] = 'Who, me?'
        src['metadata']['token'] = 'secret'
        wf = arranger.Workflow(src)
        metadata = wf.get_metadata()
        self.assertNotIn('actor', metadata)
        self.assertIn('name', metadata)
        self.assertNotIn('token', metadata)

    def test_workflow_jobscount(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        org = wf._jobs_count
        wf.inc_jobs_count()
        self.assertEqual(wf._jobs_count, org + 1)

    def test_workflow_jobscount_of(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf._jobs_count = arranger.MAX_JOBS
        self.assertRaises(arranger.ExecutionError, wf.inc_jobs_count)

    def test_workflow_start(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.start_timer = MagicMock()
        wf.reschedule_jobs = MagicMock()
        wf.start()
        wf.start_timer.assert_called_once()
        wf.reschedule_jobs.assert_called_once()

    def test_workflow_start_norunson(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW_NORUNSON))
        wf.start_timer = MagicMock()
        wf.cancel = MagicMock()
        wf.start()
        wf.start_timer.assert_called_once()
        wf.cancel.assert_not_called()
        self.assertEqual(wf.manifest['jobs']['prepare']['runs-on'], [])

    def test_workflow_canceltimer(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        timer = MagicMock()
        wf.timers = {'timer_uuId': timer}
        wf.cancel_timer('timer_uuId')
        self.assertNotIn('timer_uuId', wf.timers)
        timer.cancel.assert_called_once()

    def test_workflow_complete_first(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.phase = arranger.PHASE_RUNNING
        mock_current = {'WoRkFlOw_Id': 1111}
        with (
            patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue,
            patch('opentf.core.arranger.info') as mock_info,
            patch('opentf.core.arranger.CURRENT_WORKFLOWS', mock_current),
        ):
            wf.complete()
        self.assertEqual(wf.phase, arranger.PHASE_COMPLETED)
        self.assertEqual(wf.status, arranger.STATUS_SUCCESS)
        mock_queue.put.assert_called_once()
        mock_info.assert_called_with(
            'Workflow %s %s (workflow_id=%s).',
            'TestSuite Example',
            'completed',
            'WoRkFlOw_Id',
        )
        self.assertNotIn('WoRkFlOw_Id', mock_current)

    def test_workflow_complete_first_outputs(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW_WITH_OUTPUTS))
        wf.phase = arranger.PHASE_RUNNING
        mock_current = {'WoRkFlOw_Id': 1111}
        with (
            patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue,
            patch('opentf.core.arranger.info') as mock_info,
            patch('opentf.core.arranger.CURRENT_WORKFLOWS', mock_current),
        ):
            wf.complete()
        self.assertEqual(wf.phase, arranger.PHASE_COMPLETED)
        self.assertEqual(wf.status, arranger.STATUS_SUCCESS)
        mock_queue.put.assert_called_once()
        mock_info.assert_called_with(
            'Workflow %s %s (workflow_id=%s).',
            'TestSuite Example',
            'completed',
            'WoRkFlOw_Id',
        )
        self.assertNotIn('WoRkFlOw_Id', mock_current)

    def test_workflow_complete_alreadydone(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.phase = arranger.PHASE_COMPLETED
        with patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue:
            wf.complete()
        self.assertEqual(arranger.PHASE_COMPLETED, wf.phase)
        self.assertEqual(arranger.STATUS_SUCCESS, wf.status)
        mock_queue.put.assert_not_called()

    def test_workflow_complete_status_failure(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.status = arranger.STATUS_FAILURE
        mock_current = {'WoRkFlOw_Id': 1111}
        with (
            patch('opentf.core.arranger.make_event') as mock_event,
            patch('opentf.core.arranger.info') as mock_info,
            patch("opentf.core.arranger.CURRENT_WORKFLOWS", mock_current),
        ):
            wf.complete()
        self.assertEqual(arranger.PHASE_COMPLETED, wf.phase)
        self.assertEqual(arranger.STATUS_FAILURE, wf.status)
        mock_event.assert_called_once()
        self.assertEqual(
            'opentestfactory.org/v1/WorkflowCanceled', mock_event.call_args[0][0]
        )
        mock_info.assert_called_once()
        mock_info.assert_called_with(
            'Workflow %s %s (workflow_id=%s).',
            'TestSuite Example',
            'failed',
            'WoRkFlOw_Id',
        )
        self.assertNotIn('WoRkFlOw_Id', mock_current)

    def test_workflow_complete_status_canceled(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.status = arranger.STATUS_CANCELED
        mock_current = {'WoRkFlOw_Id': 1111}
        with (
            patch('opentf.core.arranger.make_event') as mock_event,
            patch('opentf.core.arranger.DISPATCH_QUEUE'),
            patch('opentf.core.arranger.info') as mock_info,
            patch('opentf.core.arranger.CURRENT_WORKFLOWS', mock_current),
        ):
            wf.complete()
        self.assertEqual(arranger.PHASE_COMPLETED, wf.phase)
        self.assertEqual(arranger.STATUS_CANCELED, wf.status)
        mock_event.assert_called_once()
        self.assertEqual(
            'opentestfactory.org/v1/WorkflowCanceled', mock_event.call_args[0][0]
        )
        mock_info.assert_called_once()
        mock_info.assert_called_with(
            'Workflow %s %s (workflow_id=%s).',
            'TestSuite Example',
            'cancelled',
            'WoRkFlOw_Id',
        )
        self.assertNotIn('WoRkFlOw_Id', mock_current)

    def test_workflow_complete_status_unknown(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.status = 'knight errant'
        mock_current = {'WoRkFlOw_Id': 1111}
        with (
            patch('opentf.core.arranger.error') as mock_error,
            patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue,
            patch('opentf.core.arranger.CURRENT_WORKFLOWS', mock_current),
        ):
            wf.complete()
        mock_queue.put.assert_not_called()
        mock_error.assert_called_once()
        self.assertNotIn('WoRkFlOw_Id', mock_current)

    def test_workflow_cancel_first(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs = MagicMock()
        wf.cancel()
        self.assertEqual(wf.status, arranger.STATUS_CANCELED)
        wf.reschedule_jobs.assert_called_once()

    def test_workflow_cancel_reason(self):
        mock_queue = MagicMock()
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs = MagicMock()
        with patch('opentf.core.arranger.DISPATCH_QUEUE', mock_queue):
            wf.cancel(reason='test')
        self.assertEqual(wf.status, arranger.STATUS_CANCELED)
        wf.reschedule_jobs.assert_called_once()
        mock_queue.put.assert_called_once()

    def test_workflow_cancel_timeout(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs = MagicMock()
        wf.timedout = True
        with patch(
            'opentf.core.arranger.channelallocator.require_channels'
        ) as mock_channels:
            wf.cancel()
        self.assertEqual(wf.status, arranger.STATUS_CANCELED)
        mock_channels.assert_called_once()
        wf.reschedule_jobs.assert_called_once()

    def test_workflow_cancel_timeout_runningjobs(self):
        mock_job = MagicMock()
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs = MagicMock()
        wf.timedout = True
        wf.current_jobs = {'jOb_iD': mock_job}
        with patch(
            'opentf.core.arranger.channelallocator.require_channels'
        ) as mock_channels:
            wf.cancel()
        self.assertEqual(wf.status, arranger.STATUS_CANCELED)
        mock_channels.assert_called_once()
        wf.reschedule_jobs.assert_called_once()
        mock_job.cancel.assert_called_once()

    def test_workflow_cancel_already_canceled(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs = MagicMock()
        wf.status = arranger.STATUS_CANCELED
        wf.cancel()
        self.assertEqual(wf.status, arranger.STATUS_CANCELED)
        wf.reschedule_jobs.assert_not_called()

    def test_workflow_cancel_abort_pending_jobs(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs = MagicMock()
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        wf.current_jobs = {'prepare': job}
        wf.cancel()
        self.assertEqual(arranger.STATUS_CANCELED, job.status)
        self.assertEqual(arranger.STATUS_CANCELED, wf.status)
        wf.reschedule_jobs.assert_called_once()

    def test_workflow_cancel_abort_pending_jobs_ko(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs = MagicMock()
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        wf.current_jobs = {'prepare': job}
        with (
            patch(
                'opentf.core.arranger.evaluate_if',
                MagicMock(side_effect=KeyError('Wroom')),
            ),
            patch('opentf.core.arranger.warning') as mock_warning,
        ):
            wf.cancel()
        mock_warning.assert_called_once()
        args = mock_warning.call_args[0][0]
        self.assertIn("Failed to re-evaluate condition for job", args)
        self.assertNotEqual(arranger.STATUS_CANCELED, job.status)
        self.assertEqual(arranger.STATUS_CANCELED, wf.status)

    def test_workflow_fail_first(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs = MagicMock()
        with patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue:
            wf.fail()
        self.assertTrue(arranger.PHASE_PENDING, wf.phase)
        self.assertEqual(arranger.STATUS_FAILURE, wf.status)
        mock_queue.put.assert_called_once()
        wf.reschedule_jobs.assert_called_once()

    def test_workflow_fail_raised(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs = MagicMock()
        with patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue:
            wf.fail(raised=True)
        self.assertEqual(arranger.PHASE_PENDING, wf.phase)
        self.assertEqual(arranger.STATUS_FAILURE, wf.status)
        mock_queue.put.assert_not_called()
        wf.reschedule_jobs.assert_called_once()

    def test_workflow_fail_fail(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.status = arranger.STATUS_FAILURE
        with patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue:
            wf.fail(raised=True)
        self.assertEqual(wf.status, arranger.STATUS_FAILURE)
        mock_queue.assert_not_called()

    def test_workflow_reschedule_jobs_skip_regular(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.results = {'prepare': {'result': 'failure', 'outputs': {}}}
        job = arranger.Job(wf.manifest['jobs']['ui-tests'], wf)
        wf.jobs_candidates['ui-tests'] = job
        wf.candidates.get_next_candidates = MagicMock(side_effect=[{'ui-tests'}, {}])
        wf.reschedule_jobs()
        self.assertEqual(arranger.PHASE_COMPLETED, job.phase)
        self.assertEqual(arranger.STATUS_SKIPPED, wf.results['ui-tests']['result'])

    def test_workflow_reschedule_jobs_executionerror(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.results = {'prepare': {'result': 'failure', 'outputs': {}}}
        job = arranger.Job(wf.manifest['jobs']['ui-tests'], wf)
        wf.jobs_candidates['ui-tests'] = job
        wf.candidates.get_next_candidates = MagicMock(side_effect=[{'ui-tests'}, {}])
        wf.cancel = MagicMock()
        with (
            patch(
                'opentf.core.arranger.evaluate_if',
                MagicMock(side_effect=arranger.ExecutionError('Boom')),
            ),
            patch('opentf.core.arranger.error') as mock_error,
        ):
            wf.reschedule_jobs()
        wf.cancel.assert_called_once_with(reason='Boom')
        mock_error.assert_called_once()

    def test_workflow_callback(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs()
        candidate = wf.jobs_candidates['prepare']
        events_queue = Queue()
        with patch('opentf.core.arranger.EVENTS_QUEUE', events_queue):
            wf.callback(wf.workflow_id, candidate.job_id, candidate.get_metadata())
        self.assertFalse(events_queue.empty())
        event = events_queue.get()
        self.assertEqual(event['kind'], 'StartJob')

    def test_workflow_callback_notfound(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        wf.reschedule_jobs()
        candidate = wf.jobs_candidates['prepare']
        events_queue = Queue()
        with patch('opentf.core.arranger.EVENTS_QUEUE', events_queue):
            wf.callback(wf.workflow_id, 'fooBar', candidate.get_metadata())
        self.assertTrue(events_queue.empty())

    def test_workflow_expand_download_job(self):
        subwf = str(_make_copy(WORKFLOW_SUBWORKFLOW))
        mock_read = MagicMock()
        mock_read.return_value.__enter__.return_value = subwf
        wf = arranger.Workflow(_make_copy(WORKFLOW_USES))
        wf.reschedule_jobs = mock_reschedule = MagicMock()
        candidate = wf.candidates.get_next_candidates()
        job_uses = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_uses.start()
        candidate = wf.candidates.get_next_candidates()
        job_download = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_download.manifest['metadata']['subworkflow_paths'] = ['/path/to/wf.yaml']
        with (
            patch('builtins.open', mock_read),
            patch('opentf.core.arranger.validate_schema', return_value=(True, None)),
            patch('opentf.core.arranger.debug') as mock_debug,
        ):
            wf.expand_download_job(job_download, job_download.name)
        self.assertEqual(4, mock_debug.call_count)
        self.assertEqual(2, mock_reschedule.call_count)
        self.assertIsNotNone(wf.manifest['jobs'].get('uses_job subwf_job'))
        candidate = wf.candidates.get_next_candidates()
        job_subwf = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        self.assertEqual(
            {'input_a': '123', 'input_b': '456'}, job_subwf.contexts['inputs']
        )
        self.assertEqual(
            {'output1': '${{ some.output.value }}'}, job_uses.manifest['outputs']
        )

    def test_workflow_expand_download_job_missing_inputs(self):
        subwf = str(_make_copy(WORKFLOW_SUBWORKFLOW))
        mock_read = MagicMock()
        mock_read.return_value.__enter__.return_value = subwf
        wf_manifest = _make_copy(WORKFLOW_USES)
        del wf_manifest['jobs']['uses_job']['with']['input_a']
        wf = arranger.Workflow(wf_manifest)
        wf.reschedule_jobs = MagicMock()
        candidate = wf.candidates.get_next_candidates()
        job_uses = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_uses.start()
        candidate = wf.candidates.get_next_candidates()
        job_download = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_download.manifest['metadata']['subworkflow_paths'] = ['/path/to/wf.yaml']
        with (
            patch('builtins.open', mock_read),
            patch('opentf.core.arranger.validate_schema', return_value=(True, None)),
            patch('opentf.core.arranger._notify_error') as mock_notify,
        ):
            self.assertFalse(wf.expand_download_job(job_download, job_download.name))
        mock_notify.assert_called_once()
        self.assertIn('input_a', mock_notify.call_args[0][2])

    def test_workflow_expand_download_job_additional_inputs(self):
        subwf = _make_copy(WORKFLOW_SUBWORKFLOW)
        del subwf['inputs']['input_b']
        subwf = str(subwf)
        mock_read = MagicMock()
        mock_read.return_value.__enter__.return_value = subwf
        wf = arranger.Workflow(_make_copy(WORKFLOW_USES))
        wf.reschedule_jobs = MagicMock()
        candidate = wf.candidates.get_next_candidates()
        job_uses = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_uses.start()
        candidate = wf.candidates.get_next_candidates()
        job_download = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_download.manifest['metadata']['subworkflow_paths'] = ['/path/to/wf.yaml']
        with (
            patch('builtins.open', mock_read),
            patch('opentf.core.arranger.validate_schema', return_value=(True, None)),
            patch('opentf.core.arranger._notify_error') as mock_notify,
        ):
            self.assertFalse(wf.expand_download_job(job_download, job_download.name))
        mock_notify.assert_called_once()
        self.assertIn('input_b', mock_notify.call_args[0][2])

    def test_workflow_expand_download_job_load_ko(self):
        mock_read = MagicMock(side_effect=Exception('Boom'))
        wf = arranger.Workflow(_make_copy(WORKFLOW_USES))
        wf.reschedule_jobs = MagicMock()
        candidate = wf.candidates.get_next_candidates()
        job_uses = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_uses.start()
        candidate = wf.candidates.get_next_candidates()
        job_download = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_download.manifest['metadata']['subworkflow_paths'] = ['/path/to/wf.yaml']
        with patch('builtins.open', mock_read):
            self.assertFalse(wf.expand_download_job(job_download, job_download.name))

    def test_workflow_expand_download_job_not_valid(self):
        mock_read = MagicMock()
        mock_read.return_value.__enter__.return_value = 'subwf'
        wf = arranger.Workflow(_make_copy(WORKFLOW_USES))
        wf.reschedule_jobs = MagicMock()
        candidate = wf.candidates.get_next_candidates()
        job_uses = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_uses.start()
        candidate = wf.candidates.get_next_candidates()
        job_download = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_download.manifest['metadata']['subworkflow_paths'] = ['/path/to/wf.yaml']
        with (
            patch('builtins.open', mock_read),
            patch('opentf.core.arranger.validate_schema', return_value=(False, 'Boom')),
        ):
            self.assertFalse(wf.expand_download_job(job_download, job_download.name))

    def test_workflow_expand_download_job_invalid_pipeline(self):
        mock_read = MagicMock()
        mock_read.return_value.__enter__.return_value = 'subwf'
        wf = arranger.Workflow(_make_copy(WORKFLOW_USES))
        wf.reschedule_jobs = MagicMock()
        candidate = wf.candidates.get_next_candidates()
        job_uses = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_uses.start()
        candidate = wf.candidates.get_next_candidates()
        job_download = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job_download.manifest['metadata']['subworkflow_paths'] = ['/path/to/wf.yaml']
        with (
            patch('builtins.open', mock_read),
            patch('opentf.core.arranger.validate_schema', return_value=(True, '')),
            patch(
                'opentf.core.arranger.validate_pipeline', return_value=(False, 'Boom!')
            ),
        ):
            self.assertFalse(wf.expand_download_job(job_download, job_download.name))

    # Job class tests

    def test_job(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        self.assertEqual(job.sequence_id, 0)
        self.assertIn('runs-on', job.contexts['job'])
        self.assertEqual(job.contexts['job']['runs-on'], ['ssh'])

    def test_job_generator_norunson(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW_GENERATOR))
        candidate = wf.candidates.get_next_candidates()
        job = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        self.assertIn('runs-on', job.contexts['job'])
        self.assertEqual(job.contexts['job']['runs-on'], [])

    def test_job_process_uses(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW_USES))
        candidate = wf.candidates.get_next_candidates()
        job = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job.workflow.start_timer = MagicMock()
        job.workflow.add_generated_jobs = mock_add = MagicMock()

        with patch('opentf.core.arranger.DOWNLOAD_JOB_TAGS', 'foo,bar'):
            job.start()
        download = mock_add.call_args[0][1]['.download_job_uses_job']
        self.assertEqual(3, len(download['runs-on']))
        self.assertEqual(
            {
                'uses': 'actions/checkout@v2',
                'with': {'repository': 'http://repo.url/repo', 'ref': 'main'},
            },
            download['steps'][0],
        )
        self.assertEqual(
            {'uses': 'actions/get-file@v1', 'with': {'path': 'repo/wf.yaml'}},
            download['steps'][1],
        )
        self.assertEqual('uses_job .download_job_uses_job', download['_full_name'])
        self.assertTrue(download['metadata']['download_job'])

    def test_job_process_uses_ref_ko(self):
        manifest = _make_copy(WORKFLOW_USES)
        manifest['jobs']['uses_job']['uses'] = 'abcdefghij'
        wf = arranger.Workflow(manifest)
        candidate = wf.candidates.get_next_candidates()
        job = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        job.fail = mock_fail = MagicMock()
        job.start()
        mock_fail.assert_called_once()
        self.assertIn('Incomplete reference provided', mock_fail.call_args[0][0])

    def test_job_outoforder(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        self.assertRaises(
            KeyError, arranger.Job, workflow['jobs']['persistence-tests'], wf
        )

    def test_job_stepscount(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        org = job._steps_count
        job.inc_steps_count()
        self.assertEqual(job._steps_count, org + 1)

    def test_job_stepscount_of(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        job._steps_count = arranger.MAX_STEPS
        self.assertRaises(arranger.ExecutionError, job.inc_steps_count)

    def test_job_cancel_force(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        job.steps_backlog = ['foo', 'bar']
        job.process_next_step = MagicMock()
        job.cancel(force=True)
        self.assertFalse(job.steps_backlog)
        job.process_next_step.assert_called()

    def test_job_cancel_unansweredprovider(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        job.steps_backlog = ['foo', 'bar']
        job._current_step = {'uses': 'foo'}
        job.process_next_step = MagicMock()
        job.cancel()
        self.assertIn('_answered', job._current_step)
        job.process_next_step.assert_called()

    def test_job_cancel_answeredprovider(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        job.steps_backlog = ['foo', 'bar']
        job._current_step = {'uses': 'foo', '_answered': True}
        job.process_next_step = MagicMock()
        job.cancel()
        self.assertIn('_answered', job._current_step)
        job.process_next_step.assert_not_called()

    def test_job_cancel_stack_cancelled(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        job.steps_backlog = ['foo', 'bar']
        job.stack = [
            {'if': 'success()', 'variables': {}, 'status': arranger.STATUS_SUCCESS}
        ]
        job._current_step = {'uses': 'foo', '_answered': True}
        job.process_next_step = MagicMock()
        job.cancel()
        self.assertIn('_answered', job._current_step)
        self.assertEqual(job.stack[0]['status'], arranger.STATUS_CANCELED)
        job.process_next_step.assert_not_called()

    def test_job_cancel_stack_notcancelled(self):
        wf = arranger.Workflow(_make_copy(WORKFLOW))
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        job.steps_backlog = ['foo', 'bar']
        job.stack = [
            {'if': 'always()', 'variables': {}, 'status': arranger.STATUS_SUCCESS}
        ]
        job._current_step = {'uses': 'foo', '_answered': True}
        job.process_next_step = MagicMock()
        job.cancel()
        self.assertIn('_answered', job._current_step)
        self.assertEqual(job.stack[0]['status'], arranger.STATUS_SUCCESS)
        job.process_next_step.assert_not_called()

    # _process_step

    def test_job_process_step_keepworkspace(self):
        workspace = 'mY_wOrsPAce'
        step = {
            'metadata': {'step_origin': []},
            'id': 'sTeP_iD',
            'keep-workspace': workspace,
        }
        wf = arranger.Workflow(_make_copy(WORKFLOW_CHANNEL_HOOKS))
        candidate = wf.candidates.get_next_candidates()
        job = arranger.Job(wf.manifest['jobs'][candidate.pop()], wf)  # type: ignore
        with patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue:
            arranger._process_step(step, job)
        mock_queue.put.assert_not_called()
        self.assertEqual(job.keep_workspace, workspace)

    def test_process_step_outcome(self):
        step = {'metadata': {'step_origin': ['parenT_stEp_iD']}, 'id': 'sTeP_iD'}
        mock_job = MagicMock()
        mock_job.get_contexts = lambda: {}
        mock_job.get_variables = MagicMock(return_value={})
        mock_job._make_special_functions = MagicMock(return_value='fooBaR')
        mock_job.status = arranger.STATUS_SUCCESS
        mock_job.stack = [
            {
                'status': 'sTatUs',
                'steps': ['this is my steps', 'list'],
            }
        ]
        mock_evaluate = MagicMock(side_effect=SystemExit('goOdbYe'))
        with (
            patch('opentf.core.arranger.evaluate_if', MagicMock(return_value=True)),
            patch('opentf.core.arranger._prepare_and_publish_step_event'),
            patch('opentf.core.arranger.evaluate_if', mock_evaluate),
            patch('opentf.core.arranger._get_values', MagicMock(return_value={})),
        ):
            self.assertRaises(SystemExit, arranger._process_step, step, mock_job)
        mock_evaluate.assert_called_once_with(
            'success()',
            {
                'variables': {},
                'job': {'status': 'sTatUs'},
                'steps': ['this is my steps', 'list'],
            },
            special_functions='fooBaR',
        )

    def test_process_step_nested(self):
        step = {'metadata': {'step_origin': ['parenT_stEp_iD']}, 'id': 'sTeP_iD'}
        mock_ppse = MagicMock()
        mock_job = MagicMock()
        mock_job.get_variables = MagicMock(return_value={})
        mock_job.status = arranger.STATUS_SUCCESS
        mock_job.stack = [
            {
                'inputs': {'ab': 12, 'c-d': '13', 'e-f': [1, 2]},
                'status': None,
            }
        ]
        with (
            patch('opentf.core.arranger.evaluate_if', MagicMock(return_value=True)),
            patch('opentf.core.arranger._prepare_and_publish_step_event', mock_ppse),
        ):
            arranger._process_step(step, mock_job)
        self.assertEqual(step['variables']['INPUT_AB'], '12')
        self.assertEqual(
            step['variables']['INPUT_C_D'], {'value': '13', 'verbatim': True}
        )
        self.assertEqual(len(step['variables']), 2)

    def test_process_step_nested_crlf(self):
        step = {'metadata': {'step_origin': ['parenT_stEp_iD']}, 'id': 'sTeP_iD'}
        mock_ppse = MagicMock()
        mock_job = MagicMock()
        mock_job.get_variables = MagicMock(return_value={})
        mock_job.status = arranger.STATUS_SUCCESS
        mock_job.stack = [
            {
                'inputs': {'ab': 12, 'c-d': '13', 'e-f': [1, 2], 'g-h': 'ab\nd'},
                'status': None,
            }
        ]
        with (
            patch('opentf.core.arranger.evaluate_if', MagicMock(return_value=True)),
            patch('opentf.core.arranger._prepare_and_publish_step_event', mock_ppse),
        ):
            arranger._process_step(step, mock_job)
        self.assertEqual(step['variables']['INPUT_AB'], '12')
        self.assertEqual(
            step['variables']['INPUT_C_D'], {'value': '13', 'verbatim': True}
        )
        self.assertNotIn('INPUT_G_H', step['variables'])
        self.assertEqual(len(step['variables']), 2)

    def test_process_step_nested_tab(self):
        step = {'metadata': {'step_origin': ['parenT_stEp_iD']}, 'id': 'sTeP_iD'}
        mock_ppse = MagicMock()
        mock_job = MagicMock()
        mock_job.get_variables = MagicMock(return_value={})
        mock_job.status = arranger.STATUS_SUCCESS
        mock_job.stack = [
            {
                'inputs': {'ab': 12, 'c-d': '13', 'e-f': [1, 2], 'g-h': 'ab\td'},
                'status': None,
            }
        ]
        with (
            patch('opentf.core.arranger.evaluate_if', MagicMock(return_value=True)),
            patch('opentf.core.arranger._prepare_and_publish_step_event', mock_ppse),
        ):
            arranger._process_step(step, mock_job)
        self.assertEqual(step['variables']['INPUT_AB'], '12')
        self.assertEqual(
            step['variables']['INPUT_C_D'], {'value': '13', 'verbatim': True}
        )
        self.assertIn('INPUT_G_H', step['variables'])
        self.assertEqual(len(step['variables']), 3)

    # Job.set_step_output

    def test_job_set_step_output_nostep(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        self.assertRaises(Exception, job.set_step_output, 'foo', 'fOo')

    def test_job_set_step_output(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        step_id = 'sTeP_iD'
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        job.set_step({'run': 'noop', 'id': step_id, 'metadata': {'step_origin': []}})
        job.set_step_output('foo', 'fOo')
        self.assertIn(step_id, job.contexts['steps'])
        self.assertEqual(job.contexts['steps'][step_id]['outputs']['foo'], 'fOo')

    # set_step_outcome

    def test_job_set_step_outcome_nostep(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        self.assertRaises(Exception, job.set_step_outcome, arranger.STATUS_FAILURE)

    def test_job_set_step_outcome(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        step_id = 'sTeP_iD'
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        job.set_step({'run': 'noop', 'id': step_id, 'metadata': {'step_origin': []}})
        with patch('opentf.core.arranger.DISPATCH_QUEUE'):
            job.set_step_outcome(arranger.STATUS_FAILURE)
        self.assertIn(step_id, job.contexts['steps'])
        self.assertEqual(job.status, arranger.STATUS_FAILURE)

    def test_job_set_step_outcome_child_failure(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        step_id = 'sTeP_iD'
        parent_step_id = 'parenT_stEp_iD'
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        job.set_step(
            {
                'run': 'noop',
                'id': step_id,
                'metadata': {'step_origin': [parent_step_id]},
            }
        )
        job.stack = [{'steps': {}, 'status': arranger.STATUS_SUCCESS}]
        with patch('opentf.core.arranger.DISPATCH_QUEUE'):
            job.set_step_outcome(arranger.STATUS_FAILURE)
        self.assertEqual(
            job.stack[-1]['steps'][step_id]['outcome'],
            arranger.STATUS_FAILURE,
        )
        self.assertEqual(
            job.stack[-1]['steps'][step_id]['conclusion'],
            arranger.STATUS_FAILURE,
        )
        self.assertEqual(job.stack[-1]['status'], arranger.STATUS_FAILURE)
        self.assertEqual(job.status, arranger.STATUS_SUCCESS)

    def test_job_set_step_outcome_child_coe_failure(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        step_id = 'sTeP_iD'
        parent_step_id = 'parenT_stEp_iD'
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        job.set_step(
            {
                'run': 'noop',
                'id': step_id,
                'metadata': {'step_origin': [parent_step_id]},
                'continue-on-error': True,
            }
        )
        job.stack = [{'steps': {}, 'status': arranger.STATUS_SUCCESS}]
        with patch('opentf.core.arranger.DISPATCH_QUEUE'):
            job.set_step_outcome(arranger.STATUS_FAILURE)
        self.assertEqual(
            job.stack[-1]['steps'][step_id]['outcome'],
            arranger.STATUS_FAILURE,
        )
        self.assertEqual(
            job.stack[-1]['steps'][step_id]['conclusion'],
            arranger.STATUS_SUCCESS,
        )
        self.assertEqual(job.stack[-1]['status'], arranger.STATUS_SUCCESS)
        self.assertEqual(job.status, arranger.STATUS_SUCCESS)

    def test_job_set_step_outcome_coe(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        step_id = 'sTeP_iD'
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        job.set_step(
            {
                'run': 'noop',
                'id': step_id,
                'metadata': {'step_origin': []},
                'continue-on-error': True,
            }
        )
        with patch('opentf.core.arranger.DISPATCH_QUEUE'):
            job.set_step_outcome(arranger.STATUS_FAILURE)
        self.assertIn(step_id, job.contexts['steps'])
        self.assertEqual(job.status, arranger.STATUS_SUCCESS)

    # assign_channel

    def test_job_assign_channel_annotations_no_hooks(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        job._assign_channel(
            {
                'channel_id': 'channElId',
                'channel_os': 'macos',
                'channel_temp': '/tmp',
                'annotations': {'hooks': []},
            }
        )
        self.assertEqual([], job.teardown_hooks)
        self.assertEqual([], job.steps_backlog)
        self.assertEqual('channElId', job.channel_id)
        self.assertEqual('macos', job.contexts['runner']['os'])
        self.assertEqual('/tmp', job.contexts['runner']['temp'])

    def test_job_assign_channel_no_annotations(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        job._assign_channel(
            {
                'channel_id': 'channElId',
                'channel_os': 'macos',
                'channel_temp': '/tmp',
            }
        )
        self.assertEqual([], job.teardown_hooks)
        self.assertEqual([], job.steps_backlog)
        self.assertEqual('channElId', job.channel_id)
        self.assertEqual('macos', job.contexts['runner']['os'])
        self.assertEqual('/tmp', job.contexts['runner']['temp'])

    def test_job_assign_channel_with_channel_hooks(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        metadata = CHANNEL_OFFER_METADATA_SETUP_HOOKS

        job._assign_channel(metadata)
        self.assertEqual([], job.teardown_hooks)
        self.assertEqual(7, len(job.steps_backlog))
        self.assertEqual(
            metadata['annotations']['hooks'][0]['before'][0]['run'],
            job.steps_backlog[0]['run'],
        )
        self.assertEqual(
            metadata['annotations']['hooks'][1]['after'][1]['run'],
            job.steps_backlog[4]['run'],
        )
        self.assertEqual(
            metadata['annotations']['hooks'][0]['after'][1]['run'],
            job.steps_backlog[6]['run'],
        )
        self.assertEqual('channElId', job.channel_id)
        self.assertEqual('macos', job.contexts['runner']['os'])
        self.assertEqual('/tmp', job.contexts['runner']['temp'])

    def test_job_assign_channel_no_annotations_workflow_hooks(self):
        workflow = _make_copy(WORKFLOW_CHANNEL_HOOKS)
        wf = arranger.Workflow(workflow)
        job = arranger.Job(workflow['jobs']['prepare'], wf)

        job._assign_channel(
            {
                'channel_id': 'channElId',
                'channel_os': 'macos',
                'channel_temp': '/tmp',
            }
        )

        self.assertEqual([], job.teardown_hooks)
        self.assertEqual(7, len(job.steps_backlog))
        self.assertEqual(
            workflow['hooks'][0]['before'][0]['run'],
            job.steps_backlog[0]['run'],
        )
        self.assertEqual(
            workflow['hooks'][1]['after'][1]['run'],
            job.steps_backlog[4]['run'],
        )
        self.assertEqual(
            workflow['hooks'][0]['after'][1]['run'],
            job.steps_backlog[6]['run'],
        )

    def test_job_assign_channel_with_channel_hooks_and_workflow(self):
        workflow = _make_copy(WORKFLOW_CHANNEL_HOOKS)
        wf = arranger.Workflow(workflow)
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        metadata = CHANNEL_OFFER_METADATA_SETUP_TEARDOWN_HOOKS

        job._assign_channel(metadata)

        self.assertEqual(1, len(job.teardown_hooks))  # type: ignore
        self.assertEqual(11, len(job.steps_backlog))
        self.assertEqual(
            metadata['annotations']['hooks'][0]['before'][0]['run'],
            job.steps_backlog[2]['run'],
        )
        self.assertEqual(
            metadata['annotations']['hooks'][0]['after'][1]['run'],
            job.steps_backlog[6]['run'],
        )
        self.assertEqual(
            workflow['hooks'][0]['after'][1]['run'],
            job.steps_backlog[10]['run'],
        )

    def test_job_selfneed(self):
        workflow = _make_copy(WORKFLOW_CHANNEL_HOOKS)
        wf = arranger.Workflow(workflow)
        self.assertRaises(
            arranger.ExecutionError,
            arranger.Job,
            {
                'metadata': {'name': 'aJobThatNeedsItself'},
                'needs': 'aze',
                'name': 'Aze',
                '_full_name': 'aze',
            },
            wf,
        )

    def test_job_need_teardown_no(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        self.assertFalse(job.need_teardown())

    def test_job_need_teardown_yes(self):
        workflow = _make_copy(WORKFLOW)
        wf = arranger.Workflow(workflow)
        job = arranger.Job(workflow['jobs']['prepare'], wf)
        job.teardown_hooks = [
            CHANNEL_OFFER_METADATA_SETUP_TEARDOWN_HOOKS['annotations']['hooks'][1]
        ]
        what = job.need_teardown()
        self.assertTrue(what)
        self.assertIsNone(job.teardown_hooks)
        self.assertEqual(3, len(job.steps_backlog))
        self.assertEqual(
            CHANNEL_OFFER_METADATA_SETUP_TEARDOWN_HOOKS['annotations']['hooks'][1][
                'after'
            ][1]['run'],
            job.steps_backlog[2]['run'],
        )

    def test_job_complete_push_results_to_workflow(self):
        wf = MagicMock()
        wf.manifest = _make_copy(WORKFLOW)
        wf.manifest['timeout-minutes'] = 360
        wf.manifest['jobs']['prepare']['_full_name'] = 'aUniqueUUiD'
        wf.manifest['jobs']['prepare']['name'] = 'prepare'
        wf.manifest['jobs']['prepare']['metadata'] = {}
        wf.manifest['jobs']['prepare']['needs'] = set()
        wf.manifest['jobs']['prepare']['outputs'] = {'foo': 'bar'}
        wf.get_metadata = MagicMock(return_value={'namespace': 'default'})
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        wf.jobs_candidates = {'prepare': job}
        wf.current_jobs = {job.job_id: job}
        job.complete()
        wf.set_job_output.assert_called_once()
        wf.set_job_result.assert_called_once()
        self.assertNotIn(job.job_id, wf.current_jobs)

    def test_job_inherit_timeout(self):
        wf = MagicMock()
        wf.manifest = _make_copy(WORKFLOW)
        wf.manifest['timeout-minutes'] = 1234
        wf.manifest['jobs']['prepare']['_full_name'] = 'aUniqueUUiD'
        wf.manifest['jobs']['prepare']['name'] = 'prepare'
        wf.manifest['jobs']['prepare']['metadata'] = {}
        wf.manifest['jobs']['prepare']['needs'] = set()
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        self.assertEqual(1234, job.manifest['timeout-minutes'])

    # process_workflow

    def test_processworkflow_missing_workflow_id(self):
        body = {"metadata": {"namespace": "myns"}}
        with (
            self.assertRaises(arranger.BadRequestError) as context,
            patch(
                'opentf.core.arranger.validate_schema',
                MagicMock(return_value=(True, '')),
            ),
        ):
            arranger.process_workflow(body, arranger.WORKFLOW)
        self.assertEqual(str(context.exception), 'Missing workflow_id.')

    def test_processworkflow_missing_namespace(self):
        body = {"metadata": {}}
        with (
            self.assertRaises(arranger.BadRequestError) as context,
            patch(
                'opentf.core.arranger.validate_schema',
                MagicMock(return_value=(True, '')),
            ),
        ):
            arranger.process_workflow(body, arranger.WORKFLOW)
        self.assertEqual(str(context.exception), 'Missing namespace.')

    def test_processworkflow_invalid_schema(self):
        body = {"metadata": {"invalid_key": "123"}}
        with (
            self.assertRaises(arranger.BadRequestError) as context,
            patch(
                'opentf.core.arranger.validate_schema',
                MagicMock(return_value=(False, '_yada_')),
            ),
        ):
            arranger.process_workflow(body, arranger.WORKFLOW)
        self.assertEqual(
            str(context.exception),
            'Not a valid opentestfactory.org/v1/Workflow: _yada_.',
        )

    def test_processworkflow_valid_authorization_header(self):
        body = {"metadata": {"workflow_id": "123", "namespace": "myns"}}
        with (
            patch('opentf.core.arranger.request', MagicMock()) as mock_request,
            patch(
                'opentf.core.arranger.get_actor',
                MagicMock(return_value='get_actor_return'),
            ),
            patch(
                'opentf.core.arranger.validate_schema',
                MagicMock(return_value=(True, '')),
            ),
        ):
            mock_request.headers = {'Authorization': 'Bearer 123'}
            arranger.process_workflow(body, arranger.WORKFLOW)
            self.assertEqual(body['metadata']['actor'], 'get_actor_return')
            self.assertEqual(body['metadata']['token'], '123')

    def test_processworkflow_missing_authorization_header(self):
        body = {"metadata": {"workflow_id": "123", "namespace": "myns"}}
        with (
            patch('opentf.core.arranger.request', MagicMock()) as mock_request,
            patch(
                'opentf.core.arranger.validate_schema',
                MagicMock(return_value=(True, '')),
            ),
        ):
            mock_request.headers = {}
            arranger.process_workflow(body, arranger.WORKFLOW)
            self.assertEqual(body['metadata']['actor'], '_localhost_')
            self.assertIsNone(body['metadata']['token'])

    # process_otherevent

    def test_processotherevent_invalid_schema(self):
        body = {"metadata": {"invalid_key": "123"}}
        with (
            self.assertRaises(arranger.BadRequestError) as context,
            patch(
                'opentf.core.arranger.validate_schema',
                MagicMock(return_value=(False, '_yada_')),
            ),
        ):
            arranger.process_otherevent(body, arranger.WORKFLOW)
        self.assertEqual(
            str(context.exception),
            'Not a valid opentestfactory.org/v1/Workflow: _yada_.',
        )

    def test_processotherevent_ok(self):
        workflow_id = 'wOrKflow_iD'
        body = {'metadata': {'workflow_id': workflow_id}}
        cw = {workflow_id: True}
        with (
            patch(
                'opentf.core.arranger.validate_schema',
                MagicMock(return_value=(True, '_yada_')),
            ),
            patch('opentf.core.arranger.CURRENT_WORKFLOWS', cw),
            patch('opentf.core.arranger.EVENTS_QUEUE') as mock_queue,
        ):
            arranger.process_otherevent(body, arranger.WORKFLOW)
            mock_queue.put.assert_called_once()

    def test_processotherevent_notfound(self):
        workflow_id = 'wOrKflow_iD'
        body = {'metadata': {'workflow_id': workflow_id}}
        with (
            patch(
                'opentf.core.arranger.validate_schema',
                MagicMock(return_value=(True, '_yada_')),
            ),
            patch('opentf.core.arranger.CURRENT_WORKFLOWS', {}),
            patch('opentf.core.arranger.EVENTS_QUEUE') as mock_queue,
        ):
            self.assertRaises(
                arranger.NotFoundError,
                arranger.process_otherevent,
                body,
                arranger.WORKFLOW,
            )
            mock_queue.put.assert_not_called()

    # process_workflowresult

    def test_process_workflowresult_ok(self):
        workflow_id = 'wOrKflow_iD'
        body = {'metadata': {'workflow_id': workflow_id}}
        with (
            patch(
                'opentf.core.arranger.validate_schema',
                MagicMock(return_value=(True, '_yada_')),
            ),
            patch('opentf.core.arranger.CURRENT_WORKFLOWS', {workflow_id: {}}),
            patch('opentf.core.arranger.EVENTS_QUEUE') as mock_queue,
        ):
            arranger.process_workflowresult(body, arranger.WORKFLOWRESULT)
        mock_queue.put.assert_called_once()

    def test_process_workflowresult_invalid_schema(self):
        workflow_id = 'wOrKflow_iD'
        body = {'metadata': {'workflow_id': workflow_id}}
        with (
            patch(
                'opentf.core.arranger.validate_schema',
                MagicMock(return_value=(False, '_yada_')),
            ),
            patch('opentf.core.arranger.CURRENT_WORKFLOWS', {workflow_id: {}}),
            patch('opentf.core.arranger.EVENTS_QUEUE') as mock_queue,
        ):
            self.assertRaises(
                arranger.BadRequestError,
                arranger.process_workflowresult,
                body,
                arranger.WORKFLOWRESULT,
            )
        mock_queue.put.assert_not_called()

    # process_generatorresult

    def test_process_generatorresult_ko_nogenerator(self):
        workflow_id = 'wOrKflow_iD'
        job_id = 'jOb_iD'
        body = {
            'kind': 'GeneratorResult',
            'metadata': {
                'name': 'job',
                'job_id': job_id,
                'job_origin': [],
                'workflow_id': workflow_id,
            },
            'jobs': {'job_1': {'runs-on': 'linux'}},
        }
        mock_workflow = MagicMock()
        mock_workflow.current_workflows = {workflow_id: mock_workflow}
        mock_job = MagicMock()
        mock_job.manifest = {'runs-on': ['linux', 'windows']}
        mock_job.full_name = 'JoB_iD'
        mock_job.workflow = MagicMock()
        mock_job.workflow.add_generated_jobs = MagicMock()
        mock_workflow.current_jobs = {job_id: mock_job}
        self.assertRaises(
            arranger.InvalidRequestError,
            arranger._handle_generatorresult,
            mock_workflow,
            body,
        )

    # process_executionresult

    def test_handle_executionresult_notcurrent(self):
        job_id = 'jOb_iD'
        body = {
            'kind': 'ExecutionResult',
            'metadata': {
                'job_id': job_id,
                'step_sequence_id': 0,
                'workflow_id': 'wOrkflow_id',
                'step_origin': [],
                'step_id': 'sTeP_iD',
            },
            'status': 0,
        }
        mock_workflow = MagicMock()
        mock_job = MagicMock()
        mock_job.set_step_outcome = MagicMock()
        mock_job.job_id = job_id
        mock_job.variables = {}
        mock_job.opentf_variables = []
        mock_workflow.current_jobs = {}
        self.assertRaises(
            arranger.InvalidRequestError,
            arranger._handle_executionresult,
            mock_workflow,
            body,
        )

    def test_handle_executionresult_variables(self):
        job_id = 'jOb_iD'
        body = {
            'kind': 'ExecutionResult',
            'metadata': {
                'job_id': job_id,
                'step_sequence_id': 0,
                'workflow_id': 'wOrkflow_id',
                'step_origin': [],
                'step_id': 'sTeP_iD',
            },
            'status': 1,
            'variables': {'foo': 'bar', 'bar': 'foo'},
        }
        mock_workflow = MagicMock()
        mock_job = MagicMock()
        mock_job.set_step_outcome = MagicMock()
        mock_job.job_id = job_id
        mock_job.variables = {}
        mock_job.opentf_variables = []
        mock_workflow.current_jobs = {job_id: mock_job}
        arranger._handle_executionresult(mock_workflow, body)
        self.assertEqual({'foo': 'bar', 'bar': 'foo'}, mock_job.variables)
        self.assertEqual(['foo', 'bar'], mock_job.opentf_variables)

    def test_handle_executionresult_download_job(self):
        job_id = 'jOb_iD'
        body = {
            'kind': 'ExecutionResult',
            'metadata': {
                'job_id': job_id,
                'step_sequence_id': 0,
                'workflow_id': 'wOrkflow_id',
                'step_origin': [],
                'step_id': 'sTeP_iD',
            },
            'status': 1,
            'attachments': ['att1'],
        }
        mock_workflow = MagicMock()
        mock_job = MagicMock()
        mock_job.manifest = {
            'metadata': {'download_job': True, 'subworkflow_paths': []}
        }
        mock_job.set_step_outcome = MagicMock()
        mock_job.job_id = job_id
        mock_workflow.current_jobs = {job_id: mock_job}
        arranger._handle_executionresult(mock_workflow, body)
        self.assertEqual(['att1'], mock_job.manifest['metadata']['subworkflow_paths'])

    def test_process_executionresult_failure(self):
        job_id = 'jOb_iD'
        body = {
            'kind': 'ExecutionResult',
            'metadata': {
                'job_id': job_id,
                'step_sequence_id': 0,
                'workflow_id': 'wOrkflow_id',
                'step_origin': [],
                'step_id': 'sTeP_iD',
            },
            'status': 1,
        }
        mock_workflow = MagicMock()
        mock_job = MagicMock()
        mock_job.set_step_outcome = MagicMock()
        mock_job.job_id = job_id
        mock_workflow.current_jobs = {job_id: mock_job}
        arranger._handle_executionresult(mock_workflow, body)
        mock_job.set_step_outcome.assert_called_once_with(arranger.STATUS_FAILURE)

    # process_providerresult

    def test_process_providerresult_badstep(self):
        mock_workflow = MagicMock()
        job = MagicMock()
        job.get_step = MagicMock(return_value={'id': 'baD_Id'})
        cjs = {'jOb_Id': job}
        self.assertRaises(
            arranger.InvalidRequestError,
            arranger._handle_providerresult,
            mock_workflow,
            PROVIDER_RESULT,
        )

    def test_process_providerresult_nocurrentstep(self):
        mock_workflow = MagicMock()
        job = MagicMock()
        job.get_step = MagicMock(return_value=None)
        cjs = {'jOb_Id': job}
        self.assertRaises(
            arranger.InvalidRequestError,
            arranger._handle_providerresult,
            mock_workflow,
            PROVIDER_RESULT,
        )

    # _handle_workflowresult

    def test_handle_workflowresult_ok(self):
        mock_workflow = MagicMock()
        mock_workflow.artifacts = []
        event = {
            'metadata': {
                'job_id': 'jobid',
                'attachments': {'/tmp/1_WR_1.txt': {}, '/tmp/1_WR_2.txt': {}},
            }
        }
        arranger._handle_workflowresult(mock_workflow, event)
        self.assertEqual(
            ['/tmp/1_WR_1.txt', '/tmp/1_WR_2.txt'], mock_workflow.artifacts
        )

    def test_handle_workflowresult_no_job_id(self):
        mock_workflow = MagicMock()
        mock_workflow.artifacts = []
        event = {'metadata': {'attachments': {'/tmp/1_WR_1.txt': {}}}}
        arranger._handle_workflowresult(mock_workflow, event)
        self.assertEqual([], mock_workflow.artifacts)

    # _add_providersteps

    def _prepend(self, items, destination, step_origin):
        for i, item in enumerate(items):
            if isinstance(item, dict):
                # item.setdefault('id', make_uuid())
                item.setdefault('metadata', {})
                item['metadata'].setdefault('step_origin', step_origin or [])
            destination.insert(i, item)

    def test_add_providersteps_windows_wd_parent(self):
        job = MagicMock()
        job.contexts = {'runner': {'os': 'windows'}}
        steps_backlog = []
        job.prepend_steps = lambda steps, step_origin: self._prepend(
            steps, steps_backlog, step_origin
        )
        job.steps_backlog = steps_backlog
        arranger._add_providersteps(
            job,
            {'working-directory': 'fOo'},
            {'metadata': {'step_origin': [], 'step_id': 'sTeP_iD'}, 'steps': [{}]},
        )
        self.assertEqual(len(job.steps_backlog), 2)
        self.assertEqual(job.steps_backlog[0]['metadata']['step_origin'], ['sTeP_iD'])
        self.assertEqual(job.steps_backlog[0]['working-directory'], 'fOo')

    def test_add_providersteps_windows_wd_parentchild(self):
        job = MagicMock()
        job.contexts = {'runner': {'os': 'windows'}}
        steps_backlog = []
        job.prepend_steps = lambda steps, step_origin: self._prepend(
            steps, steps_backlog, step_origin
        )
        job.steps_backlog = steps_backlog
        arranger._add_providersteps(
            job,
            {'working-directory': 'fOo'},
            {
                'metadata': {'step_origin': [], 'step_id': 'sTeP_iD'},
                'steps': [{'working-directory': 'aBc'}],
            },
        )
        self.assertEqual(len(job.steps_backlog), 2)
        self.assertEqual(job.steps_backlog[0]['metadata']['step_origin'], ['sTeP_iD'])
        self.assertEqual(job.steps_backlog[0]['working-directory'], 'fOo\\aBc')

    def test_add_providersteps_from_channelhooks(self):
        job = MagicMock()
        job.contexts = {'runner': {'os': 'windows'}}
        steps_backlog = []
        job.prepend_steps = lambda steps, step_origin: self._prepend(
            steps, steps_backlog, step_origin
        )
        job.steps_backlog = steps_backlog
        arranger._add_providersteps(
            job,
            {'working-directory': 'fOo'},
            {
                'metadata': {
                    'step_origin': [],
                    'step_id': 'sTeP_iD',
                    'annotations': {'opentestfactory.org/hooks': {'channel': 'setup'}},
                },
                'steps': [{'working-directory': 'aBc'}],
            },
        )
        self.assertEqual(len(job.steps_backlog), 2)
        self.assertEqual(
            'setup',
            job.steps_backlog[0]['metadata']['annotations'][
                'opentestfactory.org/hooks'
            ]['channel'],
        )

    # _apply_provider_hooks

    def test_apply_provider_hooks_nohook(self):
        mock_job = MagicMock()
        with patch('opentf.core.arranger.debug') as mock_debug:
            arranger._apply_provider_hooks([], {}, [], [], {}, mock_job)
        mock_debug.assert_called_once()
        mock_job.assert_not_called()

    def test_apply_provider_hooks_nomatch(self):
        mock_job = MagicMock()
        steps = []
        with patch('opentf.core.arranger.debug') as mock_debug:
            arranger._apply_provider_hooks(
                steps, {}, ACTIONSCHECKOUT_BEFORE_HOOK, [], {}, mock_job
            )
        self.assertFalse(steps)
        mock_job.assert_not_called()

    def test_apply_provider_hooks_nofull(self):
        steps = []
        mock_job = MagicMock()
        with patch('opentf.core.arranger.debug'):
            arranger._apply_provider_hooks(
                steps,
                {
                    'opentestfactory.org/categoryPrefix': 'actions',
                    'opentestfactory.org/category': 'checkout2',
                },
                ACTIONSCHECKOUT_BEFORE_HOOK,
                [],
                {},
                mock_job,
            )
        self.assertFalse(steps)

    def test_apply_provider_hooks_prematch(self):
        mock_job = MagicMock()
        steps = [{'run': 'sTeP'}]
        with patch('opentf.core.arranger.debug') as mock_debug:
            arranger._apply_provider_hooks(
                steps,
                {
                    'opentestfactory.org/categoryPrefix': 'actions',
                    'opentestfactory.org/category': 'checkout',
                },
                ACTIONSCHECKOUT_BEFORE_HOOK,
                [],
                {},
                mock_job,
            )
        self.assertEqual(len(steps), 2)
        self.assertEqual(steps[-1], {'run': 'sTeP'})
        mock_job.assert_not_called()

    def test_apply_provider_hooks_prematch2(self):
        mock_job = MagicMock()
        steps = [{'run': 'sTeP'}]
        with patch('opentf.core.arranger.debug'):
            arranger._apply_provider_hooks(
                steps,
                {
                    'opentestfactory.org/categoryPrefix': 'actions',
                    'opentestfactory.org/category': 'checkout',
                },
                ACTIONSCHECKOUT_BEFORE_HOOK,
                ACTIONSCHECKOUT_BEFORE_HOOK,
                {},
                mock_job,
            )
        self.assertEqual(len(steps), 3)
        self.assertEqual(steps[-1], {'run': 'sTeP'})
        mock_job.assert_not_called()

    # _complete_providerstep

    def test_complete_providerstep_badstep(self):
        step_id = 'sTeP_iD'
        job = MagicMock()
        job.status = arranger.STATUS_SUCCESS
        job.set_status = MagicMock()
        job.contexts = {'steps': {}}
        job.stack = {}
        with patch('opentf.core.arranger.DISPATCH_QUEUE'):
            self.assertRaises(Exception, arranger._complete_providerstep, job)

    def test_complete_providerstep_toplevel_statusok(self):
        step_id = 'sTeP_iD'
        job = MagicMock()
        job.status = arranger.STATUS_SUCCESS
        job.set_step_outcome = MagicMock()
        job.contexts = {'steps': {}}
        job.stack = [
            {
                'parent_id': None,
                'continue-on-error': False,
                'steps': {},
                'status': arranger.STATUS_SUCCESS,
                'parent_step': {},
                'outputs': {},
            }
        ]
        with patch('opentf.core.arranger.DISPATCH_QUEUE'):
            arranger._complete_providerstep(job)
        self.assertNotIn(step_id, job.stack)
        job.set_step_outcome.assert_called_once_with(arranger.STATUS_SUCCESS)

    def test_complete_providerstep_toplevel_statusok_outputs(self):
        step_id = 'sTeP_iD'
        job = MagicMock()
        job.status = arranger.STATUS_SUCCESS
        job.set_step_outcome = MagicMock()
        job.contexts = {'steps': {}}
        job.stack = [
            {
                'parent_id': None,
                'continue-on-error': False,
                'steps': {},
                'status': arranger.STATUS_SUCCESS,
                'parent_step': {},
                'outputs': {'foo': 'bar', 'baz': 'foobar'},
                'inputs': {},
                'variables': {},
            }
        ]
        with patch('opentf.core.arranger.DISPATCH_QUEUE'):
            arranger._complete_providerstep(job)
        self.assertNotIn(step_id, job.stack)
        job.set_step_outcome.assert_called_once_with(arranger.STATUS_SUCCESS)
        job.set_step_output.assert_called()
        self.assertEqual(job.set_step_output.call_count, 2)

    def test_complete_providerstep_toplevel_statusko(self):
        step_id = 'sTeP_iD'
        job = MagicMock()
        job.status = arranger.STATUS_SUCCESS
        job.set_step_outcome = MagicMock()
        job.contexts = {'steps': {}}
        job.stack = [
            {
                'parent_id': None,
                'continue-on-error': False,
                'steps': {},
                'status': arranger.STATUS_FAILURE,
                'parent_step': {},
                'outputs': {},
            }
        ]
        with patch('opentf.core.arranger.DISPATCH_QUEUE'):
            arranger._complete_providerstep(job)
        self.assertNotIn(step_id, job.stack)
        job.set_step_outcome.assert_called_once_with(arranger.STATUS_FAILURE)

    def test_complete_providerstep_toplevel_coe_stillok(self):
        step_id = 'sTeP_iD'
        job = MagicMock()
        job.status = arranger.STATUS_SUCCESS
        job.set_status = MagicMock()
        job.contexts = {'steps': {}}
        job.stack = [
            {
                'parent_id': None,
                'continue-on-error': True,
                'steps': {},
                'status': arranger.STATUS_FAILURE,
                'parent_step': {},
                'outputs': {},
            }
        ]
        with patch('opentf.core.arranger.DISPATCH_QUEUE'):
            arranger._complete_providerstep(job)
        self.assertNotIn(step_id, job.stack)
        job.set_status.assert_not_called()

    # _make_providercommand

    def test_make_providercommand(self):
        step = {'name': 'aNamE', 'nononono': 'never', 'uses': 'abc/def@ghi'}
        metadata = {}
        what = arranger._make_providercommand(step, metadata=metadata, contexts={})
        self.assertIn('name', what['step'])
        self.assertNotIn('nononono', what['step'])
        self.assertIn('apiVersion', what)
        self.assertEqual(metadata['labels']['opentestfactory.org/category'], 'def')
        self.assertEqual(
            metadata['labels']['opentestfactory.org/categoryPrefix'], 'abc'
        )
        self.assertEqual(
            metadata['labels']['opentestfactory.org/categoryVersion'], 'ghi'
        )

    # _prepare_and_publish_step_event

    def test_prepare_and_publish_step_event_ongoing(self):
        job = MagicMock()
        job.starttime = datetime.datetime.now()
        job.timeout = 60
        job.sequence_id = 1
        with patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue:
            arranger._prepare_and_publish_step_event(
                {
                    'run': 'noop',
                    'working-directory': 'workIngDirectORy',
                    'shell': 'myShell {0}',
                },
                {'job_id': 'jOb_iD', 'step_origin': []},
                {},
                job,
            )
        args = mock_queue.put.call_args[0][0]
        self.assertEqual(args['working-directory'], 'workIngDirectORy')

    def test_prepare_and_publish_step_event_artifacts(self):
        wf = MagicMock()
        wf.artifacts = ['/tmp/WFID_WR_1.json', '/tmp/WFID_WR_2.json']
        job = MagicMock()
        job.starttime = datetime.datetime.now()
        job.timeout = 60
        job.sequence_id = 1
        job.workflow_id = 'WFID'
        job.workflow = wf
        with patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue:
            arranger._prepare_and_publish_step_event(
                {
                    'run': 'noop',
                    'working-directory': 'workIngDirectORy',
                    'shell': 'myShell {0}',
                },
                {'job_id': 'jOb_iD', 'step_origin': []},
                {},
                job,
            )
        args = mock_queue.put.call_args[0][0]
        self.assertEqual(args['metadata']['artifacts'], wf.artifacts)

    def test_prepare_and_publish_step_event_useworkspace(self):
        workspace = 'mY_wOrkspACe'
        job = MagicMock()
        job.starttime = datetime.datetime.now()
        job.timeout = 60
        job.sequence_id = 1
        job.workspace = workspace
        with patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue:
            arranger._prepare_and_publish_step_event(
                {
                    'run': 'noop',
                },
                {
                    'job_id': 'jOb_iD',
                    'step_origin': [],
                    'annotations': {
                        'opentestfactory.org/hooks': {
                            'channel': 'setup',
                            'use-workspace': 'no',
                        }
                    },
                },
                {},
                job,
            )
        args = mock_queue.put.call_args[0][0]
        self.assertEqual(
            args['metadata']['annotations']['opentestfactory.org/hooks'][
                'use-workspace'
            ],
            workspace,
        )

    def test_prepare_and_publish_step_event_keepworkspace(self):
        job = MagicMock()
        job.starttime = datetime.datetime.now()
        job.timeout = 60
        job.sequence_id = 1
        job.keep_workspace = False
        with patch('opentf.core.arranger.DISPATCH_QUEUE') as mock_queue:
            arranger._prepare_and_publish_step_event(
                {
                    'run': 'noop',
                },
                {
                    'job_id': 'jOb_iD',
                    'step_origin': [],
                    'annotations': {
                        'opentestfactory.org/hooks': {
                            'channel': 'teardown',
                            'keep-workspace': 'yada',
                        }
                    },
                },
                {},
                job,
            )
        args = mock_queue.put.call_args[0][0]
        self.assertFalse(
            args['metadata']['annotations']['opentestfactory.org/hooks'][
                'keep-workspace'
            ]
        )

    # junk

    def test_timedout(self):
        mock = MagicMock()
        workflow_id = 'wOrKflow_iD'
        cw = {workflow_id: mock}
        with (
            patch('opentf.core.arranger.EVENTS_QUEUE') as mock_eq,
            patch('opentf.core.arranger.CURRENT_WORKFLOWS', cw),
        ):
            arranger.timedout(workflow_id)
        mock_eq.put.assert_called_once()
        self.assertTrue(mock.timedout)

    # misc.

    def test_evaluatestepfields(self):
        step = {'continue-on-error': True}
        with patch('opentf.core.arranger.evaluate_if') as mock_ei:
            arranger._evaluate_step_fields(step, {})
        mock_ei.assert_not_called()
        self.assertTrue(step['continue-on-error'])

    def test_evaluatestepfields_expr(self):
        step = {'continue-on-error': '${{ 1 == 1 }}'}
        arranger._evaluate_step_fields(step, {})
        self.assertTrue(step['continue-on-error'])

    # _prepare_outputs

    def test_prepareoutputs_short(self):
        definition = {'abc': 'def', 'ghi': 'jhl'}
        result = arranger._prepare_outputs(definition, {})
        self.assertEqual(len(result), 2)
        self.assertIn('abc', result)
        self.assertIn('ghi', result)
        self.assertEqual(result['abc'], 'def')

    def test_prepareoutputs_bad(self):
        definition = {'abc': {'value': 'def'}, 'ghi': '${{ bad }}'}
        result = arranger._prepare_outputs(definition, {})
        self.assertEqual(len(result), 2)
        self.assertIn('abc', result)
        self.assertIn('ghi', result)
        self.assertEqual(result['abc'], 'def')
        self.assertIn('could not evaluate expression', result['ghi'])

    # workflow_trace

    def test_workflow_trace_1(self):
        mock_timeout = MagicMock(side_effect=arranger.ServiceError('stop'))
        dispatch_queue = Queue()
        events_queue = Queue()
        for event in EXECUTION_TRACE_1:
            events_queue.put(event)
        events_queue.put(
            {
                'kind': 'TimeoutError',
                'metadata': EXECUTION_TRACE_1[0]['metadata'],
            }
        )
        with (
            patch('opentf.core.arranger._handle_timeouterror', mock_timeout),
            patch('opentf.core.arranger.DISPATCH_QUEUE', dispatch_queue),
            patch('opentf.core.arranger.EVENTS_QUEUE', events_queue),
            patch('opentf.core.arranger.warning', MagicMock(side_effect=SystemExit())),
        ):
            self.assertRaises(SystemExit, arranger.handle_events)
        dispatched = list(dispatch_queue.queue)
        self.assertFalse(any(e['kind'] == 'WorkflowCanceled' for e in dispatched))
        self.assertTrue(any(e['kind'] == 'WorkflowCompleted' for e in dispatched))

    def test_workflow_trace_2(self):
        mock_timeout = MagicMock(side_effect=arranger.ServiceError('stop'))
        dispatch_queue = Queue()
        events_queue = Queue()
        for event in EXECUTION_TRACE_2:
            events_queue.put(event)
        events_queue.put(
            {
                'kind': 'TimeoutError',
                'metadata': EXECUTION_TRACE_2[0]['metadata'],
            }
        )
        with (
            patch('opentf.core.arranger._handle_timeouterror', mock_timeout),
            patch('opentf.core.arranger.DISPATCH_QUEUE', dispatch_queue),
            patch('opentf.core.arranger.EVENTS_QUEUE', events_queue),
            patch('opentf.core.arranger.warning', MagicMock(side_effect=SystemExit())),
        ):
            self.assertRaises(SystemExit, arranger.handle_events)
        dispatched = list(dispatch_queue.queue)
        self.assertTrue(any(e['kind'] == 'WorkflowCanceled' for e in dispatched))
        self.assertFalse(any(e['kind'] == 'WorkflowCompleted' for e in dispatched))

    # _handle_startjob

    def test_handlestartjob_toomany(self):
        event = {'metadata': {'job_id': 'jOb_iD'}, 'candidate': {}}
        job = MagicMock()
        job.job_id = 'jOb_iD'
        job.full_name = 'joBfullNamE'
        job.start = MagicMock(side_effect=arranger.ExecutionError('bOOm'))
        workflow = MagicMock()
        workflow.jobs_candidates = {job.full_name: job}
        arranger._handle_startjob(workflow, event)
        job.start.assert_called_once()
        job.cancel.assert_called_once()
        workflow.cancel.assert_called_once_with(
            reason='Could not start job jOb_iD: bOOm'
        )

    # telemetry

    def test_get_pending_running_wf_ratio(self):
        mock_wf_1 = MagicMock()
        mock_wf_1.phase = 'PENDING'
        mock_wf_2 = MagicMock()
        mock_wf_2.phase = 'RUNNING'
        wfs = {'wf_1': mock_wf_1, 'wf_2': mock_wf_2}
        with patch('opentf.core.arranger.CURRENT_WORKFLOWS', wfs):
            res = arranger._get_pending_running_wf_ratio('_')
        self.assertEqual(0.5, res)

    def test_get_running_pending_jobs_ratio(self):
        mock_wf = MagicMock()
        mock_wf.phase = 'RUNNING'
        mock_wf.current_jobs = {'b': {}}
        job_a = MagicMock()
        job_a.phase = 'PENDING'
        job_b = MagicMock()
        job_b.phase = 'RUNNING'
        mock_wf.jobs_candidates = {'a': job_a, 'b': job_b}
        wfs = {'wf': mock_wf}
        with patch('opentf.core.arranger.CURRENT_WORKFLOWS', wfs):
            res = arranger._get_running_pending_jobs_ratio('_')
        self.assertEqual(0.5, res)

    def test_workflow_phase_pending_running_completed_set_metrics(self):
        with patch('opentf.core.arranger.TELEMETRY_MANAGER') as mock_telemetry:
            wf = arranger.Workflow(_make_copy(WORKFLOW))
            wf.phase = 'RUNNING'
            mock_telemetry.set_metric.assert_called_once()
            self.assertEqual(1, mock_telemetry.set_metric.call_args[0][1])
            wf.phase = 'COMPLETED'
            self.assertEqual(-1, mock_telemetry.set_metric.call_args_list[1][0][1])
            self.assertEqual(1, mock_telemetry.set_metric.call_args_list[2][0][1])

    def test_workflow_reschedule_jobs_set_metrics(self):
        with patch('opentf.core.arranger.TELEMETRY_MANAGER') as mock_telemetry:
            wf = arranger.Workflow(_make_copy(WORKFLOW))
            wf.candidates.get_next_candidates = MagicMock(return_value={'prepare'})
            wf.jobs_candidates = {}
            wf.start_or_require_channels_for_ready_jobs = MagicMock()
            wf.candidates.no_more_jobs = MagicMock(return_value=False)
            wf.reschedule_jobs()
            mock_telemetry.set_metric.assert_called_once()
            self.assertEqual(1, mock_telemetry.set_metric.call_args[0][1])
            self.assertEqual(
                {'workflow_id': 'WoRkFlOw_Id'},
                mock_telemetry.set_metric.call_args[0][2],
            )

    def test_job_pending_running_completed_set_metrics(self):
        with patch('opentf.core.arranger.TELEMETRY_MANAGER') as mock_telemetry:
            workflow = _make_copy(WORKFLOW)
            wf = arranger.Workflow(workflow)
            job = arranger.Job(workflow['jobs']['prepare'], wf)
            job.phase = 'RUNNING'
            metrics = mock_telemetry.set_metric.call_args_list
            self.assertEqual(1, metrics[0][0][1])
            self.assertEqual({'workflow_id': job.workflow_id}, metrics[0][0][2])
            job.phase = 'COMPLETED'
            self.assertEqual(-1, metrics[1][0][1])
            self.assertEqual({'workflow_id': job.workflow_id}, metrics[1][0][2])
            self.assertEqual(1, metrics[2][0][1])
            self.assertEqual({'workflow_id': job.workflow_id}, metrics[2][0][2])

    def test_job_set_step_outcome_set_metrics(self):
        with patch('opentf.core.arranger.TELEMETRY_MANAGER') as mock_telemetry:
            workflow = _make_copy(WORKFLOW)
            wf = arranger.Workflow(workflow)
            job = arranger.Job(workflow['jobs']['prepare'], wf)
            job.set_step(
                {'run': 'foo', 'id': 'sTeP_iD', 'metadata': {'step_origin': []}}
            )
            job.set_step_outcome('SUCCESS')
            metrics = mock_telemetry.set_metric.call_args_list
            self.assertEqual(1, metrics[0][0][1])
            self.assertEqual(
                {'job_id': job.job_id, 'workflow_id': job.workflow_id}, metrics[0][0][2]
            )
            self.assertEqual(-1, metrics[1][0][1])

    def test_job_prepare_and_publish_step_event_set_metrics(self):
        with (
            patch('opentf.core.arranger.TELEMETRY_MANAGER') as mock_telemetry,
            patch('opentf.core.arranger._make_providercommand'),
            patch('opentf.core.arranger._make_executioncommand'),
            patch('opentf.core.arranger.DISPATCH_QUEUE.put'),
        ):
            workflow = _make_copy(WORKFLOW)
            wf = arranger.Workflow(workflow)
            job = arranger.Job(workflow['jobs']['prepare'], wf)
            job.phase = 'RUNNING'
            job.workflow.start_timer = MagicMock()
            arranger._prepare_and_publish_step_event({'run': ''}, {}, {}, job)
            metrics = mock_telemetry.set_metric.call_args_list
            self.assertEqual(1, metrics[1][0][1])
            self.assertEqual(
                {'job_id': job.job_id, 'workflow_id': job.workflow_id}, metrics[1][0][2]
            )
            self.assertEqual(1, metrics[2][0][1])

    def test_handle_workflow_set_metrics(self):
        with (
            patch('opentf.core.arranger.TELEMETRY_MANAGER') as mock_telemetry,
            patch('opentf.core.arranger.Workflow'),
        ):
            arranger._handle_workflow('wf_id', {})
        mock_telemetry.set_metric.assert_called_once()
        self.assertEqual(1, mock_telemetry.set_metric.call_args[0][1])

    # main

    def test_main_preparation_thread_fail(self):
        mock_svc = MagicMock()
        mock_threading = MagicMock()
        mock_threading.Thread = MagicMock(side_effect=Exception('ooOps'))
        mock_fatal = MagicMock(side_effect=SystemExit('ooOps'))
        with (
            patch('opentf.core.arranger.threading', mock_threading),
            patch('opentf.core.arranger.fatal', mock_fatal),
        ):
            self.assertRaises(SystemExit, arranger.main, mock_svc)
        mock_fatal.assert_called_once()


if __name__ == '__main__':
    unittest.main()
