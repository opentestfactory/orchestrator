# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Cucumber Provider unit tests.

- execute tests
- cucumber tests
"""

import os
import unittest
import sys

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.cucumber import main, implementation

import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    'contexts': {'runner': {'os': 'linux'}},
    'step': {'uses': 'foo'},
    'runs-on': ['linux'],
}

PROVIDERMANIFESTCACHE = {
    (None, 'execute', None): ({'test': {'required': True}}, False),
    (None, 'params', None): (
        {
            'data': {
                'description': 'The data to use for the automated test.',
                'required': True,
            },
            'format': {
                'description': 'The format to use for the automated test data.',
                'required': True,
            },
        },
        True,
    ),
}


########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestCucumber(unittest.TestCase):
    """
    The class containing the Cucumber provider unit test methods.
    """

    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONTEXT'][opentf.toolkit.INPUTS_KEY] = PROVIDERMANIFESTCACHE
        app.config['CONTEXT'][opentf.toolkit.OUTPUTS_KEY] = {}
        self.app = app.test_client()
        self.assertFalse(app.debug)

    ###########################
    # Tests for Cucumber < 5

    # execute

    def test_execute_nok(self):
        """
        In a Cucumber < 5 context, checks the error message content when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'cucumber/execute'}
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn("'test'", msg)

    def test_execute_ok(self):
        """
        In a Cucumber < 5 context, checks the presence of 4 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/execute',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature'
                },
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)

    def test_execute_with_3_sharps_ok(self):
        """
        In a Cucumber < 5 context, checks the presence of 4 steps when execute
        action is called with 3 sharps mandatory in the test reference..
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/execute',
                'with': {
                    'test': 'cucumberProject#src/test/java/features/sample.feature##'
                },
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)

    # def test_execute_ok_wd(self):
    #     mock = MagicMock(return_value=mockresponse)
    #     with patch('opentf.toolkit.core.publish', mock):
    #         command = PROVIDERCOMMAND.copy()
    #         command['step'] = {
    #             'uses': 'cucumber/execute',
    #             'with': {
    #                 'test': 'cucumberProject/src/test/java/features/sample.feature'
    #             },
    #             'working-directory': 'foo',
    #         }
    #         _dispatch(implementation.execute_action, command)
    #     mock.assert_called_once()
    #     steps = mock.call_args[0][0]['steps']
    #     self.assertTrue(len(steps) == 4)
    #     self.assertEqual(steps[0]['working-directory'], 'foo')
    #     self.assertEqual(steps[1]['working-directory'], 'foo')
    #     self.assertEqual(steps[2]['working-directory'], 'foo/cucumberProject/target')
    #     self.assertEqual(steps[3]['working-directory'], 'foo')

    def test_execute_extra_options_win(self):
        """
        In a Cucumber < 5/Windows context, checks the presence of the
        CUCUMBER_EXTRA_OPTIONS environment variable in the test execution
        command.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/execute',
                'with': {
                    'test': 'cucumberProject#src/test/java/features/sample.feature##'
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        self.assertIn(
            '%SQUASH_TF_CUCUMBER_TEST_TAG% %CUCUMBER_EXTRA_OPTIONS%"', steps[1]['run']
        )

    def test_execute_extra_options_lin(self):
        """
        In a Cucumber < 5/Linux context, checks the presence of the
        CUCUMBER_EXTRA_OPTIONS environment variable in the test execution
        command.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/execute',
                'with': {
                    'test': 'cucumberProject#src/test/java/features/sample.feature##'
                },
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        self.assertIn(
            '$SQUASH_TF_CUCUMBER_TEST_TAG $CUCUMBER_EXTRA_OPTIONS"', steps[1]['run']
        )

    # cucumber

    def test_cucumber_nok(self):
        """
        In a Cucumber < 5 context, checks the error message content when native
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'cucumber/cucumber'}
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn("'test'", msg)

    def test_cucumber_nok_unknownreport(self):
        """
        In a Cucumber < 5 context, checks the error message content when a
        non-existing reporter name is provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit', 'foobar'],
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn('Unexpected reporter', msg)

    def test_cucumber_ok_noreport(self):
        """
        In a Cucumber < 5 context, checks the length and purpose of steps when
        no report in included within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature'
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        self.assertEqual('', steps[0]['run'])
        self.assertNotEqual('', steps[1]['run'])

    def test_cucumber_ok_junit(self):
        """
        In a Cucumber < 5 context, checks the presence of the XML report when
        JUnit report in included within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit'],
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        self.assertEqual('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertEqual(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"',
            steps[2]['run'],
        )

    def test_cucumber_ok_junithtml(self):
        """
        In a Cucumber < 5 context, checks the presence of the XML and HTML report
        files when both reporters are provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit', 'html'],
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        self.assertIn('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertIn('rm -rf cucumberProject/target/html-report.html', steps[0]['run'])
        self.assertIn('rm -f cucumberProject/target/html-report.tar', steps[0]['run'])
        attach_xml = 'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"'
        self.assertTrue(attach_xml in steps[2]['run'] or attach_xml in steps[3]['run'])
        attach_html = 'echo "::attach::`pwd`/cucumberProject/target/html-report"'
        self.assertFalse(
            attach_html in steps[2]['run'] or attach_html in steps[3]['run']
        )
        attach_html_report = (
            'echo "::attach::`pwd`/cucumberProject/target/html-report.tar"'
        )
        self.assertTrue(
            attach_html_report in steps[2]['run']
            or attach_html_report in steps[3]['run']
        )

    def test_cucumber_with_3_sharps_ok_junithtml(self):
        """
        In a Cucumber < 5 context, checks the presence of the XML and HTML report
        files when both reporters are provided within the PEAC
        when 3 sharps mandatory exists in the test reference.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit', 'html'],
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        self.assertIn('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertIn('rm -rf cucumberProject/target/html-report.html', steps[0]['run'])
        self.assertIn('rm -f cucumberProject/target/html-report.tar', steps[0]['run'])
        attach_xml = 'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"'
        self.assertTrue(attach_xml in steps[2]['run'] or attach_xml in steps[3]['run'])
        attach_html = 'echo "::attach::`pwd`/cucumberProject/target/html-report"'
        self.assertFalse(
            attach_html in steps[2]['run'] or attach_html in steps[3]['run']
        )
        attach_html_report = (
            'echo "::attach::`pwd`/cucumberProject/target/html-report.tar"'
        )
        self.assertTrue(
            attach_html_report in steps[2]['run']
            or attach_html_report in steps[3]['run']
        )

    def test_cucumber_ok_tags(self):
        """
        In a Cucumber < 5 context, checks the presence of the tag filter syntax
        within the launch step, along with the tag described in the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit'],
                    'tag': 'TaG1',
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        self.assertEqual('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertIn(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"',
            steps[2]['run'],
        )
        self.assertIn(' --tags @TaG1"', steps[1]['run'])

    def test_cucumber_nok_tags_tag_expression(self):
        """
        In a Cucumber < 5 context, checks the error message content when both
         'tag' and 'tags' fields are provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'tag': 'TaG1',
                    'tags': '@TaG1 and @TaG2',
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn('fields cannot coexist', msg)

    def test_param_linux_nok_unsupported_char_exists_in_dataset_value(self):
        """
        In a Cucumber < 5 context, checks the error message content when
        an unsupported character exists in a 'tag' field on a Linux execution
        environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.cucumber.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/params',
                'with': {
                    'data': {
                        'test': {'DSNAME': 'tag3"``"tag4'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertTrue(
            steps[0]['run'].startswith(
                '''echo "::error::The dataset name includes a character that the Cucumber provider does not support'''
            )
        )

    def test_param_windows_nok_unsupported_char_exists_in_dataset_value(self):
        """
        In a Cucumber < 5 context, checks the error message content when
        an unsupported character exists in a 'tag' field on a Windows execution
        environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.cucumber.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/params',
                'with': {
                    'data': {
                        'test': {'DSNAME': 'tag3(^_^)tag4'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertTrue(
            steps[0]['run'].startswith(
                "echo ::error::The dataset name includes a character that the Cucumber provider does not support"
            )
        )

    ###########################
    # Tests for Cucumber 5+

    # 'execute' action

    def test_execute5_nok(self):
        """
        In a Cucumber 5+ context, checks the error message content when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'cucumber5/execute'}
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn("'test'", msg)

    def test_execute5_ok(self):
        """
        In a Cucumber 5+ context, checks the presence of 4 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/execute',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature'
                },
            }
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)

    def test_execute5_quote_old(self):
        """
        In a Cucumber 5+ context, checks the presence of 4 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/execute',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature#nom d\'une feature'
                },
            }
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        self.assertIn('-f "cucumberProject/pom.xml"', steps[1]['run'])
        self.assertNotIn('une feature', steps[1]['run'])
        self.assertEqual(steps[2]['working-directory'], 'cucumberProject')

    def test_execute5_quote(self):
        """
        In a Cucumber 5+ context, checks the presence of 4 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/execute',
                'with': {
                    'test': 'full/path/to/cucumberProject#src/test/java/features/sample.feature#nom d\'une feature#scenario'
                },
            }
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        self.assertIn('-f "full/path/to/cucumberProject/pom.xml"', steps[1]['run'])
        self.assertNotIn('une feature', steps[1]['run'])
        self.assertEqual(steps[2]['working-directory'], 'full/path/to/cucumberProject')

    def test_execute5_doublequote_old(self):
        """
        In a Cucumber 5+ context, checks the presence of 4 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/execute',
                'with': {
                    'test': 'cucumberstringtest/src/test/resources/stringtest/case.feature#Strings are "uppercasable'
                },
            }
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        self.assertIn('-f "cucumberstringtest/pom.xml"', steps[1]['run'])
        self.assertNotIn('uppercasable', steps[1]['run'])
        self.assertEqual(steps[2]['working-directory'], 'cucumberstringtest')

    def test_execute5_doublequote(self):
        """
        In a Cucumber 5+ context, checks the presence of 4 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/execute',
                'with': {
                    'test': 'full/path/to/cucumberProject#src/test/java/features/sample.feature#nom d"une feature#scenario'
                },
            }
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        self.assertIn('-f "full/path/to/cucumberProject/pom.xml"', steps[1]['run'])
        self.assertEqual(steps[2]['working-directory'], 'full/path/to/cucumberProject')

    def test_execute5_with_3_sharps_ok(self):
        """
        In a Cucumber 5+ context, checks the presence of 4 steps when execute
        action is called with 3 sharps mandatory in the test reference.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/execute',
                'with': {
                    'test': 'cucumberProject#src/test/java/features/sample.feature##'
                },
            }
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)

    def test_execute5_extra_options_win(self):
        """
        In a Cucumber 5+/Windows context, checks the presence of the
        CUCUMBER_EXTRA_OPTIONS environment variable in the test execution
        command.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/execute',
                'with': {
                    'test': 'cucumberProject#src/test/java/features/sample.feature##'
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        self.assertIn(
            '%SQUASH_TF_CUCUMBER_TEST_TAG% %CUCUMBER_EXTRA_OPTIONS%', steps[1]['run']
        )

    def test_execute5_extra_options_lin(self):
        """
        In a Cucumber 5+/Linux context, checks the presence of the
        CUCUMBER_EXTRA_OPTIONS environment variable in the test execution
        command.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/execute',
                'with': {
                    'test': 'cucumberProject#src/test/java/features/sample.feature##'
                },
            }
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        self.assertIn(
            '$SQUASH_TF_CUCUMBER_TEST_TAG $CUCUMBER_EXTRA_OPTIONS', steps[1]['run']
        )

    # 'cucumber5' action

    def test_cucumber5_nok(self):
        """
        In a Cucumber 5+ context, checks the error message content when native
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'cucumber5/cucumber'}
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn("'test'", msg)

    def test_cucumber5_nok_unknownreport(self):
        """
        In a Cucumber 5+ context, checks the error message content when a
        non-existing reporter name is provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit', 'foobar'],
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn('Unexpected reporter', msg)

    def test_cucumber5_ok_noreport(self):
        """
        In a Cucumber 5+ context, checks the length and purpose of steps when
        no report in included within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature'
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        self.assertEqual('', steps[0]['run'])
        self.assertNotEqual('', steps[1]['run'])

    def test_cucumber5_ok_junit(self):
        """
        In a Cucumber 5+ context, checks the presence of the XML report when
        JUnit report in included within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit'],
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        self.assertEqual('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertEqual(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"',
            steps[2]['run'],
        )

    def test_cucumber5_ok_junithtml(self):
        """
        In a Cucumber 5+ context, checks the presence of the XML and HTML report
        files when both reporters are provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit', 'html'],
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        step_0_run = steps[0]['run']
        self.assertIn('rm -f cucumberProject/target/report.xml', step_0_run)
        self.assertIn('rm -f cucumberProject/target/html-report.html', step_0_run)
        self.assertIn('rm -f cucumberProject/target/html-report.tar', step_0_run)
        attach_xml = 'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"'
        self.assertTrue(attach_xml in steps[2]['run'] or attach_xml in steps[3]['run'])
        attach_html = 'echo "::attach::`pwd`/cucumberProject/target/html-report"'
        self.assertFalse(
            attach_html in steps[2]['run'] or attach_html in steps[3]['run']
        )
        attach_tar = 'echo "::attach::`pwd`/cucumberProject/target/html-report.tar"'
        self.assertTrue(attach_tar in steps[2]['run'] or attach_tar in steps[3]['run'])

    def test_cucumber5_with_3_sharps_ok_junithtml(self):
        """
        In a Cucumber 5+ context, checks the presence of the XML and HTML report
        files when both reporters are provided within the PEAC
        when 3 sharps mandatory exists in the test reference.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject#src/test/java/features/sample.feature##',
                    'reporters': ['junit', 'html'],
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 4)
        step_0_run = steps[0]['run']
        self.assertIn('rm -f cucumberProject/target/report.xml', step_0_run)
        self.assertIn('rm -f cucumberProject/target/html-report.html', step_0_run)
        self.assertIn('rm -f cucumberProject/target/html-report.tar', step_0_run)
        attach_xml = 'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"'
        self.assertTrue(attach_xml in steps[2]['run'] or attach_xml in steps[3]['run'])
        attach_html = 'echo "::attach::`pwd`/cucumberProject/target/html-report"'
        self.assertFalse(
            attach_html in steps[2]['run'] or attach_html in steps[3]['run']
        )
        attach_html_report = (
            'echo "::attach::`pwd`/cucumberProject/target/html-report.tar"'
        )
        self.assertTrue(
            attach_html_report in steps[2]['run']
            or attach_html_report in steps[3]['run']
        )

    def test_cucumber5_ok_tags(self):
        """
        In a Cucumber 5+ context, checks the presence of the tag filter syntax
        within the launch step, along with the tag described in the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit'],
                    'tag': 'TaG1',
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        self.assertEqual('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertIn(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"',
            steps[2]['run'],
        )
        self.assertIn('-Dcucumber.filter.tags=\"@TaG1\"', steps[1]['run'])

    def test_cucumber5_ok_tag_expression(self):
        """
        In a Cucumber 5+ context, checks the presence of the tag filter syntax
        within the launch step, along with the tag described in the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit'],
                    'tags': '@TaG1 and @TaG2',
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        self.assertEqual('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertIn(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"',
            steps[2]['run'],
        )
        self.assertIn('-Dcucumber.filter.tags=\"@TaG1 and @TaG2\"', steps[1]['run'])

    def test_cucumber5_nok_tags_tag_expression(self):
        """
        In a Cucumber 5+ context, checks the error message content when both
         'tag' and 'tags' fields are provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'tag': 'TaG1',
                    'tags': '@TaG1 and @TaG2',
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn('fields cannot coexist', msg)

    def test_param_linux_ok(self):
        """
        Checks the runner command with params when param action is called
        on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.cucumber.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        step_0_uses = steps[0]['uses']
        step_0_with = steps[0]['with']
        self.assertIn('actions/create-file@v1', step_0_uses)
        self.assertIn('strange_value', step_0_with['data']['global']['firstGlobal'])
        self.assertIn('value1', step_0_with['data']['test']['key1'])
        self.assertIn('echo export _SQUASH_TF_TESTCASE_PARAM_FILES=', steps[2]['run'])
        self.assertIn(
            'echo export _SQUASH_TF_TESTCASE_PARAM_FILES_ABSOLUTE=$OPENTF_WORKSPACE/',
            steps[2]['run'],
        )

    def test_param_windows_ok(self):
        """
        Checks the runner command with params when param action is called
        on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.cucumber.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        step_0_uses = steps[0]['uses']
        step_0_with = steps[0]['with']
        self.assertIn('actions/create-file@v1', step_0_uses)
        self.assertIn('strange_value', step_0_with['data']['global']['firstGlobal'])
        self.assertIn('value1', step_0_with['data']['test']['key1'])
        self.assertIn('@echo set "_SQUASH_TF_TESTCASE_PARAM_FILES=', steps[2]['run'])
        self.assertIn(
            '@echo set "_SQUASH_TF_TESTCASE_PARAM_FILES_ABSOLUTE=%OPENTF_WORKSPACE%\\',
            steps[2]['run'],
        )

    def test_param_invalid_format_nok(self):
        """
        Checks the runner command with an invalid format param
        when param action is called.
        """
        with patch(
            'opentf.plugins.cucumber.implementation.core.validate_params_inputs',
            MagicMock(side_effect=SystemExit(2)),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'invalid_format/params@v1',
                },
            }
            self.assertRaisesRegex(
                SystemExit, '2', _dispatch, implementation.param_action, command
            )

    def test_param5_linux_nok_unsupported_char_exists_in_dataset_value(self):
        """
        In a Cucumber 5+ context, checks the error message content when
        an unsupported character exists in a 'tag' field on a Linux execution
        environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.cucumber.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/params',
                'with': {
                    'data': {
                        'test': {'DSNAME': 'tag3@@@@ tag4'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            _dispatch(implementation.param5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertTrue(
            steps[0]['run'].startswith(
                '''echo "::error::The dataset name includes a character that the Cucumber provider does not support'''
            )
        )

    def test_param5_windows_nok_unsupported_char_exists_in_dataset_value(self):
        """
        In a Cucumber 5+ context, checks the error message content when
        an unsupported character exists in a 'tag' field on a Windows execution
        environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.cucumber.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/params',
                'with': {
                    'data': {
                        'test': {'DSNAME': 'tag3__"""tag4'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.param5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertTrue(
            steps[0]['run'].startswith(
                "echo ::error::The dataset name includes a character that the Cucumber provider does not support"
            )
        )

    def test_param_linux_nok_specific_unsupported_char(self):
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.cucumber.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/params',
                'with': {
                    'data': {
                        'test': {'DSNAME': 'I&|&I'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            command['runs-on'] = ['linux']
            _dispatch(implementation.param_action, command)
            mock.assert_called_once()
            steps = mock.call_args[0][0]
            self.assertTrue(
                steps[0]['run'].startswith(
                    'echo "::error::The dataset name includes a character that the Cucumber provider does not support'
                )
            )

    def test_param_windows_nok_unsupported_pattern(self):
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.cucumber.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/params',
                'with': {
                    'data': {
                        'test': {'DSNAME': '%variable%'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.param_action, command)
            mock.assert_called_once()
            steps = mock.call_args[0][0]
            self.assertTrue(
                steps[0]['run'].startswith(
                    'echo ::error::The dataset name contains a pattern which is not supported in Cucumber tags'
                )
            )


if __name__ == '__main__':
    unittest.main()
