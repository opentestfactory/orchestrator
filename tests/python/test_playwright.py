# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Playwright Provider unit tests."""

import os
import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.playwright import main, implementation
import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200

########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    "contexts": {'runner': {'os': 'linux'}},
    "step": {'uses': 'foo'},
    "runs-on": "linux",
}

PROVIDERMANIFESTCACHE = {
    ('playwright', 'execute', None): ({'test': {'required': True}}, False),
    ('playwright', 'params', None): (
        {
            'data': {
                'description': 'The data to use for the automated test.',
                'required': True,
            },
            'format': {
                'description': 'The format to use for the automated test data.',
                'required': True,
            },
        },
        True,
    ),
}

########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestPlaywright(unittest.TestCase):
    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONTEXT'][opentf.toolkit.INPUTS_KEY] = PROVIDERMANIFESTCACHE
        app.config['CONTEXT'][opentf.toolkit.OUTPUTS_KEY] = {}
        self.app = app.test_client()
        self.assertFalse(app.debug)

    def tearDown(self):
        PROVIDERCOMMAND['contexts']['runner']['os'] = 'linux'

    # execute

    def test_execute_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/execute',
                'with': {'test': 'pwProject/gui/set1#example.spec.ts'},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(3, len(steps))
        run = steps[0]['run']
        self.assertIn(
            'test pwProject/gui/set1/example.spec.ts --reporter=html,junit', run
        )

    def test_execute_ok_quote_win(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/execute',
                'with': {'test': 'pwwin/cli/set2#example*#grep test'},
            }
            command['contexts']['runner']['os'] = 'windows'
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(3, len(steps))
        run = steps[0]['run']
        self.assertIn('test pwwin/cli/set2/example* --grep="grep test"', run)

    def test_execute_ok_quote_lin(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/execute',
                'with': {'test': 'pwlin/zshell/set2#example*#grep test'},
            }
            command['contexts']['runner']['os'] = 'linux'
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(3, len(steps))
        run = steps[0]['run']
        self.assertIn('test pwlin/zshell/set2/example* --grep=\'grep test\'', run)

    def test_execute_ok_quoted_quote_lin(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/execute',
                'with': {'test': 'pwlin/zshell/set2#example*#"grep test"'},
            }
            command['contexts']['runner']['os'] = 'linux'
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(3, len(steps))
        run = steps[0]['run']
        self.assertIn('test pwlin/zshell/set2/example* --grep=\'grep test\'', run)

    def test_execute_ok_var_env_win(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/execute',
                'with': {'test': 'pwwin/cli/set3#*'},
            }
            command['contexts']['runner']['os'] = 'windows'
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(3, len(steps))
        run = steps[0]['run']
        self.assertIn(
            'set PW_TEST_HTML_REPORT_OPEN=never\nset PLAYWRIGHT_HTML_REPORT=playwright-report\nset PLAYWRIGHT_JUNIT_OUTPUT_NAME=playwright-report/pw_junit_report.xml\nnpx',
            run,
        )
        self.assertIn('test pwwin/cli/set3/*', run)

    def test_execute_ok_var_env_lin(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/execute',
                'with': {'test': 'pwlin/shell/set4#foo.spec.ts'},
            }
            command['contexts']['runner']['os'] = 'linux'
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(3, len(steps))
        run = steps[0]['run']
        self.assertIn(
            'export PW_TEST_HTML_REPORT_OPEN=never\nexport PLAYWRIGHT_HTML_REPORT=playwright-report\nexport PLAYWRIGHT_JUNIT_OUTPUT_NAME=playwright-report/pw_junit_report.xml\nnpx',
            run,
        )

    # param

    def test_param_ok(self):
        mock = MagicMock(return_value=mockresponse)
        mock_export = MagicMock()
        mock_validate = MagicMock()
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.playwright.implementation.core.export_variables',
                mock_export,
            ),
            patch(
                'opentf.plugins.playwright.implementation.core.validate_params_inputs',
                mock_validate,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/params',
                'with': {
                    'format': implementation.SQUASHTM_FORMAT,
                    'data': {
                        'global': {'GLVAR': 'global'},
                        'test': {'TESTVAR': 'test'},
                    },
                },
            }
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        mock_validate.assert_called_once()
        mock_export.assert_called_once_with(
            {'TESTVAR': 'test'}, {'GLVAR': 'global'}, verbatim=True
        )

    # npx

    def test_npx_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/npx',
                'with': {
                    'test': 'otherFolder/gui/set2/example_2.spec.ts',
                    'browser': 'chromium',
                    'extra-options': '--list',
                },
            }
            _dispatch(implementation.npx_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(1, len(steps))
        run = steps[0]['run']
        self.assertIn(
            'test otherFolder/gui/set2/example_2.spec.ts --browser=chromium --list', run
        )

    def test_npx_two_reporters(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/npx',
                'with': {
                    'test': 'otherFolder/gui/set2/example_2.spec.ts',
                    'reporters': ['junit', 'html'],
                    'grep': 'foo bar',
                    'browser': 'chromium',
                    'extra-options': '--list',
                },
            }
            _dispatch(implementation.npx_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(4, len(steps))
        run = steps[1]['run']
        self.assertIn('--grep=\'foo bar\'', run)
        self.assertIn('PW_TEST_HTML_REPORT_OPEN=never', run)
        self.assertIn(
            'PLAYWRIGHT_JUNIT_OUTPUT_NAME=playwright-report/pw_junit_report.xml', run
        )
        self.assertIn('PLAYWRIGHT_HTML_REPORT=playwright-report', run)
        self.assertIn('--reporter=html,junit', run)

    def test_npx_one_reporter(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/npx',
                'with': {
                    'test': 'otherFolder/gui/set2/example_2.spec.ts',
                    'reporters': ['junit', 'moo'],
                },
            }
            _dispatch(implementation.npx_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(3, len(steps))
        run = steps[1]['run']
        self.assertIn('PW_TEST_HTML_REPORT_OPEN=never', run)
        self.assertIn(
            'PLAYWRIGHT_JUNIT_OUTPUT_NAME=playwright-report/pw_junit_report.xml', run
        )
        self.assertNotIn('PLAYWRIGHT_HTML_REPORT=playwright-report', run)
        self.assertIn('--reporter=junit', run)

    def test_npx_reporters_no_valid_reporters(self):
        mock = MagicMock(return_value=mockresponse)
        mock_warning = MagicMock()
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch('opentf.toolkit.core.warning', mock_warning),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'playwright/npx',
                'with': {
                    'test': 'otherFolder/gui/set2/example_2.spec.ts',
                    'reporters': ['foo', 'moo'],
                },
            }
            _dispatch(implementation.npx_action, command)
        mock.assert_called_once()
        mock_warning.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(1, len(steps))
        run = steps[0]['run']
        self.assertIn('PW_TEST_HTML_REPORT_OPEN=never', run)
        self.assertNotIn('--reporter', run)
