# Copyright (c) 2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import logging
import os
import unittest
import sys
from datetime import datetime

from unittest.mock import MagicMock, mock_open, patch

from requests import Response


sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.trackerpublisher import main

########################################################################
# Helpers

mockresponse = Response()
mockresponse.status_code = 200


def _make_response(status_code, json_value):
    mock = Response()
    mock.status_code = status_code
    mock.json = lambda: json_value  # pyright: ignore
    return mock


########################################################################
# Templates

EVENT_WC_GL_MIN = {
    'kind': 'WorkflowCompleted',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {
        'name': 'workFlow',
        'workflow_id': 'wOrKflow_iD',
        'labels': {'gitlab/project-id': '123', 'gitlab/issue-iid': '456'},
    },
}

EVENT_WC_GL_MISSING = {
    'kind': 'WorkflowCompleted',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {
        'name': 'workFlow',
        'workflow_id': 'wOrKflow_iD',
        'labels': {'gitlab/keep-history': 'test', 'gitlab/issue-iid': '456'},
    },
}

EVENT_WC_GL_EMPTY = {
    'kind': 'WorkflowCompleted',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {
        'name': 'workFlow',
        'workflow_id': 'wOrKflow_iD',
        'labels': {
            'gitlab/project-id': '123',
            'gitlab/issue-iid': '456',
            'gitlab/mr-iid': '',
            'gitlab/label': '',
        },
    },
}

EVENT_WC_GITLAB_MAX = {
    'kind': 'WorkflowCompleted',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {
        'name': 'workFlow',
        'workflow_id': 'wOrKflow_iD',
        'labels': {
            'gitlab/project-path': 'foo/bar',
            'gitlab/issue-iid': '456',
            'gitlab/instance': 'default',
            'gitlab/keep-history': 'true',
            'gitlab/label': 'QualityGate',
            'qualitygate-mode': 'quality.gate',
        },
    },
}

EVENT_WC_NOT_GL = {
    'kind': 'WorkflowCompleted',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {'name': 'name', 'workflow_id': 'wOrKflow_iD'},
}

EVENT_OTHER = {
    'kind': 'Notification',
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'metadata': {'name': 'name', 'workflow_id': 'wOrKflow_iD'},
}

INSTANCES = {
    'default_instance': 'foo',
    'instances': {
        'foo': {
            'name': 'foo',
            'type': 'gitlab',
            'endpoint': 'bar',
            'token': 'tOkeN',
            'default-qualitygate': 'deFauLt',
        }
    },
}

RAW_INSTANCES = {
    'default-instance': 'foo',
    'instances': [
        {
            'name': 'foo',
            'type': 'gitlab',
            'endpoint': 'bar',
            'token': 'tOkeN',
            'default-qualitygate': 'deFauLt',
        }
    ],
}

RAW_INSTANCES_OTHERTYPE = {
    'default-instance': 'foo',
    'instances': [
        {
            'name': 'foo',
            'type': 'notgitlab',
            'endpoint': 'bar',
            'token': 'tOkeN',
            'default-qualitygate': 'deFauLt',
        }
    ],
}

DUPLICATE_RAW_INSTANCES = {
    'default-instance': 'double',
    'instances': [
        {
            'name': 'double',
            'type': 'gitlab',
            'endpoint': 'bar',
            'token': 'tOkeN',
            'default-qualitygate': 'deFauLt',
        },
        {
            'name': 'double',
            'type': 'gitlab',
            'endpoint': 'bar',
            'token': 'tOkeN',
            'default-qualitygate': 'deFauLt',
        },
        {
            'name': 'one',
            'type': 'gitlab',
            'endpoint': 'bar',
            'token': 'tOkeN',
            'default-qualitygate': 'deFauLt',
        },
        {
            'name': 'one',
            'type': 'gitlab',
            'endpoint': 'bar',
            'token': 'tOkeN',
            'default-qualitygate': 'deFauLt',
        },
    ],
}


QG_RESPONSE = {'details': {'rules': {'a': 'b'}, 'status': 'SUCCESS'}}


########################################################################


class TestTrackerPublisher(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def test_process_inbox_wc_with_gitlab_labels(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WC_GL_MIN
        mock_msr = MagicMock()
        mock_queue = MagicMock()

        with (
            patch('opentf.plugins.trackerpublisher.main.request', mock_request),
            patch(
                'opentf.plugins.trackerpublisher.main.make_status_response', mock_msr
            ),
            patch(
                'opentf.plugins.trackerpublisher.main.PENDING_QUERIES.put', mock_queue
            ),
        ):
            main.process_inbox()
        mock_queue.assert_called_once()
        self.assertEqual('wOrKflow_iD', mock_queue.call_args[0][0][0])
        self.assertEqual(
            EVENT_WC_GL_MIN['metadata']['labels'], mock_queue.call_args[0][0][2]
        )
        mock_msr.assert_called_once_with('OK', '')

    def test_process_inbox_wc_without_gitlab_labels(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WC_NOT_GL
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.trackerpublisher.main.request', mock_request),
            patch(
                'opentf.plugins.trackerpublisher.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()

        mock_msr.assert_called_once_with('OK', '')

    def test_process_inbox_wc_missing_gitlab_labels(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WC_GL_MISSING
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.trackerpublisher.main.request', mock_request),
            patch(
                'opentf.plugins.trackerpublisher.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()
        mock_msr.assert_called_once_with(
            'OK', 'Workflow missing mandatory gitlab labels.'
        )

    def test_process_inbox_wc_empty_gitlab_labels(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WC_GL_EMPTY
        mock_msr = MagicMock()
        mock_warning = MagicMock()
        with (
            patch('opentf.plugins.trackerpublisher.main.request', mock_request),
            patch(
                'opentf.plugins.trackerpublisher.main.make_status_response', mock_msr
            ),
            patch('opentf.plugins.trackerpublisher.main.warning', mock_warning),
        ):
            main.process_inbox()
        mock_msr.assert_called_once_with('OK', '')
        self.assertEqual(2, mock_warning.call_count)
        calls = mock_warning.call_args_list
        self.assertEqual('gitlab/mr-iid', calls[0][0][1])
        self.assertEqual('gitlab/label', calls[1][0][1])

    def test_process_inbox_invalid_body(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(side_effect=Exception('NoBody'))
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.trackerpublisher.main.request', mock_request),
            patch(
                'opentf.plugins.trackerpublisher.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()

        mock_msr.assert_called_once_with('BadRequest', 'Could not parse body: NoBody.')

    def test_process_inbox_body_not_json(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: 'not json'
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.trackerpublisher.main.request', mock_request),
            patch(
                'opentf.plugins.trackerpublisher.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()

        mock_msr.assert_called_once_with('BadRequest', 'Body must be a JSON object.')

    def test_process_inbox_missing_kind(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: {'apiVersion': 'opentestfactory.org/v1alpha1'}
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.trackerpublisher.main.request', mock_request),
            patch(
                'opentf.plugins.trackerpublisher.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()

        mock_msr.assert_called_once_with('BadRequest', 'Missing apiVersion or kind.')

    def test_process_inbox_unexpected_schema(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_OTHER
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.trackerpublisher.main.request', mock_request),
            patch(
                'opentf.plugins.trackerpublisher.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()

        mock_msr.assert_called_once_with(
            'BadRequest',
            'Unexpected schema: expected opentestfactory.org/v1/WorkflowCompleted, got opentestfactory.org/v1alpha1/Notification.',
        )

    def test_process_inbox_invalid_schema(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: {
            'apiVersion': 'opentestfactory.org/v1',
            'kind': 'WorkflowCompleted',
        }
        mock_msr = MagicMock()
        with (
            patch('opentf.plugins.trackerpublisher.main.request', mock_request),
            patch(
                'opentf.plugins.trackerpublisher.main.make_status_response', mock_msr
            ),
        ):
            main.process_inbox()

        mock_msr.assert_called_once()
        self.assertEqual('BadRequest', mock_msr.call_args[0][0])
        self.assertIn(
            "Not a valid WorkflowCompleted request: 'metadata' is a required property",
            mock_msr.call_args[0][1],
        )

    def test_handle_completed_wf_ok(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = INSTANCES
        mock_gcs = MagicMock(return_value={'endpoint': 'localhost'})
        mock_queue = MagicMock()
        mock_queue.get = MagicMock(
            return_value=('wOrKflow_iD', 'token', {'gitlab/instance': 'default'})
        )
        mock_qpqs = MagicMock(side_effect=Exception('Oops'))
        mock_error = MagicMock(side_effect=Exception('Oh no'))

        with (
            patch('opentf.plugins.trackerpublisher.main.get_context_service', mock_gcs),
            patch('opentf.plugins.trackerpublisher.main.PENDING_QUERIES', mock_queue),
            patch(
                'opentf.plugins.trackerpublisher.main.query_and_publish_qualitygate_status',
                mock_qpqs,
            ),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
        ):
            self.assertRaises(Exception, main.handle_completed_workflow)
        mock_qpqs.assert_called_once()
        result = mock_qpqs.call_args[0]
        self.assertEqual('default', result[1]['gitlab/instance'])
        self.assertEqual(INSTANCES['instances']['foo'], result[2])

    def test_handle_completed_wf_no_configured_gl_instances(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = {}
        mock_gcs = MagicMock(return_value={'endpoint': 'localhost'})
        mock_queue = MagicMock()
        mock_queue.get = MagicMock(
            return_value=('wOrKflow_iD', 'token', {'gitlab/instance': 'default'})
        )
        mock_qpqs = MagicMock(side_effect=Exception('Oops'))
        mock_error = MagicMock(side_effect=Exception('Oh no'))
        mock_warning = MagicMock(side_effect=Exception('I warned you'))

        with (
            patch('opentf.plugins.trackerpublisher.main.get_context_service', mock_gcs),
            patch('opentf.plugins.trackerpublisher.main.PENDING_QUERIES', mock_queue),
            patch(
                'opentf.plugins.trackerpublisher.main.query_and_publish_qualitygate_status',
                mock_qpqs,
            ),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
            patch('opentf.plugins.trackerpublisher.main.warning', mock_warning),
        ):
            self.assertRaises(Exception, main.handle_completed_workflow)
        mock_qpqs.assert_not_called()
        mock_warning.assert_called_once_with(
            'No Tracker instances configured, not attempting to publish result to tracker.'
        )

    def test_handle_completed_wf_gl_instance_not_found(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = INSTANCES
        mock_gcs = MagicMock(return_value={'endpoint': 'localhost'})
        mock_queue = MagicMock()
        mock_queue.get = MagicMock(
            return_value=('wOrKflow_iD', 'token', {'gitlab/instance': 'xYzAbC'})
        )
        mock_qpqs = MagicMock(side_effect=Exception('Oops'))
        mock_error = MagicMock(side_effect=Exception('Oh no'))
        mock_gi = MagicMock(return_value=None)

        with (
            patch('opentf.plugins.trackerpublisher.main.get_context_service', mock_gcs),
            patch('opentf.plugins.trackerpublisher.main.PENDING_QUERIES', mock_queue),
            patch('opentf.plugins.trackerpublisher.main._get_instance', mock_gi),
            patch(
                'opentf.plugins.trackerpublisher.main.query_and_publish_qualitygate_status',
                mock_qpqs,
            ),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
        ):
            self.assertRaises(Exception, main.handle_completed_workflow)
        mock_qpqs.assert_not_called()
        self.assertEqual(
            'Could not find instance "%s", not attempting to publish result to tracker.',
            mock_error.call_args_list[0][0][0],
        )
        self.assertEqual('xYzAbC', mock_error.call_args_list[0][0][1])

    def test_get_instance_not_found_in_config(self):
        plugin = main.plugin
        conf = plugin.config['CONFIG'] = {}
        conf['default_instance'] = 'foo'
        conf['instances'] = {}
        mock_error = MagicMock()

        with patch('opentf.plugins.trackerpublisher.main.error', mock_error):
            response = main._get_instance({'gitlab/instance': 'moo'})
        self.assertIsNone(response)
        mock_error.assert_called_once()
        msg = mock_error.call_args[0]
        self.assertEqual(
            'Tracker instance "%s" not found in configuration file.', msg[0]
        )
        self.assertEqual('moo', msg[1])

    def test_query_and_publish_ok(self):
        mockresponse = _make_response(200, {'details': {'status': 'SUCCESS'}})
        mock_get = MagicMock(return_value=mockresponse)
        mock_pin = MagicMock()

        with (
            patch('requests.get', mock_get),
            patch(
                'opentf.plugins.trackerpublisher.main.publish_as_issue_note', mock_pin
            ),
        ):
            main.query_and_publish_qualitygate_status(
                'url', {'qualitygate-mode': 'miam'}, {'c': 'd'}, headers=None
            )
        mock_pin.assert_called_once_with(
            {'details': {'status': 'SUCCESS'}},
            {'qualitygate-mode': 'miam'},
            'miam',
            {'c': 'd'},
        )

    def test_query_and_publish_wf_running_wf_done(self):
        mockresponse1 = _make_response(200, {'details': {'status': 'RUNNING'}})
        mockresponse2 = _make_response(200, {'details': {'status': 'SUCCESS'}})
        mock_get = MagicMock(side_effect=[mockresponse1, mockresponse2])
        mock_sleep = MagicMock()
        mock_pin = MagicMock()

        with (
            patch('requests.get', mock_get),
            patch(
                'opentf.plugins.trackerpublisher.main.publish_as_issue_note', mock_pin
            ),
            patch('time.sleep', mock_sleep),
        ):
            main.query_and_publish_qualitygate_status(
                'url', {'qualitygate-mode': 'miam'}, {'c': 'd'}, headers=None
            )
        mock_sleep.assert_called_once_with(1)
        mock_pin.assert_called_once_with(
            {'details': {'status': 'SUCCESS'}},
            {'qualitygate-mode': 'miam'},
            'miam',
            {'c': 'd'},
        )

    def test_query_and_publish_wf_hanged_up(self):
        mockresponse = _make_response(200, {'details': {'status': 'RUNNING'}})
        mock_get = MagicMock(return_value=mockresponse)
        mock_pin = MagicMock()
        mock_datetime = MagicMock()
        mock_datetime.now = MagicMock(
            side_effect=[
                datetime(1970, 1, 1, 15, 00, 00),
                datetime(1970, 1, 1, 17, 00, 00),
            ]
        )
        mock_error = MagicMock()

        with (
            patch('requests.get', mock_get),
            patch(
                'opentf.plugins.trackerpublisher.main.publish_as_issue_note', mock_pin
            ),
            patch('time.sleep', MagicMock()),
            patch('opentf.plugins.trackerpublisher.main.datetime', mock_datetime),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
            patch(
                'opentf.plugins.trackerpublisher.main._is_workflow_done',
                MagicMock(return_value=False),
            ),
        ):
            main.query_and_publish_qualitygate_status(
                'url', {'qualitygate-mode': 'miam'}, {'c': 'd'}, headers=None
            )
        mock_error.assert_called_once_with(
            'Failed to apply the quality gate "%s".  Request timed out.', 'miam'
        )

    def test_query_and_publish_quality_gate_ko(self):
        mockresponse = _make_response(404, {'message': 'Not Found'})
        mock_get = MagicMock(return_value=mockresponse)
        mock_error = MagicMock()

        with (
            patch('requests.get', mock_get),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
        ):
            main.query_and_publish_qualitygate_status(
                'url', {'a': 'b'}, {'default-qualitygate': 'deFauLt'}, headers=None
            )
        mock_error.assert_called_once()
        msg = mock_error.call_args[0]
        self.assertEqual(
            'Failed to apply the quality gate "%s".  Error code %d, message: %s', msg[0]
        )
        self.assertEqual('deFauLt', msg[1])
        self.assertEqual(404, msg[2])
        self.assertEqual('Not Found', msg[3])

    def test_query_and_publish_wf_notest(self):
        mockresponse = _make_response(200, {'details': {'status': 'NOTEST'}})
        mock_get = MagicMock(return_value=mockresponse)
        mock_pin = MagicMock()
        mock_error = MagicMock()

        with (
            patch('requests.get', mock_get),
            patch(
                'opentf.plugins.trackerpublisher.main.publish_as_issue_note', mock_pin
            ),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
        ):
            self.assertRaises(
                Exception,
                main.query_and_publish_qualitygate_status,
                'url',
                {'qualitygate-mode': 'miam'},
                {'c': 'd'},
                headers=None,
            )
        mock_pin.assert_not_called()

    def test_query_and_publish_wf_failed(self):
        mockresponse = _make_response(200, {'details': {'status': 'FAILED'}})
        mock_get = MagicMock(return_value=mockresponse)
        mock_pin = MagicMock()
        mock_error = MagicMock()

        with (
            patch('requests.get', mock_get),
            patch(
                'opentf.plugins.trackerpublisher.main.publish_as_issue_note', mock_pin
            ),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
        ):
            self.assertRaises(
                Exception,
                main.query_and_publish_qualitygate_status,
                'url',
                {'qualitygate-mode': 'miam'},
                {'c': 'd'},
                headers=None,
            )
        mock_pin.assert_not_called()

    def test_get_qualitygate_mode_not_found(self):
        mock_debug = MagicMock()
        with patch('opentf.plugins.trackerpublisher.main.debug', mock_debug):
            response = main._get_qualitygate_mode({'a': 'b'}, {'c': 'd'})
        mock_debug.assert_called_once_with(
            'Quality gate mode not found.  Applying "strict" mode by default.'
        )
        self.assertEqual({'mode': 'strict'}, response)

    def test_publish_as_issue_note_with_project_path_and_label(self):
        mock_drtt = MagicMock()
        mock_debug = MagicMock()

        with (
            patch(
                'opentf.plugins.trackerpublisher.main.dispatch_results_by_target_type',
                mock_drtt,
            ),
            patch('opentf.plugins.trackerpublisher.main.debug', mock_debug),
        ):
            main.publish_as_issue_note(
                QG_RESPONSE,
                EVENT_WC_GITLAB_MAX['metadata']['labels'],
                'mode',
                {'endpoint': 'gl.gl.gl:0000', 'keep-history': False},
            )
        mock_debug.assert_called_once_with(
            'Publishing quality gate "%s" results to GitLab.', 'mode'
        )
        mock_drtt.assert_called_once()
        msg = mock_drtt.call_args[0]
        self.assertEqual('foo%2Fbar', msg[0]['project'])
        self.assertEqual('mode', msg[1])
        self.assertEqual(QG_RESPONSE['details'], msg[2])
        self.assertEqual('QualityGate', msg[0]['label'])

    def test_publish_as_issue_note_with_project_id(self):
        mock_drtt = MagicMock()
        labels = {
            'gitlab/project-id': '123',
            'gitlab/issue-iid': '000',
            'gitlab/keep-history': True,
        }
        with patch(
            'opentf.plugins.trackerpublisher.main.dispatch_results_by_target_type',
            mock_drtt,
        ):
            main.publish_as_issue_note(
                QG_RESPONSE,
                labels,
                'mode',
                {'endpoint': 'gl.gl.gl:0000', 'keep-history': False},
            )
        mock_drtt.assert_called_once()
        msg = mock_drtt.call_args[0]
        self.assertEqual('123', msg[0]['project'])
        self.assertTrue(msg[0]['keep_history'])
        self.assertEqual('000', msg[0]['issue'])

    def test_publish_as_mr_note_with_history_from_instance(self):
        mock_drtt = MagicMock()
        labels = {'gitlab/project-id': '123', 'gitlab/mr-iid': '000'}
        with patch(
            'opentf.plugins.trackerpublisher.main.dispatch_results_by_target_type',
            mock_drtt,
        ):
            main.publish_as_issue_note(
                QG_RESPONSE,
                labels,
                'mode',
                {'endpoint': 'gl.gl.gl:0000', 'keep-history': 'false'},
            )
        mock_drtt.assert_called_once()
        msg = mock_drtt.call_args[0]
        self.assertEqual('123', msg[0]['project'])
        self.assertEqual('false', msg[0]['keep_history'])
        self.assertEqual('000', msg[0]['mr'])

    def test_publish_as_mr_note_and_as_issue_note(self):
        mock_drtt = MagicMock()
        labels = {
            'gitlab/project-id': '123',
            'gitlab/mr-iid': '000',
            'gitlab/issue-iid': '111',
        }
        with patch(
            'opentf.plugins.trackerpublisher.main.dispatch_results_by_target_type',
            mock_drtt,
        ):
            main.publish_as_issue_note(
                QG_RESPONSE,
                labels,
                'mode',
                {'endpoint': 'gl.gl.gl:0000', 'keep-history': 'false'},
            )
        mock_drtt.assert_called_once()
        msg = mock_drtt.call_args[0]
        self.assertEqual('123', msg[0]['project'])
        self.assertEqual('false', msg[0]['keep_history'])
        self.assertEqual('000', msg[0]['mr'])
        self.assertEqual('111', msg[0]['issue'])

    def test_read_configuration_notgitlab_notok(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = {}

        with (
            patch(
                'opentf.plugins.trackerpublisher.main.read_and_validate',
                MagicMock(return_value=RAW_INSTANCES_OTHERTYPE),
            ),
            patch('opentf.plugins.trackerpublisher.main.warning') as mock_warning,
        ):
            main._read_configuration(plugin, 'foo', 'bar')
        self.assertIsNotNone(plugin.config['CONFIG'].get('instances'))
        self.assertIsNotNone(plugin.config['CONFIG'].get('default_instance'))
        self.assertEqual(
            plugin.config['CONFIG']['default_instance'],
            RAW_INSTANCES['default-instance'],
        )
        mock_warning.assert_called_once_with(
            'Found an instance of unsupported type "%s".  Ignoring.', 'notgitlab'
        )

    def test_read_configuration_ok(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = {}

        with patch(
            'opentf.plugins.trackerpublisher.main.read_and_validate',
            MagicMock(return_value=RAW_INSTANCES),
        ):
            main._read_configuration(plugin, 'foo', 'bar')
        self.assertIsNotNone(plugin.config['CONFIG'].get('instances'))
        self.assertIsNotNone(plugin.config['CONFIG'].get('default_instance'))
        self.assertEqual(
            plugin.config['CONFIG']['default_instance'],
            RAW_INSTANCES['default-instance'],
        )

    def test_read_configuration_update_old(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = {
            'default_instance': 'boom',
            'instances': [{'name': 'unnamed'}],
        }
        mock_read = mock_open(read_data=json.dumps(RAW_INSTANCES))

        with (
            patch('builtins.open', mock_read),
            patch(
                'opentf.plugins.trackerpublisher.main.validate_schema',
                MagicMock(return_value=(True, 'bar')),
            ),
        ):
            main._read_configuration(plugin, 'foo', 'bar')
        self.assertNotEqual('boom', plugin.config['CONFIG']['default_instance'])
        self.assertNotIn('unnamed', plugin.config['CONFIG']['instances'])
        self.assertIn('foo', plugin.config['CONFIG']['instances'])

    def test_read_configuration_no_configfile(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = {'instances': 'check'}
        main._read_configuration(plugin, None, 'bar')
        self.assertEqual('check', plugin.config['CONFIG']['instances'])

    def test_read_configuration_notdict(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = {'instances': 'check', 'default_instance': 'recheck'}
        mock_read = mock_open(read_data='data')
        mock_error = MagicMock()

        with (
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
            patch('builtins.open', mock_read),
        ):
            main._read_configuration(plugin, 'blah', 'bar')
        mock_error.assert_called_once_with(
            'Invalid Tracker Publisher configuration file "%s": %s.  Ignoring.',
            'blah',
            'Config file is not an object.',
        )
        self.assertIsNone(plugin.config['CONFIG'].get('instances'))
        self.assertIsNone(plugin.config['CONFIG'].get('default_instance'))

    def test_read_configuration_invalid_schema(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = {}
        mock_error = MagicMock()

        with (
            patch(
                'opentf.plugins.trackerpublisher.main.read_and_validate',
                MagicMock(side_effect=ValueError('false')),
            ),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
        ):
            main._read_configuration(plugin, 'foo', 'bar')
        mock_error.assert_called_once_with(
            'Invalid Tracker Publisher configuration file "%s": %s.  Ignoring.',
            'foo',
            'false',
        )

    def test_read_configuration_no_default_instance(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = {}
        instances = RAW_INSTANCES.copy()
        instances['default-instance'] = 'not foo'
        mock_error = MagicMock()
        mock_rv = MagicMock(return_value=instances)

        with (
            patch('opentf.plugins.trackerpublisher.main.read_and_validate', mock_rv),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
        ):
            main._read_configuration(plugin, 'foo', 'bar')
        mock_error.assert_called_once_with(
            'Invalid Tracker Publisher configuration: default instance "%s" not found in instances.  Ignoring.',
            'not foo',
        )

    def test_read_configuration_duplicate_instance_names(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = {}
        mock_error = MagicMock()
        mock_rv = MagicMock(return_value=DUPLICATE_RAW_INSTANCES)

        with (
            patch('opentf.plugins.trackerpublisher.main.read_and_validate', mock_rv),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
        ):
            main._read_configuration(plugin, 'foo', 'bar')
        mock_error.assert_called_once()
        msg = mock_error.call_args[0]
        self.assertEqual('Duplicated instance names: "%s".  Ignoring.', msg[0])
        self.assertIn('double', msg[1])
        self.assertIn('one', msg[1])

    def test_read_configuration_ko(self):
        plugin = main.plugin
        plugin.config['CONFIG'] = {}
        mock_error = MagicMock()
        mock_rv = MagicMock(side_effect=Exception('Big and nasty expection'))

        with (
            patch('opentf.plugins.trackerpublisher.main.read_and_validate', mock_rv),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
        ):
            main._read_configuration(plugin, 'foo', 'bar')
        mock_rv.assert_called_once()
        mock_error.assert_called_once_with(
            'Error while reading "%s" Tracker Publisher configuration: %s.',
            'foo',
            'Big and nasty expection',
        )

    def test_start_plugin_ok(self):
        mock_plugin = MagicMock()
        mock_wf = MagicMock()
        mock_ra = MagicMock()
        mock_sub = MagicMock()
        mock_unsub = MagicMock()
        mock_debug = MagicMock()
        mock_thread = MagicMock()

        with (
            patch(
                'opentf.plugins.trackerpublisher.main.get_context_service', MagicMock()
            ),
            patch.dict('os.environ', {'TRACKERPUBLISHER_INSTANCES': '/way/too/long'}),
            patch('opentf.plugins.trackerpublisher.main.run_app', mock_ra),
            patch('opentf.plugins.trackerpublisher.main.threading.Thread', mock_thread),
            patch('opentf.plugins.trackerpublisher.main.watch_file', mock_wf),
            patch('opentf.plugins.trackerpublisher.main.subscribe', mock_sub),
            patch('opentf.plugins.trackerpublisher.main.unsubscribe', mock_unsub),
            patch('opentf.plugins.trackerpublisher.main.debug', mock_debug),
        ):
            main.main(mock_plugin)
        self.assertEqual(3, mock_debug.call_count)
        mock_wf.assert_called_once()
        self.assertEqual('/way/too/long', mock_wf.call_args[0][1])
        mock_thread.assert_called_once()
        mock_sub.assert_called_once()
        mock_ra.assert_called_once()
        self.assertEqual(1, mock_unsub.call_count)

    def test_start_plugin_without_configuration(self):
        mock_plugin = MagicMock()
        mock_warning = MagicMock()

        with (
            patch(
                'opentf.plugins.trackerpublisher.main.get_context_service', MagicMock()
            ),
            patch('opentf.plugins.trackerpublisher.main.warning', mock_warning),
            patch('opentf.plugins.trackerpublisher.main.threading.Thread', MagicMock()),
            patch.dict('os.environ', {'wroom': 'wroom'}),
            patch('opentf.plugins.trackerpublisher.main.subscribe', MagicMock()),
            patch('opentf.plugins.trackerpublisher.main.unsubscribe', MagicMock()),
            patch('opentf.plugins.trackerpublisher.main.run_app', MagicMock()),
        ):
            main.main(mock_plugin)

        mock_warning.assert_called_once()
        self.assertIn(
            'No tracker instances file provided.', mock_warning.call_args[0][0]
        )

    def test_start_plugin_workflow_thread_ko(self):
        mock_plugin = MagicMock()
        mock_error = MagicMock()
        mock_thread = MagicMock(side_effect=Exception('Quit'))

        with (
            patch(
                'opentf.plugins.trackerpublisher.main.get_context_service', MagicMock()
            ),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
            patch('opentf.plugins.trackerpublisher.main.threading.Thread', mock_thread),
            patch.dict('os.environ', {'TRACKERPUBLISHER_INSTANCES': '/way/too/long'}),
        ):
            self.assertRaises(SystemExit, main.main, mock_plugin)
        mock_error.assert_called_once_with(
            'Could not start completed workflows handling thread: %s.', 'Quit'
        )

    def test_start_plugin_subscription_ko(self):
        mock_plugin = MagicMock()
        mock_wf = MagicMock()
        mock_sub = MagicMock(side_effect=Exception('Another exception'))
        mock_thread = MagicMock()
        mock_error = MagicMock()

        with (
            patch(
                'opentf.plugins.trackerpublisher.main.get_context_service', MagicMock()
            ),
            patch.dict('os.environ', {'TRACKERPUBLISHER_INSTANCES': '/way/too/long'}),
            patch('opentf.plugins.trackerpublisher.main.threading.Thread', mock_thread),
            patch('opentf.plugins.trackerpublisher.main.watch_file', mock_wf),
            patch('opentf.plugins.trackerpublisher.main.subscribe', mock_sub),
            patch('opentf.plugins.trackerpublisher.main.error', mock_error),
        ):
            self.assertRaises(SystemExit, main.main, mock_plugin)
        mock_error.assert_called_once_with(
            'Could not subscribe to eventbus: %s.', 'Another exception'
        )

    def test_warning(self):
        mock_warning = MagicMock()
        with patch(
            'opentf.plugins.trackerpublisher.main.plugin.logger.warning', mock_warning
        ):
            main.warning('warn')
        mock_warning.assert_called_once_with('warn')


if __name__ == '__main__':
    unittest.main()
