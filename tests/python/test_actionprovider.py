# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""ActionProvider unit tests.

- actions selection tests
- create-file format tests
- formatters tests
"""

import logging
import os
import unittest
import sys

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.actionprovider import checkout, main, files

import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    'contexts': {'runner': {'os': 'linux'}},
    'step': {'uses': 'foo'},
    'runs-on': ['linux'],
}

PROVIDERMANIFESTCACHE = {
    (None, 'execute', None): ({'test': {'required': True}}, False),
    (None, 'params', None): (
        {
            'data': {
                'description': 'The data to use for the automated test.',
                'required': True,
            },
            'format': {
                'description': 'The format to use for the automated test data.',
                'required': True,
            },
        },
        True,
    ),
}


PREPAREINCEPTION_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'prepare-inception',
}
CREATEFILE_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'create-file',
}
DELETEFILE_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'delete-file',
}
TOUCHFILE_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'touch-file',
}
CHECKOUT_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'checkout',
}

CHECKOUT_V3_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'checkout',
    'opentestfactory.org/categoryVersion': 'v3',
}

GETFILES_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'get-files',
}
GETFILE_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'get-file',
}
CREATEARCHIVE_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'create-archive',
}

UPLOADARTIFACT_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'upload-artifact',
}

DOWNLOADARTIFACT_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'download-artifact',
}


REPOSITORY_0 = REPOSITORY_1_PUBLIC = 'http://github.com/a/b.git'
REPOSITORY_1 = 'http://foo:@github.com/a/b.git'
REPOSITORY_2 = 'https://foo:@github.com/a/b.git'
REPOSITORY_3 = 'https://:bar@github.com/a/b.git'
REPOSITORY_4 = 'https://foo:bar@github.com/a/b.git'
REPOSITORY_5 = 'http://foo:@github.com:8080/a/b.git'

REPOSITORY_01_PRIVATE = 'http://aBC:dEF@github.com/a/b.git'

CONTEXTS_PUBLIC = {
    'resources': {
        'repositories': {
            'abc': {'repository': 'a/b.git', 'endpoint': 'http://github.com/'}
        }
    }
}
CONTEXTS_PRIVATE = {
    'resources': {
        'repositories': {
            'abc': {'repository': 'a/b.git', 'endpoint': 'http://aBC:dEF@github.com/'}
        }
    }
}

########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestActionProvider(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONTEXT'][opentf.toolkit.INPUTS_KEY] = PROVIDERMANIFESTCACHE
        app.config['CONTEXT'][opentf.toolkit.OUTPUTS_KEY] = {}
        self.app = app.test_client()
        self.assertFalse(app.debug)

    # actions

    def test_inception_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = PREPAREINCEPTION_LABELS
            command['step'] = {'with': {'foo.xml': {'url': 'yada'}}, 'uses': 'foo'}
            _dispatch(files.prepareinception_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual('::inception::WORKFLOW_ID::foo.xml::yada', steps[0]['run'])

    def test_inception_action_badfile(self):
        mock_fail = MagicMock(side_effect=Exception('oopS'))
        with (
            patch('opentf.toolkit.core.fail', mock_fail),
            patch(
                'opentf.toolkit.core._getbody',
                MagicMock(return_value={'metadata': {'workflow_id': 'woId'}}),
            ),
        ):
            inputs = {'foo.xml': ''}
            self.assertRaisesRegex(
                Exception, 'oopS', files.prepareinception_action, inputs
            )
        mock_fail.assert_called_once_with('Unknown resources.files: foo.xml.')

    def test_inception_action_badfilegoodfile(self):
        mock_fail = MagicMock(side_effect=Exception('oopS'))
        with (
            patch('opentf.toolkit.core.fail', mock_fail),
            patch(
                'opentf.toolkit.core._getbody',
                MagicMock(return_value={'metadata': {'workflow_id': 'woId'}}),
            ),
        ):
            inputs = {'foo.xml': '', 'bar.html': 'something'}
            self.assertRaisesRegex(
                Exception, 'oopS', files.prepareinception_action, inputs
            )
        mock_fail.assert_called_once_with('Unknown resources.files: foo.xml.')

    def test_touchfile_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = TOUCHFILE_LABELS
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            _dispatch(files.touchfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual('touch foo', steps[0]['run'])

    def test_touchfile_action_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = TOUCHFILE_LABELS
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.touchfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual('@type nul >>foo', steps[0]['run'])

    def test_deletefile_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = DELETEFILE_LABELS
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            _dispatch(files.deletefile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual('rm -f foo', steps[0]['run'])

    def test_deletefile_action_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = DELETEFILE_LABELS
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            _dispatch(files.deletefile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual('@if exist foo @del /f/q foo', steps[0]['run'])

    def test_getfile_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            _dispatch(files.getfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual('echo "::attach::`pwd`/foo"', steps[0]['run'])

    def test_getfile_action_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual('@echo ::attach::%CD%\\foo', steps[0]['run'])

    def test_getfiles_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {'with': {'pattern': 'foo'}, 'uses': 'foo'}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual(
            'if test -z "$(find . -maxdepth 1 -name \'foo\' -print -quit)"; then (echo "::error::The specified pattern does not match any files." & exit 1); else for f in foo ; do test -f "$(pwd)/$f" && echo "::attach::$(pwd)/$f" ; done; fi',
            steps[0]['run'],
        )

    def test_getfiles_action_warn_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': 'foo', 'warn-if-not-found': 'bar'},
                'uses': 'foo',
            }
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn(
            'for f in foo ; do test -f "$(pwd)/$f" && echo "::attach::$(pwd)/$f" ; done',
            steps[0]['run'],
        )
        self.assertIn(
            '''if test -z "$(find . -maxdepth 1 -name 'foo' -print -quit)"; then''',
            steps[0]['run'],
        )

    def test_getfiles_action_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {'with': {'pattern': 'foo'}, 'uses': 'foo'}
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual(
            '(@for %%i in (foo) do @echo ::attach::%CD%\\%%i) & if not exist foo (echo ::error::The specified pattern does not match any files. & exit 1)',
            steps[0]['run'],
        )

    def test_getfiles_action_windows_type_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {'with': {'pattern': 'foo', 'type': 'bar'}, 'uses': 'foo'}
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn(' type=bar', steps[0]['run'])

    def test_getfiles_action_warn_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': 'foo', 'warn-if-not-found': 'bar'},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual(
            '(@for %%i in (foo) do @echo ::attach::%CD%\\%%i) & if not exist foo echo ::warning::bar & exit 0',
            steps[0]['run'],
        )

    def test_getfiles_action_skip_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': 'foo', 'skip-if-not-found': 'true'},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual(
            'if test -z "$(find . -maxdepth 1 -name \'foo\' -print -quit)"; then exit 0; else for f in foo ; do test -f "$(pwd)/$f" && echo "::attach::$(pwd)/$f" ; done; fi',
            steps[0]['run'],
        )

    def test_getfiles_action_skip_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': 'foo', 'skip-if-not-found': 'true'},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual(
            '(@for %%i in (foo) do @echo ::attach::%CD%\\%%i) & if not exist foo exit 0',
            steps[0]['run'],
        )

    def test_getfiles_action_warn_and_skip(self):
        mock_fail = MagicMock(side_effect=Exception('oopS'))
        with (
            patch('opentf.toolkit.core.fail', mock_fail),
            patch(
                'opentf.toolkit.core.runner_on_windows', MagicMock(return_value=False)
            ),
        ):
            inputs = {
                'warn-if-not-found': 'true',
                'skip-if-not-found': 'true',
                'pattern': '*.*',
            }
            self.assertRaisesRegex(Exception, 'oopS', files.getfiles_action, inputs)
        mock_fail.assert_called_once_with(
            'Both warn-if-not-found and skip-if-not-found specified.  You can only specify one.'
        )

    def test_getfilesrecurse_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {'with': {'pattern': '**/foo'}, 'uses': 'foo'}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual(
            'if test -z "$(find . -name \'foo\' -print -quit)"; then (echo "::error::The specified pattern does not match any files." & exit 1); else for f in $(find -name \'foo\'); do if test -f $f; then echo "::attach::$(pwd)/$f"; fi; done fi',
            steps[0]['run'],
        )

    def test_getfilesrecurse_action_warn_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': '**/foo', 'warn-if-not-found': 'bar'},
                'uses': 'foo',
            }
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn(
            'if test -z "$(find . -name \'foo\' -print -quit)"', steps[0]['run']
        )
        self.assertIn(
            '''else for f in $(find -name \'foo\'); do if test -f $f; then echo "::attach::$(pwd)/$f"; fi; done''',
            steps[0]['run'],
        )

    def test_getfilesrecurse_action_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {'with': {'pattern': '**\\foo'}, 'uses': 'foo'}
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual(
            '(@for /f %%i in (\'dir /s /b /a:-d "foo"\') do @echo ::attach::%%i) || (echo ::error::The specified pattern does not match any files. & exit 1)',
            steps[0]['run'],
        )

    def test_getfilesrecurse_action_warn_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': '**\\foo', 'warn-if-not-found': 'bar'},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual(
            '(@for /f %%i in (\'dir /s /b /a:-d "foo"\') do @echo ::attach::%%i) || echo ::warning::bar & exit 0',
            steps[0]['run'],
        )

    def test_getfilesrecurse_action_skip_ok_windows(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': '**/foo', 'skip-if-not-found': 'true'},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertEqual(
            '(@for /f %%i in (\'dir /s /b /a:-d "foo"\') do @echo ::attach::%%i) || exit 0',
            steps[0]['run'],
        )

    def test_createfile_action_ini_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEFILE_LABELS
            command['step'] = {
                'with': {
                    'data': {'foo': {'bar': 'baz'}},
                    'format': 'ini',
                    'path': 'foo.ini',
                },
                'uses': 'foo',
            }
            _dispatch(files.createfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn('[foo]\nbar=baz\n', steps[0]['run'])

    def test_createfile_action_ini_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {
                'with': {
                    'data': {'foo': {'bar': 'baz'}},
                    'format': 'ini',
                    'path': 'foo.ini',
                },
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn('W2Zvb10KYmFyPWJhego=', steps[0]['run'])

    def test_createfile_action_json_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {
                'with': {
                    'data': {'foo': {'bar': 'baz'}},
                    'format': 'json',
                    'path': 'foo.ini',
                },
                'uses': 'foo',
            }
            _dispatch(files.createfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn('{"foo": {"bar": "baz"}}', steps[0]['run'])

    def test_createfile_action_yaml_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {
                'with': {
                    'data': {'foo': {'bar': 'baz'}},
                    'format': 'yaml',
                    'path': 'foo.ini',
                },
                'uses': 'foo',
            }
            _dispatch(files.createfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn('foo:\n  bar: baz\n', steps[0]['run'])

    # uploadartifact

    def test_uploadartifact_action_ok_win_silent_warn(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = UPLOADARTIFACT_LABELS
            command['step'] = {
                'with': {
                    'name': 'foo.txt',
                    'path': 'file.txt',
                    'if-no-file-found': 'do_nothing',
                },
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.uploadartifact_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn(
            '@if exist file.txt (@echo ::upload type=application/vnd.opentestfactory.artifact+binary,name=foo.txt::%CD%\\file.txt)',
            steps[0]['run'],
        )
        self.assertIn(
            'else (echo ::warning::File file.txt not found. && exit 0)',
            steps[0]['run'],
        )

    def test_uploadartifact_action_ok_lin_explicit_warn(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = UPLOADARTIFACT_LABELS
            command['step'] = {
                'with': {
                    'path': 'doc.html',
                    'if-no-file-found': 'warn',
                },
                'uses': 'bar',
            }
            _dispatch(files.uploadartifact_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn(
            '[ -f doc.html ] && echo "::upload type=application/vnd.opentestfactory.artifact+binary::`pwd`/doc.html"',
            steps[0]['run'],
        )
        self.assertIn(
            '|| (echo "::warning::File doc.html not found." && exit 0)',
            steps[0]['run'],
        )

    def test_uploadartifact_action_ok_error(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = UPLOADARTIFACT_LABELS
            command['step'] = {
                'with': {
                    'path': 'doc.html',
                    'if-no-file-found': 'error',
                },
                'uses': 'boom',
            }
            _dispatch(files.uploadartifact_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn(
            '|| (echo "::error::File doc.html not found." && exit 1)', steps[0]['run']
        )

    def test_uploadartifact_action_ok_ignore(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = UPLOADARTIFACT_LABELS
            command['step'] = {
                'with': {
                    'path': 'doc.html',
                    'if-no-file-found': 'ignore',
                },
                'uses': 'wroom',
            }
            _dispatch(files.uploadartifact_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(1, len(steps))
        self.assertNotIn('|| echo "::warn::', steps[0]['run'])
        self.assertIn('|| (exit 0)', steps[0]['run'])

    # download-artifact

    def test_downloadartifact_action_ok_name(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = DOWNLOADARTIFACT_LABELS
            command['step'] = {
                'with': {
                    'name': 'artifact.txt',
                },
                'uses': 'wroom',
            }
            _dispatch(files.downloadartifact_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(1, len(steps))
        self.assertIn('echo "::download file=artifact.txt::"', steps[0]['run'])

    def test_downloadartifact_action_ok_pattern(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = DOWNLOADARTIFACT_LABELS
            command['step'] = {
                'with': {
                    'pattern': '*.txt',
                },
                'uses': 'wroom',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.downloadartifact_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(1, len(steps))
        self.assertIn('@echo ::download pattern=*.txt::', steps[0]['run'])

    def test_downloadartifact_action_ok_path(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = DOWNLOADARTIFACT_LABELS
            command['step'] = {
                'with': {
                    'path': '/some/path',
                },
                'uses': 'wroom',
            }
            _dispatch(files.downloadartifact_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(1, len(steps))
        self.assertIn('download pattern=*::/some/path', steps[0]['run'])

    def test_downloadartifact_action_no_inputs(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = DOWNLOADARTIFACT_LABELS
            command['step'] = {
                'uses': 'wroom',
            }
            _dispatch(files.downloadartifact_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(1, len(steps))
        self.assertIn('::download pattern=*::', steps[0]['run'])

    def test_downloadartifact_action_ko_name_and_pattern(self):
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.toolkit.core.fail', side_effect=Exception('Boom')
            ) as mock_fail,
        ):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = DOWNLOADARTIFACT_LABELS
            command['step'] = {
                'with': {
                    'name': 'artifact.txt',
                    'pattern': '*.txt',
                },
                'uses': 'wroom',
            }
            _dispatch(files.downloadartifact_action, command)
        mock.assert_not_called()
        mock_fail.assert_called_once_with(
            'Only one of `name`, `pattern` should be specified.'
        )

    # # checkout

    def test_checkout_repository(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {'with': {'repository': 'rEPOSITORY'}, 'uses': 'foo'}
            _dispatch(checkout.checkoutv2_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn('git clone rEPOSITORY', steps[0]['run'])

    def test_checkout_repositoryref(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': 'rEPOSITORY', 'ref': 'fOOBAR'},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv2_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertIn('fOOBAR', run)
        self.assertNotIn('cd rEPOSITORY', run)
        self.assertIn('git clone -b fOOBAR rEPOSITORY', run)

    def test_checkout_repositorysha_1(self):
        sha = 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333'
        repository = 'rEPOSITORY'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository, 'ref': sha},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv2_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertIn('cd rEPOSITORY', run)
        self.assertIn(f'git checkout {sha}', run)
        self.assertIn(f'git clone -n {repository}', run)

    def test_checkout_repositorysha_1bis(self):
        sha = 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333'
        repository = 'https://example.com/foo/rEPOSITORY'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository, 'ref': sha},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv2_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertIn('cd rEPOSITORY', run)
        self.assertIn(f'git checkout {sha}', run)
        self.assertIn(f'git clone -n {repository}', run)

    def test_checkout_repositorysha_2(self):
        sha = 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333'
        repository = 'https://example.com/foo/rEPOSITORY.git'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository, 'ref': sha},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv2_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertIn('cd rEPOSITORY', run)
        self.assertIn(f'git checkout {sha}', run)
        self.assertIn(f'git clone -n {repository}', run)

    def test_checkout_repositorysha_3(self):
        sha = 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333'
        repository = 'https://example.com/foo/rEPOSITORY.git/'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository, 'ref': sha},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv2_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertIn('cd rEPOSITORY', run)
        self.assertIn(f'git checkout {sha}', run)
        self.assertIn(f'git clone -n {repository}', run)

    def test_checkout_repositorysha_with_path(self):
        sha = 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333'
        repository = 'https://example.com/foo/rEPOSITORY.git/'
        path = '/home/swEEt/home'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository, 'ref': sha, 'path': path},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv2_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertNotIn('cd rEPOSITORY', run)
        self.assertIn(f'cd {path}', run)
        self.assertIn(f'git checkout {sha}', run)
        self.assertIn(f'git clone -n {repository}', run)

    def test_checkout_repository_with_password(self):
        repository = 'https://login:password@example.com/foo/rEPOSITORY.git/'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv2_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 2)
        mask = steps[0]['run']
        run = steps[1]['run']
        self.assertEqual('echo "::add-mask::password"', mask)
        self.assertIn(
            'git clone https://login:password@example.com/foo/rEPOSITORY.git/', run
        )

    def test_checkout_repository_with_password_2(self):
        repository = 'https://login:pass{@{iç@#]word@example.com/foo/rEPOSITORY.git/'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv2_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertIn(repository, run)

    def test_checkout_repositorysha_256(self):
        sha = '6604b9607da67cf37ec881ab94c308de294138332136777fa95ad2b74c2c7da8'
        repository = 'rEPOSITORY'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository, 'ref': sha},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv2_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertIn('cd rEPOSITORY', run)
        self.assertIn(f'git checkout {sha}', run)
        self.assertIn(f'git clone -n {repository}', run)

    def test_checkout_repository_v3(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {'with': {'repository': 'rEPOSITORY'}, 'uses': 'foo'}
            _dispatch(checkout.checkoutv3_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn(
            'git clone --depth 1 --no-single-branch rEPOSITORY', steps[0]['run']
        )

    def test_checkout_repository_v3_ref_depth(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {
                    'repository': 'rEPOSITORY',
                    'ref': 'brAncH',
                    'fetch-depth': '1263',
                },
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv3_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn('git clone --depth 1263 -b brAncH rEPOSITORY', steps[0]['run'])

    def test_checkout_repository_v3_depth_no_ref(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': 'rEPOSITORY', 'fetch-depth': '42'},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv3_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn(
            'git clone --depth 42 --no-single-branch rEPOSITORY', steps[0]['run']
        )

    def test_checkout_repository_v3_depth_zero(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': 'rEPOSITORY', 'fetch-depth': 0},
                'uses': 'foo',
            }
            _dispatch(checkout.checkoutv3_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertIn('git clone rEPOSITORY', steps[0]['run'])

    def test_maybe_override_0(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_0, None), REPOSITORY_0)

    def test_maybe_override_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_1, None), REPOSITORY_1)

    def test_maybe_override_2(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_2, None), REPOSITORY_2)

    def test_maybe_override_3(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_3, None), REPOSITORY_3)

    def test_maybe_override_4(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_4, None), REPOSITORY_4)

    def test_maybe_override_5(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_5, None), REPOSITORY_5)

    def test_maybe_override_0_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_0, {}), REPOSITORY_0)

    def test_maybe_override_1_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_1, {}), REPOSITORY_1)

    def test_maybe_override_2_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_2, {}), REPOSITORY_2)

    def test_maybe_override_3_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_3, {}), REPOSITORY_3)

    def test_maybe_override_4_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_4, {}), REPOSITORY_4)

    def test_maybe_override_5_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_5, {}), REPOSITORY_5)

    def test_maybe_override_0_2(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_0, {'resources': {}}), REPOSITORY_0
        )

    def test_maybe_override_0_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_0, CONTEXTS_PUBLIC), REPOSITORY_0
        )

    def test_maybe_override_1_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_1, CONTEXTS_PUBLIC), REPOSITORY_1_PUBLIC
        )

    def test_maybe_override_0_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_0, CONTEXTS_PRIVATE),
            REPOSITORY_01_PRIVATE,
        )

    def test_maybe_override_1_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_1, CONTEXTS_PRIVATE),
            REPOSITORY_01_PRIVATE,
        )

    def test_maybe_override_2_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_2, CONTEXTS_PUBLIC), REPOSITORY_2
        )

    def test_maybe_override_3_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_3, CONTEXTS_PUBLIC), REPOSITORY_3
        )

    def test_maybe_override_4_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_4, CONTEXTS_PUBLIC), REPOSITORY_4
        )

    def test_maybe_override_5_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_5, CONTEXTS_PUBLIC), REPOSITORY_5
        )

    def test_maybe_override_2_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_2, CONTEXTS_PRIVATE), REPOSITORY_2
        )

    def test_maybe_override_3_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_3, CONTEXTS_PRIVATE), REPOSITORY_3
        )

    def test_maybe_override_4_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_4, CONTEXTS_PRIVATE), REPOSITORY_4
        )

    def test_maybe_override_5_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_5, CONTEXTS_PRIVATE), REPOSITORY_5
        )

    # low-level formatters

    def test_createfile_ini_content(self):
        data = {
            "global": {"firstGlobal": "strange_value"},
            "test": {"key1": "value1", "key2": "value2"},
        }

        content = files.produce_ini_content(data)
        expected = (
            '[global]\nfirstGlobal=strange_value\n[test]\nkey1=value1\nkey2=value2\n'
        )
        self.assertEqual(expected, content)

    # create-archive

    def test_createarchive_action_windows_file_ok(self):
        """Windows unit test to archive a single file"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar', 'patterns': ['foo']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            """for /f %%G in ("foo") do if exist %%G (echo %%G""", steps[1]['run']
        )

    def test_createarchive_action_windows_glob_ok(self):
        """Windows unit test to recursively archive all 'foo' files"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar', 'patterns': ['**\\foo']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn("""'dir /b /s /a:-D "foo"'""", steps[1]['run'])

    def test_createarchive_action_windows_multifolders_ok(self):
        """Windows unit test to archive all present objets in 'foo\\' folder"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {
                    'path': 'foo.tar',
                    'patterns': ['foo\\bar\\', 'foo\\', 'foo\\bar\\and\\me\\'],
                },
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            """for /f %%G in ("foo") do if exist %%G\\nul (echo %%G""", steps[1]['run']
        )

    def test_createarchive_action_linux_glob_with_star_ok(self):
        """Linux unit test to recursively archive all 'foo*.bar' files"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar', 'patterns': ['**/foo*.bar']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn("""find .  -name "foo*.bar" -type f -print""", steps[1]['run'])

    def test_createarchive_action_linux_multifolders_ok(self):
        """Linux unit test to archive all present objects in 'folder/foo/' folder"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {
                    'path': 'foo.tar',
                    'patterns': [
                        'folder/foo/bar/',
                        'folder/foo/',
                        'folder/foo/bar/foo/',
                    ],
                },
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn('if [ -d "folder/foo" ]; then echo "folder/foo"', steps[1]['run'])

    def test_createarchive_action_linux_fullfilename_ok(self):
        """Linux unit test to archive a file with his path"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar', 'patterns': ['folder/foo']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn('if [ -f "folder/foo" ]; then echo "folder/foo"', steps[1]['run'])

    def test_createarchive_action_linux_fullfilename_with_star_ok(self):
        """Linux unit test to archive all 'foo*.bar' files in 'folder' foler"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar', 'patterns': ['folder/foo*.bar']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            """find folder -maxdepth 1 -name "foo*.bar" -type f -print""",
            steps[1]['run'],
        )

    def test_createarchive_action_warn_windows_ok(self):
        """Windows unit test to display a warning message if no pattern is found"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {
                    'path': 'foo.tar',
                    'patterns': ['folder\\foo_dont_exists'],
                    'warn-if-not-found': True,
                },
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn("""echo ::warning::The specified patterns""", steps[2]['run'])

    def test_createarchive_action_warn_linux_ok(self):
        """Linux unit test to display a warning message if no pattern is found"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {
                    'path': 'foo.tar',
                    'patterns': ['folder/foo_dont_exists/'],
                    'warn-if-not-found': True,
                },
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 3)
        self.assertIn("""echo "::warning::The specified patterns""", steps[3]['run'])

    def test_createarchive_action_warn_custom_message_windows_ok(self):
        """Windows unit test to display a warning message if no pattern is found"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {
                    'path': 'foo.tar',
                    'patterns': ['folder\\foo_dont_exists'],
                    'warn-if-not-found': "Archive empty!",
                },
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn("""echo ::warning::Archive empty!""", steps[2]['run'])

    def test_createarchive_action_warn_custom_message_linux_ok(self):
        """Linux unit test to display a warning message if no pattern is found"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {
                    'path': 'foo.tar',
                    'patterns': ['folder/foo_dont_exists/'],
                    'warn-if-not-found': "Empty archive!",
                },
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 3)
        self.assertIn("""echo "::warning::Empty archive!""", steps[3]['run'])

    def test_createarchive_action_windows_recursive_mode_ok(self):
        """Windows unit test to recursively archive all objects in current folder"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar.gz', 'patterns': ['**\\*']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 3)
        self.assertIn("""'dir /b /s /a:D "*"'""", steps[1]['run'])
        self.assertIn("""'dir /b /s /a:-D "*"'""", steps[2]['run'])

    def test_createarchive_action_linux_recursive_mode_ok(self):
        """Linux unit test to recursively archive all objects in current folder"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar.gz', 'patterns': ['**/']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 3)
        self.assertIn("""find .  -name "*" -type f -print""", steps[1]['run'])
        self.assertIn("""find .  -name "*" -type d -print""", steps[2]['run'])

    def test_createarchive_action_windows_doublestar_filename_ok(self):
        """Windows unit test to archive only files present in current folder"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar.gz', 'patterns': ['**']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            """for /f "tokens=*" %%G in ('dir /b /a:-D "*"')""", steps[1]['run']
        )

    def test_createarchive_action_linux_doublestar_filename_ok(self):
        """Linux unit test to archive only files present in current folder"""
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEARCHIVE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar.gz', 'patterns': ['**']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            """find . -maxdepth 1 -name "*" -type f -print | grep -v list_tar_files""",
            steps[1]['run'],
        )


if __name__ == '__main__':
    unittest.main()
