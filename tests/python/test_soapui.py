# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""SoapUI Provider unit tests.

- execute tests
- param tests
- soapui tests
"""

import os
import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.soapui import main, implementation

import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    'contexts': {'runner': {'os': 'linux'}},
    'step': {'uses': 'foo'},
    'runs-on': ['linux'],
}

PROVIDERMANIFESTCACHE = {
    ('junit', 'execute', None): ({'test': {'required': True}}, False),
    ('junit', 'params', None): (
        {
            'data': {
                'description': 'The data to use for the automated test.',
                'required': True,
            },
            'format': {
                'description': 'The format to use for the automated test data.',
                'required': True,
            },
        },
        True,
    ),
    ('junit', 'mvntest', None): (
        {
            'test': {'description': 'The test reference.', 'required': True},
            'properties': {
                'description': '...',
                'required': False,
            },
            'extra-options': {
                'description': 'Specify additional parameters to pass to Robot Framework.',
                'required': False,
            },
        },
        False,
    ),
}

########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestSoapui(unittest.TestCase):
    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONTEXT'][opentf.toolkit.INPUTS_KEY] = PROVIDERMANIFESTCACHE
        self.app = app.test_client()
        self.assertFalse(app.debug)

    # execute

    def test_execute_linux_project_ok(self):
        """
        Checks the runner command with a project item in the test definition
        when execute action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/execute',
                'with': {'test': 'soapuiProject/path/to/project.xml'},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            "testrunner.sh -j $SOAPUI_GLOBAL_PROPERTIES $SOAPUI_PROJECT_PROPERTIES 'soapuiProject/path/to/project.xml'",
            steps[0]['run'],
        )

    def test_execute_linux_testsuite_ok(self):
        """
        Checks the runner command with project and testsuite items
        in the test definition when execute action is called
        on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/execute',
                'with': {'test': 'soapuiProject/path/to/project.xml#testSuite#'},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            "testrunner.sh -j $SOAPUI_GLOBAL_PROPERTIES $SOAPUI_PROJECT_PROPERTIES -s'testSuite' 'soapuiProject/path/to/project.xml'",
            steps[0]['run'],
        )

    def test_execute_linux_testcase_ok(self):
        """
        Checks the runner command with project, testsuite and testcase items
        in the test definition when execute action is called
        on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/execute',
                'with': {
                    'test': 'soapuiProject/path/to/project.xml#testSuite#testCase'
                },
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            "testrunner.sh -j $SOAPUI_GLOBAL_PROPERTIES $SOAPUI_PROJECT_PROPERTIES -s'testSuite' -c'testCase' 'soapuiProject/path/to/project.xml'",
            steps[0]['run'],
        )

    def test_execute_linux_testcase_without_testsuite_ok(self):
        """
        Checks the runner command with project and testcase items
        in the test definition when execute action is called
        on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/execute',
                'with': {'test': 'soapuiProject/path/to/project.xml##testCase'},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            "testrunner.sh -j $SOAPUI_GLOBAL_PROPERTIES $SOAPUI_PROJECT_PROPERTIES -c'testCase' 'soapuiProject/path/to/project.xml'",
            steps[0]['run'],
        )

    def test_execute_windows_project_ok(self):
        """
        Checks the runner command with a project item in the test definition
        when execute action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/execute',
                'with': {'test': 'soapuiProject/path/to/project.xml'},
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            'testrunner.bat -j %SOAPUI_GLOBAL_PROPERTIES% %SOAPUI_PROJECT_PROPERTIES% "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_execute_windows_testsuite_ok(self):
        """
        Checks the runner command with project and testsuite items
        in the test definition when execute action is called
        on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/execute',
                'with': {'test': 'soapuiProject/path/to/project.xml#testSuite#'},
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            'testrunner.bat -j %SOAPUI_GLOBAL_PROPERTIES% %SOAPUI_PROJECT_PROPERTIES% -s"testSuite" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_execute_windows_testcase_ok(self):
        """
        Checks the runner command with project, testsuite and testcase items
        in the test definition when execute action is called
        on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/execute',
                'with': {
                    'test': 'soapuiProject/path/to/project.xml#testSuite#testCase'
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            'testrunner.bat -j %SOAPUI_GLOBAL_PROPERTIES% %SOAPUI_PROJECT_PROPERTIES% -s"testSuite" -c"testCase" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_execute_windows_testcase_without_testsuite_ok(self):
        """
        Checks the runner command with project and testcase items
        in the test definition when execute action is called
        on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/execute',
                'with': {'test': 'soapuiProject/path/to/project.xml##testCase'},
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            'testrunner.bat -j %SOAPUI_GLOBAL_PROPERTIES% %SOAPUI_PROJECT_PROPERTIES% -c"testCase" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    # param

    def test_param_ok(self):
        """
        Checks the runner command with params when param action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        mock_validate = MagicMock()
        mock_export = MagicMock()
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.soapui.implementation.core.export_variables',
                mock_export,
            ),
            patch(
                'opentf.plugins.soapui.implementation.core.validate_params_inputs',
                mock_validate,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            _dispatch(implementation.param_action, command)
        mock_validate.assert_called_once_with(
            {
                'data': {
                    'global': {'firstGlobal': 'strange_value'},
                    'test': {'key1': 'value1', 'key2': 'value2'},
                },
                'format': 'tm.squashtest.org/params@v1',
            }
        )
        mock_export.assert_called_once_with(
            {
                'SOAPUI_GLOBAL_PROPERTIES': '-GfirstGlobal=strange_value',
                'SOAPUI_PROJECT_PROPERTIES': '-Pkey1=value1 -Pkey2=value2',
            },
            verbatim=True,
        )
        mock.assert_called_once()

    # soapui

    def test_soapui_linux_project_ok(self):
        """
        Checks the runner command with a project input
        when soapui action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {'project': 'soapuiProject/path/to/project.xml'},
            }
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.sh -j "soapuiProject/path/to/project.xml"', steps[0]['run']
        )

    def test_soapui_linux_testsuite_ok(self):
        """
        Checks the runner command with project and testsuite inputs
        when soapui action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'testsuite': 'testSuite',
                },
            }
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.sh -j -s"testSuite" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_linux_testcase_ok(self):
        """
        Checks the runner command with project, testsuite and testcase inputs
        when soapui action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'testsuite': 'testSuite',
                    'testcase': 'testCase',
                },
            }
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.sh -j -c"testCase" -s"testSuite" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_linux_extra_options_ok(self):
        """
        Checks the runner command with project and extra-options inputs
        when soapui action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'extra-options': '-J -a',
                },
            }
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.sh -j -J -a "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_linux_system_properties_ok(self):
        """
        Checks the runner command with project and system-properties inputs
        when soapui action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'system-properties': {'id': '1', 's_nb': '11'},
                },
            }
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.sh -j -Did="1" -Ds_nb="11" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_linux_global_properties_ok(self):
        """
        Checks the runner command with project and global-properties inputs
        when soapui action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'global-properties': {'id': '2', 'g_nb': '22'},
                },
            }
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.sh -j -Gid="2" -Gg_nb="22" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_linux_project_properties_ok(self):
        """
        Checks the runner command with project and project-properties inputs
        when soapui action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'project-properties': {'id': '3', 'p_nb': '33'},
                },
            }
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.sh -j -Pid="3" -Pp_nb="33" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_linux_target_ok(self):
        """
        Checks the runner command with project and target inputs
        when soapui action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'target': 'path/to/my reports/',
                },
            }
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            'testrunner.sh -j -f"path/to/my_reports" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )
        self.assertIn('path/to/my_reports', steps[2]['working-directory'])

    def test_soapui_linux_testsuite_target_ok(self):
        """
        Checks the runner command with project, testsuite and target inputs
        when soapui action is called on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'testsuite': 'testSuite',
                    'target': 'myreports/',
                },
            }
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            'testrunner.sh -j -s"testSuite" -f"myreports" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )
        self.assertIn(
            '''echo "::attach type=application/vnd.opentestfactory.soapui-surefire+xml::`pwd`/myreports/TEST-testSuite.xml"''',
            steps[2]['run'],
        )

    def test_soapui_windows_project_ok(self):
        """
        Checks the runner command with a project input
        when soapui action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {'project': 'soapuiProject/path/to/project.xml'},
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.bat -j "soapuiProject/path/to/project.xml"', steps[0]['run']
        )

    def test_soapui_windows_testsuite_ok(self):
        """
        Checks the runner command with project and testsuite inputs
        when soapui action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'testsuite': 'testSuite',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.bat -j -s"testSuite" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_windows_testcase_ok(self):
        """
        Checks the runner command with project, testsuite and testcase inputs
        when soapui action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'testsuite': 'testSuite',
                    'testcase': 'testCase',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.bat -j -c"testCase" -s"testSuite" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_windows_extra_options_ok(self):
        """
        Checks the runner command with project and extra-options inputs
        when soapui action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'extra-options': '-J -a',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.bat -j -J -a "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_windows_system_properties_ok(self):
        """
        Checks the runner command with project and system-properties inputs
        when soapui action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'system-properties': {'id': '1', 's_nb': '11'},
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.bat -j -Did="1" -Ds_nb="11" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_windows_global_properties_ok(self):
        """
        Checks the runner command with project and global-properties inputs
        when soapui action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'global-properties': {'id': '2', 'g_nb': '22'},
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.bat -j -Gid="2" -Gg_nb="22" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_windows_project_properties_ok(self):
        """
        Checks the runner command with project and project-properties inputs
        when soapui action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'project-properties': {'id': '3', 'p_nb': '33'},
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 1)
        self.assertIn(
            'testrunner.bat -j -Pid="3" -Pp_nb="33" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )

    def test_soapui_windows_target_ok(self):
        """
        Checks the runner command with project and target inputs
        when soapui action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'target': 'path\\to/my folders\\',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            'testrunner.bat -j -f"path\\to\\my_folders" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )
        self.assertIn('path\\to\\my_folders', steps[2]['working-directory'])

    def test_soapui_windows_testsuite_target_ok(self):
        """
        Checks the runner command with project, testsuite and target inputs
        when soapui action is called on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'soapui/soapui',
                'with': {
                    'project': 'soapuiProject/path/to/project.xml',
                    'testsuite': 'testSuite',
                    'target': 'myreports\\',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.soapui_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertGreater(len(steps), 2)
        self.assertIn(
            'testrunner.bat -j -s"testSuite" -f"myreports" "soapuiProject/path/to/project.xml"',
            steps[0]['run'],
        )
        self.assertIn(
            '''echo ::attach type=application/vnd.opentestfactory.soapui-surefire+xml::%CD%\\myreports\\TEST-testSuite.xml''',
            steps[2]['run'],
        )


if __name__ == '__main__':
    unittest.main()
