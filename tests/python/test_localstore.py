# Copyright (c) 2022-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Localstore unit tests."""

import os
import sys
import unittest

from collections import defaultdict
from datetime import datetime, timedelta

from queue import Queue
from unittest.mock import MagicMock, patch

from requests import Response


########################################################################

EVENT_WR = {
    'kind': 'WorkflowResult',
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'metadata': {'workflow_id': 'wOrKflow_iD'},
}

EVENT_WR_ATTACHMENTS = {
    'kind': 'WorkflowResult',
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'metadata': {'workflow_id': 'wOrKflow_iDD'},
    'attachments': {'att1', 'att2', 'att3'},
}

EVENT_WC = {
    'kind': 'WorkflowCompleted',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {'workflow_id': 'wOrKflow_iD'},
}

EVENT_WF = {
    'kind': 'Workflow',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {'workflow_id': 'wOrKflow_iD', 'namespace': 'nAmEspace'},
}

EVENT_OTHER = {
    'kind': 'FooBar',
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'metadata': {'workflow_id': 'wOrKflow_iD'},
}

ATTACHMENT = '/tmp/40c127b7-7f14-4037-81f3-31fdddb8a7cc-bb2530cf-fbb7-4814-a43f-0f9db80903ee_30_output.xml'
ATTACHMENT_UUID = 'bb2530cf-fbb7-4814-a43f-0f9db80903ee'
WORKFLOW_UUID = 'a0f57054-9024-4071-b53a-e52d1a454a84'

########################################################################

mockresponse = Response()
mockresponse.status_code = 200

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.core import localstore


class TestLocalStore(unittest.TestCase):
    # process inbox

    def test_process_inbox_empty(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: {}
        mock_msr = MagicMock()
        with (
            patch('opentf.core.localstore.request', mock_request),
            patch('opentf.core.localstore.make_status_response', mock_msr),
        ):
            localstore.process_inbox()
        mock_msr.assert_called_once_with(
            'Invalid', 'Not an object or no kind specified.'
        )

    def test_process_inbox_wr(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WR
        mock_msr = MagicMock()
        with (
            patch('opentf.core.localstore.request', mock_request),
            patch('opentf.core.localstore.make_status_response', mock_msr),
        ):
            localstore.process_inbox()
        mock_msr.assert_called_once_with('OK', '')

    def test_process_inbox_wf(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WF
        mock_msr = MagicMock()
        mock_wfn = {}
        with (
            patch('opentf.core.localstore.request', mock_request),
            patch('opentf.core.localstore.make_status_response', mock_msr),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
        ):
            localstore.process_inbox()
        self.assertEqual({'wOrKflow_iD': 'nAmEspace'}, mock_wfn)
        mock_msr.assert_called_once_with('OK', '')

    def test_process_inbox_add_attachments(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WR_ATTACHMENTS
        mock_msr = MagicMock()
        mock_wfa = defaultdict(set)
        with (
            patch('opentf.core.localstore.request', mock_request),
            patch('opentf.core.localstore.make_status_response', mock_msr),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch(
                'opentf.core.localstore.os.path.exists', MagicMock(return_value=True)
            ),
        ):
            localstore.process_inbox()
        self.assertEqual({'wOrKflow_iDD': {'att1', 'att2', 'att3'}}, mock_wfa)
        mock_msr.assert_called_once_with('OK', '')

    def test_process_inbox_add_existing_attachments(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WR_ATTACHMENTS
        mock_msr = MagicMock()
        mock_wfa = defaultdict(set)
        with (
            patch('opentf.core.localstore.request', mock_request),
            patch('opentf.core.localstore.make_status_response', mock_msr),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch(
                'opentf.core.localstore.os.path.exists',
                MagicMock(side_effect=[True, False, False]),
            ),
        ):
            localstore.process_inbox()
        self.assertEqual(1, len(mock_wfa['wOrKflow_iDD']))
        mock_msr.assert_called_once_with('OK', '')

    def test_process_inbox_wc(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_WC
        mock_msr = MagicMock()
        mock_clp = MagicMock()
        with (
            patch('opentf.core.localstore.request', mock_request),
            patch('opentf.core.localstore.make_status_response', mock_msr),
            patch('opentf.core.localstore.CLEANUP_PENDING.put', mock_clp),
        ):
            localstore.process_inbox()
        self.assertEqual('wOrKflow_iD', mock_clp.call_args[0][0][1])
        mock_msr.assert_called_with('OK', '')

    def test_process_inbox_other(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: EVENT_OTHER
        mock_msr = MagicMock()
        with (
            patch('opentf.core.localstore.request', mock_request),
            patch('opentf.core.localstore.make_status_response', mock_msr),
        ):
            localstore.process_inbox()
        mock_msr.assert_called_once_with('OK', '')

    def test_process_inbox_body_ko(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(side_effect=Exception('Not a body'))
        mock_msr = MagicMock()
        with (
            patch('opentf.core.localstore.request', mock_request),
            patch('opentf.core.localstore.make_status_response', mock_msr),
        ):
            localstore.process_inbox()
        mock_msr.assert_called_once_with(
            'BadRequest', 'Could not parse body: Not a body.'
        )

    def test_process_inbox_wf_id_ko(self):
        mock_request = MagicMock()
        mock_request.get_json = lambda: {'kind': 'kind', 'apiVersion': '1.0'}
        mock_msr = MagicMock()
        with (
            patch('opentf.core.localstore.request', mock_request),
            patch('opentf.core.localstore.make_status_response', mock_msr),
        ):
            localstore.process_inbox()
        mock_msr.assert_called_once_with('Invalid', 'No workflow_id specified.')

    # main

    def test_main_mock(self):
        mock_fatal = MagicMock()
        mock_threading = MagicMock()
        with (
            patch('opentf.core.localstore.fatal', mock_fatal),
            patch('opentf.core.localstore.threading', mock_threading),
            patch('opentf.core.localstore.subscribe', MagicMock()),
            patch('opentf.core.localstore.unsubscribe', MagicMock()),
            patch('opentf.core.localstore.run_app', MagicMock()),
        ):
            localstore.main(MagicMock())
        mock_fatal.assert_not_called()
        mock_threading.assert_not_called()

    def test_main_thread_exception(self):
        mock_error = MagicMock()
        mock_sub = MagicMock()
        mock_unsub = MagicMock()
        mock_ra = MagicMock()
        mock_threading = MagicMock(side_effect=Exception('Thread not threading'))
        with (
            patch('opentf.core.localstore.error', mock_error),
            patch('opentf.core.localstore.threading.Thread', mock_threading),
            patch('opentf.core.localstore.subscribe', mock_sub),
            patch('opentf.core.localstore.unsubscribe', mock_unsub),
            patch('opentf.core.localstore.run_app', mock_ra),
        ):
            self.assertRaises(SystemExit, localstore.main, MagicMock())
        mock_error.assert_called_once_with(
            'Could not start attachment cleanup thread: %s.', 'Thread not threading'
        )
        mock_sub.assert_not_called()
        mock_ra.assert_not_called()
        mock_unsub.assert_not_called()

    def test_main_subs_exception(self):
        mock_fatal = MagicMock(side_effect=Exception('Done'))
        mock_sub = MagicMock(side_effect=Exception('Subscription not subscribing'))
        mock_unsub = MagicMock()
        mock_ra = MagicMock()
        mock_threading = MagicMock()
        with (
            patch('opentf.core.localstore.fatal', mock_fatal),
            patch('opentf.core.localstore.threading.Thread', mock_threading),
            patch('opentf.core.localstore.subscribe', mock_sub),
            patch('opentf.core.localstore.unsubscribe', mock_unsub),
            patch('opentf.core.localstore.run_app', mock_ra),
        ):
            self.assertRaisesRegex(Exception, 'Done', localstore.main, MagicMock())
        mock_fatal.assert_called_once_with(
            'Could not subscribe to eventbus: %s.', 'Subscription not subscribing'
        )
        mock_ra.assert_not_called()
        mock_unsub.assert_not_called()

    # clean attachments

    def test_clean_attachments_sleep_if_pending(self):
        workflow_id = 'wOrKflow_iD'
        mock_error = MagicMock(side_effect=SystemExit())
        mock_queue = Queue()
        mock_queue.put((datetime.now(), workflow_id))
        mock_wfa = {workflow_id: set()}
        mock_sleep = MagicMock(side_effect=SystemExit())
        with (
            patch('opentf.core.localstore.error', mock_error),
            patch('opentf.core.localstore.CLEANUP_PENDING', mock_queue),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.sleep', mock_sleep),
        ):
            self.assertRaises(SystemExit, localstore.clean_attachments)
        mock_sleep.assert_called_once()
        self.assertFalse(mock_queue.empty())

    def test_clean_attachments_sleep_if_second_pending(self):
        workflow_id_1 = 'wOrKflow_iD'
        workflow_id_2 = 'wOrKflow_iD2'
        mock_error = MagicMock(side_effect=SystemExit())
        mock_queue = Queue()
        mock_queue.put((datetime.now() - timedelta(days=1), workflow_id_1))
        mock_queue.put((datetime.now(), workflow_id_2))
        mock_wfa = {workflow_id_2: set()}
        mock_sleep = MagicMock(side_effect=SystemExit())
        with (
            patch('opentf.core.localstore.error', mock_error),
            patch('opentf.core.localstore.CLEANUP_PENDING', mock_queue),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.sleep', mock_sleep),
        ):
            self.assertRaises(SystemExit, localstore.clean_attachments)
        mock_sleep.assert_called_once()
        self.assertFalse(mock_queue.empty())

    def test_clean_attachments_notpending(self):
        workflow_id = 'wOrKflow_iD'
        mock_error = MagicMock(side_effect=SystemExit())
        mock_warning = MagicMock(side_effect=Exception())
        mock_queue = Queue()
        mock_queue.put((datetime.now() - timedelta(days=1), workflow_id))
        mock_wfa = {workflow_id: {'xxx'}}
        mock_os = MagicMock()
        mock_os.unlink = MagicMock(side_effect=IOError())
        with (
            patch('opentf.core.localstore.error', mock_error),
            patch('opentf.core.localstore.CLEANUP_PENDING', mock_queue),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.warning', mock_warning),
            patch('opentf.core.localstore.os', mock_os),
        ):
            self.assertRaises(SystemExit, localstore.clean_attachments)
        mock_warning.assert_called_once()
        self.assertTrue(mock_queue.empty())
        mock_os.unlink.assert_called()

    def test_clean_attachments_clean_maps(self):
        workflow_id = 'wOrKflow_iD'
        mock_error = MagicMock(side_effect=SystemExit())
        mock_warning = MagicMock(side_effect=Exception())
        mock_queue = Queue()
        mock_queue.put((datetime.now() - timedelta(days=1), workflow_id))
        mock_queue.put((datetime.now() - timedelta(days=2), 'wf_id'))
        mock_wfa = {workflow_id: {'xxx'}, 'wf_id': {'yyy'}}
        mock_wns = {workflow_id: 'default', 'wf_id': 'not default'}
        mock_os = MagicMock()
        mock_os.unlink = MagicMock(side_effect=[None, IOError()])
        with (
            patch('opentf.core.localstore.error', mock_error),
            patch('opentf.core.localstore.CLEANUP_PENDING', mock_queue),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.warning', mock_warning),
            patch('opentf.core.localstore.os', mock_os),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wns),
        ):
            self.assertRaises(SystemExit, localstore.clean_attachments)
        self.assertTrue(mock_queue.empty())
        mock_os.unlink.assert_called()
        self.assertEqual({'wf_id': {'yyy'}}, mock_wfa)
        self.assertEqual({'wf_id': 'not default'}, mock_wns)

    def test_clean_attachments_clean_maps_ns_ko(self):
        workflow_id = 'wOrKflow_iD'
        mock_error = MagicMock(side_effect=SystemExit())
        mock_warning = MagicMock(side_effect=Exception())
        mock_queue = Queue()
        mock_queue.put((datetime.now() - timedelta(days=1), workflow_id))
        mock_queue.put((datetime.now() - timedelta(days=2), 'wf_id'))
        mock_wfa = {workflow_id: {'xxx'}, 'wf_id': {'yyy'}}
        mock_wns = {'a': 'b'}
        mock_os = MagicMock()
        mock_os.unlink = MagicMock(side_effect=[None, IOError()])
        with (
            patch('opentf.core.localstore.error', mock_error),
            patch('opentf.core.localstore.CLEANUP_PENDING', mock_queue),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.warning', mock_warning),
            patch('opentf.core.localstore.os', mock_os),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wns),
        ):
            self.assertRaises(SystemExit, localstore.clean_attachments)
        self.assertTrue(mock_queue.empty())
        mock_os.unlink.assert_called()
        self.assertEqual({'wf_id': {'yyy'}}, mock_wfa)
        self.assertEqual({'a': 'b'}, mock_wns)

    # get file

    def test_get_file_ok(self):
        mock_sf = MagicMock()
        mock_wfa = {
            'wf_id': {
                ATTACHMENT,
                'not_attachment-zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
                'not_id-yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy',
            }
        }
        mock_wfn = {'wf_id': 'ns'}
        mock_ns = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.method = 'GET'
        with (
            patch('opentf.core.localstore.send_file', mock_sf),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
            patch('opentf.core.localstore.can_use_namespace', mock_ns),
            patch('opentf.core.localstore.request', mock_request),
        ):
            localstore.get_file('wf_id', ATTACHMENT_UUID)
        mock_sf.assert_called_once_with(
            ATTACHMENT,
            download_name=ATTACHMENT_UUID + '_output.xml',
            mimetype='application/octet-stream',
        )

    def test_get_file_ok_executionlog_pattern(self):
        mock_sf = MagicMock()
        mock_wfa = {
            'wf_id': {
                '/tmp/'
                + WORKFLOW_UUID
                + '-'
                + ATTACHMENT_UUID
                + '_WR_executionlog.txt',
                'not_attachment-zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
                'not_id-yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy',
            }
        }
        mock_wfn = {'wf_id': 'ns'}
        mock_ns = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.method = 'GET'
        with (
            patch('opentf.core.localstore.send_file', mock_sf),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
            patch('opentf.core.localstore.can_use_namespace', mock_ns),
            patch('opentf.core.localstore.request', mock_request),
        ):
            localstore.get_file('wf_id', ATTACHMENT_UUID)
        mock_sf.assert_called_once_with(
            '/tmp/' + WORKFLOW_UUID + '-' + ATTACHMENT_UUID + '_WR_executionlog.txt',
            download_name=ATTACHMENT_UUID + '_executionlog.txt',
            mimetype='application/octet-stream',
        )

    def test_get_file_ok_allurereport_pattern(self):
        mock_sf = MagicMock()
        mock_wfa = {
            ATTACHMENT_UUID: {
                f'/tmp/allureReporting/{ATTACHMENT_UUID}/allure-report.tar',
                'not_attachment-zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
                'not_id-yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy',
            }
        }
        mock_wfn = {ATTACHMENT_UUID: 'ns'}
        mock_ns = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.method = 'GET'
        with (
            patch('opentf.core.localstore.send_file', mock_sf),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
            patch('opentf.core.localstore.can_use_namespace', mock_ns),
            patch('opentf.core.localstore.request', mock_request),
        ):
            localstore.get_file(ATTACHMENT_UUID, f'{ATTACHMENT_UUID}')
        mock_sf.assert_called_once_with(
            '/tmp/allureReporting/' + ATTACHMENT_UUID + '/allure-report.tar',
            download_name=ATTACHMENT_UUID + '_allure-report.tar',
            mimetype='application/octet-stream',
        )

    def test_get_file_namespace_ko(self):
        mock_sf = MagicMock()
        mock_wfa = {'wf_id': {'not_attachment', 'not_id'}}
        mock_wfn = {'wf_id': 'ns'}
        mock_ns = MagicMock(return_value=False)
        mock_msr = MagicMock()
        with (
            patch('opentf.core.localstore.send_file', mock_sf),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.make_status_response', mock_msr),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
            patch('opentf.core.localstore.can_use_namespace', mock_ns),
        ):
            localstore.get_file('wf_id', ATTACHMENT_UUID)
        mock_msr.assert_called_once_with(
            'Forbidden', 'Token not allowed to access workflows in namespace ns.'
        )
        mock_sf.assert_not_called()

    def test_get_file_wf_id_ko(self):
        mock_sf = MagicMock()
        mock_wfa = {'not_wf_id': {'not_attachment', 'not_id'}}
        mock_msr = MagicMock()
        with (
            patch('opentf.core.localstore.send_file', mock_sf),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.make_status_response', mock_msr),
        ):
            localstore.get_file('wf_id', ATTACHMENT_UUID)
        mock_msr.assert_called_once_with(
            'NotFound',
            'Workflow wf_id not found in localstore. Its attachments may have been previously cleaned up.',
        )
        mock_sf.assert_not_called()

    def test_get_file_no_candidates(self):
        mock_sf = MagicMock()
        mock_wfa = {
            'wf_id': {
                '/tmp/not_attachment-yyyyyyyyyyyyyyyyyyyyyyy',
                '/tmp/no_attachment-xxxxxxxxxxxxxxxxxxxxxxxxxxxx',
                '/tmp/not_id-zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
            }
        }
        mock_wfn = {'wf_id': 'ns'}
        mock_ns = MagicMock(return_value=True)
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.method = 'GET'
        with (
            patch('opentf.core.localstore.send_file', mock_sf),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
            patch('opentf.core.localstore.can_use_namespace', mock_ns),
            patch('opentf.core.localstore.make_status_response', mock_msr),
            patch('opentf.core.localstore.request', mock_request),
        ):
            localstore.get_file('wf_id', ATTACHMENT_UUID)
        mock_msr.assert_called_once_with(
            'NotFound', f'Attachment {ATTACHMENT_UUID} not found for workflow wf_id.'
        )
        mock_sf.assert_not_called()

    def test_get_file_get_headers_no_candidates(self):
        mock_wfa = {
            'wf_id': {
                '/tmp/not_attachment-yyyyyyyyyyyyyyyyyyyyyyy',
                '/tmp/no_attachment-xxxxxxxxxxxxxxxxxxxxxxxxxxxx',
                '/tmp/not_id-zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
            }
        }
        mock_wfn = {'wf_id': 'ns'}
        mock_ns = MagicMock(return_value=True)
        mock_mr = MagicMock()
        mock_request = MagicMock()
        mock_request.method = 'HEAD'
        with (
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
            patch('opentf.core.localstore.can_use_namespace', mock_ns),
            patch('opentf.core.localstore.make_response', mock_mr),
            patch('opentf.core.localstore.request', mock_request),
            patch('opentf.core.localstore._matches', MagicMock(return_value=False)),
        ):
            localstore.get_file('wf_id', ATTACHMENT_UUID)
        mock_mr.assert_called_once()
        call = mock_mr.call_args[0]
        self.assertEqual(404, call[-1])
        self.assertEqual('NotFound', call[0]['reason'])

    def test_get_file_send_file_exception(self):
        mock_sf = MagicMock(side_effect=Exception('Boom!'))
        mock_wfa = {
            'wf_id': {
                ATTACHMENT,
                'not_attachment-zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
                'not_id-yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy',
            }
        }
        mock_wfn = {'wf_id': 'ns'}
        mock_ns = MagicMock(return_value=True)
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.method = 'GET'
        with (
            patch('opentf.core.localstore.send_file', mock_sf),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
            patch('opentf.core.localstore.can_use_namespace', mock_ns),
            patch('opentf.core.localstore.make_status_response', mock_msr),
            patch('opentf.core.localstore.request', mock_request),
            patch(
                'opentf.core.localstore.os.path.exists', MagicMock(return_value=True)
            ),
        ):
            localstore.get_file('wf_id', ATTACHMENT_UUID)
        mock_msr.assert_called_once_with(
            'InternalError',
            f'Could not send file `{ATTACHMENT}` ({ATTACHMENT_UUID}): Boom!',
        )

    def test_get_file_get_headers_file_ok(self):
        mock_response = MagicMock()
        mock_msr = MagicMock(return_value=mock_response)
        mock_wfa = {
            'wf_id': {
                ATTACHMENT,
                'not_attachment-zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
                'not_id-yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy',
            }
        }
        mock_wfn = {'wf_id': 'ns'}
        mock_ns = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.method = 'HEAD'
        with (
            patch('opentf.core.localstore.make_status_response', mock_msr),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
            patch('opentf.core.localstore.can_use_namespace', mock_ns),
            patch('opentf.core.localstore.request', mock_request),
            patch(
                'opentf.core.localstore.os.path.exists', MagicMock(return_value=True)
            ),
        ):
            localstore.get_file('wf_id', ATTACHMENT_UUID)
        mock_msr.assert_called_once_with('OK', '')

    def test_get_file_get_headers_file_ko(self):
        mock_response = MagicMock()
        mock_msr = MagicMock(return_value=mock_response)
        mock_wfa = {
            'wf_id': {
                ATTACHMENT,
                'not_attachment-zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz',
                'not_id-yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy',
            }
        }
        mock_wfn = {'wf_id': 'ns'}
        mock_ns = MagicMock(return_value=True)
        mock_request = MagicMock()
        mock_request.method = 'HEAD'
        with (
            patch('opentf.core.localstore.make_status_response', mock_msr),
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
            patch('opentf.core.localstore.can_use_namespace', mock_ns),
            patch('opentf.core.localstore.request', mock_request),
        ):
            localstore.get_file('wf_id', ATTACHMENT_UUID)
        mock_msr.assert_called_once_with(
            'NotFound',
            f'Expected attachment {ATTACHMENT_UUID} not found for workflow wf_id.',
        )

    def test_get_file_no_namespace(self):
        mock_wfa = {'wf_id': {ATTACHMENT}}
        mock_wfn = {'not_wf_id': 'ns'}
        mock_msr = MagicMock()
        with (
            patch('opentf.core.localstore.WORKFLOWS_ATTACHMENTS', mock_wfa),
            patch('opentf.core.localstore.WORKFLOWS_NAMESPACES', mock_wfn),
            patch('opentf.core.localstore.make_status_response', mock_msr),
        ):
            localstore.get_file('wf_id', ATTACHMENT_UUID)
        mock_msr.assert_called_once_with(
            'NotFound',
            'Workflow wf_id not found in localstore. It may have been already cleaned up.',
        )


if __name__ == '__main__':
    unittest.main()
