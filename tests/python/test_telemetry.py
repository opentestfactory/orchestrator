# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Telemetry unit tests."""

import unittest

from enum import Enum
from importlib import reload
from unittest.mock import MagicMock, patch

import opentf.core.telemetry as telemetry

########################################################################
### Datasets


def _test_callback(_):
    return _


class UnitTestMetrics(Enum):
    TEST_COUNTER = (
        'namespace.service.foo',
        '1',
        'Counter description',
        None,
    )
    TEST_GAUGE = (
        'namespace.service.bar',
        '1',
        'Gauge description',
        None,
    )
    TEST_OBSERVABLE_GAUGE = (
        'namespace.service.foobar',
        '1',
        'Observable gauge description',
        _test_callback,
    )

    def __init__(
        self,
        metric_name,
        unit,
        description,
        callbacks,
    ):
        self.metric_name = metric_name
        self.unit = unit
        self.description = description
        self.callbacks = callbacks


class NotMetrics(Enum):
    TEST_NOT_METRIC = ('foo',)

    def __init__(self, not_metric_name):
        self.not_metric_name = not_metric_name


SVC = 'test.service'

ARGS = (SVC, UnitTestMetrics)
########################################################################
### TestTelemetry


class TestTelemetry(unittest.TestCase):

    def setUp(self) -> None:
        self.environ_patch = patch.dict('os.environ', {'OPENTF_TELEMETRY': 'yes'})
        self.environ_patch.start()
        reload(telemetry)
        self.addCleanup(self.environ_patch.stop)

    # TelemetryManager

    def test_telemetrymanager_init_ok(self):
        mock_meter = MagicMock()
        with (
            patch(
                'opentf.core.telemetry._initialize_telemetry', return_value=mock_meter
            ) as mock_init,
            patch('opentf.core.telemetry._callback_wrapper') as mock_callback,
        ):
            instance = telemetry.TelemetryManager(*ARGS)
        mock_init.assert_called_once_with(SVC)
        self.assertTrue(instance.enabled)
        self.assertIn('TEST_COUNTER', instance.svc_metrics)
        self.assertIn('TEST_GAUGE', instance.svc_metrics)
        self.assertIn('TEST_OBSERVABLE_GAUGE', instance.svc_metrics)
        mock_meter.create_counter.assert_called_once_with(
            'namespace.service.foo', unit='1', description='Counter description'
        )
        mock_meter.create_observable_gauge.assert_called_once_with(
            'namespace.service.foobar',
            unit='1',
            description='Observable gauge description',
            callbacks=[mock_callback()],
        )
        mock_meter.create_up_down_counter.assert_called_once_with(
            'namespace.service.bar', unit='1', description='Gauge description'
        )

    def test_telemetrymanager_init_telemetry_disabled(self):
        with patch.dict('os.environ', {'OPENTF_TELEMETRY': 'no'}):
            reload(telemetry)
            instance = telemetry.TelemetryManager(*ARGS)
        self.assertFalse(instance.enabled)
        self.assertIsNone(instance.meter)
        self.assertEqual({}, instance.svc_metrics)

    def test_telemetrymanager_init_no_meter(self):
        with patch('opentf.core.telemetry._initialize_telemetry', return_value=None):
            self.assertRaisesRegex(
                telemetry.OpentfError,
                'Telemetry must be initialized before creating metrics',
                telemetry.TelemetryManager,
                SVC,
                UnitTestMetrics,
            )

    def test_telemetrymanager_set_metric_ok(self):
        mock_meter = MagicMock()
        with patch(
            'opentf.core.telemetry._initialize_telemetry', return_value=mock_meter
        ):
            instance = telemetry.TelemetryManager(*ARGS)
            self.assertTrue(instance.enabled)
            instance.set_metric(UnitTestMetrics.TEST_COUNTER, 1, {'foo': 'bar'})
            instance.svc_metrics['TEST_COUNTER'].add.assert_any_call(1, {'foo': 'bar'})
            instance.set_metric(UnitTestMetrics.TEST_GAUGE, 1, {'bar': 'foo'})
            instance.svc_metrics['TEST_GAUGE'].add.assert_any_call(1, {'bar': 'foo'})

    def test_telemetrymanager_set_metric_telemetry_disabled(self):
        with patch.dict('os.environ', {'OPENTF_TELEMETRY': 'no'}):
            reload(telemetry)
            instance = telemetry.TelemetryManager(*ARGS)
            self.assertFalse(instance.enabled)
            res = instance.set_metric(UnitTestMetrics.TEST_COUNTER, 1)
            self.assertIsNone(res)

    def test_telemetrymanager_set_metric_typeerror(self):
        mock_meter = MagicMock()
        with patch(
            'opentf.core.telemetry._initialize_telemetry', return_value=mock_meter
        ):
            instance = telemetry.TelemetryManager(*ARGS)
            self.assertTrue(instance.enabled)
            self.assertRaisesRegex(
                TypeError,
                'Metric must be an Enum instance, got str.',
                instance.set_metric,
                'SOME_INEXISTENT_COUNTER',
                1,
            )

    def test_telemetrymanager_set_metric_keyerror(self):
        mock_meter = MagicMock()
        with (
            patch(
                'opentf.core.telemetry._initialize_telemetry', return_value=mock_meter
            ),
            patch('opentf.core.telemetry.logging.error') as mock_error,
        ):
            instance = telemetry.TelemetryManager(*ARGS)
            self.assertTrue(instance.enabled)
            instance.set_metric(NotMetrics.TEST_NOT_METRIC, 1, {'foo': 'bar'})
        mock_error.assert_called_once_with('Metric %s not found.', 'TEST_NOT_METRIC')

    def test_telemetrymanager_set_metric_exception(self):
        mock_meter = MagicMock()
        with (
            patch(
                'opentf.core.telemetry._initialize_telemetry', return_value=mock_meter
            ),
            patch('opentf.core.telemetry.logging.error') as mock_error,
        ):
            instance = telemetry.TelemetryManager(*ARGS)
            self.assertTrue(instance.enabled)
            instance.svc_metrics['TEST_COUNTER'].add = MagicMock(
                side_effect=Exception('Boom')
            )
            instance.set_metric(UnitTestMetrics.TEST_COUNTER, 1, {'foo': 'bar'})
        mock_error.assert_called_once_with(
            'Failed to set metric %s: %s.', 'TEST_COUNTER', 'Boom'
        )

    # _initialize_telemetry

    def test_initialize_telemetry_ok(self):
        with (
            patch('opentf.core.telemetry.Resource') as mock_resource,
            patch('opentf.core.telemetry.PeriodicExportingMetricReader') as mock_reader,
            patch('opentf.core.telemetry.MeterProvider') as mock_provider,
            patch(
                'opentf.core.telemetry.metrics.set_meter_provider'
            ) as mock_set_provider,
            patch(
                'opentf.core.telemetry.metrics.get_meter', return_value='Meeeeeeeter!'
            ) as mock_meter,
            patch('opentf.core.telemetry.OTLPMetricExporter') as mock_exporter,
        ):
            res = telemetry._initialize_telemetry(SVC)
        mock_resource.assert_called_once_with(attributes={'service.name': SVC})
        mock_reader.assert_called_once()
        mock_exporter.assert_called_once_with(endpoint=telemetry.OTLP_EXPORTER_ENDPOINT)
        mock_provider.assert_called_once_with(
            resource=mock_resource(), metric_readers=[mock_reader()]
        )
        mock_set_provider.assert_called_once_with(mock_provider())
        mock_meter.assert_called_once_with('opentf.core.telemetry')
        self.assertEqual('Meeeeeeeter!', res)

    def test_initialize_telemetry_telemetry_disabled(self):
        with patch.dict('os.environ', {'OPENTF_TELEMETRY': 'no'}):
            reload(telemetry)
            self.assertIsNone(telemetry._initialize_telemetry(SVC))

    def test_initialize_telemetry_exception(self):
        with (
            patch('opentf.core.telemetry.Resource', side_effect=Exception('Wroom')),
            patch('opentf.core.telemetry.logging.error') as mock_error,
        ):
            self.assertRaisesRegex(
                Exception, 'Wroom', telemetry._initialize_telemetry, SVC
            )
        mock_error.assert_called_once()
        self.assertIn('Failed to initialize OpenTelemetry', mock_error.call_args[0][0])

    # _callback_wrapper

    def test_callback_wrapper_ok(self):
        with patch(
            'opentf.core.telemetry.metrics.Observation', return_value=123
        ) as mock_obs:
            mock_callback = MagicMock(return_value=456)
            wrapped_callback = telemetry._callback_wrapper(mock_callback)
            res = list(wrapped_callback(10))
        self.assertEqual(res[0], mock_obs.return_value)


if __name__ == '__main__':
    unittest.main()
