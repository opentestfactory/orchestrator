# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Insight Collector HTML reports generator unit tests."""

import logging
import os
import sys
import unittest

import xml.etree.ElementTree as ET

from unittest.mock import MagicMock, patch

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.insightcollector.xmlreport import (
    _get_job_suite_testcases,
    _create_report_xml,
    prepare_xml_report_event,
)
from opentf.plugins.insightcollector.exceptions import InsightCollectorError

########################################################################
# Templates and helpers

TESTCASE_METADATA = {
    "0cc51879-15d4-470e-a6f7-1eee4b6a7757": {
        "metadata": {
            "name": "Test Statuses#Test Failure",
            "creationTimestamp": "today",
            "job_id": "random_job_id",
        },
        "status": "FAILURE",
        "test": {
            "job": "robot-execute-test",
            "uses": "robotframework/execute@v1",
            "technology": "robotframework",
            "runs-on": ["robotframework", "linux"],
            "managed": False,
            "outcome": "failure",
            "suiteName": "Test Statuses",
            "testCaseName": "Test Failure",
        },
        "execution": {
            "duration": 0,
            "failureDetails": {
                "message": "Failed",
                "text": "With details",
                "type": "SomeFailure",
            },
        },
    },
    "937151d5-f1d8-4c94-8b3d-aa69ad8c9a02": {
        "metadata": {
            "name": "Test Statuses#Test Error",
            "creationTimestamp": "yesterday",
            "job_id": "random_job_id",
        },
        "status": "ERROR",
        "test": {
            "job": "robot-execute-test",
            "uses": "robotframework/execute@v1",
            "technology": "robotframework",
            "runs-on": ["robotframework", "linux"],
            "managed": False,
            "outcome": "error",
            "suiteName": "Test Statuses",
            "testCaseName": "Test Error",
        },
        "execution": {
            "duration": 100,
        },
    },
    "85e09781-6aed-4e30-8ee2-7a7fb7b35476": {
        "metadata": {
            "name": "Test Statuses#Test OK",
            "creationTimestamp": "tomorrow",
            "job_id": "random_job_id",
        },
        "status": "SUCCESS",
        "test": {
            "job": "robot-execute-test",
            "uses": "robotframework/execute@v1",
            "technology": "robotframework",
            "runs-on": ["robotframework", "linux"],
            "managed": False,
            "outcome": "success",
            "suiteName": "Test Statuses",
            "testCaseName": "Test OK",
        },
        "execution": {
            "errorsList": [{"message": "General Error", "timestamp": "the day before"}],
            "duration": 1000,
        },
    },
}

TESTCASE_METADATA_TEST_NAMES = {
    "first": {
        "metadata": {
            "name": "Just Suite#",
            "creationTimestamp": "yesterday",
            "job_id": "random_job_id",
        },
        "status": "ERROR",
        "test": {
            "job": "robot-execute-test",
            "uses": "robotframework/execute@v1",
            "technology": "robotframework",
            "runs-on": ["robotframework", "linux"],
            "managed": False,
            "outcome": "error",
            "suiteName": "Just Suite",
            "testCaseName": "Just Suite",
        },
        "execution": {
            "duration": 102,
        },
    },
    "second": {
        "metadata": {
            "name": "#Just Case",
            "timestamp": "tomorrow",
            "job_id": "random_job_id",
        },
        "status": "SUCCESS",
        "test": {
            "job": "robot-execute-test",
            "uses": "robotframework/execute@v1",
            "technology": "robotframework",
            "runs-on": ["robotframework", "linux"],
            "managed": False,
            "outcome": "success",
            "suiteName": "Just Case",
            "testCaseName": "Just Case",
        },
        "execution": {
            "duration": 150,
        },
    },
}


TESTRESULT_EVENTS = [
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "Notification",
        "metadata": {
            "attachment_origin": ["c87af3ac-0cff-4d4b-b394-72775c877da1"],
            "name": "run31",
            "workflow_id": "c4e3d866-45ae-46e5-95c6-d1dd394ba99a",
        },
        "spec": {
            "testResults": [
                {
                    "attachment_origin": "c87af3ac-0cff-4d4b-b394-72775c877da1",
                    "id": "0cc51879-15d4-470e-a6f7-1eee4b6a7757",
                    "name": "Test Statuses#Test Failure",
                    "status": "FAILURE",
                },
                {
                    "attachment_origin": "c87af3ac-0cff-4d4b-b394-72775c877da1",
                    "id": "937151d5-f1d8-4c94-8b3d-aa69ad8c9a02",
                    "name": "Test Statuses#Test Error",
                    "status": "FAILURE",
                },
                {
                    "attachment_origin": "c87af3ac-0cff-4d4b-b394-72775c877da1",
                    "id": "85e09781-6aed-4e30-8ee2-7a7fb7b35476",
                    "name": "Test Statuses#Test Skipped",
                    "status": "SUCCESS",
                },
            ]
        },
    },
    {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "attachments": [
            "/tmp/1e65baa6-23dc-4804-b1f7-c75f61d43de6-c87af3ac-0cff-4d4b-b394-72775c877da1_29_output.xml"
        ],
        "kind": "ExecutionResult",
        "metadata": {
            "attachments": {
                "/tmp/1e65baa6-23dc-4804-b1f7-c75f61d43de6-c87af3ac-0cff-4d4b-b394-72775c877da1_29_output.xml": {
                    "type": "application/vnd.opentestfactory.robotframework-output+xml",
                    "uuid": "c87af3ac-0cff-4d4b-b394-72775c877da1",
                }
            },
            "channel_id": "0f75321a-bfea-4476-be3c-819ddc3bbb8f",
            "channel_os": "linux",
            "creationTimestamp": "2023-11-20T10:31:15.007851",
            "job_id": "1e65baa6-23dc-4804-b1f7-c75f61d43de6",
            "job_origin": [],
            "name": "run31",
            "namespace": "default",
            "step_id": "85572039-6e3e-4513-8ae4-49a592d0b4e4",
            "step_origin": [
                "06ea60e0-1a21-4f7f-a30e-efabc7f2e8c9",
                "fdc5f3d7-1c2f-417e-b106-61349c4ce473",
            ],
            "step_sequence_id": 29,
            "workflow_id": "c4e3d866-45ae-46e5-95c6-d1dd394ba99a",
        },
        "status": 0,
    },
    {
        "apiVersion": "opentestfactory.org/v1beta1",
        "kind": "ProviderResult",
        "metadata": {
            "creationTimestamp": "2023-11-20T10:31:15.033833",
            "step_id": "06ea60e0-1a21-4f7f-a30e-efabc7f2e8c9",
        },
    },
]

########################################################################


class TestInsightCollectorXmlReport(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def test_prepare_xml_report_event_ok(self):
        mock_content = MagicMock()
        mock_proxy = MagicMock()
        mock_proxy.get_datasource = MagicMock(return_value=TESTCASE_METADATA)
        with patch(
            'opentf.plugins.insightcollector.xmlreport.make_workflowresult_event',
            mock_content,
        ):
            prepare_xml_report_event(
                mock_proxy,
                {'name': 'boo'},
                'workflow_id',
                'r_id',
                'true',
            )
        mock_content.assert_called_once()
        self.assertEqual('boo', mock_content.call_args[0][1])

    def test_prepare_xml_report_event_with_scope(self):
        mock_content = MagicMock()
        mock_proxy = MagicMock()
        mock_proxy.get_datasource = MagicMock(return_value=TESTCASE_METADATA)
        with patch(
            'opentf.plugins.insightcollector.xmlreport.make_workflowresult_event',
            mock_content,
        ):
            prepare_xml_report_event(
                mock_proxy,
                {'name': 'foo', 'spec': {'scope': 'test.managed=true'}},
                'workflow_id',
                'r_id',
                'true',
            )
        mock_content.assert_called_once()
        mock_proxy.get_datasource.assert_called_once_with(
            'testcases', scope='test.managed=true'
        )

    def test_prepare_xml_report_event_no_testcase_metadata(self):
        mock_proxy = MagicMock()
        mock_proxy.get_datasource = MagicMock(return_value={})
        with patch(
            'opentf.core.datasources._has_testresult',
            MagicMock(return_value=True),
        ):
            self.assertRaisesRegex(
                InsightCollectorError,
                'No testcase metadata retrieved for the workflow wOrKflow_iD, not publishing the Surefire execution report.',
                prepare_xml_report_event,
                mock_proxy,
                {'foo': 'bar'},
                'wOrKflow_iD',
                None,
                'true',
            )

    def test_prepare_xml_report_event_ok_check_event(self):
        mock_event = MagicMock()
        mock_proxy = MagicMock()
        mock_proxy.get_datasource = MagicMock(return_value=TESTCASE_METADATA)
        with (
            patch(
                'opentf.core.datasources._has_testresult',
                MagicMock(side_effect=[True, False, False]),
            ),
            patch(
                'opentf.plugins.insightcollector.report_utils.make_event',
                mock_event,
            ),
            patch(
                'opentf.plugins.insightcollector.report_utils.make_uuid',
                MagicMock(return_value='UuId'),
            ),
            patch('builtins.open'),
        ):
            prepare_xml_report_event(
                mock_proxy,
                {'name': 'boo'},
                'workflow_id',
                'r_id',
                'true',
            )
            self.assertEqual(
                '/tmp/workflow_id-UuId_WR_boo.xml',
                mock_event.call_args[1]['attachments'][0],
            )

    def test_prepare_xml_report_event_ko_throw_exception(self):
        mock_event = MagicMock()
        mock_proxy = MagicMock()
        mock_proxy.get_datasource = MagicMock(return_value=TESTCASE_METADATA)
        with (
            patch(
                'opentf.core.datasources._has_testresult',
                MagicMock(side_effect=[True, False, False]),
            ),
            patch(
                'opentf.plugins.insightcollector.report_utils.make_event',
                mock_event,
            ),
            patch(
                'opentf.plugins.insightcollector.report_utils.make_uuid',
                MagicMock(return_value='UuId'),
            ),
            patch('builtins.open', MagicMock(side_effect=Exception('Boom'))),
        ):
            self.assertRaisesRegex(
                InsightCollectorError,
                'Failed to publish report /tmp/workflow_id-UuId_WR_boo.xml for workflow workflow_id: Boom.',
                prepare_xml_report_event,
                mock_proxy,
                {'name': 'boo'},
                'workflow_id',
                'r_id',
                'true',
            )

    # check xml

    def test_create_report_xml_ok_check_xml(self):
        data = _get_job_suite_testcases(TESTCASE_METADATA)
        xml = _create_report_xml(data, 'wf')
        xml = ET.tostring(xml)
        self.assertIn(b'tests="3" failures="1" errors="1" skipped="0" time="1.1"', xml)
        self.assertEqual(3, xml.count(b'<testcase id'))
        self.assertEqual(4, xml.count(b'<failure message'))
        self.assertEqual(4, xml.count(b'robot-execute-test.Test Statuses"'))
        self.assertEqual(1, xml.count(b'Test failed, status: error'))
        for x in ['000', '001', '002']:
            s = f'[{x}] robot-execute-test.Test Statuses'.encode('utf-8')
            self.assertEqual(1, xml.count(s))

    # check data parsing

    def test_get_jobsuite_testcases_check_parsed_data(self):
        data = _get_job_suite_testcases(TESTCASE_METADATA_TEST_NAMES)
        self.assertEqual(
            [
                'random_job_id.robot-execute-test.Just Suite',
                'random_job_id.robot-execute-test.Just Case',
            ],
            list(data.keys()),
        )
        self.assertEqual(
            'Just Suite',
            data['random_job_id.robot-execute-test.Just Suite']['first']['short_name'],
        )
