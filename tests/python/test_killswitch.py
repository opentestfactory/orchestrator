# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Receptionist unit tests."""

import logging
import os
import unittest
import sys

from unittest.mock import MagicMock, patch

from requests import Response

mockresponse = Response()
mockresponse.status_code = 200

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.core import killswitch


########################################################################

UUID_OK = '6120ee73-2296-4dc6-9b3b-b6f2d4b4a622'
UUID_NOK = '120ee73-2296-4dc6-9b3b-b6f2d4b4a622'


########################################################################


def build_observer_payload_mock(observer_status="DONE"):
    return {
        "details": {
            "status": observer_status,
            "items": [],
        }
    }


def build_observer_mock(http_status=200, observer_status="DONE"):
    response = Response()
    response.status_code = http_status
    response.json = MagicMock(return_value=build_observer_payload_mock(observer_status))
    return response


class TestKillswitch(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        app = killswitch.app
        app.config['CONTEXT']['services'] = {
            'observer': {'endpoint': 'http://localhost'}
        }
        app.config['CONTEXT']['enable_insecure_login'] = True
        self.app = app.test_client()
        self.assertFalse(app.debug)

    def test_get_namespace_empty(self):
        self.assertRaises(ValueError, killswitch.get_namespace, [])

    def test_get_namespace_nons(self):
        self.assertRaises(
            ValueError, killswitch.get_namespace, [{}, {'metadata': {'name': 'foo'}}]
        )

    def test_get_namespace_ok(self):
        self.assertEqual(
            killswitch.get_namespace([{'metadata': {'namespace': 'foo'}}]), 'foo'
        )

    def test_respond_422(self):
        response = self.app.delete(f'/workflows/{UUID_NOK}')
        self.assertEqual(response.status_code, 422)

    def test_respond_404_when_observer_said_404(self):
        mockresponse = Response()
        mockresponse.status_code = 404
        with patch('requests.get', MagicMock(return_value=mockresponse)):
            response = self.app.delete(f'/workflows/{UUID_OK}')
            self.assertEqual(response.status_code, 404)

    def test_respond_401_when_observer_said_401(self):
        mockresponse = Response()
        mockresponse.status_code = 401
        with patch('requests.get', MagicMock(return_value=mockresponse)):
            response = self.app.delete(f'/workflows/{UUID_OK}')
            self.assertEqual(response.status_code, 401)

    def test_respond_403_when_observer_said_403(self):
        mockresponse = Response()
        mockresponse.status_code = 403
        with patch('requests.get', MagicMock(return_value=mockresponse)):
            response = self.app.delete(f'/workflows/{UUID_OK}')
            self.assertEqual(response.status_code, 403)

    def test_respond_200_when_observer_said_200(self):
        mockresponse = Response()
        mockresponse.status_code = 200
        mockresponse.json = lambda: {
            'details': {
                'status': 'DONE',
                'items': [{'metadata': {'namespace': 'foo'}}],
            }
        }
        with (
            patch('requests.get', MagicMock(return_value=mockresponse)),
            patch('opentf.core.killswitch.publish', MagicMock()),
        ):
            response = self.app.delete(f'/workflows/{UUID_OK}')
            self.assertEqual(response.status_code, 200)

    def test_respond_200_when_observer_said_200_withsource(self):
        mockresponse = Response()
        mockresponse.status_code = 200
        mockresponse.json = lambda: {
            'details': {
                'status': 'DONE',
                'items': [{'metadata': {'namespace': 'foo'}}],
            }
        }
        with (
            patch('requests.get', MagicMock(return_value=mockresponse)),
            patch('opentf.core.killswitch.publish') as mock_publish,
        ):
            response = self.app.delete(f'/workflows/{UUID_OK}?source=me')
            self.assertEqual(response.status_code, 200)
        mock_publish.assert_called_once()
        event = mock_publish.call_args[0][0]
        self.assertIn('details', event)
        self.assertEqual(event['details']['source'], 'me')

    def test_respond_200_when_observer_said_200_dryrun(self):
        mockresponse = Response()
        mockresponse.status_code = 200
        mockresponse.json = lambda: {
            'details': {
                'status': 'DONE',
                'items': [{'metadata': {'namespace': 'foo'}}],
            }
        }
        with (
            patch('requests.get', MagicMock(return_value=mockresponse)),
            patch('opentf.core.killswitch.publish') as mock_publish,
        ):
            response = self.app.delete(f'/workflows/{UUID_OK}?dryRun')
            self.assertEqual(response.status_code, 200)
        mock_publish.assert_not_called()

    def test_respond_403_when_observer_said_200_badns(self):
        mockresponse = Response()
        mockresponse.status_code = 200
        mockresponse.json = lambda: {
            'details': {
                'status': 'DONE',
                'items': [{'metadata': {'namespace': 'foo'}}],
            }
        }
        with (
            patch('requests.get', MagicMock(return_value=mockresponse)),
            patch('opentf.core.killswitch.publish', MagicMock()),
            patch(
                'opentf.core.killswitch.can_use_namespace',
                MagicMock(return_value=False),
            ),
        ):
            response = self.app.delete(f'/workflows/{UUID_OK}')
            self.assertEqual(response.status_code, 403)


if __name__ == '__main__':
    unittest.main()
