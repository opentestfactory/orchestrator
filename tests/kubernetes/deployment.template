---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: VERSION
  labels:
    app: VERSION
spec:
  replicas: 1
  selector:
    matchLabels:
      app: VERSION
  template:
    metadata:
      labels:
        app: VERSION
    spec:
      containers:
      - name: squashtf
        image: docker.squashtest.org/opentestfactory/allinone:DOCKER_TAG
        imagePullPolicy: "Always"
        ports:
        - containerPort: 7774
        - containerPort: 7775
        - containerPort: 7776
        - containerPort: 38368
        resources:
          limits:
            memory: "512Mi"
          requests:
            memory: "512Mi"
            cpu: "0.2"
        volumeMounts:
        - name: squashtf-trusted-key
          mountPath: /etc/squashtf
      imagePullSecrets:
      - name: "docker-registry"
      volumes:
      - name: squashtf-trusted-key
        secret:
          secretName: squashtf-trusted-keys

---
apiVersion: v1
kind: Service
metadata:
  name: VERSION
spec:
  selector:
    app: VERSION
  ports:
    - protocol: TCP
      port: 7774
      name: receptionist
    - protocol: TCP
      port: 38368
      name: eventbus
    - protocol: TCP
      port: 7775
      name: observer
    - protocol: TCP
      port: 7776
      name: killswitch

---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: VERSION
  annotations:
    traefik.ingress.kubernetes.io/request-modifier: "ReplacePathRegex: ^/VERSION/(.*) /$1"
spec:
  rules:
  - host: demo.new-forge.squashtest.org
    http:
      paths:
      - path: /VERSION/workflows
        backend:
          serviceName: VERSION
          servicePort: receptionist
      - path: /VERSION/workflows/{id:[a-z0-9-]+}/status
        backend:
          serviceName: VERSION
          servicePort: observer
      - path: /VERSION/workflows/{id:[a-z0-9-]+}
        backend:
          serviceName: VERSION
          servicePort: killswitch
